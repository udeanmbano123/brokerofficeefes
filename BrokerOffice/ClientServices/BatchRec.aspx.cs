﻿using BrokerOffice.ATS;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using System.Transactions;
using System.Collections;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace BrokerOffice.ClientServices
{
    public partial class BatchRec : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception)
                {


                }
            }
            try
            {
                BindGrids();
            }
            catch (Exception)
            {


            }
            try
            {
                if (this.IsPostBack)
                {

                }
                else
                {

                    drpname.DataSource = GetDataSource();
                    drpname.DataBind();
                    drpname.SelectedIndex = 1;
                  
                }

            }
            catch (Exception)
            {
                //Response.Redirect("~/User/Index", false);        //write redirect
                //Context.ApplicationInstance.CompleteRequest(); // end response


            }

        }
        private DataTable GetDataSource()
        {
            int count = 0;
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));

            dtSource.Rows.Add(0, "Select File to remove transactions");
            var dd = (from c in db.bhtrans
                     let file = c.fileName
                     select new { file }).Distinct();
                foreach (var p in dd)
                {
                count += 1;
                dtSource.Rows.Add(count, p.file);
                }

           


            return dtSource;
        }

        public void BindGrids()
        {
            string nme = "";
            var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            foreach (var dd in users)
            {
                nme = dd.BrokerCode;
            }
            if (drpname.SelectedItem.Text != "Select File to remove transactions") {
                
                //gvAll.DataSource = buy.Where(a =>System.DateTime.Parse(a.DatePosted)>date && System.DateTime.Parse(a.DatePosted)<date3).ToList();
                gvAll.DataSource = GetEarners(drpname.SelectedItem.Text);
                gvAll.DataBind();
            }
        }
        public List<UplodedTrans> GetEarners(string a)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SELECT UplodedTransID,[TransactionDate],[ValueDate],[Reference],[Description],[Debit],[Credit],[Balance],[clientnumber],[fileName],[UploadedBy],[UploadedDate],[Batch] FROM UplodedTrans where fileName='" + a + "'";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<UplodedTrans>();
            while (reader.Read())
            {
                var accountDetails = new UplodedTrans
                {
                    UplodedTransID= Convert.ToInt32(reader.GetValue(0)),
                    TransactionDate = Convert.ToDateTime(reader.GetValue(1)),
                    ValueDate = Convert.ToDateTime(reader.GetValue(2)),
                    Reference = reader.GetValue(3).ToString(),
                      Description = reader.GetValue(4).ToString(),
                       Debit = Convert.ToDecimal(reader.GetValue(5)),
                       Credit = Convert.ToDecimal(reader.GetValue(6)),
                       Balance = Convert.ToDecimal(reader.GetValue(7)),
                    clientnumber = reader.GetValue(8).ToString(),
                    fileName = reader.GetValue(9).ToString(),
                    UploadedBy = reader.GetValue(10).ToString(),
                    UploadedDate = Convert.ToDateTime(reader.GetValue(11)),
                    Batch = reader.GetValue(12).ToString()
                              };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        public string GetNames(string acc)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }
                        
        public  DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            gvAll.PageIndex = e.NewPageIndex;
            gvAll.DataBind();
            BindGrids();
        }
        private void SetData()
        {
            int currentCount = 0;
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                    .Cells[0].FindControl("chkAll");
            chkAll.Checked = true;
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvAll.Rows[i]
                                .Cells[0].FindControl("chk");
                if (chk != null)
                {
                    chk.Checked = arr.Contains(gvAll.DataKeys[i].Value);
                    if (!chk.Checked)
                        chkAll.Checked = false;
                    else
                        currentCount++;
                }
            }
            hfCount.Value = (arr.Count - currentCount).ToString();
        }
        private void GetData()
        {
            ArrayList arr;
            if (ViewState["SelectedRecords"] != null)
                arr = (ArrayList)ViewState["SelectedRecords"];
            else
                arr = new ArrayList();
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                .Cells[0].FindControl("chkAll");
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (chkAll.Checked)
                {
                    if (!arr.Contains(gvAll.DataKeys[i].Value))
                    {
                        arr.Add(gvAll.DataKeys[i].Value);
                    }
                }
                else
                {
                    CheckBox chk = (CheckBox)gvAll.Rows[i]
                                       .Cells[0].FindControl("chk");
                    if (chk.Checked)
                    {
                        if (!arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Add(gvAll.DataKeys[i].Value);
                        }
                    }
                    else
                    {
                        if (arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Remove(gvAll.DataKeys[i].Value);
                        }
                    }
                }
            }
            ViewState["SelectedRecords"] = arr;
        }
        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.bhtrans.ToList().Where(a=>a.UplodedTransID==Convert.ToInt32(gvAll.DataKeys[i].Value));
                    foreach (var c in dbc)
                    {
                        UplodedTrans my = db.bhtrans.Find(c.UplodedTransID);
                      db.bhtrans.Remove(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage(count);
        }
        private void ShowMessage(int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("alert('");
            sb.Append(count.ToString());
            sb.Append("The Bank Reconciliation transactions have been deleted.');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),
                            "script", sb.ToString());
        }

        private void ShowMessage2(int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("alert('");
            sb.Append(count.ToString());
            sb.Append("The Bank Reconciliation transactions have been reconciled.');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),
                            "script", sb.ToString());
        }
        private void ShowMessage3(int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("alert('");
            sb.Append(count.ToString());
            sb.Append("The Bank Reconciliation transactions have been unreconciled.');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),
                            "script", sb.ToString());
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpname.SelectedItem.Text == "Select File to remove transactions")
                {
                    msgbox("Select File to remove transactions");
                    return;
                }
                if (gvAll.Rows.Count == 0)
                {
                    msgbox("The grid must contain batch transactions");
                    return;
                }
            }
            catch (Exception)
            {

                msgbox("Please select a file to remove transactions");
                return;
            }
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.bhtrans.ToList().Where(a => a.UplodedTransID == Convert.ToInt32(gvAll.DataKeys[i].Value));
                    foreach (var c in dbc)
                    {
                        UplodedTrans my = db.bhtrans.Find(c.UplodedTransID);
                        db.bhtrans.Remove(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }

                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage(count);

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpname.SelectedItem.Text == "Select File to remove transactions" || drpname.SelectedItem.Text == "")
                {
                    msgbox("Please select a file to remove transactions");
                    return;
                }
                else
                {
                    BindGrids();
                }
            }
            catch (Exception)
            {

                msgbox("Please select a file to remove transactions");
                return;
            }

        }

        protected void gvAll_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpname.SelectedItem.Text == "Select File to remove transactions")
                {
                    msgbox("Select File to remove transactions");
                    return;
                }
                if (gvAll.Rows.Count == 0)
                {
                    msgbox("The grid must contain batch transactions");
                    return;
                }
            }
            catch (Exception)
            {

                msgbox("Please select a file to reconcile transactions");
                return;
            }
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.bhtrans.ToList().Where(a => a.UplodedTransID == Convert.ToInt32(gvAll.DataKeys[i].Value));
                    foreach (var c in dbc)
                    {
                        UplodedTrans my = db.bhtrans.Find(c.UplodedTransID);
                        my.Batch = "Reconciled";
                        db.bhtrans.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }

                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage2(count);

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpname.SelectedItem.Text == "Select File to remove transactions")
                {
                    msgbox("Select File to remove transactions");
                    return;
                }
                if (gvAll.Rows.Count == 0)
                {
                    msgbox("The grid must contain batch transactions");
                    return;
                }
            }
            catch (Exception)
            {

                msgbox("Please select a file to unreconcile transactions");
                return;
            }
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.bhtrans.ToList().Where(a => a.UplodedTransID == Convert.ToInt32(gvAll.DataKeys[i].Value));
                    foreach (var c in dbc)
                    {
                        UplodedTrans my = db.bhtrans.Find(c.UplodedTransID);
                        my.Batch = "UnReconciled";
                        db.bhtrans.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }

                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage3(count);

        }
    }

       
        
    }
    

