﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class AdjustedHoldings
    {
        [Key]
        public int AdjustedHoldingsID { get; set; }
        public string Buyer { get; set; }
        public string seller { get; set; }
        public decimal Price { get; set; }
        public DateTime  Dates {get;set;}
        public int TradeID { get; set; }
    }
}