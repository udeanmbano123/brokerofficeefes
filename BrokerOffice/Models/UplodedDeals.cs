﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class UplodedDeals
    {   [Key]
        public int UplodedDealsID { get; set; }
        public string Exchange { get; set; }
        public string Market { get; set; }
        public string Symbol { get; set; }
        public string Trader { get; set; }
        public string Client { get; set; }
        public string Broker { get; set; }
        public string OrderNumber { get; set; }
        public string Price { get; set; }
        public string Volume { get; set; }
        public string TicketNumber { get; set; }
        public DateTime ExecutionDateTime { get; set; }
        public string BuySell { get; set; }
       public string ShortSell { get; set; }

        public string fileName { get; set; }
        public string UploadedBy { get; set; }

        public Boolean trans { get; set; }

}
}