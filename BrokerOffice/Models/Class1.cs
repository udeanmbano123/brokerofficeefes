﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Tbl_MatchedDeals
    {
        public long ID { get; set; }

        public long? Deal { get; set; }

        [StringLength(50)]
        public string BuyCompany { get; set; }

        [StringLength(50)]
        public string SellCompany { get; set; }

        [StringLength(100)]
        public string Buyer { get; set; }

        [StringLength(100)]
        public string Seller { get; set; }

        public decimal? Quantity { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Trade { get; set; }

        public decimal? DealPrice { get; set; }

        [StringLength(50)]
        public string DealFlag { get; set; }

        [StringLength(70)]
        public string Instrument { get; set; }
    }

    public partial class Tbl_MatchedOrders
    {
        [Key]
        public long ID { get; set; }
        public Nullable<long> Deal { get; set; }
        public string BuyCompany { get; set; }
        public string SellCompany { get; set; }
        public string Buyercdsno { get; set; }
        public string Sellercdsno { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<System.DateTime> Trade { get; set; }
        public Nullable<decimal> DealPrice { get; set; }
        public string DealFlag { get; set; }
        public string instrument { get; set; }

        public int RefID { get; set; }

        public string BrokerCode { get; set; }
    }

    public partial class Order_Lives
    {

        [Key]
        public int Order_LivesID { get; set; }
        public long OrderNo { get; set; }
        public string OrderType { get; set; }
        public string Company { get; set; }
        public string SecurityType { get; set; }
        public string CDS_AC_No { get; set; }
        public string Broker_Code { get; set; }
        public string Client_Type { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public string Shareholder { get; set; }
        public string ClientName { get; set; }
        public Nullable<int> TotalShareHolding { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<System.DateTime> Create_date { get; set; }
        public Nullable<System.DateTime> Deal_Begin_Date { get; set; }
        public Nullable<System.DateTime> Expiry_Date { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
        public Nullable<int> AvailableShares { get; set; }
        public string OrderPref { get; set; }
        public string OrderAttribute { get; set; }
        public string Marketboard { get; set; }
        public string TimeInForce { get; set; }
        public string OrderQualifier { get; set; }
        public string BrokerRef { get; set; }
        public string ContraBrokerId { get; set; }
        public Nullable<decimal> MaxPrice { get; set; }
        public Nullable<decimal> MiniPrice { get; set; }
        public Nullable<bool> Flag_oldorder { get; set; }
        public string OrderNumber { get; set; }
        public string Currency { get; set; }
        public Nullable<bool> FOK { get; set; }
        public Nullable<bool> Affirmation { get; set; }
    }
    public class securitytotals
    {
        public string TotalRegistration { get; set; }
        public string totalfunds { get; set; }
        public string CDCHOLDINGS { get; set; }
        public string totalholdings { get; set; }
        public string Incomingsells { get; set; }
        public string Incomingbuys { get; set; }
        public string Pendingsettlement { get; set; }
        public string todaysettlement { get; set; }
    }
    public class Brokerss
    {
        public string Name { get; set; }
        public string CDSNo { get; set; }
        public string Phone { get; set; }
        public string Broker { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string AccountType { get; set; }
        public string TradingStatus { get; set; }
    }
    public class marketwatch
    {

        public string company { get; set; }


        public string Volume { get; set; }


        public string Bid { get; set; }


        public string VolumeSell { get; set; }


        public string Ask { get; set; }


        public string LastTradedPrice { get; set; }


        public string Lastmatched { get; set; }


        public string lastvolume { get; set; }


        public string TotalVolume { get; set; }


        public string Turnover { get; set; }


        public string Open { get; set; }


        public string High { get; set; }


        public string Low { get; set; }


        public string AveragePrice { get; set; }


        public string Change { get; set; }


        public string percchange { get; set; }


        public string url { get; set; }


        public string url2 { get; set; }

    }

    public class marketwatchzse
    {
        public string Ticker { get; set; }
        public string ISIN { get; set; }
        public string Best_Ask { get; set; }
        public string Best_bid { get; set; }
        public string Current_price { get; set; }
    }
    public class loadbuys
    {
        public string Company { get; set; }
        public string Quantity { get; set; }
        public string BasePrice { get; set; }
        public string OrderPref { get; set; }
        public string TIF { get; set; }
        public string Block { get; set; }
        public string mine { get; set; }
    }
    public class loadsells
    {
        public string Company { get; set; }
        public string Quantity { get; set; }
        public string BasePrice { get; set; }
        public string OrderPref { get; set; }
        public string TIF { get; set; }
        public string Block { get; set; }
        public string mine { get; set; }
    }
}