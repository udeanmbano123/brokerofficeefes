﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class TPB
    {

        [Key]
        public int TPBID { get; set; }


        public string TradingBoard{ get; set; }
        public string TradingPlatform { get; set; }
        public int TradingBoardID { get; set; }
        public int TradingPlatformID { get; set; }
        [ForeignKey("TradingPlatformID")]
        public virtual TradingPlatform TradingPlatforms { get; set; }

        [ForeignKey("TradingBoardID")]
        public virtual TradingBoard TradingBoards { get; set; }

    }
}