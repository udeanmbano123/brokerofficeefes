﻿using Microsoft.Owin;
using Owin;
using BrokerOffice;

namespace BrokerOffice
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
