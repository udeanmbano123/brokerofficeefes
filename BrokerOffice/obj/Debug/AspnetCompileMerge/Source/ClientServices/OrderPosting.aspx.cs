﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.Reporting;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.ClientServices
{
    public partial class OrderPosting : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];

            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

            }
            else
            {
                LoadNames();
                LoadCurrencies();
                LoadIssuer();
                loadClientTypes();
                loadTP();
                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
            }
        }
        protected void getDetails(string x)
        {
            drpname.Items.Clear();
            drpname.DataSource = null;
            drpname.Items.Clear();
            drpname.DataSource = null;

            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.Order_Lives.ToList().Where(a => a.OrderNo == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                

                    drporder.Items.Insert(0, new ListItem(my.OrderType.ToString(), my.OrderType));
                    
                     txtsecurity.Text=my.SecurityType ;
                   
                   Tax.Text=my.Tax.ToString() ;
                     Shareholder.Text=my.Shareholder;
                    //my.ClientName = drpname.SelectedItem.Text;
                    drpname.Items.Insert(0, new ListItem(my.ClientName, my.CDS_AC_No));
                   // my.CDS_AC_No = drpname.SelectedValue.ToString();
                   TotalShareHolding.Text=my.TotalShareHolding.ToString();
                    begindate.Value=my.Deal_Begin_Date;
                   expirydate.Value=my.Expiry_Date ;
                    qty.Text=my.Quantity.ToString() ;
                     txtbase.Text=my.BasePrice.ToString();
                   availableshrs.Text=my.AvailableShares.ToString();
                ordepref.Text= my.OrderPref  ;
                   ordA.Text=my.OrderAttribute  ;
                mrktBoard.Text=my.Marketboard ;
                 max.Text=my.MaxPrice.ToString();
                mini.Text= my.MiniPrice.ToString();
                    //drpCurr.SelectedValue.ToString();
                    var curr = db.para_Currencies.ToList().Where(a=>a.CurrencyCode==my.Currency);
                    foreach(var c in curr)
                    {
                        per = c.CurrencyName;
                    }
                    drpCurr.Items.Insert(0, new ListItem(per, my.Currency));
                    my.TP = drpTP.SelectedItem.Text;
                    my.TPBoard = drpTP.SelectedItem.Text;
                    drpTP.Items.Insert(0, new ListItem(my.TP, my.TP));
                    drpTPB.Items.Insert(0, new ListItem(my.TPBoard, my.TPBoard));
                    Company.Items.Insert(0, new ListItem(my.Company, my.CompanyISIN.ToString()));
                    txtClient.Items.Insert(0, new ListItem(my.Client_Type, my.Client_Type.ToString()));
                   txtMobile.Text=my.mobile ;
                }
                LoadNames2();
                LoadCurrencies2();
                LoadIssuer2();
                loadClientTypes2();
            }

            return;
        }
        protected void loadTP()
        {
            drpTP.Items.Clear();
            drpTP.DataSource = null;
            var ds = from c in db.TradingPlatforms
                     select new

                     {

                         ID = c.TradingPlatformID,

                         ISIN = c.Code,

                     };
            drpTP.DataSource = ds.ToList();
            drpTP.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpTP.DataValueField = "ID"; // to retrive specific  textfield name 
                                         //assigning datasource to the dropdownlist
            drpTP.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpTP.Items.Insert(0, new ListItem("Please Select a Trading Platform", "0"));

        }
        protected void loadClientTypes2()
        {
            txtClient.Items.Clear();
            txtClient.DataSource = null;
            var ds = from c in db.Client_Types
                     select new

                     {

                         ID = c.ClientType,

                         ISIN = c.ClientType,

                     };
            txtClient.DataSource = ds.ToList();
            txtClient.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            txtClient.DataValueField = "ID"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            txtClient.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");

           

        }
        protected void loadClientTypes()
        {
           txtClient.Items.Clear();
            txtClient.DataSource = null;
            var ds = from c in db.Client_Types
                     select new

                     {

                         ID = c.ClientType,

                         ISIN = c.ClientType,

                     };
            txtClient.DataSource = ds.ToList();
            txtClient.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            txtClient.DataValueField = "ID"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            txtClient.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");
          
        txtClient.Items.Insert(0, new ListItem("Please Select a client type", "0"));
            
    

        }
        protected void LoadIssuer2()
        {
            Company.Items.Clear();
            Company.DataSource = null;
            var ds = from c in db.StockSecuritiess
                     select new

                     {

                         ID = c.StockSecuritiesID,

                         ISIN = c.StockISIN,

                     };
            Company.DataSource = ds.ToList();
            Company.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            Company.DataValueField = "ID"; // to retrive specific  textfield name 
                                           //assigning datasource to the dropdownlist
            Company.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

           

        }
        protected void LoadIssuer()
        {
            Company.Items.Clear();
            Company.DataSource = null;
            var ds = from c in db.para_issuers
                     select new

                     {

                         ID = c.ISIN,

                         ISIN =c.Company,

                     };
            Company.DataSource = ds.ToList();
            Company.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            Company.DataValueField = "ID"; // to retrive specific  textfield name 
                                           //assigning datasource to the dropdownlist
            Company.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
         
                Company.Items.Insert(0, new ListItem("Please Select a COMPANY", "0"));
          

        }
        protected void LoadNames2()
        {
            drpname.Items.Clear();
            drpname.DataSource = null;
            var ds = from c in db.Account_Creations
                     select new

                     {

                         CDSC_Number = c.CDSC_Number,

                         CustomerNames = "Firstname: " + c.OtherNames + " Surname:" + c.Surname_CompanyName + " ID " + c.CDSC_Number,

                     };
            drpname.DataSource = ds.ToList();
            drpname.DataTextField = "CustomerNames";
            // text field name of table dispalyed in dropdown
            drpname.DataValueField = "CDSC_Number"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            drpname.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

           
        }
        protected void LoadNames()
        {
            drpname.Items.Clear();
            drpname.DataSource = null;
            var ds = from c in db.Account_Creations
                     select new

                     {

                         CDSC_Number = c.CDSC_Number,

                         CustomerNames = "Firstname: " + c.OtherNames + " Surname:" + c.Surname_CompanyName + " ID " + c.CDSC_Number,

                     };
            drpname.DataSource = ds.ToList();
            drpname.DataTextField = "CustomerNames";
            // text field name of table dispalyed in dropdown
            drpname.DataValueField = "CDSC_Number"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            drpname.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            
                drpname.Items.Insert(0, new ListItem("Please Select a customer", "0"));
           
        }
        protected void LoadCurrencies2()
        {
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

          

        }
        protected void LoadCurrencies()
        {
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            
                drpCurr.Items.Insert(0, new ListItem("Please Select a currency", "0"));
            

        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            if (drpname.SelectedItem.Text == "Please Select a customer")
            {
                msgbox("Select a customer");
                return;
            }
            else if (Company.SelectedItem.Text == "Please Select a COMPANY")
            {
                msgbox("Select a company");
                return;
            }
            else if (txtClient.SelectedItem.Text == "Please Select a client type")
            {
                msgbox("Select a client type");
                return;
            }
            else if (drpCurr.SelectedItem.Text == "Please Select a currency")
            {
                msgbox("Select a currency");
                return;
            }
            else if (begindate.Text=="")
            {
                msgbox("Select a begin deal date");
                return;
            }
            else if (expirydate.Text == "")
            {
                msgbox("Select a expiry deal date");
                return;
            }else if(qty.Text=="")
            {
                msgbox("Select a quantity");
                return;
            }
            else if (Tax.Text == "")
            {
                msgbox("Enter tax");
                return;
            }
            else if (drpTP.SelectedItem.Text== "Please Select a Trading Platform")
            {
                msgbox("Select a Trading Platform");
                return;
            }
            else if (drpTPB.SelectedItem.Text == "Please Select a Trading Platform")
            {
                msgbox("Please Select a Trading Platform Board");
                return;
            }



            if (Submit.Text == "Submit")
            {
                Order_Live my = new Order_Live();

                my.OrderType = drporder.SelectedValue.ToString();
     
                my.SecurityType = txtsecurity.Text;
                my.Client_Type = txtClient.Text;
                my.Tax = Convert.ToDecimal(Tax.Text);
                my.Shareholder = Shareholder.Text;
                my.ClientName = drpname.SelectedItem.Text;
                my.CDS_AC_No = drpname.SelectedValue.ToString();
                my.TotalShareHolding = Convert.ToDecimal(TotalShareHolding.Text);
                my.Deal_Begin_Date = Convert.ToDateTime(begindate.Value);
                my.Expiry_Date = Convert.ToDateTime(expirydate.Value);
                my.Quantity = Convert.ToInt32(qty.Text);
                if(txtbase.Text==""|| txtbase.Text == null)
                {
                    txtbase.Text = "0";
                }
                decimal n;
                bool isNumericT = decimal.TryParse(txtbase.Text, out n);

                if (isNumericT == false)
                {
                    txtbase.Text = "0";
                }
                my.BasePrice = Convert.ToDecimal(txtbase.Text);
                my.AvailableShares = Convert.ToDecimal(availableshrs.Text);
                my.OrderPref = ordepref.Text;
                my.OrderAttribute = ordA.Text;
                my.Marketboard = mrktBoard.Text;
                if (max.Text == "" || max.Text == null)
                {
                    max.Text = "0";
                }
                decimal s;
                bool isNumericS = decimal.TryParse(max.Text, out s);

                if (isNumericS == false)
                {
                    max.Text = "0";
                }
                my.MaxPrice = Convert.ToDecimal(max.Text);
                if (mini.Text == "" || mini.Text == null)
                {
                   mini.Text = "0";
                }
                decimal p;
                bool isNumericZ = decimal.TryParse(mini.Text, out p);

                if (isNumericZ == false)
                {
                    mini.Text = "0";
                }
                my.MiniPrice = Convert.ToDecimal(mini.Text);
                my.Company = Company.SelectedItem.Text;
                my.CompanyISIN = Company.SelectedValue.ToString();
                drpCurr.SelectedValue.ToString();
                my.TP = drpTP.SelectedItem.Text;
                my.TPBoard = drpTP.SelectedItem.Text;
                my.Create_date = DateTime.Now;
                my.mobile = txtMobile.Text;
                my.PostedBy = WebSecurity.CurrentUserName;
                db.Order_Lives.Add(my);

                //Update table  

                //MN0-BBO
                //Language-English
                //Username
                //Password
                //Telephone Sel Order
                //No Notes-Quantity
                //UssD Buy

                try
                {

                    if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount") == true)
                    {

                        WebReference.EscrowServiceSoap escorw = new WebReference.EscrowServiceSoap();
                        var edc = "";
                        if (my.OrderType == "Sell")
                        {
                            edc = escorw.ReceiveUSSDSellOrder(my.CDS_AC_No, my.Quantity.ToString(), my.mobile, my.Company, "BBO1", "finsec2016.es!@", "BBO", "English", my.MaxPrice.ToString());
                        }
                        else if (my.OrderType == "Buy")
                        {
                            edc = escorw.ReceiveUSSDBuyOrder(my.CDS_AC_No, my.mobile.ToString(), my.Quantity.ToString(), my.Company, "BBO1", "finsec2016.es!@", "BBO", "English", my.MaxPrice.ToString());
                        }
                    }
                    else
                    {
                        WebReference2.EscrowServiceSoap escorw2 = new WebReference2.EscrowServiceSoap();
                        var edc = "";
                        if (my.OrderType == "Sell")
                        {
                            edc = escorw2.ReceiveUSSDSellOrder(my.CDS_AC_No, my.Quantity.ToString(), my.mobile, my.Company, "BBO1", "finsec2016.es!@", "BBO", "English", my.MaxPrice.ToString());
                        }
                        else if (my.OrderType == "Buy")
                        {
                            edc = escorw2.ReceiveUSSDBuyOrder(my.CDS_AC_No, my.mobile.ToString(), my.Quantity.ToString(), my.Company, "BBO1", "finsec2016.es!@", "BBO", "English", my.MaxPrice.ToString());
                        }

                    }


                }
                catch (Exception)
                {

                    msgbox("No internet connection to webservice");
                }
                BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                long maxid = 0;
                try
                {
                    maxid = db.Order_Lives.ToList().Max(a => a.OrderNo);
                }
                catch (Exception)
                {

                    maxid = 0;
                }
                decimal maxprice = Convert.ToDecimal(max.Text);
                decimal quantity = Convert.ToDecimal(qty.Text);
                int num = 0;
                //Cash Deposits
                var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Receivable"));


                foreach (var c in acc)
                {
                    num = c.AccountNumber;
                }
                //trading charges
                var tr = db.TradingChargess.ToList();
                BrokerOffice.Models.Trans my3 = new BrokerOffice.Models.Trans();
                //Debit
                decimal total = 0;
                foreach (var q in tr)
                {
                    if (drporder.SelectedValue.ToString() == "Buy")
                    {
                        if (q.ChargedAs == "Flat")
                        {
                            my3.Account = q.chargeaccountcode.ToString();
                            my3.Category = "Order Posting";
                            my3.Credit = q.chargevalue;
                            my3.Debit = 0;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            //debit account number
                            my3.Account = drpname.SelectedValue.ToString();
                            my3.Category = q.chartaccount;
                            my3.Credit = 0;
                            my3.Debit = q.chargevalue;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                            //debit
                            my3.Account = num.ToString();
                            my3.Category = "Accounts Receivable";
                            my3.Credit = 0;
                            my3.Debit = q.chargevalue;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = "Accounts Receivable";
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            total += q.chargevalue;
                        }
                        else if (q.ChargedAs == "Percentage")
                        {
                            my3.Account = q.chargeaccountcode.ToString();
                            my3.Category = "Order Posting";
                            my3.Credit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Debit = 0;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            //debit account number
                            my3.Account = drpname.SelectedValue.ToString();
                            my3.Category = q.chartaccount;
                            my3.Credit = 0;
                            my3.Debit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                            //debit cashdeposits
                            my3.Account = num.ToString();
                            my3.Category = "Accounts Receivable";
                            my3.Credit = 0;
                            my3.Debit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = "Accounts Receivable";
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            total += q.chargevalue;
                        }
                    }
                    else
                    {
                        if (q.ChargedAs == "Flat")
                        {
                            my3.Account = q.chargeaccountcode.ToString();
                            my3.Category = "Order Posting";
                            my3.Debit = q.chargevalue;
                            my3.Credit = 0;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            //debit account number
                            my3.Account = drpname.SelectedValue.ToString();
                            my3.Category = q.chartaccount;
                            my3.Debit = 0;
                            my3.Credit = q.chargevalue;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                            //debit
                            my3.Account = num.ToString();
                            my3.Category = "Accounts Receivable";
                            my3.Debit = 0;
                            my3.Credit = q.chargevalue;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = "Accounts Receivable";
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            total += q.chargevalue;
                        }
                        else if (q.ChargedAs == "Percentage")
                        {
                            my3.Account = q.chargeaccountcode.ToString();
                            my3.Category = "Order Posting";
                            my3.Debit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Credit = 0;
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            //debit account number
                            my3.Account = drpname.SelectedValue.ToString();
                            my3.Category = q.chartaccount;
                            my3.Debit = 0;
                            my3.Credit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = q.chartaccount;
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                            //debit cashdeposits
                            my3.Account = num.ToString();
                            my3.Category = "Accounts Receivable";
                            my3.Debit = 0;
                            my3.Credit = ((q.chargevalue / 100) * (maxprice * quantity));
                            my3.Narration = q.chartaccount;
                            my3.Reference_Number = maxid.ToString();
                            my3.TrxnDate = DateTime.Now;
                            my3.Post = DateTime.Now;
                            my3.Type = "Accounts Receivable";
                            my3.PostedBy = WebSecurity.CurrentUserName;
                            db.Transs.Add(my3);

                            //Update table  
                            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            total += q.chargevalue;
                        }
                    }


                }
            
                if (drporder.SelectedValue.ToString() == "Buy")
                {
                    //debit account number
                    my2.Account = drpname.SelectedValue.ToString();
                    my2.Category = "Deal";
                    my2.Credit = 0;
                    my2.Debit = (maxprice * quantity) + total;
                    my2.Narration = "Deal";
                    my2.Reference_Number = maxid.ToString();
                    my2.TrxnDate = DateTime.Now;
                    my2.Post = DateTime.Now;
                    my2.Type = "Purchase Deal";
                    my2.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my2);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }
                else if (drporder.SelectedValue.ToString() == "Sell")
                {
                    //debit account number
                    my2.Account = drpname.SelectedValue.ToString();
                    my2.Category = "Deal";
                    my2.Credit = (maxprice * quantity) + total;
                    my2.Debit = 0;
                    my2.Narration = "Deal";
                    my2.Reference_Number = maxid.ToString();
                    my2.TrxnDate = DateTime.Now;
                    my2.Post = DateTime.Now;
                    my2.Type = "Sell Deal";
                    my2.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my2);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }






                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                //msgbox("Saved Sucessfully");
           
             
                //insert into chart of accounts

                //Liability account
                var liab = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Liability"));
                int lacc = 0;
                foreach(var c in liab)
                {
                    lacc = c.AccountNumber;
                }
              
                //Debit
                my2.Account =lacc.ToString();
                my2.Category = "Liability";
                my2.Credit = (maxprice * quantity)+total;
                my2.Debit = 0;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Order Posting";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
               

               
                my2.Account = num.ToString();
                my2.Category = "Deal";
                my2.Credit = 0;
                my2.Debit = (maxprice * quantity) + total;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Order Posting";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());





                //var edc = escorw.Createnewaccount(code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
                //my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, "", my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
                //my.Accountnumber, my.mobile_money);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the order";
                Response.Redirect("~/Orders/Index");
                //msgbox(edc);
            }
            else if (Submit.Text == "Update")
            {
                var my = db.Order_Lives.Find(Convert.ToInt32(txtFkey.Text));
                my.OrderType = drporder.SelectedValue.ToString();
                //my.Company = txtcompany.Text;
                my.SecurityType = txtsecurity.Text;
                my.Client_Type = txtClient.Text;
                my.Tax = Convert.ToDecimal(Tax.Text);
                my.Shareholder = Shareholder.Text;
                my.ClientName = drpname.SelectedItem.Text;
                my.CDS_AC_No = drpname.SelectedValue.ToString();
                my.TotalShareHolding = Convert.ToDecimal(TotalShareHolding.Text);
                my.Deal_Begin_Date = Convert.ToDateTime(begindate.Value);
                my.Expiry_Date = Convert.ToDateTime(expirydate.Value);
                my.Quantity = Convert.ToInt32(qty.Text);
                if (txtbase.Text == "" || txtbase.Text == null)
                {
                    txtbase.Text = "0";
                }
                decimal n;
                bool isNumericT = decimal.TryParse(txtbase.Text, out n);

                if (isNumericT == false)
                {
                    txtbase.Text = "0";
                }
                my.BasePrice = Convert.ToDecimal(txtbase.Text);
                my.AvailableShares = Convert.ToDecimal(availableshrs.Text);
                my.OrderPref = ordepref.Text;
                my.OrderAttribute = ordA.Text;
                my.Marketboard = mrktBoard.Text;
                if (max.Text == "" || max.Text == null)
                {
                    max.Text = "0";
                }
                decimal s;
                bool isNumericS = decimal.TryParse(max.Text, out s);

                if (isNumericS == false)
                {
                    max.Text = "0";
                }
                my.MaxPrice = Convert.ToDecimal(max.Text);
                if (mini.Text == "" || mini.Text == null)
                {
                    mini.Text = "0";
                }
                decimal p;
                bool isNumericZ = decimal.TryParse(mini.Text, out p);

                if (isNumericZ == false)
                {
                    mini.Text = "0";
                }
                my.MiniPrice = Convert.ToDecimal(mini.Text);
                drpCurr.SelectedValue.ToString();
                my.Company = Company.SelectedValue.ToString();
               my.CompanyISIN =Company.SelectedItem.Text;
                my.TP = drpTP.SelectedItem.Text;
                my.TPBoard = drpTP.SelectedItem.Text;
                my.mobile = txtMobile.Text;
                //Update Row in Table  
                db.Order_Lives.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                string strscript = "<script>alert('You have successfully updated the order');window.location.href='../Orders/Index'</script>";

                if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                }

            }
        }
        protected void Clear()
        {
            Response.Redirect("~/Orders/Index");
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void drpname_SelectedIndexChanged(object sender, EventArgs e)
        {
            Shareholder.Text = drpname.SelectedItem.Text;

            var hold = db.ClientPortfolioss.ToList().Where(a => a.ClientNumber == drpname.SelectedValue.ToString()).Sum(a=>a.Holdings);


            availableshrs.Text = hold.ToString();
            TotalShareHolding.Text= hold.ToString();

        }

        protected void drpTP_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpTPB.Items.Clear();
            drpTPB.DataSource = null;
            int my = Convert.ToInt32(drpTP.SelectedValue.ToString());
            var ds = from c in db.TradingBoards
                     where c.TradingPlatformID==my
                     select new

                     {

                         ID = c.BoardName,

                         ISIN = c.BoardName,

                     };
            drpTPB.DataSource = ds.ToList();
            drpTPB.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpTPB.DataValueField = "ID"; // to retrive specific  textfield name 
                                          //assigning datasource to the dropdownlist
            drpTPB.DataBind(); //binding dropdownlist
                               // allocate.Items.Insert(0, "Select");
            drpTPB.Items.Insert(0, new ListItem("Please Select a Tradiong Platform Board", "0"));

        }

        protected void btnInvoice_Click(object sender, EventArgs e)
        {
            if (max.Text =="")
            {
                msgbox("The maximum base price must be selected");
                return;
            }
            else if (qty.Text=="")
            {
                msgbox("The quantity must be selected");
                return;
            }

          
            //string chargedAs = "";

            //decimal chargevalue = 0;
            //decimal total = 0;
            decimal qty2 = Convert.ToDecimal(qty.Text);
            decimal maxprice = Convert.ToDecimal(max.Text);

            //var tr = db.TradingChargess.ToList().Where(a=>a.tradeside==drporder.SelectedValue.ToString());

            //foreach(var c in tr)
            //{
            //    chargedAs = c.ChargedAs;
            //    chargevalue = c.chargevalue;
            //}

            //if (chargedAs == "Percentage")
            //{
            //    total = (maxprice * qty2) + ((chargevalue / 100) * (maxprice * qty2));
            //}
            //else if (chargedAs == "Flat")
            //{
            //    total = (maxprice * qty2) + chargevalue;
            //}
            //ASPxDocumentViewer1.ReportTypeName = "";
            //ASPxDocumentViewer1.ReportTypeName = "BrokerOffice.Reporting.XtraQuotation";
            //XtraQuotation report = new XtraQuotation();
            //report.Parameters["Account"].Value = drpname.SelectedValue.ToString();

            //report.Parameters["Max"].Value = maxprice.ToString();

            //report.Parameters["Quantity"].Value = qty2.ToString();

            //report.RequestParameters = false;

            //ASPxDocumentViewer1.Report = report;

            //report.Parameters["yourParameter2"].Value = secondValue;


            //report.Parameters["yourParameter2"].Value = secondValue;
            myIframe.Src = "~/Reporting/Quotation.aspx#ASPxDocumentViewer1?Account=" + drpname.SelectedValue.ToString() + "&max=" + maxprice.ToString()+ "&qty="+ qty2.ToString();
 
             ASPxPopupControl1.PopupHorizontalAlign = PopupHorizontalAlign.WindowCenter;
            ASPxPopupControl1.PopupVerticalAlign = PopupVerticalAlign.WindowCenter;
            ASPxPopupControl1.AllowDragging = true;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowOnPageLoad = true;
            ASPxPopupControl1.Modal = true;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowRefreshButton = true;
            ASPxPopupControl1.ShowShadow = true;
   
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

        protected void max_TextChanged(object sender, EventArgs e)
        {
            decimal s;
            bool isNumericS = decimal.TryParse(max.Text, out s);

            if (isNumericS == false)
            {
                max.Text = "0";
                msgbox("Must be numeric");
            }

        }

        protected void mini_TextChanged(object sender, EventArgs e)
        {
            decimal s;
            bool isNumericS = decimal.TryParse(mini.Text, out s);

            if (isNumericS == false)
            {
                mini.Text = "0";
                msgbox("Must be numeric");
            }
        }

        protected void qty_TextChanged(object sender, EventArgs e)
        {
            int s;
            bool isNumericS = int.TryParse(qty.Text, out s);

            if (isNumericS == false)
            {
                qty.Text = "0";
                msgbox("Must be integer i.e 8");
            }
        }

        protected void txtbase_TextChanged(object sender, EventArgs e)
        {
            decimal s;
            bool isNumericS = decimal.TryParse(txtbase.Text, out s);

            if (isNumericS == false)
            {
                txtbase.Text = "0";
                msgbox("Must be numeric");
            }
        }
    }
}