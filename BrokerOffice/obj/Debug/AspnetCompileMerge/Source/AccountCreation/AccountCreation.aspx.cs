﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Database = WebMatrix.Data.Database;
namespace BrokerOffice.AccountCreation
{
    public partial class AccountCreation : System.Web.UI.Page
    {
        
        public string id="";
       
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        
            id = Request.QueryString["id"];

 loadMemberGrid();
            if (this.IsPostBack==true)
            {
               
                 TabName.Value = Request.Form[TabName.UniqueID];
               
            }
            else if(IsPostBack == false){
                System.Web.HttpContext.Current.Session["Tab"] = "";
                ClearMember();
                LoadBank();
                LoadID();
                loadClientTypes();
                LoadCurrencies();
                LoadCountry();
                Label19.Visible = false;
                txtReg.Visible = false;
                Label20.Visible = false;
                txtID.Visible = false;
                if (id== " "|| id==null)
                {
                  
                }
                else
                {
                 getDetails(id);
                }
                ASPxComboBox1.DataSource = GetDataSource();
                ASPxComboBox1.DataBind();
            }
        
           
        }
        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
        var dd = db.Account_Creations.ToList();
            foreach (var fg in dd)
            {
              dtSource.Rows.Add(fg.CDSC_Number,fg.Surname_CompanyName+fg.OtherNames);
               
            }
                
                return dtSource;
        }


        protected void getDetails(string x)
        {

            cmbBank.Items.Clear();
            cmbBank.DataSource= null;
            cmbBranch.Items.Clear();
            cmbBranch.DataSource = null;
            if (x != "")
            {
             btnSubmit.Text = "Update";
 txtFkey.Text = x;

                var acc = db.Account_Creations.ToList().Where(a => a.ID_==Convert.ToInt32(x));
               
                foreach(var my in acc)
                {
                
                     drpTitle.SelectedValue=my.Title ;
                    drpTitle.SelectedItem.Text = my.Title;
                    txtFirstName.Text= my.OtherNames ;
                    txtMiddle.Text=  my.Middlename;
                    txtSurname.Text=my.Surname_CompanyName  ;
                     
                  txtResident.Text=my.Resident;
                    txtDOB.Date = Convert.ToDateTime(my.DateofBirth_Incorporation);

                    RadioButtonList1.SelectedValue=my.Gender;
                     intials.Text=my.Initials;
                    //contact
                      addr1.Text=my.Address1;
                     addr2.Text=my.Address2;
                    addr3.Text=my.Address3  ;


                    //ident
                    //my.ClientSuffix = drpSuffix.SelectedValue.ToString();
                     txtReg.Text=my.RNum;
                   txtID.Text  =my.Identification ;

                    //banking
                    Nationality.Items.Insert(0, new ListItem(my.Nationality.ToString(), my.Nationality.ToString()));

                    if (my.divaccounttype!="")
                    {
                        divacctype.Items.Insert(0, new ListItem(my.divaccounttype.ToString(), my.divaccounttype.ToString()));

                    }
                    drpTax.Items.Insert(0, new ListItem(my.TaxBracket.ToString(), my.TaxBracket.ToString()));
                    drpSuffix.Items.Insert(0, new ListItem(my.ClientSuffix, my.ClientSuffix));
                    cmbBank.Items.Insert(0, new ListItem(my.Bankname,my.Bank  ));
                     txtTown.Text =my.Town;
                    txtPostCode.Text=my.PostCode ;
                    txtFax.Text=my.FaxNumber ;
                   txtTel.Text= my.TelephoneNumber ;
                    txtEmail.Text=my.Emailaddress ;
                    cmbBranch.Items.Insert(0, new ListItem( my.BranchName,my.Branch));
                    my.Accountnumber = txtAcc.Text;
                  
                    drpID.Items.Insert(0, new ListItem(my.idtype, my.idtype));
                    RadioButtonList2.SelectedValue=my.accountcategory;
              
                    drpCountry.Items.Insert(0, new ListItem(my.country, my.country));
               

                    drpCurr.Items.Insert(0, new ListItem(my.currency, my.currency));
                    dvpayee.Text=my.divpayee;
                 
                    divbank.Items.Insert(0, new ListItem(my.divbank, my.divbankcode));
        
                    divbranch.Items.Insert(0, new ListItem(my.divbranch, my.divbranchcode));
                   dividpayee.Text=my.dividnumber;
                    drpID2.Items.Insert(0, new ListItem(my.idtype2, my.idtype2));
                    dep.Items.Insert(0, new ListItem(my.depname, my.depcode));
                    txtAccD.Text=my.divacc;
                   divacctype.Text=my.divaccounttype ;
                    // my.Date_Created = Convert.ToDateTime(txtDOB.Text);
                    txtMobile.Text = my.MobileNumber;
                    idPay.SelectedValue=my.mobile_money;
                     manAccount.Text=my.manAccount;
                  
                    manBank.Items.Insert(0, new ListItem(my.manBank, my.manBankCode));
                    manBranch.Items.Insert(0, new ListItem(my.manBranch, my.manBranchCode));
                    manNames.Text=my.manNames;
                    manAddress.Text = my.manAddress;
                    mobilewallet.Text=my.mobilewalletnumber;
                    if (my.mobile_money == "ONE WALLET")
                    {
                        lblMobi.Visible = true;
                        mobilewallet.Visible = true;
                    }
                    else
                    {
                        lblMobi.Visible = false;
                        mobilewallet.Visible = false;
                    }
                }
                LoadBank2();
                LoadID2();
                loadClientTypes2();
                LoadCurrencies2();
                LoadCountry2();
            }
          
            return;
        }
        protected void LoadBank2()
        {
            
            var ds = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                        Bankname = c.bank_name,

                     };
            cmbBank.DataSource = ds.ToList();
            cmbBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            cmbBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                    //assigning datasource to the dropdownlist
            cmbBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");


            var ds6 = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                         Bankname = c.bank_name,

                     };
            manBank.DataSource = ds6.ToList();
           manBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            manBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            manBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

            
            var ds3 = from c in db.Depositorys
                      select new

                      {

                          Bankcode = c.Code,

                          Bankname = c.Name,

                      };
            dep.DataSource = ds3.ToList();
            dep.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            dep.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            dep.DataBind(); //binding dropdownlist
                            // allocate.Items.Insert(0, "Select");
    
        }
        protected void LoadBank()
        {
            cmbBank.Items.Clear();
            cmbBank.DataSource = null;
            var ds = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                         Bankname = c.bank_name,

                     };
            cmbBank.DataSource = ds.ToList();
            cmbBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            cmbBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            cmbBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            cmbBank.Items.Insert(0, new ListItem("Please Select a bank", "0"));

          divbank.Items.Clear();
            divbank.DataSource = null;
            var ds2 = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                         Bankname = c.bank_name,

                     };
            divbank.DataSource = ds2.ToList();
            divbank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            divbank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            divbank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            divbank.Items.Insert(0, new ListItem("Please Select a bank", "0"));

            manBank.Items.Clear();
           manBank.DataSource = null;
            var ds6 = from c in db.Banks
                      select new

                      {

                          Bankcode = c.bank,

                          Bankname = c.bank_name,

                      };
            manBank.DataSource = ds6.ToList();
            manBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            manBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            manBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            manBank.Items.Insert(0, new ListItem("Please Select a bank", "0"));

            dep.Items.Clear();
           dep.DataSource = null;
            var ds3 = from c in db.Depositorys
                      select new

                      {

                          Bankcode = c.Code,

                          Bankname = c.Name,

                      };
           dep.DataSource = ds3.ToList();
            dep.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            dep.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            dep.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
           dep.Items.Insert(0, new ListItem("Please Select a Depository", "0"));
        }

        protected void txtID_TextChanged(object sender, EventArgs e)
        {
    
            txtID.Focus();
            return;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            if(RadioButtonList2.SelectedIndex==0)
            {
                txtSurname.Text += txtCompanyName.Text;
            }
                
                
            if (txtFirstName.Text == "")
            {
                msgbox("First name is required !");
                return;
            }else if (txtSurname.Text=="")
            {
                msgbox("Surname is required !");
                return;
            }
            else if (txtDOB.Text == "")
            {
                msgbox("Date of birth is required !");
                return;
            }
            else if (Nationality.SelectedItem.Text== "Please Select a nationality")
            {
                msgbox("Nationality is required !");
                return;
            }
            else if (addr1.Text == "")
            {
                msgbox("Address is required !");
                return;
            }
            else if (txtTown.Text == "")
            {
                msgbox("Town is required !");
                return;
            }
            else if (txtEmail.Text == "")
            {
                msgbox("Email is required !");
                return;
            }
            else if (txtMobile.Text == "")
            {
                msgbox("Mobile is required !");
                return;
            }
            else if (drpCountry.SelectedItem.Text == "Please Select a country")
            {
                msgbox("Country is required !");
                return;
            }
            else if (RadioButtonList1.SelectedIndex < 0)
            {
                msgbox("Gender is required !");
                return;
            }
            else if (RadioButtonList2.SelectedIndex<0 )
            {
                msgbox("Company or individual is required !");
                return;
            }
           
            else if (cmbBank.SelectedItem.Text == "Please Select a bank")
            {
                msgbox("Select a Bank is required !");
                return;
            }
            else if (divbank.SelectedItem.Text == "Please Select a bank")
            {
                msgbox("Select a Dividend Bank is required !");
                return;
            }
            else if (drpID.SelectedItem.Text == "Please Select an ID type")

            {
                msgbox("Select an id type !");
                return;
            }
            else if (drpSuffix.SelectedItem.Text == "Please Select a client type")
            {
                msgbox("Clientsuffix is required !");
                return;
            }else if (drpCountry.SelectedItem.Text == "Please Select a country")
            {
                msgbox("Country is required !");
                return;
            }
            else if (drpID2.SelectedItem.Text == "Please Select an ID type")

            {
                msgbox("ID type is required !");
                return;
            }
            else if (idPay.SelectedIndex < 0)
            {
                msgbox("Select a payment method !");
                return;
            }
            else if (txtTown.Text=="")
            {
                msgbox("Select town!");
                return;
            }
              else if (txtMobile.Text=="")
            {
                msgbox("Please enter mobile number!");
                return;
            }
            else if (txtAcc.Text == "")
            {
                msgbox("Account number is required!");
                return;
            }



            if (btnSubmit.Text == "Submit")
            {
                Account_Creation my = new Account_Creation();
                //generate CDSNUMBEr
                var con2 = Database.Open("SBoardConnection");
                var audit = "select max(ID_) as ID from Account_Creation";
                var list13 = con2.Query(audit).ToList();
                int code25 = 0;
                try
                {
                foreach (var row in list13)
                {
                    code25 = row.ID;
                }

                }

                 catch(Exception f)
                {
                    code25 = 0;
                }
            
                int size = 5;


                var agent = "select top 1 AgentCode from Agent";
                var list14 = con2.Query(agent).ToList();
                string code22 = "";
                foreach (var row in list14)
                {
                    code22 = row.AgentCode;
                }
                string concode25 = Convert.ToString((code25 + 1));
                string cdsNo = concode25.PadLeft(10, '0') + code22 + concode25;
                my.CDSC_Number = cdsNo;
                my.Title = drpTitle.SelectedValue.ToString();
                my.OtherNames = txtFirstName.Text;
                my.Middlename = txtMiddle.Text;
                my.Surname_CompanyName = txtSurname.Text;
                my.Nationality = Nationality.SelectedItem.Text;
                my.Resident = txtResident.Text;
                my.DateofBirth_Incorporation = Convert.ToDateTime(txtDOB.Text);

                my.Gender = RadioButtonList1.SelectedValue.ToString();
                my.Initials = intials.Text;
                //contact
                my.Address1 = addr1.Text;
                my.Address2 = addr2.Text;
                my.Address3 = addr3.Text;
                my.Town = txtTown.Text;
                my.PostCode = txtPostCode.Text;
                my.FaxNumber = txtFax.Text;
                my.TelephoneNumber = txtTel.Text;
                    my.Emailaddress = txtEmail.Text;
                //ident
                my.ClientSuffix = drpSuffix.SelectedValue.ToString();
                my.RNum = txtReg.Text;
                my.Identification = txtID.Text;

                //banking
                my.TaxBracket = Convert.ToInt32(drpTax.SelectedValue.ToString());
                my.Bank = cmbBank.SelectedValue.ToString();
                my.Branch = cmbBranch.SelectedValue.ToString();
                my.Bankname = cmbBank.SelectedItem.Text;
                my.BranchName = cmbBranch.SelectedItem.Text;
                my.Accountnumber = txtAcc.Text;
                my.idtype = drpID.SelectedItem.Text;
                my.accountcategory = RadioButtonList2.SelectedValue.ToString();
                my.country = drpCountry.SelectedValue.ToString();
                my.currency = drpCurr.SelectedValue.ToString();
                my.divpayee= dvpayee.Text;
                my.divbank= divbank.SelectedItem.Text;
                my.divbankcode = divbank.SelectedValue.ToString();
                my.divbranch = divbranch.SelectedItem.Text;
                my.divbranchcode= divbranch.SelectedValue.ToString();
                my.divacc = txtAccD.Text;
                my.divaccounttype = divacctype.Text;
                my.dividnumber = dividpayee.Text;
                if (drpID2.SelectedValue.ToString() == "0")
                {
                    my.idtype2 = "0";
                }
                else
                {
                    my.idtype2 = drpID2.SelectedValue.ToString();
                }
                my.Date_Created = DateTime.Now;
                my.MobileNumber = txtMobile.Text;
                my.mobile_money = idPay.SelectedValue.ToString();
                my.depname = dep.SelectedItem.Text;
                my.depcode = dep.SelectedValue.ToString();
                my.manAccount = manAccount.Text;
                my.manBank = manBank.SelectedItem.Text;
                my.manBankCode = manBank.SelectedValue.ToString();
                my.manBranch = manBranch.SelectedItem.Text;
                my.manBranchCode = manBranch.SelectedValue.ToString();
                my.manNames = manNames.Text;
                my.manAddress = manAddress.Text;
                my.mobilewalletnumber = mobilewallet.Text;
                //Insert Blank Row in Table  
                my.CreatedBy = WebSecurity.CurrentUserName.ToString();


               // if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx") == true)
               // {
               //     WebReference.EscrowServiceSoap escorw = new WebReference.EscrowServiceSoap();
               //     var edc = escorw.Createnewaccount(code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
               //    my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, "", my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
               //    my.Accountnumber, my.mobile_money);
               //     my.CDSC_Number = Regex.Replace(edc.ToString(), "<[^>]+>", string.Empty); ;
               //}
               // else
               // {


               //     WebReference2.EscrowServiceSoap escorw2 = new WebReference2.EscrowServiceSoap();
               //     var edc = escorw2.Createnewaccount(code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
               //    my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, "", my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
               //    my.Accountnumber, my.mobile_money);
               //     my.CDSC_Number = Regex.Replace(edc.ToString(), "<[^>]+>", string.Empty); ;

               //     }



                //finaaly save
                db.Account_Creations.Add(my);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                UpdateMember(my.CDSC_Number);

                db.Database.ExecuteSqlCommand("Exec NewAccounts");
                System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + "You have successfully updated the client";
                Response.Redirect("~/Account_Creation/Index");


            }
            else if (btnSubmit.Text == "Update")
            {
                var p = db.Account_Creations.ToList().Where(a => a.ID_==Convert.ToInt32(txtFkey.Text));

                string cc = "";
                foreach (var d in p)
                {
                    cc = d.CDSC_Number;
                }

                var ps = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number==cc);

                int f = 0;

                foreach(var q in ps)
                {
                    f = q.ID_;
                }
                var my = db.Account_CreationPendingss.Find(f);
                my.Title = drpTitle.SelectedValue.ToString();
                my.OtherNames = txtFirstName.Text;
                my.Middlename = txtMiddle.Text;
                my.Surname_CompanyName = txtSurname.Text;
                my.Nationality = Nationality.SelectedItem.Text;
                my.Resident = txtResident.Text;
                my.DateofBirth_Incorporation = Convert.ToDateTime(txtDOB.Text);

                my.Gender = RadioButtonList1.SelectedValue.ToString();
                my.Initials = intials.Text;
                //contact
                my.Address1 = addr1.Text;
                my.Address2 = addr2.Text;
                my.Address3 = addr3.Text;


                //ident
                my.ClientSuffix = drpSuffix.SelectedValue.ToString();
                my.RNum = txtReg.Text;
                my.Identification = txtID.Text;

                //banking
                my.TaxBracket = Convert.ToInt32(drpTax.SelectedValue.ToString());
                my.Bank = cmbBank.SelectedValue.ToString();
                my.Branch = cmbBranch.SelectedValue.ToString();
                my.Bankname = cmbBank.SelectedItem.Text;
                my.BranchName = cmbBranch.SelectedItem.Text;
                my.Accountnumber = txtAcc.Text;
                my.idtype = drpID.SelectedItem.Text;
                my.accountcategory = RadioButtonList2.SelectedValue.ToString();
                my.country = drpCountry.SelectedValue.ToString();
                my.currency = drpCurr.SelectedValue.ToString();
                my.divpayee = dvpayee.Text;
                my.divbank = divbank.SelectedItem.Text;
                my.divbankcode = divbank.SelectedValue.ToString();
                my.divbranch = divbranch.SelectedItem.Text;
                my.divbranchcode = divbranch.SelectedValue.ToString();
                my.divacc = txtAccD.Text;
                my.divaccounttype = divacctype.Text;
                my.Town = txtTown.Text;
                my.PostCode = txtPostCode.Text;
                my.FaxNumber = txtFax.Text;
                my.TelephoneNumber = txtTel.Text;
                    my.Emailaddress = txtEmail.Text;
                my.MobileNumber = txtMobile.Text;
                //Update Row in Table  
                my.dividnumber = dividpayee.Text;
                my.mobile_money = idPay.SelectedValue.ToString();
                if (drpID2.SelectedValue.ToString() == "0")
                {
                    my.idtype2 = "0";
                }
                else
                {
                   my.idtype2 = drpID2.SelectedValue.ToString();
                }
               
                my.depname = dep.SelectedItem.Text;
                my.depcode = dep.SelectedValue.ToString();
                my.update_type = "TOUPDATE";
                my.ModifiedBy = WebSecurity.CurrentUserName.ToString();
             
                db.Account_CreationPendingss.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                UpdateMember(my.CDSC_Number);

                System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + "You have successfully updated the client";
                Response.Redirect("~/Account_Creation/Index");
                string strscript = "<script>alert('You have successfully updated the client');window.location.href='../Account_Creation/Index'</script>";

                //if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                //}

            }

        }

        protected void Clear()
        {
            Response.Redirect("~/Account_Creation/Index");
        }
        public string CreateRandomPassword(int size)
        {
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string password = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            password = passwordString;
            return password;
        }
        protected void cmbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
           
          
            cmbBranch.Items.Clear();
            cmbBranch.DataSource = null;
            var ds = from c in db.Branches
                     where c.bank==cmbBank.SelectedValue.ToString()
                     select new

                     {

                     branchcode = c.branch,

                         branchname =c.branch_name,

                     };
            cmbBranch.DataSource = ds.ToList();
            cmbBranch.DataTextField = "branchname";
            // text field name of table dispalyed in dropdown
            cmbBranch.DataValueField = "branchcode"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            cmbBranch.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");
            cmbBranch.Items.Insert(0, new ListItem("Please Select a branch", "0"));
        }

        protected void divbank_SelectedIndexChanged(object sender, EventArgs e)
        {
           divbranch.Items.Clear();
            divbranch.DataSource = null;
            var ds = from c in db.Branches
                     where c.bank == cmbBank.SelectedValue.ToString()
                     select new

                     {

                         branchcode = c.branch,

                         branchname = c.branch_name,

                     };
            divbranch.DataSource = ds.ToList();
            divbranch.DataTextField = "branchname";
            // text field name of table dispalyed in dropdown
            divbranch.DataValueField = "branchcode"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            divbranch.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");
            divbranch.Items.Insert(0, new ListItem("Please Select a branch", "0"));
        }

        protected void LoadID()
        {
            drpID.Items.Clear();
            drpID.DataSource = null;
            var ds = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID.DataSource = ds.ToList();
            drpID.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpID.Items.Insert(0, new ListItem("Please Select an ID type", "0"));

            drpID2.Items.Clear();
            drpID2.DataSource = null;
            var ds2 = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID2.DataSource = ds2.ToList();
            drpID2.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID2.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID2.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpID2.Items.Insert(0, new ListItem("Please Select an ID type", "0"));

        }
        protected void LoadID2()
        {
            drpID.Items.Clear();
            drpID.DataSource = null;
            var ds = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID.DataSource = ds.ToList();
            drpID.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");


            drpID2.Items.Clear();
            drpID2.DataSource = null;
            var ds2 = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID2.DataSource = ds2.ToList();
            drpID2.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID2.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID2.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");

        }

        protected void loadClientTypes2()
        {
           
            var ds = from c in db.Client_Types
                     select new

                     {

                         ID = c.ClientType,

                         ISIN = c.ClientType,

                     };
            drpSuffix.DataSource = ds.ToList();
            drpSuffix.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpSuffix.DataValueField = "ID"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpSuffix.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");

            
            var dss = from c in db.DivTypes
                      select new

                      {

                          ID = c.DiviTypeName,

                          ISIN = c.DiviTypeName

                      };
            int cc = dss.ToList().Count();
            divacctype.DataSource = dss.ToList();
            divacctype.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            divacctype.DataValueField = "ID"; // to retrive specific  textfield name 
                                              //assigning datasource to the dropdownlist
            divacctype.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

          

        }
        protected void loadClientTypes()
        {
          drpSuffix.Items.Clear();
            drpSuffix.DataSource = null;
            var ds = from c in db.Client_Types
                     select new

                     {

                         ID = c.ClientType,

                         ISIN = c.ClientType,

                     };
            drpSuffix.DataSource = ds.ToList();
            drpSuffix.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpSuffix.DataValueField = "ID"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpSuffix.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");

            drpSuffix.Items.Insert(0, new ListItem("Please Select a client type", "0"));

            divacctype.Items.Clear();
            divacctype.DataSource = null;
            var dss = from c in db.DivTypes
                     select new

                     {

                         ID = c.DiviTypeName,

                         ISIN = c.DiviTypeName,

                     };
            int cc = dss.ToList().Count();
            divacctype.DataSource = dss.ToList();
            divacctype.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            divacctype.DataValueField = "ID"; // to retrive specific  textfield name 
                                              //assigning datasource to the dropdownlist
            divacctype.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

            divacctype.Items.Insert(0, new ListItem("Please Select a div type", "0"));


        }
        protected void LoadCurrencies2()
        {
           
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");



        }
        protected void LoadCurrencies()
        {
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

            drpCurr.Items.Insert(0, new ListItem("Please Select a currency", "0"));


        }

        protected void LoadCountry2()
        {
            var ds = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            drpCountry.DataSource = ds.ToList();
            drpCountry.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCountry.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            drpCountry.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

            var dss = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            Nationality.DataSource = dss.ToList();
            Nationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            Nationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            Nationality.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");



        }
        protected void LoadCountry()
        {
            drpCountry.Items.Clear();
            drpCountry.DataSource = null;
            var ds = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            drpCountry.DataSource = ds.ToList();
            drpCountry.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCountry.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            drpCountry.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

            drpCountry.Items.Insert(0, new ListItem("Please Select a country", "0"));


            Nationality.Items.Clear();
            Nationality.DataSource = null;
            var dss = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            Nationality.DataSource = dss.ToList();
            Nationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            Nationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            Nationality.DataBind(); //binding dropdownlist
                                    // allocate.Items.Insert(0, "Select");

            Nationality.Items.Insert(0, new ListItem("Please Select a nationality", "0"));


        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList2.SelectedIndex==0)
            {
                Label19.Visible = true;
                txtReg.Visible = true;
                Label20.Visible = false;
                txtID.Visible = false;
            }
            else if (RadioButtonList2.SelectedIndex == 1)
            {
                Label19.Visible =false;
                txtReg.Visible = false;
                Label20.Visible =true;
                txtID.Visible = true;
            }
            }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

     

        protected void upload2_Click(object sender, EventArgs e)
        {
                   
            Accounts_Documents my = new Accounts_Documents();
            my.Name = FileUpload.PostedFile.FileName;
            my.ContentType = System.IO.Path.GetExtension(FileUpload.PostedFile.FileName);
            my.doc_generated = "";
            var content = new byte[FileUpload.PostedFile.ContentLength];
            my.Data = content;
            db.Accounts_Documentss.AddOrUpdate(my);
            db.SaveChanges(WebSecurity.CurrentUserName);
            loadMemberGrid();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            Label44.Text = "contact";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxIdentiication_Click(object sender, EventArgs e)
        {
            Label44.Text = "identification";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxContact_Click(object sender, EventArgs e)
        {
            Label44.Text = "contact";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxBanking_Click(object sender, EventArgs e)
        {
            Label44.Text = "banking";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxIdentificationBack_Click(object sender, EventArgs e)
        {
            Label44.Text = "identification";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxTrading_Click(object sender, EventArgs e)
        {
            Label44.Text = "trading";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxBankingBack_Click(object sender, EventArgs e)
        {
            Label44.Text = "banking";
            TabName.Value = Label44.Text;
            return;
        }

        protected void manBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            manBranch.Items.Clear();
            manBranch.DataSource = null;
            var ds = from c in db.Branches
                     where c.bank == manBank.SelectedValue.ToString()
                     select new

                     {

                         branchcode = c.branch,

                         branchname = c.branch_name,

                     };
            manBranch.DataSource = ds.ToList();
            manBranch.DataTextField = "branchname";
            // text field name of table dispalyed in dropdown
            manBranch.DataValueField = "branchcode"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            manBranch.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");
            manBranch.Items.Insert(0, new ListItem("Please Select a branch", "0"));

        }

        protected void idPay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(idPay.SelectedItem.Text=="ONE WALLET")
            {
                lblMobi.Visible = true;
                mobilewallet.Visible = true;
            }else
            {
                lblMobi.Visible = false;
                mobilewallet.Visible = false;
            }
        }


        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                object key = null;
                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        key = ASPxGridView1.GetRowValues(i, "id");

                    }

                }
                
            }
            catch (Exception)
            {


            }


        }


        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(e.Keys[ASPxGridView1.KeyFieldName].ToString());
                string c = i.ToString();

                Accounts_Documents user = db.Accounts_Documentss.Find(i);
                db.Accounts_Documentss.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                return;
            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }
        }

        protected void ClearMember()
        {
            var remove = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated));
            int count = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated)).Count();
            foreach (var detail in remove)
            {
               Accounts_Documents user = db.Accounts_Documentss.Find(detail.Accounts_DocumentsID);
                db.Accounts_Documentss.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }



        }
        protected void loadMemberGrid()
        {
            ASPxGridView1.DataSource = null;
            ASPxGridView1.DataBind();
            if (id == "")
            {
                var mem = (from p in db.Accounts_Documentss
                           where string.IsNullOrEmpty(p.doc_generated)
                           select new { p.Accounts_DocumentsID, p.Name, p.doc_generated, p.ContentType }).ToList();

                ASPxGridView1.DataSource = mem.ToList();
                ASPxGridView1.DataBind();
            }
            else
            {
                int my = Convert.ToInt32(id);
                string nme = "";
                var c = db.Account_Creations.ToList().Where(a => a.ID_ == my);
                foreach (var e in c)
                {
                    nme = e.CDSC_Number;
                }
                var mem = (from p in db.Accounts_Documentss
                           where p.doc_generated == nme || string.IsNullOrEmpty(p.doc_generated)
                           select new { p.Accounts_DocumentsID, p.Name, p.doc_generated, p.ContentType }).ToList();

                ASPxGridView1.DataSource = mem.ToList();
                ASPxGridView1.DataBind();
            }

            
        }


        protected void UpdateMember(string c)
        {

            
        

            List<Accounts_Documents> results = (from p in db.Accounts_Documentss
                                         where string.IsNullOrEmpty(p.doc_generated)
                                         select p).ToList();
WebReference.EscrowServiceSoap escorw = new WebReference.EscrowServiceSoap();
           foreach (Accounts_Documents p in results)
           {               p.doc_generated = c;
             //var edc2 = escorw.SubmitDocuments(p.doc_generated,Convert.ToBase64String(p.Data),p.Name,p.ContentType);

     }

            db.SaveChanges(WebSecurity.CurrentUserName);
        }


    
        protected void ASPxComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == ASPxComboBox1.SelectedItem.Value);

            foreach(var p in c)
            {
                dividpayee.Text = p.Identification;
               
                manBranch.Items.Insert(0, new ListItem(p.BranchCode,p.BranchName));
                manAccount.Text = p.Accountnumber;
                manBank.Items.Insert(0, new ListItem(p.Bankcode,p.Bankname));
                manNames.Text = p.Surname_CompanyName + " " + p.OtherNames;
                manAddress.Text = p.Address1;
            }
        }

        protected void RadioButtonList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList3.SelectedIndex == 0)
            {
                ASPxComboBox1.Visible = true;
                dividpayee.Text = "";
                manAccount.Text = "";
                manNames.Text = "";
                manAddress.Text = "";
            }
            else if (RadioButtonList3.SelectedIndex == 1)
            {
                ASPxComboBox1.Visible = false;
                dividpayee.Text = "";
                manAccount.Text = "";
                manNames.Text = "";
                manAddress.Text = "";
            }
        }
    }
    }
