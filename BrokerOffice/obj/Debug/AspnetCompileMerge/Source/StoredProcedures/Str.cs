﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.StoredProcedures
{
    public class Str
    {

        public string newaccount()
        {
            var AccP = @"
Create  PROCEDURE [dbo].[NewAccounts]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Insert into Accounts creation
	insert into Account_CreationPending([RecordType]
      ,[ClientSuffix]
      ,[JointAcc]
      ,[Identification]
      ,[Title]
      ,[Initials]
      ,[OtherNames]
      ,[Surname_CompanyName]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Town]
      ,[PostCode]
      ,[FaxNumber]
      ,[Emailaddress]
      ,[DateofBirth_Incorporation]
      ,[Gender]
      ,[Nationality]
      ,[Resident]
      ,[TaxBracket]
      ,[TelephoneNumber]
      ,[Bank]
      ,[Branch]
      ,[ClientType]
      ,[CDSC_Number]
      ,[Notification_Sent]
      ,[Callback_Endpoint]
      ,[MNO_]
      ,[Date_Created]
      ,[File_Sent_DirectDebt]
      ,[File_Sent_AccountCr]
      ,[PIN_No]
      ,[Middlename]
      ,[RNum]
      ,[MobileNumber]
      ,[Bankname]
      ,[Bankcode]
      ,[BranchName]
      ,[BranchCode]
      ,[Accountnumber]
      ,[idtype]
      ,[accountcategory]
      ,[country]
      ,[currency]
      ,[divpayee]
      ,[divbank]
      ,[divbankcode]
      ,[divbranch]
      ,[divbranchcode]
      ,[divacc]
      ,[divaccounttype]
      ,[idtype2]
      ,[dividnumber]
      ,[mobile_money]
      ,[depname]
      ,[depcode]
      ,[manBank]
      ,[manBankCode]
      ,[manBranch]
      ,[manBranchCode]
      ,[manAccount]
      ,[manNames]
      ,[manAddress]
      ,[mobilewalletnumber]
      ,[Broker]
      ,[update_type])
	  select Top 1 [RecordType]
      ,[ClientSuffix]
      ,[JointAcc]
      ,[Identification]
      ,[Title]
      ,[Initials]
      ,[OtherNames]
      ,[Surname_CompanyName]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Town]
      ,[PostCode]
      ,[FaxNumber]
      ,[Emailaddress]
      ,[DateofBirth_Incorporation]
      ,[Gender]
      ,[Nationality]
      ,[Resident]
      ,[TaxBracket]
      ,[TelephoneNumber]
      ,[Bank]
      ,[Branch]
      ,[ClientType]
      ,[CDSC_Number]
      ,[Notification_Sent]
      ,[Callback_Endpoint]
      ,[MNO_]
      ,getdate()
      ,[File_Sent_DirectDebt]
      ,[File_Sent_AccountCr]
      ,[PIN_No]
      ,[Middlename]
      ,[RNum]
      ,[MobileNumber]
      ,[Bankname]
      ,[Bankcode]
      ,[BranchName]
      ,[BranchCode]
      ,[Accountnumber]
      ,[idtype]
      ,[accountcategory]
      ,[country]
      ,[currency]
      ,[divpayee]
      ,[divbank]
      ,[divbankcode]
      ,[divbranch]
      ,[divbranchcode]
      ,[divacc]
      ,[divaccounttype]
      ,[idtype2]
      ,[dividnumber]
      ,[mobile_money]
      ,[depname]
      ,[depcode]
      ,[manBank]
      ,[manBankCode]
      ,[manBranch]
      ,[manBranchCode]
      ,[manAccount]
      ,[manNames]
      ,[manAddress]
      ,[mobilewalletnumber]
      ,[Broker]
      ,'New'
	  from Account_Creation
	  order by ID_ desc
  
END";

            return AccP;
        }


        public string newaccountAuth()
        {
            var AccP = @"
Create  PROCEDURE [dbo].[NewAccountsAuth]
@cust_no nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Insert into Accounts creation
Update Account_Creation SET RecordType=p.RecordType
      ,[ClientSuffix]=p.[ClientSuffix]
      ,[JointAcc]=p.[JointAcc]
      ,[Identification]=p.[Identification]
      ,[Title]=p.[Title]
      ,[Initials]=p.[Initials]
      ,[OtherNames]=p.[OtherNames]
      ,[Surname_CompanyName]=p.[Surname_CompanyName]
      ,[Address1]=p.[Address1]
      ,[Address2]=p.[Address2]
      ,[Address3]=p.[Address3]
      ,[Town]=p.[Town]
      ,[PostCode]=p.[PostCode]
      ,[FaxNumber]=p.[FaxNumber]
      ,[Emailaddress]=p.[Emailaddress]
      ,[DateofBirth_Incorporation]=p.[DateofBirth_Incorporation]
      ,[Gender]=p.[Gender]
      ,[Nationality]=p.[Nationality]
      ,[Resident]=p.[Resident]
      ,[TaxBracket]=p.[TaxBracket]
      ,[TelephoneNumber]=p.[TelephoneNumber]
      ,[Bank]=p.[Bank]
      ,[Branch]=p.[Branch]
      ,[ClientType]=p.[ClientType]
      ,[CDSC_Number]=p.[CDSC_Number]
      ,[Notification_Sent]=p.[Notification_Sent]
      ,[Callback_Endpoint]=p.[Callback_Endpoint]
      ,[MNO_]=p.[MNO_]
      ,[Date_Created]=p.[Date_Created]
      ,[File_Sent_DirectDebt]=p.[File_Sent_DirectDebt]
      ,[File_Sent_AccountCr]=p.[File_Sent_AccountCr]
      ,[PIN_No]=p.[PIN_No]
      ,[Middlename]=p.[Middlename]
      ,[RNum]=p.[RNum]
      ,[MobileNumber]=p.[MobileNumber]
      ,[Bankname]=p.[Bankname]
      ,[Bankcode]=p.[Bankcode]
      ,[BranchName]=p.[BranchName]
      ,[BranchCode]=p.[BranchCode]
      ,[Accountnumber]=p.[Accountnumber]
      ,[idtype]=p.[idtype]
      ,[accountcategory]=p.[accountcategory]
      ,[country]=p.[country]
      ,[currency]=p.[currency]
      ,[divpayee]=p.[divpayee]
      ,[divbank]=p.[divbank]
      ,[divbankcode]=p.[divbankcode]
      ,[divbranch]=p.[divbranch]
      ,[divbranchcode]=p.[divbranchcode]
      ,[divacc]=p.[divacc]
      ,[divaccounttype]=p.[divaccounttype]
      ,[idtype2]=p.[idtype2]
      ,[dividnumber]=p.[dividnumber]
      ,[mobile_money]=p.[mobile_money]
      ,[depname]=p.[depname]
      ,[depcode]=p.[depcode]
       ,[manBank]=p.[manBank]
      ,[manBankCode]=p.[manBankCode]
      ,[manBranch]=p.[manBranch]
      ,[manBranchCode]=p.[manBranchCode]
      ,[manAccount]=p.[manAccount]
      ,[manNames]=p.[manNames]
      ,[manAddress]=p.[manAddress]
      ,[mobilewalletnumber]=p.[mobilewalletnumber]
      ,[Broker]=p.[Broker]
	 
     from Account_Creation a inner join Account_CreationPending p
	  ON  a.CDSC_Number=p.CDSC_Number
	 where p.CDSC_Number=@cust_no
  
END";

            return AccP;
        }
    }
}