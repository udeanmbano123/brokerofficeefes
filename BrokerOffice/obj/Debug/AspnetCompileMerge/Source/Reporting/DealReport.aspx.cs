﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class DealReport: System.Web.UI.Page
    {
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                try
                {
                    XtraDeal report = new XtraDeal();
                    report.Parameters["Account"].Value = HttpContext.Current.Session["Acc"].ToString();
                    report.Parameters["OrderNo"].Value = HttpContext.Current.Session["Ord"].ToString();
                    report.Parameters["SID"].Value = HttpContext.Current.Session["SID"].ToString();
                    report.Parameters["QTY"].Value = HttpContext.Current.Session["qty"].ToString();
                    report.Parameters["Dte"].Value = HttpContext.Current.Session["Dte"].ToString();
                    // 
                    //  HttpContext.Current.Session["max"]
                    // HttpContext.Current.Session["qty"]
                    //
                    //
                    //report.Parameters["yourParameter2"].Value = secondValue;

                    ASPxDocumentViewer1.Report = report;
                }
                catch (Exception)
                {

                    Response.Redirect("~/Account");
                }
            }
        }


    }
}