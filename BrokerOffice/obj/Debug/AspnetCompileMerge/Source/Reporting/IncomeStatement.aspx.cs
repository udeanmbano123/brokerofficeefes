﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BrokerOffice.DAO;

namespace BrokerOffice.Reporting
{
    public partial class IncomeStatement : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["id"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                var taxs = db.Taxes.ToList().Where(a=>a.isset=true);
                decimal tx = 0;

                foreach(var c in taxs)
                {
                    tx = c.Percentage;
                }

                XtraIncome report = new XtraIncome();
                report.Parameters["CDate"].Value =id;
                report.Parameters["Tax"].Value =tx;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }

        
    }
}