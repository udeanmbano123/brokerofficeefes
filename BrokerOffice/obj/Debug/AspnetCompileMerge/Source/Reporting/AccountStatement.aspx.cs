﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class AccountStatement : System.Web.UI.Page
    {
        public string id = "";
        public string frmdate = "";
        public string todate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["Account"];
            frmdate= Request.QueryString["Begin"];
            todate = Request.QueryString["End"];
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

               XtraAccStmt report = new XtraAccStmt();
                report.Parameters["Account"].Value = id;
                report.Parameters["FDate"].Value = frmdate;
                report.Parameters["TDate"].Value = todate;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }
    }
}