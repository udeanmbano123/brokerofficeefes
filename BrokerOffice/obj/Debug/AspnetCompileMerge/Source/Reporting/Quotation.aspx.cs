﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class Quotation : System.Web.UI.Page
    {
        public string Account = "";
        public decimal max = 0;
        public decimal qty = 0;
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            Account = Request.QueryString["Account"];
            max = Convert.ToDecimal(Request.QueryString["max"]);
            qty = Convert.ToDecimal(Request.QueryString["qty"]);

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraQuotation report = new XtraQuotation();
                report.Parameters["Account"].Value =Account;

                report.Parameters["Max"].Value = max;

                report.Parameters["Quantity"].Value = qty;

                report.RequestParameters = false;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}