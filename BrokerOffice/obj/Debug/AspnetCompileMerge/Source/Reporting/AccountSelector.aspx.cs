﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class AccountSelector : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                txtSearch.Visible = false;
                SLbl.Visible = false;
                btnSearch.Visible = false;
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //txtCustomerNumber.Text = GridView1.SelectedRow.Cells[2].Text;
            //txtCustomerNames.Text = GridView1.SelectedRow.Cells[3].Text + " " + GridView1.SelectedRow.Cells[4].Text;
            txtAcc.Text = GridView1.SelectedRow.Cells[3].Text;
      


        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            if (sel.SelectedIndex==0)
            {
                loadAccounts();
            }
            else if (sel.SelectedIndex == 1)
            {
                loadCustomers();
            }
           // loadCustomers();
        }
        protected void loadCustomers()
        {
            var action = from v in db.Account_Creations 
                         let Name=v.OtherNames
                         let Surname=v.Surname_CompanyName
                         let Account=v.CDSC_Number
                         where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
                         select new { Name, Surname,Account };

            GridView1.DataSource = action.ToList();
            GridView1.DataBind();
        }
        protected void loadAccounts()
        {
            var action = from s in db.Accounts_Masters
                         let Name=s.AccountName
                         let Description=s.Description
                         let Account=s.AccountNumber
                         select new { Name,Description,Account };

            GridView1.DataSource = action.ToList();
            GridView1.DataBind();
        }

        protected void sel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sel.SelectedIndex == 0)
            {
                txtSearch.Visible =false;
                SLbl.Visible = false;
                btnSearch.Visible = false;
                loadAccounts();
            }
            else if (sel.SelectedIndex == 1)
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
                txtSearch.Visible =true;
                SLbl.Visible = true;
                btnSearch.Visible = true;
             
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadCustomers();
        }

        protected void txtView_Click(object sender, EventArgs e)
        {
            if (txtAcc.Text == "")
            {
                msgbox("Account Number must be selected");
                return;
            }
            else if (begindate.Text=="")
            {
                msgbox("Begin date must be selected");
                return;
            }
            else if (expirydate.Text == "")
            {
                msgbox("End date must be selected");
                return;
            }
            else
            {
                Response.Redirect("~/Reporting/AccountStatement.aspx?Account="+txtAcc.Text+"&Begin="+begindate.Text+ "&End="+expirydate.Text);
            }
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
    }
}