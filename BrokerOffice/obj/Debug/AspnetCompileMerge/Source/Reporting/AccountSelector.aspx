﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="AccountSelector.aspx.cs" Inherits="BrokerOffice.Reporting.AccountSelector" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div style="margin-left:15%;margin-right:10%;background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <l style="color:white">Account Statement</l>
       
    </div> 

  <div class="form-group">
      <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <div class="col-md-10">
                <asp:RadioButtonList ID="sel" runat="server" AutoPostBack="true" OnSelectedIndexChanged="sel_SelectedIndexChanged" RepeatDirection="Horizontal">
                    <asp:ListItem Value="GL" Text="GL Group"></asp:ListItem>
                     <asp:ListItem Value="Cust" Text="Customer"></asp:ListItem>
                </asp:RadioButtonList>
             </div>
        </div>
         <div class="form-group">
      <asp:Label ID="SLbl" runat="server" CssClass="control-label col-md-2" Text="Search"></asp:Label>
            <div class="col-md-10">
                <dx:ASPxTextBox ID="txtSearch" runat="server" CssClass="form-control" Width="170px"></dx:ASPxTextBox>
                <dx:ASPxButton ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click"></dx:ASPxButton>
             </div>
        </div>
    <asp:GridView ID="GridView1" CssClass="table table-striped" runat="server"   EmptyDataText="No records has been added."
    AutoGenerateSelectButton="true" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True" PageSize="10" OnPageIndexChanging="grdData_PageIndexChanging"
    >   
      <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />

</asp:GridView>
       <div class="form-group" style="margin-left:2%">
      <asp:Label ID="Label2"  runat="server" Text="Selected Account Number"></asp:Label>
            <div class="col-md-10">
                <dx:ASPxTextBox ID="txtAcc" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
             </div>
        </div>
       
    <table class="table table-striped">
                      <tr>
                          <td>
<asp:Label ID="from" runat="server"  Text="From"></asp:Label>
<dx:ASPxDateEdit ID="begindate" CssClass="form-control" runat="server"></dx:ASPxDateEdit>
                          </td>
                          <td>
<asp:Label ID="to" runat="server"  Text="To"></asp:Label>
<dx:ASPxDateEdit ID="expirydate" CssClass="form-control"  runat="server"></dx:ASPxDateEdit>
                          </td>
                          <td>
                              <asp:Label  runat="server" Visiible="false" Text=""></asp:Label>
                              <dx:ASPxButton ID="txtView" runat="server" CssClass="btn btn-primary" Text="View" OnClick="txtView_Click"></dx:ASPxButton>
                          </td>
                          </tr>
        </table></div>
  </asp:content>

