﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class Trans : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];

        }

        protected void drpdebit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDbAcc.Text = drpdebit.SelectedValue.ToString();
        }

        protected void drpCredit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCrAcc.Text=drpCredit.SelectedValue.ToString();
        }

        protected void Debit_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dr = Debit.SelectedValue.ToString();
            string cr = Credit.SelectedValue.ToString();

            if(dr=="Client" && cr=="Client")
            {
                msgbox("Client transactions are not allowed");
                return;
            }

            if (Debit.SelectedIndex == 0)
            {
                drpdebit.Items.Clear();
                drpdebit.DataSource = null;
                var ds = from c in db.Accounts_Masters
                         select new

                         {

                             GroupID = c.AccountNumber,

                             GroupName = c.AccountName,

                         };
                drpdebit.DataSource = ds.ToList();
                drpdebit.DataTextField = "GroupName";
                // text field name of table dispalyed in dropdown
                drpdebit.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                     //assigning datasource to the dropdownlist
                drpdebit.DataBind(); //binding dropdownlist
                                     // allocate.Items.Insert(0, "Select");
                drpdebit.Items.Insert(0, new ListItem("Please Select a group", "0"));
            }
            else if(  Debit.SelectedIndex == 1){
                drpdebit.Items.Clear();
                drpdebit.DataSource = null;
                var ds = from c in db.Account_Creations
                         select new

                         {

                             GroupID = c.CDSC_Number,

                             GroupName = c.Surname_CompanyName+","+c.OtherNames+" CustNo "+c.CDSC_Number,

                         };
                drpdebit.DataSource = ds.ToList();
                drpdebit.DataTextField = "GroupName";
                // text field name of table dispalyed in dropdown
                drpdebit.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                     //assigning datasource to the dropdownlist
                drpdebit.DataBind(); //binding dropdownlist
                                     // allocate.Items.Insert(0, "Select");
                drpdebit.Items.Insert(0, new ListItem("Please Select a client", "0"));
            }
        }

        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void Credit_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dr = Debit.SelectedValue.ToString();
            string cr = Credit.SelectedValue.ToString();

            if (dr == "Client" && cr == "Client")
            {
                msgbox("Client transactions are not allowed");
                return;
            }

            if (Credit.SelectedIndex == 0)
            {
                drpCredit.Items.Clear();
                drpCredit.DataSource = null;
                var ds = from c in db.Accounts_Masters
                         select new

                         {

                             GroupID = c.AccountNumber,

                             GroupName = c.AccountName,

                         };
                drpCredit.DataSource = ds.ToList();
                drpCredit.DataTextField = "GroupName";
                // text field name of table dispalyed in dropdown
                drpCredit.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
                drpCredit.DataBind(); //binding dropdownlist
                                      // allocate.Items.Insert(0, "Select");
                drpCredit.Items.Insert(0, new ListItem("Please Select a group", "0"));
            }
            else if (Credit.SelectedIndex == 1)
            {
                drpCredit.Items.Clear();
                drpCredit.DataSource = null;
                var ds = from c in db.Account_Creations
                         select new

                         {

                             GroupID = c.CDSC_Number,

                             GroupName = c.Surname_CompanyName + "," + c.OtherNames + " CustNo " + c.CDSC_Number,

                         };
                drpCredit.DataSource = ds.ToList();
                drpCredit.DataTextField = "GroupName";
                // text field name of table dispalyed in dropdown
                drpCredit.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
                drpCredit.DataBind(); //binding dropdownlist
                                      // allocate.Items.Insert(0, "Select");
                drpCredit.Items.Insert(0, new ListItem("Please Select a client", "0"));
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if(drpdebit.SelectedItem.Text== "Please Select a group" || drpdebit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }else if (drpCredit.SelectedItem.Text == "Please Select a group" || drpCredit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }
            else if (txtnarr.Text=="")
            {
                msgbox("Narration is required");
                return;
             
            }
            else if(txtref.Text==""){
                msgbox("Reference is required");
                return;
            }


            decimal n;
            bool isNumericT = decimal.TryParse(txtamt.Text, out n);

            if (isNumericT == false)
            {
                msgbox("The amount has to be a figure");
                txtamt.Focus();
                return;
            }
      
       
        

            //submit
            BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
            //Debit
            my.Account = txtDbAcc.Text;
            my.Category = Debit.SelectedValue.ToString();
            my.Credit = 0;
            my.Debit = Convert.ToDecimal(txtamt.Text);
            my.Narration = txtnarr.Text;
            my.Reference_Number = txtref.Text;
            my.TrxnDate = DateTime.Now;
            my.Post = DateTime.Now;
            my.Type ="Trans Posting";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            // Credit//
            my.Account = txtCrAcc.Text;
            my.Category = Credit.SelectedValue.ToString();
            my.Credit = Convert.ToDecimal(txtamt.Text);
            my.Debit = 0;
            my.Narration = txtnarr.Text;
            my.Reference_Number = txtref.Text;
            my.TrxnDate = DateTime.Now;
            my.Post = DateTime.Now;
            my.Type = "Trans Posting";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

            //Update reduce the batch

            if (Credit.SelectedValue =="Client")
            {
            
            var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Payable"));

            int num = 0;
            foreach (var c in acc)
            {
                num = c.AccountNumber;
            }


            //submit
            BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
            //Debit
            my2.Account = num.ToString();
            my2.Category = "Trans Posting";
            my2.Credit = Convert.ToDecimal(txtamt.Text);
                my2.Debit = 0;
            my2.Narration = txtnarr.Text;
            my2.Reference_Number = "Trans Posting";
            my2.TrxnDate = DateTime.Now;
            my2.Post = DateTime.Now;
            my2.Type = "Trans Posting";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

        }else if (Debit.SelectedValue == "Client")
            {
                var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Receivable"));

                int num = 0;
                foreach (var c in acc)
                {
                    num = c.AccountNumber;
                }


                //submit
                BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                //Debit
                my2.Account = num.ToString();
                my2.Category = "Trans Posting";
                my2.Credit = 0;
                my2.Debit = Convert.ToDecimal(txtamt.Text);
                my2.Narration = txtnarr.Text;
                my2.Reference_Number = "Trans Posting";
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Trans Posting";

                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }
           Clear();
        }
        protected void Clear()
        {
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the transaction";
           
            Response.Redirect("~/TransRecords/Index");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }
    }
}