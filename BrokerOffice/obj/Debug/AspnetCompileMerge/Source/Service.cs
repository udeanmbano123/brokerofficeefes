﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web;

namespace BrokerOffice
{
    public class Service
    {
        public Service()
        {

        }
        public int mynum = 0;
        public int mynum2 = 0;
        private SBoardContext db = new SBoardContext();

       public int trades()
        {


            try
            {

                List<Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<Tbl_MatchedDeals>>(SendSMS());


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number

                            let Deal = s.Deal
                            let BuyCompany = s.BuyCompany
                            let SellCompany = s.SellCompany
                            let Buyer = s.Buyer
                            let Seller = s.Seller
                            let Quantity = s.Quantity
                            let Trade = s.Trade
                            let DealPrice = s.DealPrice
                            select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number

                             let Deal = s.Deal
                             let BuyCompany = s.BuyCompany
                             let SellCompany = s.SellCompany
                             let Buyer = s.Buyer
                             let Seller = s.Seller
                             let Quantity = s.Quantity
                             let Trade = s.Trade
                             let DealPrice = s.DealPrice
                             select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };

                //mynum= dbsel.Concat(dbsel2).ToList().Count();

                var xy = dbsel.Concat(dbsel2).ToList();
                int counter = 0;

                foreach (var c in xy)
                {
                    counter++;
                }


                mynum = counter;

                return counter;
            }
            catch (Exception)
            {

                return 0;
            }
           // tradesnum(counter);
        }

        public int tradesnum()
        {
            //this.mynum = x;

            return this.mynum;
        }
        public int settledtradesnum()
        {
            //

            return this.mynum2;
        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string SendSMS()
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                //Sending SMS To User
                WebClient client = new WebClient();
                string URL = "";
                if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount") == true)
                {

                    URL = "http://192.168.3.245/BrokerService/api/Deals/Deals";
                }else
                {
                   URL = "http://197.155.235.78/BrokerService/api/Deals/Deals"; 
                }

                  
             
                SmsStatusMsg = client.DownloadString(URL);
                if (SmsStatusMsg.Contains("<br>"))
                {
                    SmsStatusMsg = SmsStatusMsg.Replace("<br>", ", ");
                }

            }
            catch (WebException e1)
            {
                SmsStatusMsg = e1.Message;
            }
            catch (Exception e2)
            {
                SmsStatusMsg = e2.Message;
            }
            return SmsStatusMsg;

        }

        public string SendSMS2()
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                //Sending SMS To User
                WebClient client = new WebClient();
                string URL = "";

                if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount") == true)
                {
                    URL = "http://192.168.3.245/BrokerService/api/Deals/SettledDeals";
                }
                else
                {

                URL = "http://197.155.235.78/BrokerService/api/Deals/SettledDeals"; 
                }

                SmsStatusMsg = client.DownloadString(URL);
                if (SmsStatusMsg.Contains("<br>"))
                {
                    SmsStatusMsg = SmsStatusMsg.Replace("<br>", ", ");
                }

            }
            catch (WebException e1)
            {
                SmsStatusMsg = e1.Message;
            }
            catch (Exception e2)
            {
                SmsStatusMsg = e2.Message;
            }
            return SmsStatusMsg;

        }
        public int settledtrades()
        {

            try
            {
                List<TblMatchedOrders> dataList = JsonConvert.DeserializeObject<List<TblMatchedOrders>>(SendSMS2());


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number
                            let SID = s.SID
                            let TradePrice = s.TradePrice
                            let Quantity = s.Quantity
                            let OrderNo = s.OrderNo
                            let Buyer = s.Buyer
                            let OrderNo2 = s.OrderNo2
                            let Seller = s.Seller
                            let Acknowledgement = s.Acknowledgement
                            let SettlementDate = s.SettlementDate
                            where s.Acknowledgement == "SETTLED"

                            select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number
                             let SID = s.SID
                             let TradePrice = s.TradePrice
                             let Quantity = s.Quantity
                             let OrderNo = s.OrderNo
                             let Buyer = s.Buyer
                             let OrderNo2 = s.OrderNo2
                             let Seller = s.Seller
                             let Acknowledgement = s.Acknowledgement
                             let SettlementDate = s.SettlementDate
                             where s.Acknowledgement == "SETTLED"

                             select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };

                var xz = dbsel.Concat(dbsel2).ToList();
                int counter = 0;

                foreach (var c in xz)
                {
                    counter++;
                }

                return counter;

            }
            catch (Exception)
            {

                return 0;
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }

}