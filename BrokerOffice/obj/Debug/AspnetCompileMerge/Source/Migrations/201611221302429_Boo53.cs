namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo53 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.para_issuer", "ISIN", c => c.String(maxLength: 12));
            DropColumn("dbo.para_branch", "branch_code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.para_branch", "branch_code", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "ISIN", c => c.String(maxLength: 50));
        }
    }
}
