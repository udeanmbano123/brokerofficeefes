namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo74 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_CreationPending", "ModifiedBy", c => c.String());
            AddColumn("dbo.Account_Creation", "CreatedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "CreatedBy");
            DropColumn("dbo.Account_CreationPending", "ModifiedBy");
        }
    }
}
