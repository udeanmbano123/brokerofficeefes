namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "mobile_money", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "mobile_money");
        }
    }
}
