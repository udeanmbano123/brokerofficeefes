namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo46 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Task", "TaskTitle", c => c.String(nullable: false));
            AlterColumn("dbo.Task", "TaskDescription", c => c.String(nullable: false));
            AlterColumn("dbo.Task", "TaskUser", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Task", "TaskUser", c => c.String());
            AlterColumn("dbo.Task", "TaskDescription", c => c.String());
            AlterColumn("dbo.Task", "TaskTitle", c => c.String());
        }
    }
}
