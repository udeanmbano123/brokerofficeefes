namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New21 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ClientPortfolios");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ClientPortfolios",
                c => new
                    {
                        ClientPortfoliosID = c.Int(nullable: false, identity: true),
                        ClientNumber = c.String(nullable: false),
                        Stock = c.String(nullable: false),
                        Holdings = c.Double(nullable: false),
                        Stockf = c.String(),
                        Clientf = c.String(),
                    })
                .PrimaryKey(t => t.ClientPortfoliosID);
            
        }
    }
}
