namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account_Creation",
                c => new
                    {
                        ID_ = c.Int(nullable: false, identity: true),
                        RecordType = c.Int(),
                        ClientSuffix = c.String(maxLength: 50),
                        JointAcc = c.String(maxLength: 50),
                        Identification = c.String(maxLength: 50),
                        Title = c.String(maxLength: 50),
                        Initials = c.String(maxLength: 50),
                        OtherNames = c.String(maxLength: 50),
                        Surname_CompanyName = c.String(maxLength: 50),
                        Address1 = c.String(maxLength: 50),
                        Address2 = c.String(maxLength: 50),
                        Address3 = c.String(maxLength: 50),
                        Town = c.String(maxLength: 50),
                        PostCode = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        FaxNumber = c.String(maxLength: 50),
                        Emailaddress = c.String(maxLength: 50),
                        DateofBirth_Incorporation = c.DateTime(storeType: "date"),
                        Gender = c.String(maxLength: 50),
                        Nationality = c.String(maxLength: 50),
                        Resident = c.String(maxLength: 50),
                        TaxBracket = c.Int(),
                        TelephoneNumber = c.String(maxLength: 50),
                        Bank = c.String(maxLength: 50),
                        Branch = c.String(maxLength: 50),
                        Accountnumber = c.String(maxLength: 50),
                        No_of_Notes_Applied = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        AmountPaid = c.Decimal(storeType: "money"),
                        PaymentRefNo = c.String(maxLength: 50),
                        ClientType = c.String(maxLength: 50),
                        BrokerReference = c.String(maxLength: 50),
                        DividendDisposalPreference = c.String(maxLength: 50),
                        CDSC_Number = c.String(maxLength: 50),
                        Notification_Sent = c.Int(),
                        Callback_Endpoint = c.String(maxLength: 50),
                        MNO_ = c.String(maxLength: 50),
                        Date_Created = c.DateTime(),
                        File_Sent_DirectDebt = c.Int(),
                        File_Sent_AccountCr = c.Int(),
                        PIN_No = c.String(maxLength: 50),
                        Middlename = c.String(maxLength: 50),
                        RNum = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.ID_);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Account_Creation");
        }
    }
}
