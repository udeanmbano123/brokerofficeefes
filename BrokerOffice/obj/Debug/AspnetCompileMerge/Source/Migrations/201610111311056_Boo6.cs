namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StockSecurities", "StockDetails", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StockSecurities", "StockDetails");
        }
    }
}
