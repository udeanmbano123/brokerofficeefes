namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo103 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.para_issuer",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Company = c.String(),
                        Date_created = c.DateTime(),
                        Issued_shares = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Status = c.Boolean(),
                        registrar = c.String(maxLength: 50),
                        Add_1 = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        Contact_Person = c.String(maxLength: 50),
                        Telephone = c.String(maxLength: 50),
                        Cellphone = c.String(maxLength: 50),
                        ISIN = c.String(maxLength: 50),
                        date_listed = c.DateTime(nullable: false),
                        email = c.String(maxLength: 50),
                        website = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.para_issuer");
        }
    }
}
