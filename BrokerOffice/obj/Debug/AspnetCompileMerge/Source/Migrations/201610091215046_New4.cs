namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "MobileNumber", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "MobileNumber");
        }
    }
}
