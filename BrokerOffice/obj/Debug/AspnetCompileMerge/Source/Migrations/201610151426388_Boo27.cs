namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo27 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Permission", "IsSecondLevel", c => c.Boolean(nullable: false));
            AddColumn("dbo.Permission", "secondlevelpermission", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Permission", "secondlevelpermission");
            DropColumn("dbo.Permission", "IsSecondLevel");
        }
    }
}
