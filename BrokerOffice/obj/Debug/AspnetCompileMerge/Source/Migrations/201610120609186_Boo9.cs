namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts_Master", "AccountNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts_Master", "AccountNumber", c => c.Long(nullable: false));
        }
    }
}
