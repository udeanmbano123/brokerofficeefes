namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo43 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientsDue",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        names = c.String(),
                        accountnumber = c.String(),
                        amounts = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ClientsDue");
        }
    }
}
