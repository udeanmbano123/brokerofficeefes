namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order_Live", "CompanyID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order_Live", "CompanyID");
        }
    }
}
