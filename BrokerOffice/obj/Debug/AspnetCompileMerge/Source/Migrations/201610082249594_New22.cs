namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New22 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientPortfolios",
                c => new
                    {
                        ClientPortfoliosID = c.Int(nullable: false, identity: true),
                        ClientNumber = c.String(nullable: false),
                        Stock = c.String(nullable: false),
                        Holdings = c.Double(nullable: false),
                        Stockf = c.String(),
                        Clientf = c.String(),
                    })
                .PrimaryKey(t => t.ClientPortfoliosID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ClientPortfolios");
        }
    }
}
