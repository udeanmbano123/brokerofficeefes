namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientPortfolios", "Stock", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientPortfolios", "Stock");
        }
    }
}
