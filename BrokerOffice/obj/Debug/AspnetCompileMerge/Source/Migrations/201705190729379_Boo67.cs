namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo67 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TradingPlatform", "State", c => c.String());
            AddColumn("dbo.TradingPlatform", "StateID", c => c.Int(nullable: false));
            AddColumn("dbo.TradingPlatform", "CountryID", c => c.Int(nullable: false));
            AddColumn("dbo.TradingPlatform", "CityID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TradingPlatform", "CityID");
            DropColumn("dbo.TradingPlatform", "CountryID");
            DropColumn("dbo.TradingPlatform", "StateID");
            DropColumn("dbo.TradingPlatform", "State");
        }
    }
}
