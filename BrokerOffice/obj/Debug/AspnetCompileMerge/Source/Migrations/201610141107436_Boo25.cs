namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo25 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account_Creation", "upload1", c => c.Byte());
            AlterColumn("dbo.Account_Creation", "upload2", c => c.Byte());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account_Creation", "upload2", c => c.Binary());
            AlterColumn("dbo.Account_Creation", "upload1", c => c.Binary());
        }
    }
}
