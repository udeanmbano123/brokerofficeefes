namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo102 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Client_Types",
                c => new
                    {
                        Client_TypesID = c.Int(nullable: false, identity: true),
                        ClientType = c.String(),
                    })
                .PrimaryKey(t => t.Client_TypesID);
            
            AddColumn("dbo.StockPrices", "stockisin", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StockPrices", "stockisin");
            DropTable("dbo.Client_Types");
        }
    }
}
