namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.para_Currencies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CurrencyCode = c.String(),
                        CurrencyName = c.String(),
                        CurrencySymbol = c.String(),
                        InternationalSTD = c.String(),
                        CurrencyStatus = c.String(),
                        Country = c.String(),
                        Pref = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.para_Currencies");
        }
    }
}
