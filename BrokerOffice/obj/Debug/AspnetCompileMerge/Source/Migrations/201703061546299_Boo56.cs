namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo56 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.para_bank", "bank", c => c.String());
            AlterColumn("dbo.para_bank", "bank_name", c => c.String());
            AlterColumn("dbo.para_issuer", "ISIN", c => c.String(maxLength: 12));
            DropColumn("dbo.para_bank", "Bank_Code");
            DropColumn("dbo.para_branch", "branch_code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.para_branch", "branch_code", c => c.String(maxLength: 50));
            AddColumn("dbo.para_bank", "Bank_Code", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "ISIN", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_bank", "bank_name", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_bank", "bank", c => c.String(maxLength: 50));
        }
    }
}
