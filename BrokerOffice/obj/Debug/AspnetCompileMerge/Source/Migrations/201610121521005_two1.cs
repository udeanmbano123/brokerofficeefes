namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class two1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.para_issuer");
            AddColumn("dbo.para_issuer", "para_issuerID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.para_issuer", "para_issuerID");
            DropColumn("dbo.para_issuer", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.para_issuer", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.para_issuer");
            DropColumn("dbo.para_issuer", "para_issuerID");
            AddPrimaryKey("dbo.para_issuer", "Id");
        }
    }
}
