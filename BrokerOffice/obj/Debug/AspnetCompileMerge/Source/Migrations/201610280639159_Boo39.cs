namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo39 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order_Live", "TotalShareHolding", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order_Live", "TotalShareHolding", c => c.Int());
        }
    }
}
