namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo64 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.region",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        code = c.String(),
                        country_id = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Depository", "State", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Depository", "State");
            DropTable("dbo.region");
        }
    }
}
