namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo101 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StockPrices",
                c => new
                    {
                        StockPricesID = c.Int(nullable: false, identity: true),
                        StockSecuritiesID = c.Int(nullable: false),
                        LastTradePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Highest_Trade_Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Lowest_Trade_Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        daterecorded = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.StockPricesID)
                .ForeignKey("dbo.StockSecurities", t => t.StockSecuritiesID, cascadeDelete: true)
                .Index(t => t.StockSecuritiesID);
            
            CreateTable(
                "dbo.StockStatus",
                c => new
                    {
                        StockStatusID = c.Int(nullable: false, identity: true),
                        stockstatius = c.String(),
                    })
                .PrimaryKey(t => t.StockStatusID);
            
            CreateTable(
                "dbo.StockTypes",
                c => new
                    {
                        StockTypesID = c.Int(nullable: false, identity: true),
                        stockname = c.String(),
                    })
                .PrimaryKey(t => t.StockTypesID);
            
            AddColumn("dbo.StockSecurities", "IssuedPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.StockSecurities", "CurrentPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.StockSecurities", "Numberofshares");
            DropColumn("dbo.StockSecurities", "StockDetails");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StockSecurities", "StockDetails", c => c.String());
            AddColumn("dbo.StockSecurities", "Numberofshares", c => c.Double(nullable: false));
            DropForeignKey("dbo.StockPrices", "StockSecuritiesID", "dbo.StockSecurities");
            DropIndex("dbo.StockPrices", new[] { "StockSecuritiesID" });
            DropColumn("dbo.StockSecurities", "CurrentPrice");
            DropColumn("dbo.StockSecurities", "IssuedPrice");
            DropTable("dbo.StockTypes");
            DropTable("dbo.StockStatus");
            DropTable("dbo.StockPrices");
        }
    }
}
