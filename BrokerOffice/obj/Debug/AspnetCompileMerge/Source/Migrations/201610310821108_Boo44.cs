namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo44 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Content", "User_UserId", "dbo.User");
            DropIndex("dbo.Content", new[] { "User_UserId" });
            DropTable("dbo.Content");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Content",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Image = c.Binary(),
                        AccountID = c.Int(nullable: false),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.Content", "User_UserId");
            AddForeignKey("dbo.Content", "User_UserId", "dbo.User", "UserId");
        }
    }
}
