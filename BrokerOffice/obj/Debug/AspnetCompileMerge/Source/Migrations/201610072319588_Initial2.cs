namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Permission", "ControllerName", c => c.String());
            AlterColumn("dbo.Permission", "ViewName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Permission", "ViewName", c => c.String(nullable: false));
            AlterColumn("dbo.Permission", "ControllerName", c => c.String(nullable: false));
        }
    }
}
