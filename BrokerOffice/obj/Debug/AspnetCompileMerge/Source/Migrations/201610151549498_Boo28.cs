namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo28 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Permission", "IsSecondLevel");
            DropColumn("dbo.Permission", "secondlevelpermission");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Permission", "secondlevelpermission", c => c.String());
            AddColumn("dbo.Permission", "IsSecondLevel", c => c.Boolean(nullable: false));
        }
    }
}
