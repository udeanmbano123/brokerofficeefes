namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StockPrices", "daterecorded", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StockPrices", "daterecorded", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
