namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo76 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Accounts_Documents");
            AddColumn("dbo.Accounts_Documents", "Accounts_DocumentsID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Accounts_Documents", "doc_generated", c => c.String());
            AddPrimaryKey("dbo.Accounts_Documents", "Accounts_DocumentsID");
            DropColumn("dbo.Accounts_Documents", "id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts_Documents", "id", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Accounts_Documents");
            AlterColumn("dbo.Accounts_Documents", "doc_generated", c => c.String(nullable: false, maxLength: 200));
            DropColumn("dbo.Accounts_Documents", "Accounts_DocumentsID");
            AddPrimaryKey("dbo.Accounts_Documents", new[] { "id", "doc_generated" });
        }
    }
}
