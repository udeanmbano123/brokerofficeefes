namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo24 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "upload1", c => c.Binary());
            AddColumn("dbo.Account_Creation", "upload2", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "upload2");
            DropColumn("dbo.Account_Creation", "upload1");
        }
    }
}
