namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo104 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.para_issuer");
            AlterColumn("dbo.para_issuer", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.para_issuer", "Company", c => c.String(nullable: false));
            AlterColumn("dbo.para_issuer", "Add_1", c => c.String(nullable: false));
            AlterColumn("dbo.para_issuer", "City", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.para_issuer", "Country", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.para_issuer", "Contact_Person", c => c.String(nullable: false, maxLength: 50));
            AddPrimaryKey("dbo.para_issuer", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.para_issuer");
            AlterColumn("dbo.para_issuer", "Contact_Person", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "Country", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "City", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "Add_1", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_issuer", "Company", c => c.String());
            AlterColumn("dbo.para_issuer", "Id", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddPrimaryKey("dbo.para_issuer", "Id");
        }
    }
}
