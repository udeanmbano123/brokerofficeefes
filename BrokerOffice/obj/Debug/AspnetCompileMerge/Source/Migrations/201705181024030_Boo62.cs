namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo62 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "BrokerName", c => c.String());
            AlterColumn("dbo.User", "BrokerCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "BrokerCode", c => c.String(nullable: false));
            AlterColumn("dbo.User", "BrokerName", c => c.String(nullable: false));
        }
    }
}
