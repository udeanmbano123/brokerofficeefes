namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionFormcs",
                c => new
                    {
                        ActionFormID = c.Int(nullable: false, identity: true),
                        ClientPortfoliosID = c.Int(nullable: false),
                        ClientNumber = c.String(),
                        ClientNames = c.String(),
                        clientholdings = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                        hour = c.String(),
                        minutes = c.String(),
                        setting = c.String(),
                        contactnotes = c.String(),
                        adviceclient = c.String(),
                        contacttype = c.String(),
                    })
                .PrimaryKey(t => t.ActionFormID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ActionFormcs");
        }
    }
}
