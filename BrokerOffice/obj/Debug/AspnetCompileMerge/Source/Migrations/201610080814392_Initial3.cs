namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.glyphicon",
                c => new
                    {
                        glyphiconID = c.Int(nullable: false, identity: true),
                        glyphiconname = c.String(),
                    })
                .PrimaryKey(t => t.glyphiconID);
            
            AddColumn("dbo.Modules", "glyphicon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "glyphicon");
            DropTable("dbo.glyphicon");
        }
    }
}
