namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo22 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Depository",
                c => new
                    {
                        DepositoryID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DepositoryID);
            
            AddColumn("dbo.TradingPlatform", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TradingPlatform", "Name");
            DropTable("dbo.Depository");
        }
    }
}
