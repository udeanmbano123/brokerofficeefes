namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo40 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order_Live", "AvailableShares", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order_Live", "AvailableShares", c => c.Int());
        }
    }
}
