namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "Bankcode", c => c.String());
            AddColumn("dbo.Account_Creation", "BranchCode", c => c.String());
            AddColumn("dbo.Account_Creation", "idtype", c => c.String());
            AddColumn("dbo.Account_Creation", "accountcategory", c => c.String());
            AddColumn("dbo.Account_Creation", "currency", c => c.String());
            AddColumn("dbo.Account_Creation", "divpayee", c => c.String());
            AddColumn("dbo.Account_Creation", "divbank", c => c.String());
            AddColumn("dbo.Account_Creation", "divbankcode", c => c.String());
            AddColumn("dbo.Account_Creation", "divbranch", c => c.String());
            AddColumn("dbo.Account_Creation", "divbranchcode", c => c.String());
            AddColumn("dbo.Account_Creation", "divacc", c => c.String());
            AddColumn("dbo.Account_Creation", "divaccounttype", c => c.String());
            AlterColumn("dbo.Account_Creation", "country", c => c.String());
            DropColumn("dbo.Account_Creation", "No_of_Notes_Applied");
            DropColumn("dbo.Account_Creation", "AmountPaid");
            DropColumn("dbo.Account_Creation", "PaymentRefNo");
            DropColumn("dbo.Account_Creation", "BrokerReference");
            DropColumn("dbo.Account_Creation", "DividendDisposalPreference");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Account_Creation", "DividendDisposalPreference", c => c.String(maxLength: 50));
            AddColumn("dbo.Account_Creation", "BrokerReference", c => c.String(maxLength: 50));
            AddColumn("dbo.Account_Creation", "PaymentRefNo", c => c.String(maxLength: 50));
            AddColumn("dbo.Account_Creation", "AmountPaid", c => c.Decimal(storeType: "money"));
            AddColumn("dbo.Account_Creation", "No_of_Notes_Applied", c => c.Decimal(precision: 18, scale: 2, storeType: "numeric"));
            AlterColumn("dbo.Account_Creation", "country", c => c.String(maxLength: 50));
            DropColumn("dbo.Account_Creation", "divaccounttype");
            DropColumn("dbo.Account_Creation", "divacc");
            DropColumn("dbo.Account_Creation", "divbranchcode");
            DropColumn("dbo.Account_Creation", "divbranch");
            DropColumn("dbo.Account_Creation", "divbankcode");
            DropColumn("dbo.Account_Creation", "divbank");
            DropColumn("dbo.Account_Creation", "divpayee");
            DropColumn("dbo.Account_Creation", "currency");
            DropColumn("dbo.Account_Creation", "accountcategory");
            DropColumn("dbo.Account_Creation", "idtype");
            DropColumn("dbo.Account_Creation", "BranchCode");
            DropColumn("dbo.Account_Creation", "Bankcode");
        }
    }
}
