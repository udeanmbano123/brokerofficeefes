// <auto-generated />
namespace BrokerOffice.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Boo32 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Boo32));
        
        string IMigrationMetadata.Id
        {
            get { return "201610261336422_Boo32"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
