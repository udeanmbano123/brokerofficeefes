namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TradingCharges", "ChargeName", c => c.String());
            AddColumn("dbo.TradingCharges", "chartaccount", c => c.String());
            AddColumn("dbo.TradingCharges", "chargeaccountcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TradingCharges", "chargeaccountcode");
            DropColumn("dbo.TradingCharges", "chartaccount");
            DropColumn("dbo.TradingCharges", "ChargeName");
        }
    }
}
