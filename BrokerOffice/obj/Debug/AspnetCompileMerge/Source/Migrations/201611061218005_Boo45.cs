namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo45 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessagesID = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Description = c.String(),
                        User = c.String(),
                        UserID = c.String(),
                        fromUser = c.String(),
                        fromUserID = c.String(),
                    })
                .PrimaryKey(t => t.MessagesID);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        TaskID = c.Int(nullable: false, identity: true),
                        TaskTitle = c.String(),
                        TaskDescription = c.String(),
                        TaskUser = c.String(),
                        TaskUserID = c.Int(nullable: false),
                        percentagedone = c.Decimal(nullable: false, precision: 18, scale: 2),
                        status = c.String(),
                        fromUser = c.String(),
                        fromUserID = c.String(),
                    })
                .PrimaryKey(t => t.TaskID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Task");
            DropTable("dbo.Messages");
        }
    }
}
