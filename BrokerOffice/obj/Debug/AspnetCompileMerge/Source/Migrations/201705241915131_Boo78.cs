namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo78 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order_Live", "PostedBy", c => c.String());
            AddColumn("dbo.Trans", "PostedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trans", "PostedBy");
            DropColumn("dbo.Order_Live", "PostedBy");
        }
    }
}
