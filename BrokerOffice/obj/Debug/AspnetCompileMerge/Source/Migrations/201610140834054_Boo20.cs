namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo20 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StockPrices", "TP", c => c.String());
            AddColumn("dbo.StockPrices", "TPBoard", c => c.String());
            AddColumn("dbo.TradingBoard", "TradingPlatformID", c => c.Int(nullable: false));
            AddColumn("dbo.TradingPlatform", "TradingPlatform_TradingPlatformID", c => c.Int());
            CreateIndex("dbo.TradingBoard", "TradingPlatformID");
            CreateIndex("dbo.TradingPlatform", "TradingPlatform_TradingPlatformID");
            AddForeignKey("dbo.TradingPlatform", "TradingPlatform_TradingPlatformID", "dbo.TradingPlatform", "TradingPlatformID");
            AddForeignKey("dbo.TradingBoard", "TradingPlatformID", "dbo.TradingPlatform", "TradingPlatformID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TradingBoard", "TradingPlatformID", "dbo.TradingPlatform");
            DropForeignKey("dbo.TradingPlatform", "TradingPlatform_TradingPlatformID", "dbo.TradingPlatform");
            DropIndex("dbo.TradingPlatform", new[] { "TradingPlatform_TradingPlatformID" });
            DropIndex("dbo.TradingBoard", new[] { "TradingPlatformID" });
            DropColumn("dbo.TradingPlatform", "TradingPlatform_TradingPlatformID");
            DropColumn("dbo.TradingBoard", "TradingPlatformID");
            DropColumn("dbo.StockPrices", "TPBoard");
            DropColumn("dbo.StockPrices", "TP");
        }
    }
}
