namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo10 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order_Live", "CompanyISIN", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order_Live", "CompanyISIN");
        }
    }
}
