namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo55 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.para_bank", "bank", c => c.String());
            AlterColumn("dbo.para_bank", "bank_name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.para_bank", "bank_name", c => c.String(maxLength: 50));
            AlterColumn("dbo.para_bank", "bank", c => c.String(maxLength: 50));
        }
    }
}
