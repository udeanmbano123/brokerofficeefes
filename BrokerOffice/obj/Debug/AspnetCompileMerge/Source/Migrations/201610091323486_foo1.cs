namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foo1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.para_bank",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        bank = c.String(maxLength: 50),
                        bank_name = c.String(maxLength: 50),
                        Bank_Code = c.String(maxLength: 50),
                        eft = c.Boolean(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.para_branch",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        bank = c.String(maxLength: 50),
                        branch = c.String(maxLength: 50),
                        branch_name = c.String(maxLength: 50),
                        branch_code = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.para_country",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        country = c.String(maxLength: 50),
                        fnam = c.String(maxLength: 50),
                        Nationality = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.para_country");
            DropTable("dbo.para_branch");
            DropTable("dbo.para_bank");
        }
    }
}
