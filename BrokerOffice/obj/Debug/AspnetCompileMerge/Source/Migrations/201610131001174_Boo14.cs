namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo14 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IDTypes",
                c => new
                    {
                        IDTypesID = c.Int(nullable: false, identity: true),
                        idname = c.String(),
                    })
                .PrimaryKey(t => t.IDTypesID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.IDTypes");
        }
    }
}
