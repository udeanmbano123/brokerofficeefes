namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationCapture", "ClientNames", c => c.String());
            AddColumn("dbo.ApplicationCapture", "tempstatus", c => c.String());
            DropColumn("dbo.ApplicationCapture", "ClientName");
            DropColumn("dbo.ApplicationCapture", "ClientSurname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ApplicationCapture", "ClientSurname", c => c.String());
            AddColumn("dbo.ApplicationCapture", "ClientName", c => c.String());
            DropColumn("dbo.ApplicationCapture", "tempstatus");
            DropColumn("dbo.ApplicationCapture", "ClientNames");
        }
    }
}
