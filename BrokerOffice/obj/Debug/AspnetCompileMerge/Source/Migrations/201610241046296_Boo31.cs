namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo31 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StockPrices", "TP", c => c.String(nullable: false));
            AlterColumn("dbo.StockPrices", "TPBoard", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StockPrices", "TPBoard", c => c.String());
            AlterColumn("dbo.StockPrices", "TP", c => c.String());
        }
    }
}
