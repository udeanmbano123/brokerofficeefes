// <auto-generated />
namespace BrokerOffice.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Boo56 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Boo56));
        
        string IMigrationMetadata.Id
        {
            get { return "201703061546299_Boo56"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
