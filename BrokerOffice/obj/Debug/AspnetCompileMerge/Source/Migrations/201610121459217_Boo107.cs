namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo107 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.para_issuer", "date_listed", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.para_issuer", "date_listed", c => c.DateTime(nullable: false));
        }
    }
}
