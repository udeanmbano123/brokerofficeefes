namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TradingCharges", "chargeaccountcode", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TradingCharges", "chargeaccountcode", c => c.String());
        }
    }
}
