namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo16 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account_Creation", "country", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account_Creation", "country", c => c.String(maxLength: 50));
        }
    }
}
