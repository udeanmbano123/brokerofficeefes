namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo13 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ActionFormcs", "Date", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ActionFormcs", "Date", c => c.DateTime(nullable: false));
        }
    }
}
