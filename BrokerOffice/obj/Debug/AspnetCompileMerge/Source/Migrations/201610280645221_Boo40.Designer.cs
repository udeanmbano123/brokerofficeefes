// <auto-generated />
namespace BrokerOffice.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Boo40 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Boo40));
        
        string IMigrationMetadata.Id
        {
            get { return "201610280645221_Boo40"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
