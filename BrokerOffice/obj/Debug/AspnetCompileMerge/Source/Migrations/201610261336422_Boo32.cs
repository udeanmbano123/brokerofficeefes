namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo32 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TransactionCharge",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        transactionCode = c.String(maxLength: 50),
                        ChargeCode = c.String(maxLength: 50),
                        BuyCharges = c.Decimal(storeType: "money"),
                        SellCharges = c.Decimal(storeType: "money"),
                        Date = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TransactionCharge");
        }
    }
}
