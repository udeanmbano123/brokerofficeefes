namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo29 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order_Live", "mobile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order_Live", "mobile");
        }
    }
}
