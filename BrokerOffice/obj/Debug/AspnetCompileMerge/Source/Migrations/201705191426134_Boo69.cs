namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo69 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account_CreationPending",
                c => new
                    {
                        ID_ = c.Int(nullable: false, identity: true),
                        RecordType = c.Int(),
                        ClientSuffix = c.String(maxLength: 255),
                        JointAcc = c.String(maxLength: 255),
                        Identification = c.String(maxLength: 255),
                        Title = c.String(maxLength: 255),
                        Initials = c.String(maxLength: 255),
                        OtherNames = c.String(maxLength: 255),
                        Surname_CompanyName = c.String(maxLength: 255),
                        Address1 = c.String(maxLength: 255),
                        Address2 = c.String(maxLength: 255),
                        Address3 = c.String(maxLength: 255),
                        Town = c.String(maxLength: 255),
                        PostCode = c.String(maxLength: 255),
                        FaxNumber = c.String(maxLength: 255),
                        Emailaddress = c.String(maxLength: 255),
                        DateofBirth_Incorporation = c.DateTime(storeType: "date"),
                        Gender = c.String(maxLength: 255),
                        Nationality = c.String(maxLength: 255),
                        Resident = c.String(maxLength: 255),
                        TaxBracket = c.Int(),
                        TelephoneNumber = c.String(maxLength: 255),
                        Bank = c.String(maxLength: 255),
                        Branch = c.String(maxLength: 255),
                        ClientType = c.String(maxLength: 255),
                        CDSC_Number = c.String(maxLength: 255),
                        Notification_Sent = c.Int(),
                        Callback_Endpoint = c.String(maxLength: 255),
                        MNO_ = c.String(maxLength: 255),
                        Date_Created = c.DateTime(),
                        File_Sent_DirectDebt = c.Int(),
                        File_Sent_AccountCr = c.Int(),
                        PIN_No = c.String(maxLength: 255),
                        Middlename = c.String(maxLength: 255),
                        RNum = c.String(maxLength: 255),
                        MobileNumber = c.String(maxLength: 100),
                        Bankname = c.String(),
                        Bankcode = c.String(),
                        BranchName = c.String(),
                        BranchCode = c.String(),
                        Accountnumber = c.String(maxLength: 255),
                        idtype = c.String(),
                        accountcategory = c.String(),
                        country = c.String(),
                        currency = c.String(),
                        divpayee = c.String(),
                        divbank = c.String(),
                        divbankcode = c.String(),
                        divbranch = c.String(),
                        divbranchcode = c.String(),
                        divacc = c.String(),
                        divaccounttype = c.String(),
                        idtype2 = c.String(),
                        dividnumber = c.String(),
                        mobile_money = c.String(),
                        depname = c.String(),
                        depcode = c.String(),
                        upload1 = c.Byte(),
                        upload2 = c.Byte(),
                        manBank = c.String(),
                        manBankCode = c.String(),
                        manBranch = c.String(),
                        manBranchCode = c.String(),
                        manAccount = c.String(),
                        manNames = c.String(),
                        manAddress = c.String(),
                        mobilewalletnumber = c.String(),
                        Broker = c.String(),
                        update_type = c.String(),
                    })
                .PrimaryKey(t => t.ID_);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Account_CreationPending");
        }
    }
}
