namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agent",
                c => new
                    {
                        AgentID = c.Int(nullable: false, identity: true),
                        AgentCode = c.String(nullable: false),
                        AgentName = c.String(nullable: false),
                        PracticingID = c.String(),
                        Email = c.String(nullable: false, maxLength: 35),
                        address = c.String(nullable: false),
                        mobilenumber = c.String(),
                        telephone = c.String(),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.AgentID);
            
            CreateTable(
                "dbo.Brokerage",
                c => new
                    {
                        BrokerageID = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false),
                        OperatingAddress = c.String(nullable: false),
                        PrincipalAgent = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BrokerageID);
            
            CreateTable(
                "dbo.ClientPortfolios",
                c => new
                    {
                        ClientPortfoliosID = c.Int(nullable: false, identity: true),
                        ClientNumber = c.String(nullable: false),
                        Holdings = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ClientPortfoliosID);
            
            CreateTable(
                "dbo.StockSecurities",
                c => new
                    {
                        StockSecuritiesID = c.Int(nullable: false, identity: true),
                        StockISIN = c.String(nullable: false),
                        Issuer = c.String(nullable: false),
                        StockType = c.String(nullable: false),
                        description = c.String(),
                        stockQty = c.Int(nullable: false),
                        StockPrice = c.Double(nullable: false),
                        Stockstatus = c.String(),
                        Numberofshares = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.StockSecuritiesID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StockSecurities");
            DropTable("dbo.ClientPortfolios");
            DropTable("dbo.Brokerage");
            DropTable("dbo.Agent");
        }
    }
}
