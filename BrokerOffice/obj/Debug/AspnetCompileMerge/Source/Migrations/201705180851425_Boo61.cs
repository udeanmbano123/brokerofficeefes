namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo61 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Agent", "AgentCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Agent", "AgentCode", c => c.String(nullable: false));
        }
    }
}
