namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo60 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "BrokerName", c => c.String(nullable: false));
            AddColumn("dbo.User", "BrokerCode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "BrokerCode");
            DropColumn("dbo.User", "BrokerName");
        }
    }
}
