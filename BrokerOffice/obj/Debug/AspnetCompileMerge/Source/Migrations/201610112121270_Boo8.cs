namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts_Master",
                c => new
                    {
                        Accounts_MasterID = c.Int(nullable: false, identity: true),
                        AccountName = c.String(),
                        AccountNumber = c.Long(nullable: false),
                        GL_GroupID = c.Int(nullable: false),
                        GroupName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Accounts_MasterID);
            
            CreateTable(
                "dbo.GL_Group",
                c => new
                    {
                        GL_GroupID = c.Int(nullable: false, identity: true),
                        GroupName = c.String(nullable: false),
                        StartingAccount = c.Int(nullable: false),
                        EndingAccount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GL_GroupID);
            
            CreateTable(
                "dbo.Trans",
                c => new
                    {
                        TransID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Category = c.String(),
                        TrxnDate = c.DateTime(nullable: false),
                        Account = c.String(nullable: false),
                        Reference_Number = c.String(nullable: false),
                        Narration = c.String(),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Post = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TransID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Trans");
            DropTable("dbo.GL_Group");
            DropTable("dbo.Accounts_Master");
        }
    }
}
