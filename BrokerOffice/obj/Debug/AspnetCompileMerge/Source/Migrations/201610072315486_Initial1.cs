namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        PermissionID = c.Int(nullable: false, identity: true),
                        ControllerName = c.String(nullable: false),
                        ViewName = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        assignedRole = c.String(nullable: false),
                        Module = c.String(nullable: false),
                        IsDashboard = c.Boolean(nullable: false),
                        IsWebForm = c.Boolean(nullable: false),
                        webFormUrl = c.String(),
                        IsActice = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PermissionID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Permission");
        }
    }
}
