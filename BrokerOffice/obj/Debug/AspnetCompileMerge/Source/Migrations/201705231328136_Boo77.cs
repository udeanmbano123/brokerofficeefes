namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo77 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts_Documents", "Name", c => c.String(maxLength: 255));
            AlterColumn("dbo.Accounts_Documents", "ContentType", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts_Documents", "ContentType", c => c.String(maxLength: 50));
            AlterColumn("dbo.Accounts_Documents", "Name", c => c.String(maxLength: 50));
        }
    }
}
