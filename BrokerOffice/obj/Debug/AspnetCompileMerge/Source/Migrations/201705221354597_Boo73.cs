namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo73 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts_Documents",
                c => new
                    {
                        id = c.Int(nullable: false),
                        doc_generated = c.String(nullable: false, maxLength: 200),
                        Name = c.String(maxLength: 50),
                        ContentType = c.String(maxLength: 50),
                        Data = c.Binary(),
                    })
                .PrimaryKey(t => new { t.id, t.doc_generated });
            
            CreateTable(
                "dbo.div_types",
                c => new
                    {
                        div_typesID = c.Int(nullable: false, identity: true),
                        DivType = c.String(),
                        DiviTypeName = c.String(),
                    })
                .PrimaryKey(t => t.div_typesID);
            
            DropColumn("dbo.Account_CreationPending", "upload1");
            DropColumn("dbo.Account_CreationPending", "upload2");
            DropColumn("dbo.Account_Creation", "upload1");
            DropColumn("dbo.Account_Creation", "upload2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Account_Creation", "upload2", c => c.Byte());
            AddColumn("dbo.Account_Creation", "upload1", c => c.Byte());
            AddColumn("dbo.Account_CreationPending", "upload2", c => c.Byte());
            AddColumn("dbo.Account_CreationPending", "upload1", c => c.Byte());
            DropTable("dbo.div_types");
            DropTable("dbo.Accounts_Documents");
        }
    }
}
