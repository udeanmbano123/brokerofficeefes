namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "Bankname", c => c.String());
            AddColumn("dbo.Account_Creation", "BranchName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "BranchName");
            DropColumn("dbo.Account_Creation", "Bankname");
        }
    }
}
