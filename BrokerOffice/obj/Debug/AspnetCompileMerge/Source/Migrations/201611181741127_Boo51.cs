namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo51 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account_Creation", "Address1", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Address2", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Address3", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account_Creation", "Address3", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Address2", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Address1", c => c.String(maxLength: 50));
        }
    }
}
