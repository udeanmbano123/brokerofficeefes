namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo30 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deposits",
                c => new
                    {
                        DepositsID = c.Int(nullable: false, identity: true),
                        CDSNUmber = c.String(nullable: false),
                        TranDate = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DepositsID);
            
            CreateTable(
                "dbo.Withdrawals",
                c => new
                    {
                        WithdrawalsID = c.Int(nullable: false, identity: true),
                        CDSNUmber = c.String(nullable: false),
                        TranDate = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.WithdrawalsID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Withdrawals");
            DropTable("dbo.Deposits");
        }
    }
}
