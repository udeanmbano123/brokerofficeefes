namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo23 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "depname", c => c.String());
            AddColumn("dbo.Account_Creation", "depcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "depcode");
            DropColumn("dbo.Account_Creation", "depname");
        }
    }
}
