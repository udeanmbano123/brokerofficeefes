namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo58 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "manBank", c => c.String());
            AddColumn("dbo.Account_Creation", "manBankCode", c => c.String());
            AddColumn("dbo.Account_Creation", "manBranch", c => c.String());
            AddColumn("dbo.Account_Creation", "manBranchCode", c => c.String());
            AddColumn("dbo.Account_Creation", "manAccount", c => c.String());
            AddColumn("dbo.Account_Creation", "manNames", c => c.String());
            AddColumn("dbo.Account_Creation", "manAddress", c => c.String());
            AddColumn("dbo.Account_Creation", "mobilewalletnumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "mobilewalletnumber");
            DropColumn("dbo.Account_Creation", "manAddress");
            DropColumn("dbo.Account_Creation", "manNames");
            DropColumn("dbo.Account_Creation", "manAccount");
            DropColumn("dbo.Account_Creation", "manBranchCode");
            DropColumn("dbo.Account_Creation", "manBranch");
            DropColumn("dbo.Account_Creation", "manBankCode");
            DropColumn("dbo.Account_Creation", "manBank");
        }
    }
}
