namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Batch", "BatchDetails", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Batch", "BatchDetails", c => c.Double(nullable: false));
        }
    }
}
