namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo52 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account_Creation", "ClientSuffix", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "JointAcc", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Identification", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Title", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Initials", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "OtherNames", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Surname_CompanyName", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Town", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "PostCode", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "FaxNumber", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Emailaddress", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Gender", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Nationality", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Resident", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "TelephoneNumber", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Bank", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Branch", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "ClientType", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "CDSC_Number", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Callback_Endpoint", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "MNO_", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "PIN_No", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Middlename", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "RNum", c => c.String(maxLength: 255));
            AlterColumn("dbo.Account_Creation", "Accountnumber", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Account_Creation", "Accountnumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "RNum", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Middlename", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "PIN_No", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "MNO_", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Callback_Endpoint", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "CDSC_Number", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "ClientType", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Branch", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Bank", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "TelephoneNumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Resident", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Nationality", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Gender", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Emailaddress", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "FaxNumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "PostCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Town", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Surname_CompanyName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "OtherNames", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Initials", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Title", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "Identification", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "JointAcc", c => c.String(maxLength: 50));
            AlterColumn("dbo.Account_Creation", "ClientSuffix", c => c.String(maxLength: 50));
        }
    }
}
