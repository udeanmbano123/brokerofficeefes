namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjustedHoldings",
                c => new
                    {
                        AdjustedHoldingsID = c.Int(nullable: false, identity: true),
                        Buyer = c.String(),
                        seller = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Dates = c.DateTime(nullable: false),
                        TradeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdjustedHoldingsID);
            
            AlterColumn("dbo.ClientPortfolios", "Holdings", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientPortfolios", "Holdings", c => c.Double(nullable: false));
            DropTable("dbo.AdjustedHoldings");
        }
    }
}
