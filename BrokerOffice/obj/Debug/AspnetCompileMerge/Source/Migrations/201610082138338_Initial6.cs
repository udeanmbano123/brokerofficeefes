namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Brokerage", "brokername", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Brokerage", "brokername");
        }
    }
}
