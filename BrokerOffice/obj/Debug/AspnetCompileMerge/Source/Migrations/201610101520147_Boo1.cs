namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationCapture",
                c => new
                    {
                        ApplicationCaptureID = c.Int(nullable: false, identity: true),
                        BatchRef = c.String(),
                        BatchID = c.Int(nullable: false),
                        BatchTotal = c.Double(nullable: false),
                        ClientNumber = c.String(),
                        ClientName = c.String(),
                        ClientSurname = c.String(),
                        ClientID = c.String(),
                        ClientUnits = c.Double(nullable: false),
                        value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ApplicationCaptureID);
            
            CreateTable(
                "dbo.Batch",
                c => new
                    {
                        BatchID = c.Int(nullable: false, identity: true),
                        Batchref = c.String(nullable: false),
                        BatchUnits = c.Double(nullable: false),
                        BatchDetails = c.Double(nullable: false),
                        tempstatus = c.String(),
                        product = c.String(),
                        IPOID = c.Int(nullable: false),
                        value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.BatchID);
            
            CreateTable(
                "dbo.IPO",
                c => new
                    {
                        IPOID = c.Int(nullable: false, identity: true),
                        issuer = c.String(),
                        targetedvalue = c.Double(nullable: false),
                        OpenningDate = c.DateTime(nullable: false),
                        ClosingDate = c.DateTime(nullable: false),
                        ListingDate = c.DateTime(nullable: false),
                        openningprice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IPOID);
            
            CreateTable(
                "dbo.Order_Live",
                c => new
                    {
                        OrderNo = c.Long(nullable: false, identity: true),
                        OrderType = c.String(maxLength: 50),
                        Company = c.String(maxLength: 50),
                        SecurityType = c.String(maxLength: 10),
                        CDS_AC_No = c.String(maxLength: 50),
                        Broker_Code = c.String(maxLength: 12),
                        Client_Type = c.String(maxLength: 50),
                        Tax = c.Decimal(precision: 18, scale: 2),
                        Shareholder = c.String(maxLength: 50),
                        ClientName = c.String(maxLength: 50),
                        TotalShareHolding = c.Int(),
                        OrderStatus = c.String(maxLength: 50),
                        Create_date = c.DateTime(),
                        Deal_Begin_Date = c.DateTime(),
                        Expiry_Date = c.DateTime(),
                        Quantity = c.Int(),
                        BasePrice = c.Decimal(storeType: "money"),
                        AvailableShares = c.Int(),
                        OrderPref = c.String(maxLength: 50),
                        OrderAttribute = c.String(maxLength: 255),
                        Marketboard = c.String(maxLength: 255),
                        TimeInForce = c.String(maxLength: 255),
                        OrderQualifier = c.String(maxLength: 255),
                        BrokerRef = c.String(maxLength: 255),
                        ContraBrokerId = c.String(maxLength: 255),
                        MaxPrice = c.Decimal(storeType: "money"),
                        MiniPrice = c.Decimal(storeType: "money"),
                        Flag_oldorder = c.Boolean(),
                        OrderNumber = c.String(maxLength: 50),
                        Currency = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.OrderNo);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Order_Live");
            DropTable("dbo.IPO");
            DropTable("dbo.Batch");
            DropTable("dbo.ApplicationCapture");
        }
    }
}
