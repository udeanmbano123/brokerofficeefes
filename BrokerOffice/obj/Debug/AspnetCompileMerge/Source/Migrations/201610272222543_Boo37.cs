namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo37 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tax",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        isset = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tax");
        }
    }
}
