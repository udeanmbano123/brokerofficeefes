namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo66 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Depository", "CityID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Depository", "CityID");
        }
    }
}
