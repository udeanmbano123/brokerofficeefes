namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo34 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionCharges", "Transcode", c => c.String(maxLength: 50));
            DropColumn("dbo.TransactionCharges", "transactionCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TransactionCharges", "transactionCode", c => c.String(maxLength: 50));
            DropColumn("dbo.TransactionCharges", "Transcode");
        }
    }
}
