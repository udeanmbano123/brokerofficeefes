namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo54 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.para_bank", "Bank_Code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.para_bank", "Bank_Code", c => c.String(maxLength: 50));
        }
    }
}
