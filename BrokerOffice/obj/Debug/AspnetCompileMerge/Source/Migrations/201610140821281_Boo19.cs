namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo19 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TradingBoard",
                c => new
                    {
                        TradingBoardID = c.Int(nullable: false, identity: true),
                        BoardCode = c.String(nullable: false),
                        BoardName = c.String(nullable: false),
                        BoardDescription = c.String(),
                    })
                .PrimaryKey(t => t.TradingBoardID);
            
            CreateTable(
                "dbo.TradingPlatform",
                c => new
                    {
                        TradingPlatformID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        ContactPerson = c.String(),
                    })
                .PrimaryKey(t => t.TradingPlatformID);
            
            AddColumn("dbo.Order_Live", "TP", c => c.String());
            AddColumn("dbo.Order_Live", "TPBoard", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order_Live", "TPBoard");
            DropColumn("dbo.Order_Live", "TP");
            DropTable("dbo.TradingPlatform");
            DropTable("dbo.TradingBoard");
        }
    }
}
