namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo47 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "DateAdded", c => c.DateTime(nullable: false));
            AddColumn("dbo.Task", "TaskDue", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Task", "TaskDue");
            DropColumn("dbo.Messages", "DateAdded");
        }
    }
}
