namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class two2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountMaintenance",
                c => new
                    {
                        AccountMaintenanceID = c.Int(nullable: false, identity: true),
                        account_type = c.String(),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        frequency = c.String(),
                    })
                .PrimaryKey(t => t.AccountMaintenanceID);
            
            CreateTable(
                "dbo.Frequencies",
                c => new
                    {
                        FrequenciesID = c.Int(nullable: false, identity: true),
                        FrequencyName = c.String(),
                    })
                .PrimaryKey(t => t.FrequenciesID);
            
            CreateTable(
                "dbo.TradingCharges",
                c => new
                    {
                        TradingChargesID = c.Int(nullable: false, identity: true),
                        stockisin = c.String(),
                        StockSecuritieesID = c.Int(nullable: false),
                        chargecode = c.String(),
                        chargedescription = c.String(),
                        tradeside = c.String(),
                        ChargedAs = c.String(),
                        chargevalue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        chargefrequency = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.TradingChargesID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TradingCharges");
            DropTable("dbo.Frequencies");
            DropTable("dbo.AccountMaintenance");
        }
    }
}
