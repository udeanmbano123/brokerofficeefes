namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo18 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "idtype2", c => c.String());
            AddColumn("dbo.Account_Creation", "dividnumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "dividnumber");
            DropColumn("dbo.Account_Creation", "idtype2");
        }
    }
}
