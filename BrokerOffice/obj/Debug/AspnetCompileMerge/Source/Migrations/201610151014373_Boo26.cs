namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo26 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "ControllerName", c => c.String());
            AddColumn("dbo.Modules", "ViewName", c => c.String());
            AddColumn("dbo.Modules", "Name", c => c.String(nullable: false));
            AddColumn("dbo.Modules", "IsWebForm", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "webFormUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "webFormUrl");
            DropColumn("dbo.Modules", "IsWebForm");
            DropColumn("dbo.Modules", "Name");
            DropColumn("dbo.Modules", "ViewName");
            DropColumn("dbo.Modules", "ControllerName");
        }
    }
}
