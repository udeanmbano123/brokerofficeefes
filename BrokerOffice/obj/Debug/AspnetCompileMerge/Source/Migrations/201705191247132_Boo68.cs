namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo68 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account_Creation", "Broker", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account_Creation", "Broker");
        }
    }
}
