namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo36 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.notification",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        userid = c.Int(nullable: false),
                        notification_type = c.String(),
                        notification_data = c.String(),
                        timestamp = c.DateTime(),
                        last_read = c.String(),
                        fromsysytem = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.notification");
        }
    }
}
