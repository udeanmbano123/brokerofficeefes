namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo50 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "MenuRank", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "MenuRank");
        }
    }
}
