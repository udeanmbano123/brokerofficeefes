namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo65 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Depository", "StateID", c => c.Int(nullable: false));
            AddColumn("dbo.Depository", "CountryID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Depository", "CountryID");
            DropColumn("dbo.Depository", "StateID");
        }
    }
}
