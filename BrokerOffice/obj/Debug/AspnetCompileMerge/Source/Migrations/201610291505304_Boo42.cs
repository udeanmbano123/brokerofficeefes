namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo42 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyEarners",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        companyname = c.String(),
                        brokername = c.String(),
                        consideration = c.Decimal(nullable: false, precision: 18, scale: 2),
                        commission = c.Decimal(nullable: false, precision: 18, scale: 2),
                        tradeType = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CompanyEarners");
        }
    }
}
