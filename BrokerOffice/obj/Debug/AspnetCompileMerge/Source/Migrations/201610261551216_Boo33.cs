namespace BrokerOffice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Boo33 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TransactionCharge", newName: "TransactionCharges");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.TransactionCharges", newName: "TransactionCharge");
        }
    }
}
