﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Depository
    {
         [Key]
        public int DepositoryID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State{ get; set; }
        [DataType(DataType.MultilineText)]
        [Required]
        public string Address { get; set; }

        public int StateID { get; set; }
        public int CountryID { get; set; }

        public int CityID { get; set; }
    }
}