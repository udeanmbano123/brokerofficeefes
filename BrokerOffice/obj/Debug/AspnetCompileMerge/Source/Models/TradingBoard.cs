﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class TradingBoard
    {
        [Key]
        public int TradingBoardID { get; set; }
        [Required]
        public string BoardCode { get; set; }
        [Required]
        public string BoardName { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Description")]
        public string BoardDescription { get; set; }
        [DisplayName("TradingPlatform")]
        public int TradingPlatformID { get; set; }
        [ForeignKey("TradingPlatformID")]
        public virtual TradingPlatform TradingPlatforms { get; set; }
    }
}