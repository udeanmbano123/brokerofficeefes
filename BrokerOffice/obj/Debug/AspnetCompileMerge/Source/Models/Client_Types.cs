﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Client_Types
    {
        public int Client_TypesID { get;set;}
        public string ClientType { get; set; }
    }
}