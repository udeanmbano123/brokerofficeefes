﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class DivTypes
    {
        public int id { get; set; }
        public string DivType { get; set; }
        public string DiviTypeName { get; set; }

    }
}