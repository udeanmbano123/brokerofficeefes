﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class StockPrices
    {
        [Key]
        public int StockPricesID { get; set; }
        public int StockSecuritiesID { get; set; }
        [DisplayName("LastTradePrice")]
        public decimal LastTradePrice { get; set; }
        [DisplayName("HighestTradePrice")]
        public decimal Highest_Trade_Price { get; set; }
        [DisplayName("LowestTradePrice")]
        public decimal Lowest_Trade_Price {get;set;}
        [DisplayName("DateRecorded")]
        public DateTime? daterecorded { get; set; }
        public string stockisin { get; set; }
      [Required]
        [DefaultValue("T")]
        public string  TP { get; set; }
        [Required]
        [DefaultValue("T")]
        public string  TPBoard { get; set; }
        [ForeignKey("StockSecuritiesID")]
        public virtual StockSecurities StockSecurities { get; set; }

    }
}