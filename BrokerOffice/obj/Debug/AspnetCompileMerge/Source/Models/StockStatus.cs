﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class StockStatus
    {
        [Key]
        public int StockStatusID { get; set; }
        public string stockstatius { get; set; }
    }
}