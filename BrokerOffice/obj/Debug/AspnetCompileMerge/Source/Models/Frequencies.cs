﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Frequencies
    {
        public int FrequenciesID { get; set; }
        public string FrequencyName { get; set; }
    }
}