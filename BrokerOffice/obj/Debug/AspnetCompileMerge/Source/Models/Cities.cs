﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Cities
    {

        [Key]
       public int id { get; set; }
      public int region_id { get; set; }
     public int country_id { get; set; }
    public int latitude { get; set; }
   public double longitude { get; set; }
   public string name { get; set; }
    }
}