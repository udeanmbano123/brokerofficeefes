﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    [Table("Permission")]
    public class Permission
    {
        public int PermissionID { get; set; }

        [DisplayName("ControllerName")]
        public string ControllerName { get; set; }

        [DisplayName("ActionName")]
        public string ViewName { get; set; }
        [Required]
        [DisplayName("Alias")]
        public string Name { get; set; }
        [Required]
        [DisplayName("AssignedRole")]
        public string assignedRole { get; set; }
        [Required]
        [DisplayName("ModuleName")]
        public string Module { get; set; }
        [DisplayName("ForDashboard")]
        public Boolean IsDashboard { get; set; }
        [DisplayName("WebForm")]
        public Boolean IsWebForm { get; set; }
        [DisplayName("WebFormUrl")]
        public string webFormUrl { get; set; }
        [DisplayName("IsActive")]
        public Boolean IsActice { get; set; }

    }
}
