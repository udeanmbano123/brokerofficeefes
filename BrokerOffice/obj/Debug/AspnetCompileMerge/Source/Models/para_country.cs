using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{

    [TrackChanges]
    public partial class para_country
    {
      [Key]
        public int id { get; set; }

        [StringLength(50)]
        [DisplayName("Country")]
        public string country { get; set; }

        [StringLength(50)]
        [DisplayName("FNam")]
        public string fnam { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }
    }
}
