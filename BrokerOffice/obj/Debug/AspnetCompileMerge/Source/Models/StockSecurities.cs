﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class StockSecurities
    {
        [Key]
        public int StockSecuritiesID { get; set; }
        [DisplayName("StockISIN")]
        [Required]
        public string StockISIN { get; set; }
        [DisplayName("Issuer")]
        [Required]
        public string Issuer { get; set; }
        [DisplayName("StockType")]
        [Required]
        public string StockType { get; set;}
        [DisplayName("Stock Description")]
        public string description { get; set; }
        [DisplayName("Total Issued Quantity")]
        public int stockQty { get; set; }
        [DisplayName("Stock Price")]
        public double StockPrice { get; set; }
        [DisplayName("Stock Status")]
        public string Stockstatus { get; set; }
        [DisplayName("Issued Price")]
        public decimal IssuedPrice { get; set; }
        [DisplayName("Current Price")]
        public decimal CurrentPrice { get; set; }
        public virtual ICollection<StockPrices> StockPrices { get; set; }
    }
}