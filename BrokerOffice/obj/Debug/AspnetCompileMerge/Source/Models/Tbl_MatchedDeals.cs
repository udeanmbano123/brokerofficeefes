﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Tbl_MatchedDeals
    {


        public string Deal { get; set; }

        [StringLength(50)]
        public string BuyCompany { get; set; }

        [StringLength(50)]
        public string SellCompany { get; set; }

        [StringLength(100)]
        public string Buyer { get; set; }

        [StringLength(100)]
        public string Seller { get; set; }

        public string Quantity { get; set; }

       
        public string Trade { get; set; }

        public string DealPrice { get; set; }


    }
}