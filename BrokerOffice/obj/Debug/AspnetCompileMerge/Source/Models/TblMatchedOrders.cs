﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class TblMatchedOrders
    {
        public string SID { get; set; }
        public string TradePrice { get; set; }
        public string Quantity { get; set; }
        public string OrderNo { get; set; }
        public string Buyer { get; set; }
        public string OrderNo2 { get; set; }
        public string Seller { get; set; }
        public string Acknowledgement { get; set; }
        public string  SettlementDate { get; set; }
    }
}