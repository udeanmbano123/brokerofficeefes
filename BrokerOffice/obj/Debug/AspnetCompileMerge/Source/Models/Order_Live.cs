using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{

    [TrackChanges]
    public class Order_Live
    {
        [Key]
        public long OrderNo { get; set; }

        [StringLength(50)]
        [DisplayName("Order Type")]
        public string OrderType { get; set; }

        [StringLength(50)]
        [DisplayName("Company")]
        public string Company { get; set; }

        [StringLength(10)]
        [DisplayName("Security type")]
        public string SecurityType { get; set; }

        [StringLength(50)]
        [DisplayName("Customer Number")]
        public string CDS_AC_No { get; set; }

        [StringLength(12)]
        public string Broker_Code { get; set; }

        [StringLength(50)]
        public string Client_Type { get; set; }
        [DefaultValue(0)]
        public decimal? Tax { get; set; }

        [StringLength(50)]
        public string Shareholder { get; set; }

        [StringLength(50)]
        public string ClientName { get; set; }

        public decimal? TotalShareHolding { get; set; }

        [StringLength(50)]
        public string OrderStatus { get; set; }

        public DateTime? Create_date { get; set; }

        public DateTime? Deal_Begin_Date { get; set; }

        public DateTime? Expiry_Date { get; set; }
        [DefaultValue(0)]
        public int? Quantity { get; set; }

        [Column(TypeName = "money")]
        [DefaultValue(0)]
        public decimal? BasePrice { get; set; }

        public decimal? AvailableShares { get; set; }

        [StringLength(50)]
        public string OrderPref { get; set; }

        [StringLength(255)]
        public string OrderAttribute { get; set; }

        [StringLength(255)]
        public string Marketboard { get; set; }

        [StringLength(255)]
        public string TimeInForce { get; set; }

        [StringLength(255)]
        public string OrderQualifier { get; set; }

        [StringLength(255)]
        public string BrokerRef { get; set; }

        [StringLength(255)]
        public string ContraBrokerId { get; set; }

        [Column(TypeName = "money")]
        [DefaultValue(0)]
        public decimal? MaxPrice { get; set; }

        [Column(TypeName = "money")]
        [DefaultValue(0)]
        public decimal? MiniPrice { get; set; }

        public bool? Flag_oldorder { get; set; }

        [StringLength(50)]
        public string OrderNumber { get; set; }

        [StringLength(30)]
        public string Currency { get; set; }

        public int CompanyID { get; set; }
        public string CompanyISIN { get; set; }

        public string TP { get; set; }
        public string TPBoard { get; set; }

        public string mobile { get; set; }

        public string PostedBy { get; set; }
    }
}
