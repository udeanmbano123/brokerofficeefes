﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class TradingCharges
    {
        [Key]
        public int TradingChargesID { get; set; }
        [DisplayName("Stock ISIN")]
        public string stockisin { get; set; }

        public int StockSecuritieesID { get; set; }
        [DisplayName("Code")]
        public string chargecode { get; set; }
        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        public string chargedescription { get; set; }
        [DisplayName("Tradeside")]
        public string tradeside { get; set; }
        [DisplayName("ChargedAs")]
        public string ChargedAs { get; set; }
        [DisplayName("Value")]
        public decimal chargevalue { get; set; }
        [DisplayName("Frequency")]
        public decimal chargefrequency { get; set; }
        [DisplayName("Trading Name")]
        public string ChargeName { get; set; }
        [DisplayName("Account")]
        public string chartaccount { get; set; }
        [DisplayName("Chart Account Code")]
        public int?  chargeaccountcode { get; set; }
    }
}