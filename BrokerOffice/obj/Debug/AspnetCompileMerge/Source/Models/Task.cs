﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Task
    {
        [Key]
        public int TaskID { get; set; }
        [DisplayName("Title")]
        [Required]
        public string TaskTitle { get; set; }
        [DisplayName("Description")]
        [Required]
        [DataType(DataType.MultilineText)]
        public string TaskDescription { get; set; }
        [DisplayName("AssignUser")]
        [Required]
        public string TaskUser { get; set; }
        public int TaskUserID { get; set; }
        [DisplayName("%Done")]
        [Range(0.00, 100.00)]
        public decimal percentagedone { get; set; }
        [DisplayName("Status")]
        public string status { get; set; }
        public string fromUser { get; set; }
        public string fromUserID { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        [Required]
        public DateTime TaskDue { get; set; }
    }
}