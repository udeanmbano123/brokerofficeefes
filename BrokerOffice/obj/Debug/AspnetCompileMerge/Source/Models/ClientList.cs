﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class ClientList
    {
        [Required]
        [DisplayName("Select Date")]
        public DateTime Asat { get; set; }
    }
}