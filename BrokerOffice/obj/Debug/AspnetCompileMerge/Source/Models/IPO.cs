﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class IPO
    {
        public int IPOID { get; set; }
        [DisplayName("Issuer")]
        public string issuer { get; set; }
        [DisplayName("Targeted Value")]
        public double targetedvalue { get; set; }
        [DisplayName("Opening Date")]
        public DateTime OpenningDate { get; set; }
        [DisplayName("Closing Date")]
        public DateTime ClosingDate { get; set; }
        [DisplayName("Listing Date")]
        public DateTime ListingDate { get; set; }
        [DisplayName("Opening Price")]
        public double openningprice { get; set; }
    }
}