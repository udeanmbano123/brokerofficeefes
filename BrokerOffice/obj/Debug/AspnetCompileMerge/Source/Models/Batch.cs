﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Batch
    {
        [Key]
        public int BatchID { get; set; }

        [Required]
        [DisplayName("Batch Ref")]
        public string Batchref { get; set; }
        [Required]
        [DisplayName("Batch Units")]
        public double BatchUnits { get; set; }
        [Required]
        [DisplayName("Batch Details")]
        public string BatchDetails { get; set; }

        public string tempstatus { get; set; }
        
        [DisplayName("Issuer IPO")]
        public string product { get; set; }

        public int IPOID { get; set; }

        public double value { get; set; }

    }
}