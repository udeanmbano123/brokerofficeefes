﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Trans
    {
        [Key]
        public int TransID { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public DateTime TrxnDate { get; set; }
        [Required]
        public string Account { get; set; }
        [DisplayName("Narration")]
        [Required]
        public string Reference_Number { get; set; }
        [DisplayName("Narration")]
        public string Narration { get; set; }
        [Required]
        public Decimal Debit { get; set; }
        [Required]
        public Decimal Credit {get;set;}
         public DateTime Post { get; set; }

        public string PostedBy { get; set; }
    }
}