﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class AccountStatement
    {
        
        public string Account { get; set; }
        public DateTime FrmDate { get; set; }
        public DateTime ToDate { get; set; } 
    }
}