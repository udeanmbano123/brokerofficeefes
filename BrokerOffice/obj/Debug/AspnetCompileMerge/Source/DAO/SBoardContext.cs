﻿
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


using TrackerEnabledDbContext.Identity;
using BrokerOffice.Models;
using BrokerOffice.DealNotes;

namespace BrokerOffice.DAO
{
    public class  SBoardContext : TrackerIdentityContext<ApplicationUser>
    {
        public SBoardContext()
            : base("SBoardConnection")
        {}

        public DbSet<div_types> DivTypes { get; set; }
        public DbSet<Accounts_Documents> Accounts_Documentss { get; set; }
        public DbSet<Cities> Cities { get; set; }
        public DbSet<Countries> Countriess { get; set; }
        public DbSet<Modules> Moduless { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<glyphicon> glyhicons { get; set; }
       
        public DbSet<Brokerage> Brokerages { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<StockSecurities> StockSecuritiess { get; set; }
        public DbSet<ClientPortfolios> ClientPortfolioss { get; set; }
        public DbSet<Account_Creation> Account_Creations { get; set; }

        public DbSet<Account_CreationPending> Account_CreationPendingss { get; set; }

        public DbSet<para_bank> Banks { get; set; }
        public DbSet<para_branch> Branches { get; set; }
        public DbSet<para_country> Countries { get; set; }
        public DbSet<IPO> IPOs { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<Order_Live> Order_Lives { get; set; }
        public DbSet<ApplicationCapture> ApplicationCaptures { get; set; }
        public DbSet<para_Currencies> para_Currencies { get; set; }
        public DbSet<ActionFormcs> ActionForm { get; set; }

        public DbSet<GL_Group> GL_Groups { get; set; }
        public DbSet<Accounts_Master> Accounts_Masters { get; set; }
        public DbSet<Trans> Transs { get; set; }
        public DbSet<StockPrices> StockPrices { get; set; }

        public DbSet<StockStatus> StockStatus { get; set; }

        public DbSet<StockTypes> StockTypes { get; set; }
        public DbSet<Client_Types> Client_Types { get; set; }

        public DbSet<para_issuer> para_issuers { get; set; }
        public DbSet<Frequencies> Frequenciess { get; set; }
        public DbSet<TradingCharges> TradingChargess { get; set; }
        public DbSet<AccountMaintenance> AccountMaintenances { get; set; }
        public DbSet<IDTypes> IDTypess { get; set; }
        public DbSet<TradingPlatform> TradingPlatforms { get; set; }
        public DbSet<TradingBoard> TradingBoards { get; set; }
        public DbSet<Depository> Depositorys { get; set; }

        public DbSet<Deposits> Deposit { get; set; }
        public DbSet<Withdrawals> Withdrawal { get; set; }
        public DbSet<AdjustedHoldings> AdjustedHoldings { get; set; }
        public DbSet<TransactionCharges> TransactionCharges { get; set; }

        public DbSet<notification> notifications { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<CompanyEarners> CompanyEarnerss { get; set; }
        public DbSet<ClientsDue> ClientsDues { get; set; }
        public DbSet<Messages> Messagess { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public DbSet<region> regions { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }




        //  public System.Data.Entity.DbSet<CDSLogin.Models.ApplicationUser> ApplicationUsers { get; set; }

    }



}