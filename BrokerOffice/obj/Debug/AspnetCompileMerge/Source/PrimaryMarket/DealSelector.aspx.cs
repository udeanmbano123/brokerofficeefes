﻿using BrokerOffice.DAO;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class DealSelector : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
            }
        }
        protected async void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //txtCustomerNumber.Text = GridView1.SelectedRow.Cells[2].Text;
            //txtCustomerNames.Text = GridView1.SelectedRow.Cells[3].Text + " " + GridView1.SelectedRow.Cells[4].Text;


            //txtOrder.Text = GridView1.SelectedRow.Cells[1].Text;
            //truncate and load transaction charges
            HttpContext.Current.Session["SID"] = GridView1.SelectedRow.Cells[1].Text;
            HttpContext.Current.Session["max"] = GridView1.SelectedRow.Cells[2].Text;
            HttpContext.Current.Session["qty"] = GridView1.SelectedRow.Cells[3].Text;
            HttpContext.Current.Session["Dte"]= GridView1.SelectedRow.Cells[8].Text;
            string Buyer = GridView1.SelectedRow.Cells[5].Text;
            string Seller = GridView1.SelectedRow.Cells[7].Text;
            string sid = GridView1.SelectedRow.Cells[1].Text;
            if (Buyer.Contains("ATS"))
            {
                HttpContext.Current.Session["Ord"] = GridView1.SelectedRow.Cells[4].Text;
                txtOrder.Text= GridView1.SelectedRow.Cells[4].Text;
                txtAcc.Text = GridView1.SelectedRow.Cells[5].Text;
                HttpContext.Current.Session["Acc"] = GridView1.SelectedRow.Cells[5].Text;
            }
            else if (Seller.Contains("ATS"))
            {
                HttpContext.Current.Session["Ord"] = GridView1.SelectedRow.Cells[6].Text;
                txtOrder.Text = GridView1.SelectedRow.Cells[6].Text;
                txtAcc.Text = GridView1.SelectedRow.Cells[7].Text;
                HttpContext.Current.Session["Acc"]= GridView1.SelectedRow.Cells[7].Text;
            }

          
         
            //  db.SubmitChanges();

            //fetch from tranasction charges
            string responJsonText2 = "";
            var user = db.Account_Creations.ToList();
            string updatedjson = "";
            int d = 0;
            int x = 0;
            string carryjson = "";
            JArray v2 = null;

            Tbl_MatchedDeals c1 = null;
            var jsonObjects = new List<string>();
            var jsonObjectsArray = "";
            //192.168.3.245
            Uri requestUri = null;
          
                requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/SpinTrans?p=" + sid + "");//replace your Url 
                                                                                                             //  dynamic dynamicJson = new ExpandoObject();
           

            //string json = "";
            // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            var objClint = new System.Net.Http.HttpClient();
            HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

            responJsonText2 = await respon.Content.ReadAsStringAsync();






            List<TransactionCharges> dataList = JsonConvert.DeserializeObject<List<TransactionCharges>>(responJsonText2);
            string num = HttpContext.Current.Session["SID"].ToString();
           int check = db.TransactionCharges.ToList().Where(a => a.Transcode == num).Count();

            if(check <= 0)
            {
            foreach(var c  in dataList)
            {
                TransactionCharges my = new TransactionCharges();
                my.Id = c.Id;
                my.Transcode =c.Transcode;
                my.ChargeCode = c.ChargeCode;
                my.BuyCharges = c.BuyCharges;
                my.SellCharges = c.SellCharges;
                my.Date = c.Date;
                db.TransactionCharges.Add(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }

            }
           
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
           
            
                loadCustomers();

            // loadCustomers();
            //SID,Price.Quantity,
          
        }
        protected async void loadCustomers()
        {

          
            string responJsonText = "";
            var user = db.Account_Creations.ToList();
            string updatedjson = "";
            int d = 0;
            int x = 0;
            string carryjson = "";
            JArray v2 = null;

            Tbl_MatchedDeals c1 = null;
            var jsonObjects = new List<string>();
            var jsonObjectsArray = "";
            //192.168.3.245
            Uri requestUri = null;
        
                requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/SettledDeals");//replace your Url 
        


            //string json = "";
            // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            var objClint = new System.Net.Http.HttpClient();
            HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

            responJsonText = await respon.Content.ReadAsStringAsync();






            List<TblMatchedOrders> dataList = JsonConvert.DeserializeObject<List<TblMatchedOrders>>(responJsonText);


            //   GridView1.DataSource = DerializeDataTable(responJsonText);
            var dbsel = from s in dataList
                        join v in db.Account_Creations on s.Buyer equals v.CDSC_Number
                        let SID = s.SID
                        let TradePrice = s.TradePrice
                        let Quantity = s.Quantity
                        let OrderNo = s.OrderNo
                        let Buyer = s.Buyer
                        let OrderNo2 = s.OrderNo2
                        let Seller = s.Seller
                        let Acknowledgement = s.Acknowledgement
                        let SettlementDate = s.SettlementDate
                        where (v.Surname_CompanyName + ' ' + v.OtherNames + ' ' + v.CDSC_Number).Contains(txtSearch.Text)

                        select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
            var dbsel2 = from s in dataList
                         join v in db.Account_Creations on s.Seller equals v.CDSC_Number
                         let SID = s.SID
                         let TradePrice = s.TradePrice
                         let Quantity = s.Quantity
                         let OrderNo = s.OrderNo
                         let Buyer = s.Buyer
                         let OrderNo2 = s.OrderNo2
                         let Seller = s.Seller
                         let Acknowledgement = s.Acknowledgement
                         let SettlementDate = s.SettlementDate
                         where (v.Surname_CompanyName+' '+v.OtherNames+' '+v.CDSC_Number).Contains(txtSearch.Text)

                         select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };

         

            var allresults = dbsel.Concat(dbsel2).ToList();
            GridView1.DataSource = allresults;
            GridView1.DataBind();

            
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadCustomers();
        }

        protected void txtView_Click(object sender, EventArgs e)
        {
            if (txtAcc.Text == "")
            {
                msgbox("Account Number must be selected");
                return;
            }
            else if (txtOrder.Text == "")
            {
                msgbox("Order number selected");
                return;
            }
            else
            {
                //  HttpContext.Current.Session["SID"]
                //  HttpContext.Current.Session["max"]
                // HttpContext.Current.Session["qty"]
                //HttpContext.Current.Session["Ord"]
                //HttpContext.Current.Session["Acc"]
                Response.Redirect("~/Reporting/DealReport.aspx");
            }
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}