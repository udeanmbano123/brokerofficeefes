﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Json.NET;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;

namespace BrokerOffice.PrimaryMarket
{
    public partial class SettledTrades : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
                // TabName.Value = Request.Form[TabName.UniqueID];
                trades();
            }
            else
            {
                // loadCustomers();
                trades();


                if (id == " " || id == null)
                {

                }
                else
                {
                   // getDetails(id);
                }
            }


        }


        public async void trades()
        {

            try
            {

                string n = "";
                string responJsonText = "";
                var user = db.Account_Creations.ToList();
                string updatedjson = "";
                int d = 0;
                int x = 0;
                string carryjson = "";
                JArray v2 = null;

                Tbl_MatchedDeals c1 = null;
                var jsonObjects = new List<string>();
                var jsonObjectsArray = "";
                //192.168.3.245
                Uri requestUri =null;
                if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount") == true)
                {
                    requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/SettledDeals");//replace your Url 
                                                                                                          //  dynamic dynamicJson = new ExpandoObject();
                }
              else
                {

                 requestUri = new Uri("http://197.155.235.78/BrokerService/api/Deals/SettledDeals"); 
                }

                //string json = "";
                // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
                var objClint = new System.Net.Http.HttpClient();
                HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

                responJsonText = await respon.Content.ReadAsStringAsync();






                List<TblMatchedOrders> dataList = JsonConvert.DeserializeObject<List<TblMatchedOrders>>(responJsonText);


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number
                            let SID = s.SID
                            let TradePrice = s.TradePrice
                            let Quantity = s.Quantity
                            let OrderNo = s.OrderNo
                            let Buyer = s.Buyer
                            let OrderNo2 = s.OrderNo2
                            let Seller = s.Seller
                            let Acknowledgement = s.Acknowledgement
                            let SettlementDate = s.SettlementDate
                            where s.Acknowledgement == "SETTLED"

                            select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number
                             let SID = s.SID
                             let TradePrice = s.TradePrice
                             let Quantity = s.Quantity
                             let OrderNo = s.OrderNo
                             let Buyer = s.Buyer
                             let OrderNo2 = s.OrderNo2
                             let Seller = s.Seller
                             let Acknowledgement = s.Acknowledgement
                             let SettlementDate = s.SettlementDate
                             where s.Acknowledgement == "SETTLED"

                             select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };

                //for updating holdimgs
                var dbsel11 = from s in dataList
                              join v in db.ClientPortfolioss on s.Buyer equals v.ClientNumber
                              let SID = s.SID
                              let TradePrice = s.TradePrice
                              let Quantity = s.Quantity
                              let OrderNo = s.OrderNo
                              let Buyer = s.Buyer
                              let OrderNo2 = s.OrderNo2
                              let Seller = s.Seller
                              let Acknowledgement = s.Acknowledgement
                              let SettlementDate = s.SettlementDate
                              where s.Acknowledgement == "SETTLED"

                              select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                var dbsel22 = from s in dataList
                              join v in db.ClientPortfolioss on s.Seller equals v.ClientNumber
                              let SID = s.SID
                              let TradePrice = s.TradePrice
                              let Quantity = s.Quantity
                              let OrderNo = s.OrderNo
                              let Buyer = s.Buyer
                              let OrderNo2 = s.OrderNo2
                              let Seller = s.Seller
                              let Acknowledgement = s.Acknowledgement
                              let SettlementDate = s.SettlementDate
                              where s.Acknowledgement == "SETTLED"

                              select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, SettlementDate };
                //update all buy

                foreach (var c in dbsel11)
                {
                    updateBuyTrade(c.Buyer, c.Seller, c.TradePrice, Convert.ToInt32(c.SID));
                }


                //update all sell

                foreach (var c in dbsel22)
                {
                    updateSellTrade(c.Buyer, c.Seller, c.TradePrice, Convert.ToInt32(c.SID));
                }


                var allresults = dbsel.Concat(dbsel2).ToList();
                GridView1.DataSource = allresults;
                GridView1.DataBind();
            }
            catch (Exception)
            {

                msgbox("No internet connection");
            }

        }
       
        public DataTable DerializeDataTable(string data)
        {
            string json = data; //"data" should contain your JSON 
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        public string checkNull(string s)
        {
            if (s == null)
            {
                return "0";
            }

            return s;
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
//            if (txtSearch.Text != "")
//            {
//var action = from s in db.ClientPortfolioss
//                         join v in db.Account_Creations on s.ClientNumber equals v.CDSC_Number
//                         let CDSNUMBER = v.CDSC_Number
//                         let Surname = v.Surname_CompanyName
//                         let Firstname = v.OtherNames
//                         let Holdings = s.Holdings
//                         let stockISIN = s.Stock
//                         let Issuer = s.Stockf
//             where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
//             select new { v.ID_, CDSNUMBER, Surname, Firstname, Holdings, stockISIN, Issuer };

//            GridView1.DataSource = action.ToList();
//            GridView1.DataBind();
//            }
//            else
//            {
//                trades();
//            }
            
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
     


        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            trades();
        }
        public void updateBuyTrade(string b, string s, string p, int d)
        {
            //     public string Buyer { get; set; }
            //public string seller { get; set; }
            //public decimal Price { get; set; }
            //public DateTime Dates { get; set; }
            //public int TradeID { get; set; }
            bool check = false;
            int sid = Convert.ToInt32(d);
            int checkid = 0;

            var ss = db.AdjustedHoldings.ToList().Where(a => a.TradeID == sid);

            foreach (var ps in ss)
            {
                checkid = ps.TradeID;
            }


            if (sid == checkid)
            {

            }else
            {

           
            AdjustedHoldings my = new AdjustedHoldings();

            my.Buyer = b;
            my.seller = s;
            my.Price = Convert.ToDecimal(p);
            my.Dates = DateTime.Now;
            my.TradeID = d;

            db.AdjustedHoldings.Add(my);
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                //update all holdings

                //using(var db2=new SBoardContext())
                //    {
                //        db2.ClientPortfolioss.Where(a => a.ClientNumber == b)
                //            .ToList()
                //            .ForEach(a => a.Holdings =(a.Holdings + my.Price));
                //       db2.SaveChanges(WebSecurity.CurrentUserName.ToString());
                //    }

                //updating all holdings
                var pz = db.ClientPortfolioss.ToList();

                foreach(var up in pz)
                {
                    ClientPortfolios uu = new ClientPortfolios();

                    uu.ClientNumber = up.ClientNumber;
                    uu.Stock = up.Stock;
                    uu.Holdings = up.Holdings + my.Price;
                    uu.Stockf = up.Stockf;
                    uu.Clientf = up.Clientf;
                    db.ClientPortfolioss.Add(uu);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }
            }
        }

        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void updateSellTrade(string b, string s, string p, int d)
        {
            bool check = false;
            int sid = Convert.ToInt32(d);
            int checkid = 0;

            var ss = db.AdjustedHoldings.ToList().Where(a => a.TradeID == sid);

            foreach (var ps in ss)
            {
                checkid = ps.TradeID;
            }


            if (sid == checkid)
            {

            }
            else
            {


                AdjustedHoldings my = new AdjustedHoldings();

                my.Buyer = b;
                my.seller = s;
                my.Price = Convert.ToDecimal(p);
                my.Dates = DateTime.Now;
                my.TradeID = d;

                db.AdjustedHoldings.Add(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                //update all holdings

                //using (var db2 = new SBoardContext())
                //{
                //    db2.ClientPortfolioss.Where(a => a.ClientNumber == b)
                //        .ToList()
                //        .ForEach(a => a.Holdings = (a.Holdings - my.Price));
                //    db2.SaveChanges(WebSecurity.CurrentUserName.ToString());
                //}

                //updating all holdings
                var pz = db.ClientPortfolioss.ToList();

                foreach (var up in pz)
                {
                    ClientPortfolios uu = new ClientPortfolios();

                    uu.ClientNumber = up.ClientNumber;
                    uu.Stock = up.Stock;
                    uu.Holdings = (up.Holdings - my.Price)*-1;
                    uu.Stockf = up.Stockf;
                    uu.Clientf = up.Clientf;
                    db.ClientPortfolioss.Add(uu);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }

            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
