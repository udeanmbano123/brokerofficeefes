﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using Json.NET;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;

namespace BrokerOffice.PrimaryMarket
{
    public partial class Trades : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
                // TabName.Value = Request.Form[TabName.UniqueID];
                trades();
            }
            else
            {
                // loadCustomers();
                trades();


                if (id == " " || id == null)
                {

                }
                else
                {
                   // getDetails(id);
                }
            }


        }

        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async void trades()
        {


            try
            {
                string n = "";
                string responJsonText = "";
                var user = db.Account_Creations.ToList();
                string updatedjson = "";
                int d = 0;
                int x = 0;
                string carryjson = "";
                JArray v2 = null;

                Tbl_MatchedDeals c1 = null;
                var jsonObjects = new List<string>();
                var jsonObjectsArray = "";
                Uri requestUri = null;
                if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount")==true)
                {
                    requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/Deals");//replace your Url 
                                                                                                   //  dynamic dynamicJson = new ExpandoObject();
                }
           

                //string json = "";
                // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
                var objClint = new System.Net.Http.HttpClient();
                HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

                responJsonText = await respon.Content.ReadAsStringAsync();

                List<Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<Tbl_MatchedDeals>>(responJsonText);


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number

                            let Deal = s.Deal
                            let BuyCompany = s.BuyCompany
                            let SellCompany = s.SellCompany
                            let Buyer = s.Buyer
                            let Seller = s.Seller
                            let Quantity = s.Quantity
                            let Trade = s.Trade
                            let DealPrice = s.DealPrice
                            select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number

                             let Deal = s.Deal
                             let BuyCompany = s.BuyCompany
                             let SellCompany = s.SellCompany
                             let Buyer = s.Buyer
                             let Seller = s.Seller
                             let Quantity = s.Quantity
                             let Trade = s.Trade
                             let DealPrice = s.DealPrice
                             select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };

                var allresults = dbsel.Concat(dbsel2).ToList();
                GridView1.DataSource = allresults;
                GridView1.DataBind();
            }
            catch (Exception)
            {

                msgbox("No internet connection");
            }

        }
       
        public DataTable DerializeDataTable(string data)
        {
            string json = data; //"data" should contain your JSON 
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        public string checkNull(string s)
        {
            if (s == null)
            {
                return "0";
            }

            return s;
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
//            if (txtSearch.Text != "")
//            {
//var action = from s in db.ClientPortfolioss
//                         join v in db.Account_Creations on s.ClientNumber equals v.CDSC_Number
//                         let CDSNUMBER = v.CDSC_Number
//                         let Surname = v.Surname_CompanyName
//                         let Firstname = v.OtherNames
//                         let Holdings = s.Holdings
//                         let stockISIN = s.Stock
//                         let Issuer = s.Stockf
//             where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
//             select new { v.ID_, CDSNUMBER, Surname, Firstname, Holdings, stockISIN, Issuer };

//            GridView1.DataSource = action.ToList();
//            GridView1.DataBind();
//            }
//            else
//            {
//                trades();
//            }
            
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
     


        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            trades();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
