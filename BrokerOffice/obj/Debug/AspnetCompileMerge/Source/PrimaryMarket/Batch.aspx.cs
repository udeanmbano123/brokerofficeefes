﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.PrimaryMarket
{
    public partial class Batch : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

            }
            else
            {
                loadProducts();
                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
            }
        }
        protected void drpPr_SelectedIndexChanged(object sender, EventArgs e)
        {
            //fetch opeening price from IPO
            var ipo = db.IPOs.ToList();
            double open = 0;

            foreach (var c in ipo)
            {
                open = c.openningprice;
            }
            double n;
            bool isNumeric = double.TryParse(txtTotal.Text, out n);

            if (isNumeric == true)
            {
                clcValue.Text = (n * open).ToString();

            }
            else
            {
                txtTotal.Text = "";
            }

        }
        protected void getDetails(string x)
        {

            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.Batches.ToList().Where(a => a.BatchID == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                    txtBatch.Text=my.Batchref;
                    txtBatchDetails.Text=my.BatchDetails ;
                   txtTotal.Text=my.BatchUnits.ToString();
                  clcValue.Text=my.value.ToString();
                    drpPr.Items.Insert(0, new ListItem(my.product, my.IPOID.ToString()));
                }
            }
            txtTotal.ReadOnly = true;
            drpPr.Enabled = false;
            return;
        }
        protected void loadProducts()
        {
            drpPr.Items.Clear();
            drpPr.DataSource = null;
            var ds = from c in db.IPOs
                     select new

                     {

                         ID = c.IPOID,

                         Issuer = "Issuer " + c.issuer + "Openning Price " + c.openningprice + "Openning Date " + c.OpenningDate + "Closing Date" + c.ClosingDate,

                     };
            drpPr.DataSource = ds.ToList();
            drpPr.DataTextField = "Issuer";
            // text field name of table dispalyed in dropdown
            drpPr.DataValueField = "ID"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            drpPr.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpPr.Items.Insert(0, new ListItem("Please Select a product", "0"));
        }



        protected void Submit_Click(object sender, EventArgs e)
        {
            //check parameters


            if (txtTotal.Text == "" || txtTotal.Text == null || clcValue.Text == "" || clcValue.Text == null)
            {
                msgbox("Total Units and Estimated value is required !");
                return;
            }

            double n;
            bool isNumericT = double.TryParse(txtTotal.Text, out n);

            if (isNumericT == false)
            {
                msgbox("Batch Units");
                return;
            }

            double c;
            bool isNumericC = double.TryParse(clcValue.Text, out c);

            if (isNumericC == false)
            {
                msgbox("Estimated Value should be calculated");
                return;
            }


            if (Submit.Text == "Submit")
            {
                BrokerOffice.Models.Batch my = new BrokerOffice.Models.Batch();
               my.Batchref= txtBatch.Text;
                my.BatchDetails=txtBatchDetails.Text;
                my.BatchUnits=Convert.ToDouble(txtTotal.Text);
                my.IPOID=Convert.ToInt32(drpPr.SelectedValue.ToString());
                my.product=drpPr.SelectedItem.Text;
                my.tempstatus = "PENDING";
                my.value = Convert.ToDouble(clcValue.Text);
                db.Batches.Add(my);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                msgbox("Saved Sucessfully");
                //Update reduce the batch

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the batch";
                Response.Redirect("~/Batch/Index");
            }
            else if (Submit.Text == "Update")
            {
                var my = db.Batches.Find(Convert.ToInt32(txtFkey.Text));
                my.Batchref = txtBatch.Text;
                my.BatchDetails = txtBatchDetails.Text;
                my.BatchUnits = Convert.ToDouble(txtTotal.Text);
                my.IPOID = Convert.ToInt32(drpPr.SelectedValue.ToString());
                my.product = drpPr.SelectedItem.Text;
                my.value = Convert.ToDouble(clcValue.Text);
                //Update Row in Table  
                db.Batches.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the batch";
                Response.Redirect("~/Batch/Index");
            }
        }
        protected void Clear()
        {
            Response.Redirect("~/Batch/Index");
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }
    }
}