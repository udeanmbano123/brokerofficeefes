﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class FrequenciesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Frequencies
        public async Task<ActionResult> Index()
        {
            return View(await db.Frequenciess.ToListAsync());
        }

        // GET: Frequencies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frequencies frequencies = await db.Frequenciess.FindAsync(id);
            if (frequencies == null)
            {
                return HttpNotFound();
            }
            return View(frequencies);
        }

        // GET: Frequencies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Frequencies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FrequenciesID,FrequencyName")] Frequencies frequencies)
        {
            if (ModelState.IsValid)
            {
                db.Frequenciess.Add(frequencies);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(frequencies);
        }

        // GET: Frequencies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frequencies frequencies = await db.Frequenciess.FindAsync(id);
            if (frequencies == null)
            {
                return HttpNotFound();
            }
            return View(frequencies);
        }

        // POST: Frequencies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FrequenciesID,FrequencyName")] Frequencies frequencies)
        {
            if (ModelState.IsValid)
            {
                db.Entry(frequencies).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(frequencies);
        }

        // GET: Frequencies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frequencies frequencies = await db.Frequenciess.FindAsync(id);
            if (frequencies == null)
            {
                return HttpNotFound();
            }
            return View(frequencies);
        }

        // POST: Frequencies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Frequencies frequencies = await db.Frequenciess.FindAsync(id);
            db.Frequenciess.Remove(frequencies);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
