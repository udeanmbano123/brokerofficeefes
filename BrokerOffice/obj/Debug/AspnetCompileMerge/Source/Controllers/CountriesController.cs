﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class CountriesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Countries
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.Countries
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.country);
                    break;

                case "module":
                    per = per.OrderBy(s => s.Nationality);
                    break;

                default:
                    per = per.OrderBy(s => s.id);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
           // return View(await db.Countries.ToListAsync());
        }

        // GET: Countries/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_country para_country = await db.Countries.FindAsync(id);
            if (para_country == null)
            {
                return HttpNotFound();
            }
            return View(para_country);
        }

        // GET: Countries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,country,fnam,Nationality")] para_country para_country)
        {
            if (ModelState.IsValid)
            {
                db.Countries.Add(para_country);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(para_country);
        }

        // GET: Countries/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_country para_country = await db.Countries.FindAsync(id);
            if (para_country == null)
            {
                return HttpNotFound();
            }
            return View(para_country);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,country,fnam,Nationality")] para_country para_country)
        {
            if (ModelState.IsValid)
            {
                db.Entry(para_country).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(para_country);
        }

        // GET: Countries/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_country para_country = await db.Countries.FindAsync(id);
            if (para_country == null)
            {
                return HttpNotFound();
            }
            return View(para_country);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            para_country para_country = await db.Countries.FindAsync(id);
            db.Countries.Remove(para_country);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
