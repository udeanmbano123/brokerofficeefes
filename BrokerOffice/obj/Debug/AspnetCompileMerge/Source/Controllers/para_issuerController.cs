﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class para_issuerController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: para_issuer
        public async Task<ActionResult> Index()
        {
            return View(await db.para_issuers.ToListAsync());
        }

        // GET: para_issuer/Details/5
        public async Task<ActionResult> Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }
            return View(para_issuer);
        }

        // GET: para_issuer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: para_issuer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "para_issuerID,Company,Date_created,Issued_shares,Status,registrar,Add_1,City,Country,Contact_Person,Telephone,Cellphone,ISIN,date_listed,email,website")] para_issuer para_issuer)
        {
            if (ModelState.IsValid)
            {
                para_issuer.Date_created = DateTime.Now;
                db.para_issuers.Add(para_issuer);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(para_issuer);
        }

        // GET: para_issuer/Edit/5
        public async Task<ActionResult> Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }
            return View(para_issuer);
        }

        // POST: para_issuer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "para_issuerID,Company,Date_created,Issued_shares,Status,registrar,Add_1,City,Country,Contact_Person,Telephone,Cellphone,ISIN,date_listed,email,website")] para_issuer para_issuer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(para_issuer).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(para_issuer);
        }

        // GET: para_issuer/Delete/5
        public async Task<ActionResult> Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }
            return View(para_issuer);
        }

        // POST: para_issuer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(decimal id)
        {
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            db.para_issuers.Remove(para_issuer);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
