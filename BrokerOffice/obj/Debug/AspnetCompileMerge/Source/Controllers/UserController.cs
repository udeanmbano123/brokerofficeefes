﻿using BrokerOffice.DAO;
using BrokerOffice.DAO.security;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{ // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
        // [CustomAuthorize(Users = "1,2")]
    public class UserController : Controller
    {
       

        private SBoardContext db = new SBoardContext();


        public ActionResult Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = db.Users.ToList().Where(a => a.Email == name);
            ViewBag.Users = "";
            foreach (var row in user)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            var pstord = db.Order_Lives.ToList().Count();

            ViewBag.Orders = pstord.ToString();
            return View();
        }

        //// GET: Report
        //public ActionResult UserReport()
        //{
        //    var list = from u in db.Saccoes
        //               join v in db.saccauths on u.ID equals v.SaccoId
        //               where v.status == "APPROVED"
        //               select u;
        //    //Create List of SelectListItem
        //    List<SelectListItem> selectlist = new List<SelectListItem>();
        //    selectlist.Add(new SelectListItem() { Text = "", Value = "" });
        ////    foreach (var row in list)
        ////    {
        ////        //Adding every record to list  
        ////        selectlist.Add(new SelectListItem { Text = row.Sacconame, Value = row.Sacconame.ToString() });
        ////    }

        ////    ViewBag.Role = selectlist;
        ////    return View();
        ////}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult UserReport([Bind(Include = "ReportName,AsAt")] Report registerreport)
        //{
        //    if (ModelState.IsValid)
        //    {

        //    }
        //    else
        //    {

        //    }
        //    var list = db.Saccoes.ToList();
        //    //Create List of SelectListItem
        //    List<SelectListItem> selectlist = new List<SelectListItem>();
        //    selectlist.Add(new SelectListItem() { Text = "", Value = "" });
        //    foreach (var row in list)
        //    {
        //        //Adding every record to list  
        //        selectlist.Add(new SelectListItem { Text = row.Sacconame, Value = row.Sacconame.ToString() });
        //    }
        //    ViewBag.Role = selectlist;
        //    return View();
        //}
    }
}