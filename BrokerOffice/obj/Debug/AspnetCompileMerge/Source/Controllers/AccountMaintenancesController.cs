﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class AccountMaintenancesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: AccountMaintenances
        public async Task<ActionResult> Index()
        {
            return View(await db.AccountMaintenances.ToListAsync());
        }

        // GET: AccountMaintenances/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountMaintenance accountMaintenance = await db.AccountMaintenances.FindAsync(id);
            if (accountMaintenance == null)
            {
                return HttpNotFound();
            }
            return View(accountMaintenance);
        }

        // GET: AccountMaintenances/Create
        public ActionResult Create()
        {
            var list3 = db.Client_Types.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.ClientType, Value = row.ClientType.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View();
        }

        // POST: AccountMaintenances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AccountMaintenanceID,account_type,amount,frequency")] AccountMaintenance accountMaintenance)
        {
            if (ModelState.IsValid)
            {
                db.AccountMaintenances.Add(accountMaintenance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list3 = db.Client_Types.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["account_type"], Value = Request["account_type"] });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.ClientType, Value = row.ClientType.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(accountMaintenance);
        }

        // GET: AccountMaintenances/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountMaintenance accountMaintenance = await db.AccountMaintenances.FindAsync(id);
            if (accountMaintenance == null)
            {
                return HttpNotFound();
            }
            var list3 = db.Client_Types.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = accountMaintenance.account_type, Value = accountMaintenance.account_type});
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.ClientType, Value = row.ClientType.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(accountMaintenance);
        }

        // POST: AccountMaintenances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AccountMaintenanceID,account_type,amount,frequency")] AccountMaintenance accountMaintenance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountMaintenance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list3 = db.Client_Types.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["account_type"], Value = Request["account_type"] });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.ClientType, Value = row.ClientType.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(accountMaintenance);
        }

        // GET: AccountMaintenances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountMaintenance accountMaintenance = await db.AccountMaintenances.FindAsync(id);
            if (accountMaintenance == null)
            {
                return HttpNotFound();
            }
            return View(accountMaintenance);
        }

        // POST: AccountMaintenances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AccountMaintenance accountMaintenance = await db.AccountMaintenances.FindAsync(id);
            db.AccountMaintenances.Remove(accountMaintenance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
