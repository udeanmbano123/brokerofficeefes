﻿using DevExpress.Web.Mvc;
using BrokerOffice.DAO.security;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class ReportingController : Controller
    {
        // GET: Reporting
        public ActionResult ClientList()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientList([Bind(Include = "Asat")] ClientList registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ClientList.aspx?id=" + registerreport.Asat);
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Posting([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/PostingOrders.aspx?From=" + registerreport.From+"&To="+registerreport.To);
            }
            return View();
        }

        public ActionResult Posting()
        {
            return View();
        }

        public ActionResult Portfolio()
        {
            return View();
        }

        public ActionResult TrialBalance()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrialBalance([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TrialBalance.aspx?id="+registerreport.DateHeld);
            }
                return View();
        }

        public ActionResult IncomeStatement()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IncomeStatement([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/IncomeStatement.aspx?id=" + registerreport.DateHeld);
            }
            return View();
        }

    }
}