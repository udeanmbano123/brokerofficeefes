﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class BranchesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Branches
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.Branches
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.branch_name);
                    break;

                case "module":
                    per = per.OrderBy(s => s.branch);
                    break;

                default:
                    per = per.OrderBy(s => s.id);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
            //return View(await db.Branches.ToListAsync());
        }

        // GET: Branches/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_branch para_branch = await db.Branches.FindAsync(id);
            if (para_branch == null)
            {
                return HttpNotFound();
            }
            return View(para_branch);
        }

        // GET: Branches/Create
        public ActionResult Create()
        {
            var list2 = db.Banks.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.bank_name, Value = row.bank.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View();
        }

        // POST: Branches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,bank,branch,branch_name")] para_branch para_branch)
        {
            if (ModelState.IsValid)
            {
                db.Branches.Add(para_branch);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list2 = db.Banks.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request["bank"], Value = Request["bank"] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.bank_name, Value = row.bank.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(para_branch);
        }

        // GET: Branches/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_branch para_branch = await db.Branches.FindAsync(id);
            if (para_branch == null)
            {
                return HttpNotFound();
            }

           string list3 = db.Banks.ToList().Where(a => a.bank == para_branch.bank).Select(a => a.bank_name).ToString();
            var list2 = db.Banks.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text =list3, Value = para_branch.bank });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.bank_name, Value = row.bank.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(para_branch);
        }

        // POST: Branches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,bank,branch,branch_name")] para_branch para_branch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(para_branch).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list2 = db.Banks.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request["bank"], Value = Request["bank"] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.bank_name, Value = row.bank.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(para_branch);
        }

        // GET: Branches/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_branch para_branch = await db.Branches.FindAsync(id);
            if (para_branch == null)
            {
                return HttpNotFound();
            }
            return View(para_branch);
        }

        // POST: Branches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            para_branch para_branch = await db.Branches.FindAsync(id);
            db.Branches.Remove(para_branch);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
