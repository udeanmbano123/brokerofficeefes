﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class IPOesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: IPOes
        public async Task<ActionResult> Index()
        {
            return View(await db.IPOs.ToListAsync());
        }

        // GET: IPOes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            return View(iPO);
        }

        // GET: IPOes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IPOes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IPOID,issuer,targetedvalue,OpenningDate,ClosingDate,ListingDate,openningprice")] IPO iPO)
        {
            if (ModelState.IsValid)
            {
                db.IPOs.Add(iPO);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(iPO);
        }

        // GET: IPOes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            return View(iPO);
        }

        // POST: IPOes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IPOID,issuer,targetedvalue,OpenningDate,ClosingDate,ListingDate,openningprice")] IPO iPO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iPO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(iPO);
        }

        // GET: IPOes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            return View(iPO);
        }

        // POST: IPOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            IPO iPO = await db.IPOs.FindAsync(id);
            db.IPOs.Remove(iPO);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
