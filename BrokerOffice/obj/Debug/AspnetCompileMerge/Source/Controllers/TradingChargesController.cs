﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class TradingChargesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: TradingCharges
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CodeSortParm = String.IsNullOrEmpty(sortOrder) ? "code" : "";
            ViewBag.StockSortParm = String.IsNullOrEmpty(sortOrder) ? "stock" : "";
            var per = from s in db.TradingChargess
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.ChargeName);
                    break;

                case "code":
                    per = per.OrderBy(s => s.chargecode);
                    break;
                case "stock":
                    per = per.OrderBy(s => s.stockisin);
                    break;
                default:
                    per = per.OrderBy(s => s.TradingChargesID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
            //return View(await db.TradingChargess.ToListAsync());
        }

        // GET: TradingCharges/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            return View(tradingCharges);
        }

        // GET: TradingCharges/Create
        public ActionResult Create()
        {
            var list3 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN.ToString() });
            }

            ViewBag.StockType = selectlist3;
            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text ="", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            return View();
        }

        // POST: TradingCharges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TradingChargesID,stockisin,StockSecuritieesID,chargecode,chargedescription,tradeside,ChargedAs,chargevalue,chargefrequency,ChargeName,chartaccount,chartaccountcode")] TradingCharges tradingCharges)
        {
            if (ModelState.IsValid)
            {
                string fellow = Request["stockisin"].ToString();
       
                var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);

                string rm = "";
                string code = "";
                foreach (var q in br)
                {
                    tradingCharges.stockisin = q.StockISIN;
                    tradingCharges.StockSecuritieesID= q.StockSecuritiesID;
                }
                string acc = Request["chartaccount"].ToString();
                int accr = Convert.ToInt32(acc);
                var brs = db.Accounts_Masters.ToList().Where(a => a.AccountNumber == accr);

                string rm2 = "";
                string code2 = "";
                foreach (var q in brs)
                {
                    tradingCharges.chartaccount = q.AccountName;
                    tradingCharges.chargeaccountcode = q.AccountNumber;
                }

                db.TradingChargess.Add(tradingCharges);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list3 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["stockisin"], Value=Request["stockisin"]});
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN.ToString() });
            }

            ViewBag.StockType = selectlist3;
            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = Request["chartaccount"], Value = Request["chartaccount"] });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            return View(tradingCharges);
        }

        // GET: TradingCharges/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            var list3 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = tradingCharges.stockisin, Value = tradingCharges.stockisin });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN.ToString() });
            }

            ViewBag.StockType = selectlist3;
            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = tradingCharges.chartaccount, Value = tradingCharges.chargeaccountcode.ToString()});
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            return View(tradingCharges);
        }

        // POST: TradingCharges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TradingChargesID,stockisin,StockSecuritieesID,chargecode,chargedescription,tradeside,ChargedAs,chargevalue,chargefrequency,ChargeName,chartaccount,chartaccountcode")] TradingCharges tradingCharges)
        {
            if (ModelState.IsValid)
            {
                string fellow = Request["stockisin"].ToString();

                var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);

                string rm = "";
                string code = "";
                foreach (var q in br)
                {
                    tradingCharges.stockisin = q.StockISIN;
                    tradingCharges.StockSecuritieesID = q.StockSecuritiesID;
                }
                string acc= Request["chartaccount"].ToString();
                int accr = Convert.ToInt32(acc);
                var brs = db.Accounts_Masters.ToList().Where(a => a.AccountNumber == accr);

                string rm2 = "";
                string code2 = "";
                foreach (var q in brs)
                {
                    tradingCharges.chartaccount= q.AccountName;
                    tradingCharges.chargeaccountcode = q.AccountNumber;
                }

                db.Entry(tradingCharges).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list3 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["stockisin"], Value = Request["stockisin"] });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN.ToString() });
            }

            ViewBag.StockType = selectlist3;

            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = Request["chartaccount"], Value = Request["chartaccount"] });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            return View(tradingCharges);
        }

        // GET: TradingCharges/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            return View(tradingCharges);
        }

        // POST: TradingCharges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            db.TradingChargess.Remove(tradingCharges);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
