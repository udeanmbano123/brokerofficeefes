﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

using WebMatrix.WebData;
using BrokerOffice.DAO.security;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;

namespace BrokerOffice.Controllers
{ //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
  // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class PermissionsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Permissions
        public ActionResult Index(string sortOrder,int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.Permissions
                        select s;
            switch (sortOrder)
            {
                case "name_desc":
                  per = per.OrderByDescending(s => s.Name);
                    break;

                case "module":
                  per = per.OrderBy(s => s.Module);
                    break;
              
                default:
                    per = per.OrderBy(s => s.PermissionID);
                    break;
            }
            int pageSize =10;
            int pageNumber = (page ?? 1);
            return View( per.ToPagedList(pageNumber, pageSize));
           // return View(await db.Permissions.ToListAsync());
        }

        // GET: Permissions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permission permission = await db.Permissions.FindAsync(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(permission);
        }

        // GET: Permissions/Create
        public ActionResult Create()
        {
           

            ViewBag.Category = regionD();
            ViewBag.Role = Countries();

            
            return View();
        }

        // POST: Permissions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PermissionID,ControllerName,ViewName,Name,assignedRole,Module,IsDashboard,IsWebForm,webFormUrl,IsActice")] Permission permission)
        {
            var name = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string username = "";
            foreach (var p in name)
            {
                username = p.Username;
            }
            int r1 = 0;
            int r2 = 0;

            try
            {
                r1 = Convert.ToInt32(Request["assignedRole"]);
            }
            catch (Exception)
            {

                r1 = 0;
            }

            try
            {
                r2 = Convert.ToInt32(Request["Module"]);

            }
            catch (Exception)
            {

                r2 = 0;
            }
            if (ModelState.IsValid)
            {
       
                var c = db.Roles.ToList().Where(a=>a.RoleId==r1);
                foreach(var k in c)
                {
                    permission.assignedRole = k.RoleName;
                }
                var d= db.Moduless.ToList().Where(a => a.ModulesID == r2);
                foreach (var y in d)
                {
                    permission.Module = y.ModulesName;
                }
                db.Permissions.Add(permission);
                await db.SaveChangesAsync(username);
                return RedirectToAction("Index");
            }
            
            int my = 0;
            var roles = db.Roles.ToList().Where(a=>a.RoleName== Request["assignedRole"]);
            foreach(var x in roles)
            {
                my = x.RoleId;
            }
            ViewBag.Category = RegionsM(my.ToString(), Request["Module"].ToString());
            ViewBag.Role = CountriesM(permission.assignedRole,my.ToString());
         
            return View(permission);
        }

        // GET: Permissions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permission permission = await db.Permissions.FindAsync(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            ViewBag.Role = CountriesM(permission.assignedRole, permission.assignedRole);
            ViewBag.Category = RegionsM(permission.Module, permission.assignedRole);

           
            return View(permission);
        }

        // POST: Permissions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PermissionID,ControllerName,ViewName,Name,assignedRole,Module,IsDashboard,IsWebForm,webFormUrl,IsActice")] Permission permission)
        {
            var name = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string username = "";
            foreach (var p in name)
            {
                username = p.Username;
            }
            int r1 = 0;
            int r2 = 0;

            try
            {
                r1 = Convert.ToInt32(Request["assignedRole"]);
            }
            catch (Exception)
            {

                r1 = 0;
            }

            try
            {
                r2 = Convert.ToInt32(Request["Module"]);

            }
            catch (Exception)
            {

                r2 = 0;
            }
            if (ModelState.IsValid && permission.assignedRole!= "Change Role")
            {
                var c = db.Roles.ToList().Where(a => a.RoleId == r1);
                foreach (var k in c)
                {
                    permission.assignedRole = k.RoleName;
                }
                var d = db.Moduless.ToList().Where(a => a.ModulesID == r2);
                foreach (var y in d)
                {
                    permission.Module = y.ModulesName;
                }

                db.Entry(permission).State = EntityState.Modified;
                await db.SaveChangesAsync(username);
                return RedirectToAction("Index");
            }
         
            int my = 0;
            var roles = db.Roles.ToList().Where(a => a.RoleName == Request["assignedRole"]);
            foreach (var x in roles)
            {
                my = x.RoleId;
            }

            ViewBag.Category = RegionsM(my.ToString(), Request["Module"].ToString());
            ViewBag.Role = CountriesM(permission.assignedRole, permission.assignedRole);


            return View(permission);
        }

        // GET: Permissions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permission permission = await db.Permissions.FindAsync(id);
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(permission);
        }

        // POST: Permissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var name = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string username = "";
            foreach (var p in name)
            {
                username = p.Username;
            }
            Permission permission = await db.Permissions.FindAsync(id);
            db.Permissions.Remove(permission);
            await db.SaveChangesAsync(username);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult AjaxMethod(string type, int value)
        {
            Permission model = new Permission();
            switch (type)
            {
                case "assignedRole":
                    ViewBag.Category = Regions(value.ToString());
                    break;
               
            }
            return Json(ViewBag.Category);
        }
        public List<SelectListItem> Regions(string s)
        {

            int pp = Convert.ToInt32(s);

            //int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Moduless where u.RoleID ==pp select u;
            if (query.Count() > 0)
            {
                foreach (var p in query)
                {
                    phy.Add(new SelectListItem { Text = p.ModulesName, Value = p.ModulesID.ToString() });
                }
            }
            return phy;
        }

        public List<SelectListItem> RegionsM(string s, string v1)
        {
        var c = db.Roles.ToList().Where(a => a.RoleName == v1);
            int v = 0;
            foreach (var p in c)
            {
                v = p.RoleId;
            }

            //int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = v1, Value = v1 });

            var query = from u in db.Moduless where u.RoleID == v select u;
            if (query.Count() > 0)
            {
                foreach (var p in query)
                {
                    phy.Add(new SelectListItem { Text = p.ModulesName, Value = p.ModulesID.ToString() });
                }
            }
            return phy;
        }

        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "", Value = "" });
            return phy;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public List<SelectListItem> CountriesM(string s1, string s2)
        {
            var c = db.Roles.ToList().Where(a=>a.RoleName==s2);
            int v3 = 0;
            foreach (var d in c)
            {
                v3 = d.RoleId;
            }
            List<SelectListItem> dept = new List<SelectListItem>();
            dept.Add(new SelectListItem { Text = s1, Value = v3.ToString() });
            var query = from u in db.Roles select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.RoleName, Value = v.RoleId.ToString() });
                }
            }
            return dept;
        }
        public List<SelectListItem> Countries()
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.Roles select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.RoleName, Value = v.RoleId.ToString() });
                }
            }
            return dept;
        }
    }
}
