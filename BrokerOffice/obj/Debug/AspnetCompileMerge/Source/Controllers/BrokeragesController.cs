﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class BrokeragesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Brokerages
        public async Task<ActionResult> Index()
        {
            return View(await db.Brokerages.ToListAsync());
        }

        // GET: Brokerages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brokerage brokerage = await db.Brokerages.FindAsync(id);
            if (brokerage == null)
            {
                return HttpNotFound();
            }
            return View(brokerage);
        }

        // GET: Brokerages/Create
        public ActionResult Create()
        {
            var list4 = db.Agents.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AgentName, Value = row.AgentName.ToString() });
            }
            ViewBag.Dlyp = selectlist4;
            return View();
        }

        // POST: Brokerages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BrokerageID,CompanyName,OperatingAddress,PrincipalAgent,brokername")] Brokerage brokerage)
        {
            if (ModelState.IsValid)
            {
                string fellow = Request["PrincipalAgent"].ToString();
                var br = db.Agents.ToList().Where(a => a.AgentName == fellow);
                string rm = "";
                string code = "";
                foreach (var q in br)
                {
                    rm = q.AgentName;
                    code = q.AgentCode;
                }
                brokerage.brokername = rm;
                brokerage.PrincipalAgent = code;
                db.Brokerages.Add(brokerage);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list4 = db.Agents.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AgentName, Value = row.AgentName.ToString() });
            }
            ViewBag.Dlyp = selectlist4;
            return View(brokerage);
        }

        // GET: Brokerages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brokerage brokerage = await db.Brokerages.FindAsync(id);
            if (brokerage == null)
            {
                return HttpNotFound();
            }
            var list4 = db.Agents.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text =brokerage.brokername, Value = brokerage.brokername });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AgentName, Value = row.AgentName.ToString() });
            }
            ViewBag.Dlyp = selectlist4;
            return View(brokerage);
        }

        // POST: Brokerages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BrokerageID,CompanyName,OperatingAddress,PrincipalAgent,brokername")] Brokerage brokerage)
        {
            if (ModelState.IsValid)
            {

                string fellow = Request["PrincipalAgent"].ToString();
                var br = db.Agents.ToList().Where(a => a.AgentName == fellow);
                string rm = "";
                string code = "";
                foreach (var q in br)
                {
                    rm = q.AgentName;
                    code = q.AgentCode;
                }
                brokerage.brokername = rm;
                brokerage.PrincipalAgent = code;
                db.Entry(brokerage).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list4 = db.Agents.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = brokerage.brokername, Value = brokerage.PrincipalAgent });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AgentName, Value = row.AgentCode.ToString() });
            }
            ViewBag.Dlyp = selectlist4;
            return View(brokerage);
        }

        // GET: Brokerages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brokerage brokerage = await db.Brokerages.FindAsync(id);
            if (brokerage == null)
            {
                return HttpNotFound();
            }
            return View(brokerage);
        }

        // POST: Brokerages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Brokerage brokerage = await db.Brokerages.FindAsync(id);
            db.Brokerages.Remove(brokerage);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
