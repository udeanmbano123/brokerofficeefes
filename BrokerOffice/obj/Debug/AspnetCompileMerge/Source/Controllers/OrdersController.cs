﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;

namespace BrokerOffice.Controllers
{
    public class OrdersController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Orders
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {


            ViewBag.CustomerSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date" : "";
            var per = from s in db.Order_Lives
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.CDS_AC_No);
                    break;

                case "date":
                    per = per.OrderBy(s => s.Create_date);
                    break;

                default:
                    per = per.OrderBy(s => s.OrderNo);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
           // return View(await db.Order_Lives.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            if (order_Live == null)
            {
                return HttpNotFound();
            }
            return View(order_Live);
        }

      

        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            if (order_Live == null)
            {
                return HttpNotFound();
            }
            return View(order_Live);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            db.Order_Lives.Remove(order_Live);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
