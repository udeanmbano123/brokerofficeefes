﻿using BrokerOffice.DAO;
using BrokerOffice.DAO.security;
using SaccoRegsitration.DAO.security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{// GET: Admin
        //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "Admin")]
        // [CustomAuthorize(Users = "1")]
    public class AdminController : Controller
    {
        private SBoardContext db = new SBoardContext();
        public ActionResult Index()
        { string name = WebSecurity.CurrentUserName;
            var user = db.Users.ToList().Where(a => a.Email == name);
            ViewBag.Users = "";
            foreach(var row in user)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            var pstord = db.Order_Lives.ToList().Count();

            ViewBag.Orders = pstord.ToString();
            return View();
        }

        public ActionResult Unlock()
        {
            return View();
        }
    }
}