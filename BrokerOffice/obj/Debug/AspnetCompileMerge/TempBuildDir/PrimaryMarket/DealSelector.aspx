﻿<%@ Page Language="C#" Async="true"   MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="DealSelector.aspx.cs" Inherits="BrokerOffice.Reporting.DealSelector" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div style="background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Broker Notes</i>
       
    </div> 


      <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <div class="form-group">
      <asp:Label ID="SLbl" runat="server" CssClass="control-label col-md-2" Text="Search"></asp:Label>
            <div class="col-md-10">
                <dx:ASPxTextBox ID="txtSearch" runat="server" CssClass="form-control" Width="170px"></dx:ASPxTextBox>
                <dx:ASPxButton ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click"></dx:ASPxButton>
             </div>
        </div>

    <asp:GridView ID="GridView1" Theme="Glass" CssClass="table table-striped" style="font-size:8.5pt;width:400px;" runat="server"   EmptyDataText="No records has been added."
    AutoGenerateSelectButton="true" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True" PageSize="10" OnPageIndexChanging="grdData_PageIndexChanging"
    >   
      <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />

</asp:GridView>
   
       <div class="form-group">
               <table class="table table-striped" style="width:300px;margin-left:2%;">
                      <tr>
             <td>
                 <dx:ASPxLabel ID="ASPxLabel1" runat="server" Theme="Moderno" Width="170px" Text="Account"></dx:ASPxLabel>
                     <dx:ASPxTextBox ID="txtAcc" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
               
             </td>
                          <td>
                              <dx:ASPxLabel ID="ASPxLabel2" runat="server" Theme="Moderno" Width="170px" Text="OrderNo"></dx:ASPxLabel>
  <dx:ASPxTextBox ID="txtOrder" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
            
                          </td>
                          </tr>
        </table>
  
       
    <table class="table table-striped" style="width:300px;margin-left:2%;">
                      <tr>
                                     <td>
                              <asp:Label  runat="server" Visiible="false" Text="" Width="170px"></asp:Label>
                              <dx:ASPxButton ID="txtView" runat="server" CssClass="btn btn-primary" Text="View" OnClick="txtView_Click"></dx:ASPxButton>
                          </td>
                          </tr>
        </table></div>
       </div>
  </asp:content>

