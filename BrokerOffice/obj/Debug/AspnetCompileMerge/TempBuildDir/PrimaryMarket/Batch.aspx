﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="Batch.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.Batch" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Batch Form</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                   Batch </a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                    Product</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                     <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                                <tr>
                                 <td>
<asp:Label runat="server" CssClass="control-label col-md-4" Text="Batch Ref"></asp:Label>
                                     <br />
<asp:TextBox id="txtBatch" CssClass="form-control" Width="150px"  runat="server"></asp:TextBox>
                                 </td>
                                      <td>
<asp:Label runat="server" CssClass="control-label col-md-4" Text="Batch Total"></asp:Label>
                                          <br />
<asp:TextBox id="txtTotal"  CssClass="form-control" Width="150px" runat="server"></asp:TextBox>
                                 </td>
                                        </tr>
                                       <tr>

                                           <td>
<asp:Label runat="server" CssClass="control-label col-md-4" Text="Batch Details"></asp:Label>
                                               <br />
   <asp:TextBox id="txtBatchDetails" CssClass="form-control" Width="150px" TextMode="Multiline" runat="server"></asp:TextBox>

                    
                                           </td>
                                
                                   </tr>
                             </table>
                        <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click"></dx:ASPxButton></dx>
                              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                      <tr>
                          <td>
                  <asp:Label runat="server" CssClass="control-label col-md-6" Text="Select Product"></asp:Label>
                          </td>
                          <td>
                       <asp:DropDownList ID="drpPr" CssClass="form-control" Width="150px" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPr_SelectedIndexChanged"></asp:DropDownList>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <asp:Label ID="Label1" runat="server" Text="Estimated Value"></asp:Label>
                          </td>
                          <td>
                              <asp:TextBox ID="clcValue" ReadOnly="true" Width="150px"  CssClass="form-control" runat="server"></asp:TextBox>
                          </td>
                      </tr>
            <tr>
                          <td>
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click" style="width: 61px" ></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                      <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                       <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton></dx>
              
                </div>
      
            </div>
        </div>
    </div>
      
    </div>
    <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>