﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="OrderPosting.aspx.cs" Inherits="BrokerOffice.ClientServices.OrderPosting" %>

<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Order Posting</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Primary</a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                   Details</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                   <tr>
                                      <td>
                                          <asp:Label ID="Label1" CssClass="control-label col-md-6" runat="server" Text="Select TP"></asp:Label>
                                                <br />
                                          <asp:DropDownList CssClass="form-control" AutoPostBack="true"  ID="drpTP" runat="server" OnSelectedIndexChanged="drpTP_SelectedIndexChanged" Width="150px"></asp:DropDownList>
                                      </td>
                                       <td>
                                           <asp:Label ID="Label2" CssClass="control-label col-md-8" runat="server" Text="Select TP Board"></asp:Label>
                                           <br />
                                           <asp:DropDownList CssClass="form-control"  ID="drpTPB" runat="server" Width="150px"></asp:DropDownList>
                                       </td>
                                      <td>
                                   <asp:Label runat="server" CssClass="control-label col-md-2" Text="OrderType"></asp:Label>
                                          <br />
                                <asp:DropDownList id="drporder" CssClass="form-control" runat="server" Width="150px" >
      <asp:ListItem Text="Buy" Value="Buy"></asp:ListItem>
      <asp:ListItem Text="Sell" Value="Sell"></asp:ListItem>
                                </asp:DropDownList>
                                    </td>
                                   </tr>
                                <tr>
                                    <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                                  
                                    <td>
                                   <asp:Label runat="server" CssClass="control-label col-md-2" Text="Company"></asp:Label>
                                        <br />
                                        <asp:DropDownList CssClass="form-control" ID="Company" runat="server" Width="150px"></asp:DropDownList>
                                    
                                    </td>
                                    <td>
                                    <asp:Label runat="server" CssClass="control-label col-md-2" Text="SecurityType "></asp:Label>
                                        <br />
                                   <asp:TextBox id="txtsecurity" CssClass="form-control" runat="server" Width="150px" ></asp:TextBox>
                                    </td><td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text=" Shareholder"></asp:Label>
                                         <br />
                                         <asp:DropDownList id="drpname" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="drpname_SelectedIndexChanged" Width="150px"></asp:DropDownList>

                                    </td>
                                </tr>
                                     <tr>
                                    <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Client_Type "></asp:Label>
                                        <br />
                      <asp:DropDownList ID="txtClient" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                                    </td>
                                    <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Tax"></asp:Label>
                                   <br />
<asp:TextBox id="Tax" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                     <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="ClientName"></asp:Label>
                                        <br />
<asp:TextBox id="Shareholder" ReadOnly="true" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                     <tr>
                                   
                                    <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="TotalShareHolding "></asp:Label>
                                        <br />
<asp:TextBox id="TotalShareHolding" CssClass="form-control" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
                                    </td>
                                   <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Mobile"></asp:Label>      <br />
<asp:TextBox id="txtMobile" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                             </table>
                            <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click" ></dx:ASPxButton></dx>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                      <tr>
                          <td>
<asp:Label runat="server"  Text="Begin"></asp:Label>
<dx:ASPxDateEdit ID="begindate" CssClass="form-control" runat="server" Width="150px"></dx:ASPxDateEdit>
                          </td>
                          <td>
<asp:Label runat="server"  Text="Expiry"></asp:Label>
<dx:ASPxDateEdit ID="expirydate" CssClass="form-control"  runat="server" Width="150px"></dx:ASPxDateEdit>
                          </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Quantity"></asp:Label>  <br />
<asp:TextBox id="qty" CssClass="form-control" runat="server" Width="150px" AutoPostBack="True" OnTextChanged="qty_TextChanged"></asp:TextBox>
                          </td>
                      </tr>
                             <tr>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text=" BasePrice"></asp:Label>
                                <br />
<asp:TextBox id="txtbase" CssClass="form-control" runat="server" Width="150px" AutoPostBack="True" OnTextChanged="txtbase_TextChanged"></asp:TextBox>
                          </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="AvailableShares"></asp:Label>
                                <br />
<asp:TextBox id="availableshrs" CssClass="form-control" runat="server" ReadOnly="True" Width="150px"></asp:TextBox>
                          </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text=" OrderPref "></asp:Label>  <br />
<asp:TextBox id="ordepref" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="OrderAttribute"></asp:Label>  <br />
<asp:TextBox id="ordA" CssClass="form-control" TextMode="Multiline" runat="server" Width="150px"></asp:TextBox>
                          </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Marketboard"></asp:Label>  <br />
<asp:TextBox id="mrktBoard" CssClass="form-control" TextMode="Multiline" runat="server" Width="150px"></asp:TextBox>
                          </td>  
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="MaxPrice($)"></asp:Label>  <br />
<asp:TextBox id="max" CssClass="form-control" runat="server" Width="150px" AutoPostBack="True" OnTextChanged="max_TextChanged"></asp:TextBox>
                           </td>
                      </tr>
                      <tr>
                    
                           <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="MiniPrice($)"></asp:Label>  <br />
<asp:TextBox id="mini" CssClass="form-control" runat="server" Width="150px" AutoPostBack="True" OnTextChanged="mini_TextChanged"></asp:TextBox>
                           </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Currency($)"></asp:Label>  <br />
<asp:DropDownList id="drpCurr" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                          </td>
                          <td></td>
                      </tr>
                    
                             
                      <tr>
                          
                          <td>
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click" ></asp:Button>
                          </td>
                 <td>
<asp:Button ID="btnInvoice" runat="server" CssClass="btn btn-primary" Text="View Invoice" OnClick="btnInvoice_Click"  ></asp:Button>
                          </td>
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                         <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click"></dx:ASPxButton></dx>
              
                </div>
      
            </div>
        </div>
    </div>
        <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" FooterText="Search Customer" HeaderText="Search Customer" Width="1184px" Modal="True" ScrollBars="Both" AllowDragging="True" ShowPageScrollbarWhenModal="True" Top="15" CloseAction="CloseButton">

             <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
           <iframe name="myIframe" src="" id="myIframe"  width="1184px" height="800px" runat =server></iframe>

                      </dx:PopupControlContentControl>
            </ContentCollection>
     
            
      </dx:ASPxPopupControl>
    </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>