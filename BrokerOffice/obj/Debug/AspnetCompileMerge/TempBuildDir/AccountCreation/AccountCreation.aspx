﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="AccountCreation.aspx.cs" Inherits="BrokerOffice.AccountCreation.AccountCreation" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
 <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Account Creation</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Personal </a></li>
                <li><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Contact Details</a></li>
                <li><a href="#identification" aria-controls="identification" role="tab" data-toggle="tab">Identification Details</a></li>
                 <li><a href="#banking" aria-controls="banking" role="tab" data-toggle="tab">Banking Details</a></li>
                 <li><a href="#trading" aria-controls="banking" role="tab" data-toggle="tab">Trading Preferences</a></li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                <div role="tabpanel" class="tab-pane active" id="personal">
                  <table class="table table-striped">
                      <tr>
                               <td>
                              <asp:Label ID="Label41" CssClass="control-label col-md-2" runat="server" Text="Depository"></asp:Label>
                                   <br />
                               <asp:Label ID="Label68" runat="server" Text="" ForeColor="#CC0000"></asp:Label> <asp:DropDownList ID="dep"  CssClass="form-control" runat="server" Width="150px">
                         
                              </asp:DropDownList>
                          </td>
                          <td>

                          </td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                       <td>
                          <asp:Label ID="Label1" CssClass="control-label col-md-2" runat="server" Text="Title"></asp:Label>
                           <br />
                        <asp:Label ID="Label69" runat="server" Text="" ForeColor="#CC0000"></asp:Label> <asp:Label ID="Label67" runat="server" Text="" ForeColor="#CC0000"></asp:Label>     <asp:Label ID="Label52" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:DropDownList ID="drpTitle" CssClass="form-control" runat="server" Width="150px">
                              <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                              <asp:ListItem Text="Mrs" Value="Mrs"></asp:ListItem>
                              <asp:ListItem Text="Miss" Value="Miss"></asp:ListItem>
                              <asp:ListItem Text="Dr" Value="Dr"></asp:ListItem>
                              <asp:ListItem Text="Rev" Value="Rev"></asp:ListItem>
                          </asp:DropDownList>
                      </td>
                                                                                      <td>
                          <asp:Label ID="Label11" CssClass="control-label col-md-2" runat="server" Text="Initials"></asp:Label>
                                                                                          <br /><br />
                         <asp:Label ID="Label66" runat="server" Text="" ForeColor="#CC0000"></asp:Label>   <asp:TextBox ID="intials" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                      <td>
                          <asp:Label ID="Label2" CssClass="control-label col-md-2" runat="server" Text="First"></asp:Label>
                          <br />
                          <asp:Label ID="Label42" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtFirstName" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                      <td>
                          <asp:Label ID="Label4" CssClass="control-label col-md-2" runat="server" Text="Middle"></asp:Label>
                          <br />
                         <asp:Label ID="Label70" runat="server" Text="" ForeColor="#CC0000"></asp:Label>   <asp:TextBox ID="txtMiddle" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                     
                      </tr>
                                            <tr>
 <td>
                          <asp:Label ID="Label3" CssClass="control-label col-md-2" runat="server" Text="Surname"></asp:Label>
   <br />
                         <asp:Label ID="Label43" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtSurname" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                                                             <td>
 
                          <asp:Label ID="Label17"  runat="server" Text="DateOfBirth"></asp:Label>
                                                                 <br />
 <asp:Label ID="Label50" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><dx:ASPxDateEdit ID="txtDOB"  CssClass="form-control" runat="server" Width="150px"></dx:ASPxDateEdit>
                         
   </td>
                      <td>
                          <asp:Label ID="Label7" CssClass="control-label col-md-2" runat="server" Text="Nationality"></asp:Label>
                          <br />
                          <asp:Label ID="Label51" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:DropDownList ID="Nationality" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                      </td>
                      <td>
                          <asp:Label ID="Label8" CssClass="control-label col-md-2" runat="server" Text="Resident"></asp:Label>
                          <br />
                            <asp:Label ID="Label71" runat="server" Text="" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtResident" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
       
                   
                      </tr>
                      <tr>
                             <td>
                          <asp:Label ID="Label6" CssClass="control-label col-md-2" runat="server" Text="Gender"></asp:Label>
                                 <asp:Label ID="Label53" runat="server" Text="*" ForeColor="#CC0000"></asp:Label>
                          </td>
                                                <td>
                      <asp:RadioButtonList ID="RadioButtonList1" CssClass="form-control" runat="server" RepeatDirection="Horizontal">
                                     <asp:ListItem Text="MALE" Value="MALE"></asp:ListItem>
                                                    <asp:ListItem Text="FEMALE" Value="FEMALE"></asp:ListItem>
                          </asp:RadioButtonList>
                      </td>
                     
                      </tr>
                  </table>
                    <dx style="float:right"><dx:ASPxButton ID="ASPxButton1" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="ASPxButton1_Click"></dx:ASPxButton></dx>
                </div>
                <div role="tabpanel" class="tab-pane" id="contact">
                     <table class="table table-striped">
                      <tr>
                       <td>
                           <asp:Label ID="Label5"  runat="server" Text="Address Line 1"></asp:Label>
                      <br />
                 <asp:Label ID="Label54" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:TextBox ID="addr1" runat="server"  CssClass="form-control" Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                                                   
                      </td>
                     <td>
                         <asp:Label ID="Label9" runat="server" Text="Address Line 2"></asp:Label>
                          <asp:TextBox ID="addr2" runat="server" CssClass="form-control"  Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                      </td>
                      <td>
                                  <asp:Label ID="Label10" runat="server" Text="Address Line 3"></asp:Label>
                          <br />
                            <asp:Label ID="Label72" runat="server" Text="" ForeColor="#CC0000"></asp:Label> 
                           <asp:TextBox ID="addr3" runat="server" CssClass="form-control"  Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                      </td>
                          <td>
 <asp:Label ID="Label12" CssClass="control-label col-md-2" runat="server" Text="Town"></asp:Label>
                              <br />
                       <asp:Label ID="Label55" runat="server" Text="*" ForeColor="#CC0000"></asp:Label>   <asp:TextBox ID="txtTown" CssClass="form-control" runat="server" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                          </td>
                      </tr>
                         <tr>
                      <td>
                          <asp:Label ID="Label13" CssClass="control-label col-md-2" runat="server" Text="PostCode"></asp:Label>
                          <br />
                           <asp:Label ID="Label75" runat="server" Text="" ForeColor="#CC0000"></asp:Label>  <asp:TextBox ID="txtPostCode" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>   
                                            <td>
                           <asp:Label ID="Label14" runat="server" Text="FaxNumber"></asp:Label>
                      <br />
                 <asp:Label ID="Label74" runat="server" Text="" ForeColor="#CC0000"></asp:Label>   <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" Visible="True" Width="150px"></asp:TextBox>
                                                   
                      </td>
     <td>
                          <asp:Label ID="Label15" CssClass="control-label col-md-2" runat="server" Text="Telephone"></asp:Label>
         <br />
  <asp:Label ID="Label73" runat="server" Text="" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtTel" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
      <td>
                          <asp:Label ID="Label16" CssClass="control-label col-md-2" runat="server" Text="EmailAddress"></asp:Label>
          <br />
                        <asp:Label ID="Label56" runat="server" Text="*" ForeColor="#CC0000"></asp:Label>  <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                      </tr>
                         <tr>                  
           <td>
                          <asp:Label ID="Label29" CssClass="control-label col-md-2" runat="server" Text="MobileNumber"></asp:Label>
               <br />
                         <asp:Label ID="Label57" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtMobile" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                     
                             </td>  
                          <td>
                          <asp:Label ID="Label32" CssClass="control-label col-md-2" runat="server" Text="Country"></asp:Label>
                              <br />
                         <asp:Label ID="Label58" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:DropDownList ID="drpCountry" CssClass="form-control" runat="server" Width="150px">
                         
                          </asp:DropDownList>
                     
                             </td>

   
                             <td></td>
                             <td></td>
                         </tr>
                                         
                  
                  </table>
                <dx style="float:right">
                    <dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton>
                  
                      <dx:ASPxButton ID="dxIdentiication" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxIdentiication_Click"></dx:ASPxButton>


                </dx>
               
                </div>
                   <div role="tabpanel" class="tab-pane" id="identification">
                                     <table class="table table-striped">
                              <tr>
                                  <td>
                                       <asp:Label ID="Label31" CssClass="control-label col-md-6" runat="server" Text="Type*"></asp:Label>
                       
                                      <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged">
                                           <asp:ListItem Text="Company" Value="LI"></asp:ListItem>
                                           <asp:ListItem Text="Individual" Value="LE"></asp:ListItem>
                                      </asp:RadioButtonList>
                                  </td>
                                  <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                              </tr>           
                      <tr>
                           <td>
                          <asp:Label ID="Label21" CssClass="control-label col-md-2" runat="server" Text="Tax"></asp:Label>
                               <br />
                            <asp:Label ID="Label76" runat="server" Text="" ForeColor="#CC0000"></asp:Label> <asp:DropDownList ID="drpTax" CssClass="form-control" runat="server" Width="150px">
                              <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                               <asp:ListItem Text="15" Value="15"></asp:ListItem>
                          </asp:DropDownList>
                      </td>
                                 <td>
                          <asp:Label ID="Label30" CssClass="control-label col-md-4" runat="server" Text="IDType"></asp:Label>
                                     <br />
                         <asp:Label ID="Label59" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:DropDownList ID="drpID" CssClass="form-control" runat="server" Width="150px">
                             

                          </asp:DropDownList>
                      </td>
                          <td>
                              <br />
                      </td>
                      <td>
                          <asp:Label ID="Label18" CssClass="control-label col-md-2" runat="server" Text="Suffix"></asp:Label>
                         <br />
                          <asp:Label ID="Label77" runat="server" Text="" ForeColor="#CC0000"></asp:Label>   <asp:DropDownList ID="drpSuffix" CssClass="form-control" runat="server" Width="150px">
                            
                          </asp:DropDownList>
                      </td>

                          </tr>
                                         <tr>
                                       <td>
                          <asp:Label ID="Label19" CssClass="control-label col-md-2" runat="server" Text="RegistrationNumber"></asp:Label>
                                       </td>
                                       <td>

                          <asp:Label ID="Label20"  CssClass="control-label col-md-2" runat="server" Text="ID"></asp:Label>

                                       </td>
                                       <td>

                          <asp:Label ID="Label65"  CssClass="control-label col-md-2" runat="server" Text="Company Name"></asp:Label>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                            <tr>
                                <td>
                                
                          <asp:TextBox ID="txtReg" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                </td>
                                <td>
                           <asp:TextBox ID="txtID" CssClass="form-control" runat="server" AutoPostBack="True" OnTextChanged="txtID_TextChanged" Width="150px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCompanyName" CssClass="form-control" runat="server" AutoPostBack="True"  Width="150px"></asp:TextBox>
                              

                                </td>
                                <td></td>
                             </tr>
                             
                                         </table>
                          <dx style="float:right">
                    <dx:ASPxButton ID="dxContact" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxContact_Click" ></dx:ASPxButton>
                    <dx:ASPxButton ID="dxBanking" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxBanking_Click"></dx:ASPxButton>


                </dx>
                </div>
                <div role="tabpanel" class="tab-pane" id="banking">
     
                               <table class="table table-striped">
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label34" CssClass="control-label" runat="server" Text="1. Settlement Details" Font-Bold="True"></asp:Label>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                      <tr>
<td>
                          <asp:Label ID="Label22" CssClass="control-label col-md-2" runat="server" Text="Bank"></asp:Label>
    <br />
                              <asp:Label ID="Label60" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:DropDownList ID="cmbBank" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbBank_SelectedIndexChanged" Width="150px"></asp:DropDownList>
                      </td>
                          <td>
                          <asp:Label ID="Label23" CssClass="control-label col-md-2" runat="server" Text="Branch"></asp:Label>
                       <br />
                            <asp:Label ID="Label61" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:DropDownList ID="cmbBranch" CssClass="form-control" runat="server" Width="150px" Height="23px"></asp:DropDownList>
                        </td>
                      <td>

                  <asp:Label ID="Label24" runat="server" Text="AccountNumber"></asp:Label>
                       <br />
                <asp:Label ID="Label62" runat="server" Text="*" ForeColor="#CC0000"></asp:Label> <asp:TextBox ID="txtAcc" runat="server"  CssClass="form-control" Visible="True" Width="150px"></asp:TextBox>
                   
                       </td>
   <td>

                      <br />
                   
                                       </td>
                          </tr>
                         
                                           <tr>
                                       <td>
                                           <asp:Label ID="Label35" CssClass="control-label" runat="server" Text="1. Dividend Details" Font-Bold="True"></asp:Label>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
<asp:Label ID="Label27" runat="server" Text="Dividend Payee"></asp:Label>
                      <br />
                 <asp:TextBox ID="dvpayee" runat="server"  CssClass="form-control" Visible="True" Width="150px"></asp:TextBox>
               
                                       </td>

                               
          <td>
<asp:Label ID="Label28" runat="server" Text="Dividend Bank"></asp:Label>
                      <br />
        <asp:DropDownList ID="divbank" CssClass="form-control" runat="server" OnSelectedIndexChanged="divbank_SelectedIndexChanged" AutoPostBack="True"  Width="150px"></asp:DropDownList>
               
                                       </td>
                                <td>
<asp:Label ID="Label25" runat="server" Text="Dividend Branch"></asp:Label>
                      <br />
      <asp:DropDownList ID="divbranch" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
               
                                       </td>   <td>
<asp:Label ID="Label26" runat="server" Text="Dividend AccountNo"></asp:Label>
                      <br />
                 <asp:TextBox ID="txtAccD" runat="server"  CssClass="form-control" Visible="True" Width="150px" ></asp:TextBox>
                                       </td>
                                   </tr>
                                   <tr>
                                    
                                       <td>
<asp:Label ID="Label36" runat="server" Text="Dividend Account Type"></asp:Label>
                      <br />
                 <asp:DropDownList ID="divacctype" runat="server"  CssClass="form-control" Visible="True" Width="150px"></asp:DropDownList>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                                    <tr>
                                       <td>
                                           <asp:Label ID="Label38" CssClass="control-label" runat="server" Text="3. Third Party Mandate" Font-Bold="True"></asp:Label>
                                       </td>
                                       <td>
                                         <asp:Label ID="Label63" CssClass="control-label col-md-6" runat="server" Text="Type*"></asp:Label>
                       <br />
                                      <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList3_SelectedIndexChanged">
                                           <asp:ListItem Text="BO Account" Value="LI"></asp:ListItem>
                                           <asp:ListItem Text="External Account" Value="LE"></asp:ListItem>
                                      </asp:RadioButtonList>
                                       </td>
                                       <td>

                                           &nbsp;</td>
                                       <td>

                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label64" runat="server" Text="Search Accounts"></asp:Label>
                                           <br />
                                           <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" ValueType="System.String" TextField="Fullnames" ValueField="ID" OnSelectedIndexChanged="ASPxComboBox1_SelectedIndexChanged" Theme="Glass"></dx:ASPxComboBox>
                                       </td>
                                       <td>
                                           
                          <asp:Label ID="Label37" CssClass="control-label col-md-8" runat="server" Text="IDType*"></asp:Label>
                                           <br />
                          <asp:DropDownList ID="drpID2" CssClass="form-control" runat="server" Width="150px">
                             

                          </asp:DropDownList>
                      </td> <td>
                                                 <strong>
                                                 <asp:Label ID="Label39" CssClass="control-label col-md-8" runat="server" Text="Dividend Payee ID*"></asp:Label>
                                        
                                                 </strong>
                                        
                             <asp:TextBox ID="dividpayee" CssClass="form-control"  runat="server" Width="150px"></asp:TextBox>
                                       </td>
                                      <td>
<asp:Label ID="Label45" runat="server" Text="Mandate Bank"></asp:Label>
                      <br />
        <asp:DropDownList ID="manBank" CssClass="form-control" runat="server" OnSelectedIndexChanged="manBank_SelectedIndexChanged" AutoPostBack="True"  Width="150px"></asp:DropDownList>
               
                                       </td>
                             
                                       </tr>
                                   <tr>
   <td>
<asp:Label ID="Label46" runat="server" Text="Mandate Branch"></asp:Label>
                      <br />
      <asp:DropDownList ID="manBranch" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
               
                                       </td> 
                                         <td>
<asp:Label ID="Label47" runat="server" Text="Mandate AccountNo"></asp:Label>
                      <br />
                 <asp:TextBox ID="manAccount" runat="server"  CssClass="form-control" Visible="True" Width="150px" ></asp:TextBox>
                                       </td>
                                          <td>
<asp:Label ID="Label48" runat="server" Text="Mandate Names"></asp:Label>
                      <br />
                 <asp:TextBox ID="manNames" runat="server"  CssClass="form-control" Visible="True" Width="150px" ></asp:TextBox>
                                       </td>
          
                      <td>
                                  <asp:Label ID="Label49" runat="server" Text="Address"></asp:Label>
                           <asp:TextBox ID="manAddress" runat="server" CssClass="form-control"  Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                      </td>                 
                                   </tr>

                                         </table>
                             <dx style="float:right">
                    <dx:ASPxButton ID="dxIdentificationBack" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxIdentificationBack_Click" ></dx:ASPxButton>
                    <dx:ASPxButton ID="dxTrading" runat="server" Theme="Moderno" CssClass="btn btn-primary" Text="Next" OnClick="dxTrading_Click"></dx:ASPxButton>


                </dx>
                </div>
                   <div role="tabpanel" class="tab-pane" id="trading">
     
                               <table class="table table-striped">
                      <tr>
                          <td>
                                   <asp:Label ID="Label33" CssClass="control-label col-md-4" runat="server" Text="Currency"></asp:Label>
                              <br />
                          <asp:DropDownList ID="drpCurr" CssClass="form-control" runat="server" Width="150px">
                             
                          </asp:DropDownList>
                          </td>
                            <td>
                                <asp:Label ID="Label40" CssClass="control-label col-md-2" Width="300px" runat="server" Text="Payment Method"></asp:Label>
                                <asp:RadioButtonList ID="idPay" runat="server" Width="450px" RepeatDirection="Horizontal" OnSelectedIndexChanged="idPay_SelectedIndexChanged" AutoPostBack="True">
                                     <asp:ListItem Text="CASH" Value="CASH" ></asp:ListItem>
                                     <asp:ListItem Text="ECOCASH" Value="ECOCASH" ></asp:ListItem>
                                   
                                     <asp:ListItem Text="NETCASH" Value="NETCASH" ></asp:ListItem>
                                     <asp:ListItem Text="TELECASH" Value="TELECASH" ></asp:ListItem>
                                    <asp:ListItem Text="ONE WALLET" Value="ONE WALLET" ></asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          </td>
                      

                                       
                                      
<//tr>
                                      <tr>
                                          <td>
  <asp:Label ID="lblMobi" runat="server" Text="Mobile Wallet Number" Visible="False"></asp:Label>
                         

                          </td>
                                          <td>
 <asp:TextBox ID="mobilewallet" runat="server"  CssClass="form-control" Visible="False" Width="150px" ></asp:TextBox>
                              
                            <td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                                     <tr>
                                       <td>
                                           <strong>Attachments</strong></td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                            <tr>
                                
                                <td>
                                <br />
                                <asp:FileUpload ID="FileUpload" runat="server" />
                                    <asp:Button ID="upload2" CssClass="btn btn-primary" runat="server" Text="Upload" OnClick="upload2_Click" />
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td></td>
                                <td></td>
                             </tr>
<tr>
                                  <td>
                                      <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                  </td>
                                       <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                           <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                       </td>
                                   </tr>
                                         </table>
                       
                    <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="Accounts_DocumentsID"  AutoGenerateColumns="false"  runat="server" Width="1029px" OnSelectionChanged="ASPxGridView1_SelectionChanged" EnableCallBacks="False" Theme="Glass" OnRowDeleting="ASPxGridView1_RowDeleting">
                        <SettingsPager AlwaysShowPager="True" PageSize="10">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    

                        <SettingsCommandButton>
                            <DeleteButton   ButtonType="Button">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                       <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="Accounts_DocumentsID" ReadOnly="True" VisibleIndex="0">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="doc_generated" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ContentType" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
       
                            <dx:GridViewCommandColumn  ShowDeleteButton="True"  VisibleIndex="10">
                </dx:GridViewCommandColumn>
            </Columns>

                      

                       

                    </dx:ASPxGridView>
                      </center>        
                       </div>
                               <dx style="float:right">
                   <%-- <dx:ASPxButton ID="dxBankingBack" CssClass="btn btn-primary" runat="server" Theme="Moderno" Text="Previous" OnClick="dxBankingBack_Click" ></dx:ASPxButton>
        --%>


                </dx>

                </div>
            </div>
        </div>
        

     <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>