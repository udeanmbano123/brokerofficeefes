﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompanyReportRange.aspx.cs" Inherits="BrokerOffice.Reporting.CompanyReportRange" %>

<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left:15%;margin-right:10%;background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <l style="color:white">Top 20 Company Earners</l>
       
    </div> 
        <button class="btn btn-primary"><a href="../Reporting/CompanyEarners.aspx" style="color:white">Back To Search</a></button>
        </div>
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.XtraCompanyRange" Theme="Moderno"></dx:ASPxDocumentViewer>
   </asp:content>
