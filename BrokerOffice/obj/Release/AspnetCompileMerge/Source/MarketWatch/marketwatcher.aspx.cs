﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.MarketWatch
{
    public partial class marketwatcher : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();

        public SqlConnection myConnection = new SqlConnection();
        public SqlDataAdapter adp;
        public SqlCommand cmd;
        public static string nm="";
        protected void Page_Load(object sender, EventArgs e)
        {
            loadmarketwatch();
            loadmarketwatchZSE();
            ASPxLabel1.Text = "Market Status: "+ marketstatus();
            if (!this.IsPostBack)
            {
                Timer2.Enabled = true;
                Timer2.Interval = 15000;
                RadStl.SelectedValue = "ZSE";
                if (RadStl.SelectedValue == "ZSE")
                {

                    Panel3.Visible = true;
                    Panel2.Visible = false;
                    Panel1.Visible = false;
                    Panel4.Visible = false;
                }
                else if (RadStl.SelectedValue == "FINSEC")
                {
                    Panel3.Visible = false;
                    Panel1.Visible = true;
                    Panel4.Visible = false;
                }
            }
        }

        public string marketstatus()
        {

            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("marketstatus", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = JsonConvert.DeserializeObject<string>(response.Content);
         
            
           
            
            return validate;
        }

        public void loadmarketwatch()
        {
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("loadmarketwatch", Method.GET);
            IRestResponse response = client.Execute(request);
            var validate = response.Content;
            List<marketwatch> dataList = JsonConvert.DeserializeObject<List<marketwatch>>(validate);

            try
            {
                GridView1.DataSource = dataList;
                GridView1.DataBind();
               
            }
            catch (Exception)
            {

               
            }
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                nm = c.role;
            }
            ASPxLabel1.Text = "Market Status: " + marketstatus();
            
        }

        public void loadmarketwatchZSE()
        {
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("marketwatchzse", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<marketwatchzse> dataList = JsonConvert.DeserializeObject<List<marketwatchzse>>(validate);

            try
            {
                GridView4.DataSource = dataList;
                GridView4.DataBind();
            }
            catch (Exception)
            {


            }
          
        }

        public void loadbuys(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
               vx = c.BrokerCode;
            }

            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("loadbuys/{company}/{broker}", Method.GET);
            request.AddUrlSegment("company",company);
            request.AddUrlSegment("broker","EFES");
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<loadbuys> dataList = JsonConvert.DeserializeObject<List<loadbuys>>(validate);

            try
            {
                GridView2.DataSource = dataList;
                GridView2.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public void loadsells(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                vx = c.BrokerCode;
            }
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("loadsells/{company}/{broker}", Method.GET);
            request.AddUrlSegment("company", company);
            request.AddUrlSegment("broker","EFES");
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<loadbuys> dataList = JsonConvert.DeserializeObject<List<loadbuys>>(validate);

            try
            {
                GridView3.DataSource = dataList;
                GridView3.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label5.Text = GridView1.SelectedValue.ToString();
            loadbuys(GridView1.SelectedValue.ToString());
            loadsells(GridView1.SelectedValue.ToString());
            Panel2.Visible = true;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        
        protected void RadStl_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (RadStl.SelectedValue == "ZSE")
            {
                //msgbox("ZSE");
                Panel3.Visible = true;
                Panel2.Visible = false;
                Panel1.Visible = false;
                Panel4.Visible = false;
            }
            else if (RadStl.SelectedValue == "FINSEC")
            {
                //msgbox("FIN");
                Panel3.Visible = false;
                Panel1.Visible = true;
                Panel4.Visible = false;
            }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {

            if (RadStl.SelectedValue == "ZSE")
            {

                Panel3.Visible = true;
                Panel2.Visible = false;
                Panel1.Visible = false;
               
                try
                {
                    Label9.Text = GridView4.SelectedValue.ToString();
                    loadbuysN(GridView4.SelectedValue.ToString());
                    loadsellsN(GridView4.SelectedValue.ToString());
                    Panel4.Visible = true;
                }
                catch (Exception)
                {

                    loadmarketwatchZSE();
                }
            }
            else if (RadStl.SelectedValue == "FINSEC")
            {
                Panel3.Visible = false;
                Panel1.Visible = true;
                try
                {
                    Label5.Text = GridView1.SelectedValue.ToString();
                    loadbuys(GridView1.SelectedValue.ToString());
                    loadsells(GridView1.SelectedValue.ToString());
                    Panel2.Visible = true;
                }
                catch (Exception)
                {

                    loadmarketwatch();
                }
                
            }


        }

        protected void GridView4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label9.Text = GridView4.SelectedValue.ToString();
            loadbuysN(GridView4.SelectedValue.ToString());
            loadsellsN(GridView4.SelectedValue.ToString());
            Panel4.Visible = true;
        }
        public void loadsellsN(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "EFES";
          
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select *, case broker_code when 'EFES' then  '~/images/yellow.gif' else NULL end as [mine]  from Pre_Order where Company='" + company + "' and ordertype='SELL' and CAST(Create_date as date)=CAST('" + DateTime.Now.ToString("dd-MMM-yyyy") + "' as date)", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView6.DataSource = ds;
                GridView6.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

        public void loadbuysN(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "EFES";
            
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select *, case broker_code when 'EFES' then  '~/images/yellow.gif' else NULL end as [mine]  from Pre_Order where Company='"+ company + "' and ordertype='BUY' and CAST(Create_date as date)=CAST('" + DateTime.Now.ToString("dd-MMM-yyyy") + "' as date)", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView5.DataSource = ds;
                GridView5.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

  

        protected void GridView6_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView6.PageIndex = e.NewPageIndex;
            Label9.Text = GridView4.SelectedValue.ToString();
            loadsellsN(GridView4.SelectedValue.ToString());
        }

        protected void GridView5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView5.PageIndex = e.NewPageIndex;
            Label9.Text = GridView4.SelectedValue.ToString();
            loadbuysN(GridView4.SelectedValue.ToString());

        }
    }
}