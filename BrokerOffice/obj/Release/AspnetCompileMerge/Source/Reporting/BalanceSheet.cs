﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Windows.Forms;

namespace BrokerOffice.Reporting
{
    public partial class BalanceSheet : DevExpress.XtraReports.UI.XtraReport
    {
        public BalanceSheet()
        {
            InitializeComponent();
        }

        private void groupFooterBand2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
   }

        private void xrLabel11_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
          
        }

        private void xrLabel6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void xrLabel2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XtraReport r = new XtraReport();
            r.LoadLayout("xrSubreport1.Plz");
            //r.Parameters["AccountID"].Value = e.Brick.Value;
            ReportPrintTool pt = new ReportPrintTool(r);
            pt.ShowPreview();
            //MessageBox.Show(((XRLabel)sender).Text);
        }

        private void xrLabel2_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            //XtraReport r = new XtraReport();
            //r.LoadLayout("xrSubreport1.Plz");
            ////r.Parameters["AccountID"].Value = e.Brick.Value;
            //ReportPrintTool pt = new ReportPrintTool(r);
            //pt.ShowPreview();
            //XtraMessageBox.Show(((XRLabel)sender).Text);
            ((XRLabel)sender).Text= "FUK ME";
        }

        private void xrLabel3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XtraChart detailReport = new XtraChart();

            // Obtain the current category's ID and Name from the e.Brick.Value property, 

            // Show the detail report in a new modal window. 
            ReportPrintTool pt = new ReportPrintTool(detailReport);
            pt.ShowPreviewDialog();
        }

        private void xrLabel3_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XtraChart detailReport = new XtraChart();

            // Obtain the current category's ID and Name from the e.Brick.Value property, 
           
            // Show the detail report in a new modal window. 
            ReportPrintTool pt = new ReportPrintTool(detailReport);
            pt.ShowPreviewDialog();
        }

        private void xrLabel3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRLabel)sender).Tag = GetCurrentRow();
        }

        private void xrLabel3_PreviewMouseMove(object sender, PreviewMouseEventArgs e)
        {
            e.PreviewControl.Cursor = Cursors.Hand;
        }

        private void xrLabel6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRLabel)sender).Tag = GetCurrentRow();

        }

        private void xrLabel2_PreviewMouseMove(object sender, PreviewMouseEventArgs e)
        {
            e.PreviewControl.Cursor = Cursors.Hand;

        }
    }
    }

