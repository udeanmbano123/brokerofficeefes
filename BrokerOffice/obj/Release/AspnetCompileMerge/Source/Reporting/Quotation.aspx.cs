﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class Quotation : System.Web.UI.Page
    {
        public string Account = "";
        public decimal max = 0;
        public decimal qty = 0;
        public string board = "";
        public string security = "";
        private string broker = "";
        public string type = "";
        public string cc = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Init(object sender, EventArgs e)
        {
          
            

            Account = Request.QueryString["Account"];
            max = Convert.ToDecimal(Request.QueryString["max"]);
            qty = Convert.ToDecimal(Request.QueryString["qty"]);
            security = Request.QueryString["Security"].ToString();
            board= Request.QueryString["Board"].ToString();
            type= Request.QueryString["Type"].ToString().ToUpper();
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                foreach (var p in c)
                {
                    broker = p.BrokerCode;
                }

                var d = db.Account_Creations.ToList().Where(a => a.CDSC_Number==Account);
                foreach (var p in d)
                {
                    cc = p.ClientType;
                }

                XtraQuotation report = new XtraQuotation();
                report.Parameters["Account"].Value =Account;

                report.Parameters["Max"].Value = max;

                report.Parameters["Quantity"].Value = qty;

                report.Parameters["Board"].Value = board;
                report.Parameters["Security"].Value = security;
                report.Parameters["Broker"].Value = broker;
                report.Parameters["Type"].Value = type;
               report.Parameters["ClientType"].Value = cc;
                report.Parameters["BR"].Value = max*qty;

                if (type != "BUY")
                {
                    report.Parameters["Stamp"].Value="Stamp duty";
                }else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                
                report.RequestParameters = false;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}