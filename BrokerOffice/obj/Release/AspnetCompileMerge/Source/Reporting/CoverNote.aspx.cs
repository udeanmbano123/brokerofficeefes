﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class CoverNote : System.Web.UI.Page
    {
        public string id = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["id"];
    


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraCover report = new XtraCover();
                report.Parameters["ID"].Value = id;

                report.CreateDocument();
                XtraCover2 report2 = new XtraCover2();
                report2.Parameters["ID"].Value = id;
                report2.CreateDocument();
                //report.Parameters["yourParameter2"].Value = secondValue;
                int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
                for (int g = 0; g < minPageCount; g++)
                {
                    report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
                }
                if (report2.Pages.Count != minPageCount)
                {
                    for (int g = minPageCount; g < report2.Pages.Count; g++)
                    {
                        report.Pages.Add(report2.Pages[g]);
                    }
                }

                // Reset all page numbers in the resulting document. 
                report.PrintingSystem.ContinuousPageNumbering = true;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}