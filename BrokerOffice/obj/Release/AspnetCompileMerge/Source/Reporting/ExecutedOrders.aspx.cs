﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class ExecutedOrders : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string broker = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["date"];
            date2 = Request.QueryString["date2"];
          

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
              
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName.ToString());
                foreach (var d in c)
                {
                    broker = d.BrokerCode;
                }
                int z = 0;

                try
                {
                    z = db.Tbl_MatchedOrders.ToList().Where(a=>a.BrokerCode==broker).Max(a => a.RefID);

                }
                catch (Exception)
                {

                    z = 0;
                }
                string nm = "";

                var cc = db.Agents.ToList().Where(a => a.AgentCode==broker);
                foreach (var d in c)
                {
                    nm= d.BrokerName;
                }

               
                XtraExecutedOrder report = new XtraExecutedOrder();
                report.Parameters["FDate"].Value = date;
                report.Parameters["TDate"].Value =date2;
                report.Parameters["Broker"].Value = broker;
                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}