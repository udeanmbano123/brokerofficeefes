﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class ApplicationsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Applications
        public async Task<ActionResult> Index()
        {
            return View(await db.ApplicationCaptures.ToListAsync());
        }

        // GET: Applications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationCapture applicationCapture = await db.ApplicationCaptures.FindAsync(id);
            if (applicationCapture == null)
            {
                return HttpNotFound();
            }
            return View(applicationCapture);
        }

    
        // GET: Applications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationCapture applicationCapture = await db.ApplicationCaptures.FindAsync(id);
            if (applicationCapture == null)
            {
                return HttpNotFound();
            }
            return View(applicationCapture);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationCapture applicationCapture = await db.ApplicationCaptures.FindAsync(id);
            db.ApplicationCaptures.Remove(applicationCapture);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the application";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
