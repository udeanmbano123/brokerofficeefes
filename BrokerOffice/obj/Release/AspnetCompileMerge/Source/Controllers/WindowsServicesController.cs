﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class WindowsServicesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: WindowsServices
        public async Task<ActionResult> Index()
        {
            return View(await db.WindowsServices.ToListAsync());
        }

        // GET: WindowsServices/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WindowsService windowsService = await db.WindowsServices.FindAsync(id);
            if (windowsService == null)
            {
                return HttpNotFound();
            }
            return View(windowsService);
        }

        // GET: WindowsServices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WindowsServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "WindowsServiceID,AccountName,AccountNumber,Action")] WindowsService windowsService)
        {
            if (ModelState.IsValid)
            {
                db.WindowsServices.Add(windowsService);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(windowsService);
        }

        // GET: WindowsServices/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WindowsService windowsService = await db.WindowsServices.FindAsync(id);
            if (windowsService == null)
            {
                return HttpNotFound();
            }
            return View(windowsService);
        }

        // POST: WindowsServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "WindowsServiceID,AccountName,AccountNumber,Action")] WindowsService windowsService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(windowsService).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(windowsService);
        }

        // GET: WindowsServices/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WindowsService windowsService = await db.WindowsServices.FindAsync(id);
            if (windowsService == null)
            {
                return HttpNotFound();
            }
            return View(windowsService);
        }

        // POST: WindowsServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WindowsService windowsService = await db.WindowsServices.FindAsync(id);
            db.WindowsServices.Remove(windowsService);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
