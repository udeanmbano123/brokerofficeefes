﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class StockTypesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: StockTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.StockTypes.ToListAsync());
        }

        // GET: StockTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockTypes stockTypes = await db.StockTypes.FindAsync(id);
            if (stockTypes == null)
            {
                return HttpNotFound();
            }
            return View(stockTypes);
        }

        // GET: StockTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StockTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StockTypesID,stockname")] StockTypes stockTypes)
        {
            if (ModelState.IsValid)
            {
                db.StockTypes.Add(stockTypes);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Stock Type";

                return RedirectToAction("Index");
            }

            return View(stockTypes);
        }

        // GET: StockTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockTypes stockTypes = await db.StockTypes.FindAsync(id);
            if (stockTypes == null)
            {
                return HttpNotFound();
            }
            return View(stockTypes);
        }

        // POST: StockTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StockTypesID,stockname")] StockTypes stockTypes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockTypes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Stock type";

                return RedirectToAction("Index");
            }
            return View(stockTypes);
        }

        // GET: StockTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockTypes stockTypes = await db.StockTypes.FindAsync(id);
            if (stockTypes == null)
            {
                return HttpNotFound();
            }
            return View(stockTypes);
        }

        // POST: StockTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            StockTypes stockTypes = await db.StockTypes.FindAsync(id);
            db.StockTypes.Remove(stockTypes);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Stock type";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
