﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO.security;
using BrokerOffice.Models;
using BrokerOffice.DAO;
using WebMatrix.WebData;
using Newtonsoft.Json;
using RestSharp;
using BrokerOffice.CDSMOdel;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "Admin,User,General,BrokerAdmin2,BrokerAdmin,Approver,ApproverUser", NotifyUrl = "/UnauthorizedPage")]

    public class StatisticsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        public SqlConnection myConnection = new SqlConnection();
        public SqlDataAdapter adp;
        public SqlCommand cmd;
        public SBoardContext vv = new SBoardContext();
        // GET: Statistics

        public ActionResult Index()
        {
            
            ViewBag.Role = Secs();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Company")] Company registerreport)
        {

            if (ModelState.IsValid && registerreport.company != "--Select--" && registerreport.company != "All")
            {
               return Redirect("~/Statistics/Company?sec=" + registerreport.company);
            }else
            {
                return Redirect("~/Statistics/CompanyAll?sec=" + registerreport.company);

            }
            ViewBag.Role = null;
            ViewBag.Role = Secs();
            return View();
        }
        public ActionResult Company(string sec)
        {
            string gen = sec;
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("Security", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Securities> dataList = JsonConvert.DeserializeObject<List<Securities>>(validate);
            string topo = "";
            var bb = dataList.ToList().Where(a=>a.Company==sec);
            foreach (var po in bb)
            {
                ViewBag.Com = po.Fnam;
                topo = po.Fnam;
            }
          
            var p = vv.Users.ToList().Where(a=>a.Email==WebSecurity.CurrentUserName);
            string nme = "";
            foreach (var c in p)
            {
                nme = c.BrokerCode;
            }

            var client2 = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request2 = new RestRequest("securitytotals/{broker}/{company}", Method.GET);
            request2.AddUrlSegment("broker","EFES");
            request2.AddUrlSegment("company",sec);
            IRestResponse response2 = client2.Execute(request2);
            string validate2 = response2.Content;
            List<securitytotals> dataList2 = JsonConvert.DeserializeObject<List<securitytotals>>(validate2);

            foreach (var myRow in dataList2)
            {
                ViewBag.lbltotal_registrations = myRow.TotalRegistration;
                ViewBag.lbltotal_funds = "$" + myRow.totalfunds;
                ViewBag.lbldepositoryholdings = myRow.CDCHOLDINGS;
                ViewBag.lbltotal_holdings = myRow.totalholdings;
                ViewBag.lblincomingsells = myRow.Incomingsells;
                ViewBag.lblincomingbuys = myRow.Incomingbuys;
                ViewBag.lblpendingsettlement = myRow.Pendingsettlement;
                ViewBag.lbltodaysettled = myRow.todaysettlement;

            }

          
            ViewBag.ListB = db.Account_Creations.ToList().OrderByDescending(a=>a.ID_).Take(7);
            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                var client77 = new RestClient("http://197.155.235.78:8447/EFESApi");
                var request77 = new RestRequest("BrokersList", Method.GET);
                IRestResponse response77 = client.Execute(request77);
                string validate77 = response77.Content;
                List<Broker> dataList77 = JsonConvert.DeserializeObject<List<Broker>>(validate77);


                var dbsel = from s in dataList77
                            select s;
                //updatebilling();
                foreach (var d in dbsel)
                {
                    if (vx == d.Company_Code)
                    {
                        n1 = d.Company_name;
                        n2 = d.Company_Code;
                    }
                }
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            n2 = "EFES";
            ViewBag.RegC = db.Account_Creations.ToList().Where(a => a.Broker == n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a=>a.update_type == "TOUPDATE").Count();
            int MB1= db.PreOrders.ToList().Where(a => a.OrderType == "BUY"  && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();

            int MB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY"  && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus== "MATCHED").Count();
            ViewBag.MB =MB1+MB2;
            int MS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();

            int MS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();
            ViewBag.MS = MS1+MS2;
            int SB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            int SB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            ViewBag.SB = SB1 + SB2;
            int SS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            int SS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            ViewBag.SS = SS1 + SS2;
            int PB1=db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "")).Count();
           
                int PB2= db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "")).Count();

            ViewBag.PB = PB1 + PB2;
            int PS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower()).Count();

            int PS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower()).Count();

            ViewBag.PS = PS1 + PS2;

            return View();
        }

        public ActionResult CompanyAll(string sec)
        {
            sec = "All";
            string gen = sec;
            string topo ="All";
            ViewBag.Com = "All";

             var p = vv.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string nme = "";
            foreach (var c in p)
            {
                nme = c.BrokerCode;
            }

           

            var client2 = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request2 = new RestRequest("securitytotals/{broker}/{company}", Method.GET);
            request2.AddUrlSegment("broker", "EFES");
            request2.AddUrlSegment("company", sec);
            IRestResponse response2 = client2.Execute(request2);
            string validate2 = response2.Content;
            List<securitytotals> dataList2 = JsonConvert.DeserializeObject<List<securitytotals>>(validate2);

            foreach (var myRow in dataList2)
            {
                ViewBag.lbltotal_registrations = myRow.TotalRegistration;
                ViewBag.lbltotal_funds = "$" + myRow.totalfunds;
                ViewBag.lbldepositoryholdings = myRow.CDCHOLDINGS;
                ViewBag.lbltotal_holdings = myRow.totalholdings;
                ViewBag.lblincomingsells = myRow.Incomingsells;
                ViewBag.lblincomingbuys = myRow.Incomingbuys;
                ViewBag.lblpendingsettlement = myRow.Pendingsettlement;
                ViewBag.lbltodaysettled = myRow.todaysettlement;

            }


            ViewBag.ListB = db.Account_Creations.ToList().OrderByDescending(a => a.ID_).Take(7);
            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                

               
                //updatebilling();
                
                        n1 = "Old Mutual Securities";
                        n2 = "OMSEC";
                
                
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            n2 = "EFES";
            ViewBag.RegC = db.Account_Creations.ToList().Where(a => a.Broker == n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a => a.update_type == "TOUPDATE").Count();
            int MB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "MATCHED").Count();

            int MB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "MATCHED").Count();
            ViewBag.MB = MB1 + MB2;
            int MS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "MATCHED").Count();

            int MS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "MATCHED").Count();
            ViewBag.MS = MS1 + MS2;
            int SB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "Settled").Count();

            int SB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "Settled").Count();

            ViewBag.SB = SB1 + SB2;
            int SS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "Settled").Count();

            int SS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "Settled").Count();

            ViewBag.SS = SS1 + SS2;
            int PB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" ).Count();

            int PB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY").Count();

            ViewBag.PB = PB1 + PB2;
            int PS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL").Count();

            int PS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL").Count();

            ViewBag.PS = PS1 + PS2;

            return View();
        }

        public ActionResult MarketWatch()
        {
            //var client = new RestClient("http://192.168.3.245/BrokerService/");
            //var request = new RestRequest("MarketWatch", Method.GET);
            //IRestResponse response = client.Execute(request);
            //string validate = response.Content;
            //List<marketwatch> dataList = JsonConvert.DeserializeObject<List<marketwatch>>(validate);
            //ViewBag.ListB = dataList;
            return Redirect("~/MarketWatch/marketwatcher.aspx");
        }
        public ActionResult MarketWatchDrill(string sec)
        {
            //var client = new RestClient("http://192.168.3.245/BrokerService/");
            //var request = new RestRequest("MarketWatchD/{s}", Method.GET);
            //request.AddUrlSegment("s", sec.Replace(".", "-"));
            //IRestResponse response = client.Execute(request);
            //string validate = response.Content;
            //List<LiveTR> dataList = JsonConvert.DeserializeObject<List<LiveTR>>(validate);
            //ViewBag.ListB = dataList.ToList().Where(a => a.OrderType.ToLower() == ("BUY").ToLower()).OrderByDescending(a => a.BasePrice);
            //ViewBag.ListS = dataList.ToList().Where(a => a.OrderType.ToLower() == ("SELL").ToLower()).OrderBy(a => a.BasePrice);
            //ViewBag.sec = sec;
            return View();
        }
        public List<Brokers> GetEarners(string p)
        {
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("registrations/{s}", Method.GET);
            request.AddUrlSegment("s", "EFES");
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Brokers> dataList = JsonConvert.DeserializeObject<List<Brokers>>(validate);

            return dataList;
        }

        public List<SelectListItem> Secs()
        {
            var client = new RestClient("http://197.155.235.78:8447/EFESApi");
            var request = new RestRequest("Security", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Securities> dataList = JsonConvert.DeserializeObject<List<Securities>>(validate);
            List<SelectListItem> phy = new List<SelectListItem>();

            phy.Add(new SelectListItem { Text = "All", Value = "All" });

            foreach (var b in dataList)
            {
                phy.Add(new SelectListItem { Text = b.Fnam, Value = b.Company });

            }

            return phy;
        }

    }
}