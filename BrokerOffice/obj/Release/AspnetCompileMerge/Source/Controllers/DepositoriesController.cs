﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class DepositoriesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Depositories
        public async Task<ActionResult> Index()
        {
            return View(await db.Depositorys.ToListAsync());
        }

        // GET: Depositories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depository depository = await db.Depositorys.FindAsync(id);
            if (depository == null)
            {
                return HttpNotFound();
            }
            return View(depository);
        }

        // GET: Depositories/Create
        public ActionResult Create()
        {

            ViewBag.Departments = Countries();

            ViewBag.City=CitiesD();
            ViewBag.State = regionD();
            return View();
        }

        [HttpPost]
        public JsonResult AjaxMethod(string type, int value)
        {
           Depository model = new Depository();
            switch (type)
            {
                case "Country":
                    ViewBag.City = Regions(value.ToString());
                    break;
                case "State":
                    ViewBag.City = Cities(value.ToString());
                 break;
              
            }
            return Json(ViewBag.City);
        }


        // POST: Depositories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DepositoryID,Code,Name,Country,City,Address,State,CountryID,StateID,CityID,PhoneNumber")] Depository depository)
        { int p = Convert.ToInt32(depository.Country);
          int pp = Convert.ToInt32(depository.State);
          int ppp= Convert.ToInt32(depository.City);
            if (ModelState.IsValid)
            {
                var k = db.Countriess.ToList().Where(a => a.id == p);

                var q = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='"+ ppp +"'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    depository.Country = d.name;
                }

                foreach (var d in q)
                {
                    depository.State = d.name;
                }

                foreach (var f in cs4)
                {
                    depository.City = f.name;
                }
                depository.CountryID = p;
                depository.StateID = pp;
                depository.CityID = ppp;
                db.Depositorys.Add(depository);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the depository";

                return RedirectToAction("Index");
            }
           
            string v1, v2;

            var c = db.Countriess.ToList().Where(a => a.id == p);
            foreach(var d in c)
            {
                ViewBag.Departments = CountriesM(d.name,d.id.ToString());
            }
            ViewBag.City = CitiesM(depository.City, ppp.ToString());
            ViewBag.State = RegionsM(depository.State, pp.ToString());
            return View(depository);
        }

        // GET: Depositories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depository depository = await db.Depositorys.FindAsync(id);
            if (depository == null)
            {
                return HttpNotFound();
            }
            int my = 0;
           
         
                ViewBag.Departments = CountriesM(depository.Country, depository.CountryID.ToString());
         
                ViewBag.State = RegionsM(depository.State, depository.StateID.ToString());
            
            ViewBag.City = CitiesM(depository.City, depository.CityID.ToString());
          
                        
       
            return View(depository);
        }

        // POST: Depositories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DepositoryID,Code,Name,Country,City,Address,State,CountryID,StateID,CityID,PhoneNumber")] Depository depository)
        {
            int p = 0;

            try
            {
                p = Convert.ToInt32(depository.Country);
            }
            catch (Exception)
            {

                p = Convert.ToInt32(depository.CountryID);
            }

            int pp = 0;
            int ppp = 0;

            try
            {
                pp = Convert.ToInt32(depository.State);
                ppp = Convert.ToInt32(depository.City);
            }
            catch (Exception)
            {


            }

            if (pp == 0)
            {
                pp = Convert.ToInt32(depository.StateID);
                ppp = Convert.ToInt32(depository.CityID);
            }
            if (ModelState.IsValid)
            {
                var k = db.Countriess.ToList().Where(a => a.id == p);

                var q = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='" + ppp + "'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    depository.Country = d.name;
                }

                foreach (var d in q)
                {
                    depository.State = d.name;
                }

                foreach (var f in cs4)
                {
                    depository.City = f.name;
                }
                depository.CountryID = p;
                depository.StateID = pp;
                depository.CityID = ppp;
                db.Entry(depository).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Depository";

                return RedirectToAction("Index");
            }
            
            string v1, v2;

            var c = db.Countriess.ToList().Where(a => a.id == p);
            foreach (var d in c)
            {
                ViewBag.Departments = CountriesM(d.name, d.id.ToString());
            }
            ViewBag.City = CitiesM(depository.City, ppp.ToString());
            ViewBag.State = RegionsM(depository.State, pp.ToString());

            return View(depository);
        }

        // GET: Depositories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depository depository = await db.Depositorys.FindAsync(id);
            if (depository == null)
            {
                return HttpNotFound();
            }
            return View(depository);
        }

        // POST: Depositories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Depository depository = await db.Depositorys.FindAsync(id);
            db.Depositorys.Remove(depository);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Depository";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public List<SelectListItem> CountriesM(string s1,string s2)
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            dept.Add(new SelectListItem { Text = s1, Value =s2});
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }
        public List<SelectListItem> Countries()
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }

        public List<SelectListItem> CitiesD()
        {

          
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a City", Value = "Select a City" });
            return phy;
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a State", Value = "Select a State" });
            return phy;
        }

        public List<SelectListItem> Regions(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.regions where u.country_id == s select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> Cities(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Cities where u.region_id==g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> CitiesM(string s,string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text =s, Value = v1});
            var query = from u in db.Cities where u.region_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> RegionsM(string s, string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.regions where u.country_id == g.ToString() select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPhysiansByDepartment(string deptId)
        {
            //Your Code For Getting Physicans Goes Here
            int g = Convert.ToInt32(deptId);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Cities where u.country_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.name.ToString() });
                }
            }
            return Json(phy, JsonRequestBehavior.AllowGet);
        }

    }
}
