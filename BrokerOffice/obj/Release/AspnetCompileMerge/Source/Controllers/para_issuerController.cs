﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class para_issuerController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: para_issuer
        public async Task<ActionResult> Index()
        {
            return View(await db.para_issuers.ToListAsync());
        }

        // GET: para_issuer/Details/5
        public async Task<ActionResult> Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }
            return View(para_issuer);
        }

        // GET: para_issuer/Create
        public ActionResult Create()
        {

            ViewBag.Departments = Countries();

            ViewBag.City = CitiesD();
            ViewBag.State = regionD();
            ViewBag.Sel = Registrars();
            return View();
        }
        [HttpPost]
        public JsonResult AjaxMethod(string type, int value)
        {
            Depository model = new Depository();
            switch (type)
            {
                case "Country":
                    ViewBag.City = Regions(value.ToString());
                    break;
                case "State":
                    ViewBag.City = Cities(value.ToString());
                    break;

            }
            return Json(ViewBag.City);
        }
        // POST: para_issuer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "para_issuerID,Company,Date_created,Issued_shares,Status,registrar,Add_1,City,Country,Contact_Person,Telephone,Cellphone,ISIN,date_listed,email,website,State,CountryID,StateID,CityID")] para_issuer para_issuer)
        {
            int p = Convert.ToInt32(para_issuer.Country);
            int pp = Convert.ToInt32(para_issuer.State);
            int ppp = Convert.ToInt32(para_issuer.City);
            if (ModelState.IsValid)
            {
                var k = db.Countriess.ToList().Where(a => a.id == p);

                var q = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='" + ppp + "'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    para_issuer.Country = d.name;
                }

                foreach (var d in q)
                {
                    para_issuer.State = d.name;
                }

                foreach (var f in cs4)
                {
                    para_issuer.City = f.name;
                }
                para_issuer.CountryID = p;
                para_issuer.StateID = pp;
                para_issuer.CityID = ppp;

                para_issuer.Date_created = DateTime.Now;
                db.para_issuers.Add(para_issuer);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Company";

                return RedirectToAction("Index");
            }
            string v1, v2;

            var c = db.Countriess.ToList().Where(a => a.id == p);
            foreach (var d in c)
            {
                ViewBag.Departments = CountriesM(d.name, d.id.ToString());
            }
            ViewBag.City = CitiesM(para_issuer.City, ppp.ToString());
            ViewBag.State = RegionsM(para_issuer.State, pp.ToString());
            ViewBag.Sel = RegistrarsM(para_issuer.Company, para_issuer.Company);
            return View(para_issuer);
        }

        // GET: para_issuer/Edit/5
        public async Task<ActionResult> Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }

            ViewBag.Departments = CountriesM(para_issuer.Country, para_issuer.CountryID.ToString());

            ViewBag.State = RegionsM(para_issuer.State, para_issuer.StateID.ToString());

            ViewBag.City = CitiesM(para_issuer.City, para_issuer.CityID.ToString());
            ViewBag.Sel = RegistrarsM(para_issuer.Company, para_issuer.Company);

            return View(para_issuer);
        }

        // POST: para_issuer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "para_issuerID,Company,Date_created,Issued_shares,Status,registrar,Add_1,City,Country,Contact_Person,Telephone,Cellphone,ISIN,date_listed,email,website,State,CountryID,StateID,CityID")] para_issuer para_issuer)
        {
            int p = 0;

            try
            {
                p = Convert.ToInt32(para_issuer.Country);
            }
            catch (Exception)
            {

                p = Convert.ToInt32(para_issuer.CountryID);
            }

            int pp = 0;
            int ppp = 0;

            try
            {
                pp = Convert.ToInt32(para_issuer.State);
                ppp = Convert.ToInt32(para_issuer.City);
            }
            catch (Exception)
            {


            }

            if (pp == 0)
            {
                pp = Convert.ToInt32(para_issuer.StateID);
                ppp = Convert.ToInt32(para_issuer.CityID);
            }
            if (ModelState.IsValid)
            {
                var k = db.Countriess.ToList().Where(a => a.id == p);

                var q = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='" + ppp + "'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    para_issuer.Country = d.name;
                }

                foreach (var d in q)
                {
                    para_issuer.State = d.name;
                }

                foreach (var f in cs4)
                {
                    para_issuer.City = f.name;
                }
                para_issuer.CountryID = p;
                para_issuer.StateID = pp;
                para_issuer.CityID = ppp;


                db.Entry(para_issuer).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Company";

                return RedirectToAction("Index");
            }

            string v1, v2;

            var c = db.Countriess.ToList().Where(a => a.id == p);
            foreach (var d in c)
            {
                ViewBag.Departments = CountriesM(d.name, d.id.ToString());
            }
            ViewBag.City = CitiesM(para_issuer.City, ppp.ToString());
            ViewBag.State = RegionsM(para_issuer.State, pp.ToString());
            ViewBag.Sel = RegistrarsM(para_issuer.Company, para_issuer.Company);
            return View(para_issuer);
        }

        // GET: para_issuer/Delete/5
        public async Task<ActionResult> Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            if (para_issuer == null)
            {
                return HttpNotFound();
            }
            return View(para_issuer);
        }

        // POST: para_issuer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(decimal id)
        {
            para_issuer para_issuer = await db.para_issuers.FindAsync(id);
            db.para_issuers.Remove(para_issuer);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Company";

            return RedirectToAction("Index");
        }

        public List<SelectListItem> CountriesM(string s1, string s2)
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            dept.Add(new SelectListItem { Text = s1, Value = s2 });
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }
        public List<SelectListItem> Countries()
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }

        public List<SelectListItem> CitiesD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a City", Value = "Select a City" });
            return phy;
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a State", Value = "Select a State" });
            return phy;
        }

        public List<SelectListItem> Regions(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.regions where u.country_id == s select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> Cities(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Cities where u.region_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> CitiesM(string s, string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.Cities where u.region_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> RegionsM(string s, string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.regions where u.country_id == g.ToString() select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<SelectListItem> Registrars()
        {

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in db.Registrar select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.ParticipantName, Value = v.ParticipantName.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> RegistrarsM(string s, string v1)
        {

            //int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.Registrar  select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.ParticipantName, Value = v.ParticipantName.ToString() });
                }
            }
            return phy;
        }
    }
}
