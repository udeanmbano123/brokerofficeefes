﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
        // [CustomAuthorize(Users = "1,2")]
    public class TradingPlatformsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: TradingPlatforms
        public async Task<ActionResult> Index()
        {
            return View(await db.TradingPlatforms.ToListAsync());
        }

        // GET: TradingPlatforms/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingPlatform tradingPlatform = await db.TradingPlatforms.FindAsync(id);
            if (tradingPlatform == null)
            {
                return HttpNotFound();
            }
            return View(tradingPlatform);
        }

        // GET: TradingPlatforms/Create
        public ActionResult Create()
        {

            ViewBag.Departments = Countries();

            ViewBag.City = CitiesD();
            ViewBag.State = regionD();
            ViewBag.Tr = Trading();
            ViewBag.Dep = Tradings();
            return View();
        }

        // POST: TradingPlatforms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TradingPlatformID,Code,Name,Country,City,Address,ContactPerson,State,CountryID,StateID,CityID,PhoneNumber,TradeBoard,TradingBoardID,Depository")] TradingPlatform tradingPlatform)
        {  int pppp = Convert.ToInt32(tradingPlatform.TradingBoardID);
            var ee = db.TradingBoards.ToList().Where(a => a.TradingBoardID == pppp);
            string q = "";
            foreach (var cc in ee)
            {
                q = cc.BoardName;
            }

            tradingPlatform.TradeBoard = q;
            tradingPlatform.TradingBoardID = pppp;
            ViewBag.Tr = TradingT(q,pppp.ToString());
            int p = Convert.ToInt32(tradingPlatform.Country);
            int pp = Convert.ToInt32(tradingPlatform.State);
            int ppp = Convert.ToInt32(tradingPlatform.City);

          
           
            if (ModelState.IsValid)
            {
                var k = db.Countriess.ToList().Where(a => a.id == p);

                var qq = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='" + ppp + "'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    tradingPlatform.Country = d.name;
                }

                foreach (var d in qq)
                {
                    tradingPlatform.State = d.name;
                }

                foreach (var f in cs4)
                {
                    tradingPlatform.City = f.name;
                }
                tradingPlatform.CountryID = p;
                tradingPlatform.StateID = pp;
                tradingPlatform.CityID = ppp;

                db.TradingPlatforms.Add(tradingPlatform);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Trading Platform";

                return RedirectToAction("Index");
            }
            string v1, v2;

            var c = db.Countriess.ToList().Where(a => a.id == p);
            foreach (var d in c)
            {
                ViewBag.Departments = CountriesM(d.name, d.id.ToString());
            }
            ViewBag.City = CitiesM(tradingPlatform.City, ppp.ToString());
            ViewBag.State = RegionsM(tradingPlatform.State, pp.ToString());
            ViewBag.Dep = TradingTs(tradingPlatform.Depository,tradingPlatform.Depository);
            return View(tradingPlatform);
        }

        // GET: TradingPlatforms/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingPlatform tradingPlatform = await db.TradingPlatforms.FindAsync(id);
            if (tradingPlatform == null)
            {
                return HttpNotFound();
            }
            try
            {
                ViewBag.Departments = CountriesM(tradingPlatform.Country, tradingPlatform.CountryID.ToString());

                ViewBag.State = RegionsM(tradingPlatform.State, tradingPlatform.StateID.ToString());

                ViewBag.City = CitiesM(tradingPlatform.City, tradingPlatform.CityID.ToString());

                ViewBag.Tr = TradingT(tradingPlatform.TradeBoard, tradingPlatform.TradingBoardID.ToString());
                ViewBag.Dep = TradingTs(tradingPlatform.Depository, tradingPlatform.Depository);
            }
            catch (Exception)
            {

                
            }
            return View(tradingPlatform);
        }
        [HttpPost]
        public JsonResult AjaxMethod(string type, int value)
        {
            Depository model = new Depository();
            switch (type)
            {
                case "Country":
                    ViewBag.City = Regions(value.ToString());
                    break;
                case "State":
                    ViewBag.City = Cities(value.ToString());
                    break;

            }
            return Json(ViewBag.City);
        }
        // POST: TradingPlatforms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TradingPlatformID,Code,Name,Country,City,Address,ContactPerson,State,CountryID,StateID,CityID,PhoneNumber,TradeBoard,TradingBoardID,Depository")] TradingPlatform tradingPlatform)
        {
            int pppp = Convert.ToInt32(tradingPlatform.TradingBoardID);
            var ee = db.TradingBoards.ToList().Where(a => a.TradingBoardID == pppp);
            string q = "";
            foreach (var cc in ee)
            {
                q = cc.BoardName;
            }

            tradingPlatform.TradeBoard = q;
            tradingPlatform.TradingBoardID = pppp;
            ViewBag.Tr = TradingT(q, pppp.ToString());
            int p = Convert.ToInt32(tradingPlatform.Country);

            int pp = 0;
            int ppp = 0;

            try
            {
                pp = Convert.ToInt32(tradingPlatform.State);
                ppp = Convert.ToInt32(tradingPlatform.City);
            }
            catch (Exception)
            {

               
            }

            if (pp == 0)
            {
                 pp = Convert.ToInt32(tradingPlatform.StateID);
                 ppp = Convert.ToInt32(tradingPlatform.CityID);
            }
            var k = db.Countriess.ToList().Where(a => a.id == p);

                var qq = db.regions.ToList().Where(a => a.id == pp);

                var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
                string cp = @"Select * from  Cities where  id='" + ppp + "'";
                var cs4 = db2.Query(cp).ToList();
                foreach (var d in k)
                {
                    tradingPlatform.Country = d.name;
                }

                foreach (var d in qq)
                {
                    tradingPlatform.State = d.name;
                }

                foreach (var f in cs4)
                {
                    tradingPlatform.City = f.name;
                }
                tradingPlatform.CountryID = p;
                tradingPlatform.StateID = pp;
                tradingPlatform.CityID = ppp;
            if (ModelState.IsValid)
            {
                
                db.Entry(tradingPlatform).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Trading Platform";

                return RedirectToAction("Index");
            }
            string v1, v2;

            ViewBag.Departments = CountriesM(tradingPlatform.Country,p.ToString());
            ViewBag.City = CitiesM(tradingPlatform.City, ppp.ToString());
            ViewBag.State = RegionsM(tradingPlatform.State, pp.ToString());
            ViewBag.Dep = TradingTs(tradingPlatform.Depository, tradingPlatform.Depository);

            return View(tradingPlatform);
        }

        // GET: TradingPlatforms/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingPlatform tradingPlatform = await db.TradingPlatforms.FindAsync(id);
            if (tradingPlatform == null)
            {
                return HttpNotFound();
            }
            return View(tradingPlatform);
        }

        // POST: TradingPlatforms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TradingPlatform tradingPlatform = await db.TradingPlatforms.FindAsync(id);
            db.TradingPlatforms.Remove(tradingPlatform);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Trading Platform";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public List<SelectListItem> CountriesM(string s1, string s2)
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            dept.Add(new SelectListItem { Text = s1, Value = s2 });
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }
        public List<SelectListItem> Countries()
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.Countriess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return dept;
        }

        public List<SelectListItem> CitiesD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a City", Value = "Select a City" });
            return phy;
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Select a State", Value = "Select a State" });
            return phy;
        }

        public List<SelectListItem> Regions(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.regions where u.country_id == s select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> Cities(string s)
        {

            int g = Convert.ToInt32(s);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Cities where u.region_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> CitiesM(string s, string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.Cities where u.region_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> RegionsM(string s, string v1)
        {

            int g = Convert.ToInt32(v1);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
            var query = from u in db.regions where u.country_id == g.ToString() select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.id.ToString() });
                }
            }
            return phy;
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadPhysiansByDepartment(string deptId)
        {
            //Your Code For Getting Physicans Goes Here
            int g = Convert.ToInt32(deptId);
            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.Cities where u.country_id == g select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.name, Value = v.name.ToString() });
                }
            }
            return Json(phy, JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> Trading()
        {

           
            List<SelectListItem> phy = new List<SelectListItem>();
   
            var query = from u in db.TradingBoards select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.BoardName, Value = v.TradingBoardID.ToString() });
                }
            }
            return phy;
        }

        public List<SelectListItem> Tradings()
        {


            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in db.Depositorys select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.Name, Value = v.Name });
                }
            }
            return phy;
        }

        public List<SelectListItem> TradingT(string s, string vv)
        {

            int my = Convert.ToInt32(vv);
            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = vv.ToString() });

            var query = from u in db.TradingBoards select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.BoardName, Value = v.TradingBoardID.ToString() });
                }
            }
            return phy;
        }

        public List<SelectListItem> TradingTs(string s, string vv)
        {

            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = vv.ToString() });

            var query = from u in db.Depositorys select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.Name, Value = v.Name });
                }
            }
            return phy;
        }

        
    }
}
