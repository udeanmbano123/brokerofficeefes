﻿using BrokerOffice.CDSMOdel;
using BrokerOffice.DAO;
using BrokerOffice.DAO.security;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using SaccoRegsitration.DAO.security;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{// GET: Admin
        //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "Admin")]
        // [CustomAuthorize(Users = "1")]
    public class AdminController : Controller
    {
        private SBoardContext db = new SBoardContext();
        public ActionResult Index()
        {

            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("BrokersList", Method.GET);
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


                var dbsel = from s in dataList
                            select s;
                //updatebilling();
                foreach (var d in dbsel)
                {
                    if (vx == d.Company_Code)
                    {
                        n1 = d.Company_name;
                        n2 = d.Company_Code;
                    }
                }
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            ViewBag.RegC = db.Account_Creations.ToList().Where(a => a.Broker == n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a => a.Broker == n2 && a.update_type == "TOUPDATE").Count();
            int MB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "MATCHED").Count();

            int MB2 = 0;
            ViewBag.MB = MB1 + MB2;
            int MS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "MATCHED").Count();

            int MS2 = 0;
            ViewBag.MS = MS1 + MS2;
            int SB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "Settled").Count();

            int SB2 = 0;
            ViewBag.SB = SB1 + SB2;
            int SS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "Settled").Count();

            int SS2 = 0;
            ViewBag.SS = SS1 + SS2;
            int PB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY").Count();

            int PB2 = 0;
            ViewBag.PB = PB1 + PB2;
            int PS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL").Count();

            int PS2 = 0;
            ViewBag.PS = PS1 + PS2;

            return View();
        }

        public void updatebilling()
        {
            var client = new RestClient("http://192.168.3.245/BrokerService");
            var request = new RestRequest("Billing", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<para_Billing> dataList = JsonConvert.DeserializeObject<List<para_Billing>>(validate);


            var pers = from s in dataList
                       select s;

            foreach (var k in pers)
            {
                List<TradingCharges> p = (from s in db.TradingChargess
                                          where s.ChargeName == k.ChargeName
                                          select s).ToList();

                foreach (TradingCharges c in p)
                {
                    TradingCharges my = db.TradingChargess.Find(c.TradingChargesID);
                    c.chargevalue =k.PercentageOrValue;
                    c.chargecode = k.ChargeCode;
                    if (k.ApplyTo == "BOTH")
                    {
                        c.tradeside = "Both";
                    }
                    else if (k.ApplyTo == "BUYER")
                    {
                        c.tradeside = "Buy";
                    }
                    else if (k.ApplyTo == "SELLER")
                    {
                        c.tradeside = "Sell";
                    }
                    if (k.Indicator == "PERCENT")
                    {
                        c.ChargedAs = "Percentage";
                    }
                    else if (k.Indicator == "FLAT")
                    {
                        c.ChargedAs = "Flat";
                    }
                    db.TradingChargess.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName);
                }
            }

        }

        public void reoveaccount(string s)
        {
            int my = 0;
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);
            foreach (var p in c)
            {
                my = p.ID_;
            }

            try
            {
                Account_Creation account_Creation = db.Account_Creations.Find(my);
                account_Creation.StatuSActive = true;
                db.Account_Creations.AddOrUpdate(account_Creation);
                db.SaveChangesAsync();
            }
            catch (Exception)
            {


            }
        }

        


        public ActionResult Unlock()
        {
            return View();
        }
    }
}