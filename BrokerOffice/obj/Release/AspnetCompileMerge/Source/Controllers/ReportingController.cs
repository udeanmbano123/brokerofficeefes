﻿using DevExpress.Web.Mvc;
using BrokerOffice.DAO.security;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.Models;
using BrokerOffice.DAO;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin,User,General,BrokerAdmin2,BrokerAdmin,Approver,ApproverUser", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class ReportingController : Controller
    {
        // GET: Reporting
        private SBoardContext db = new SBoardContext();

        public ActionResult MatchedDealsS()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsS([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsS.aspx?DateHeld=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ClientList()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientList([Bind(Include = "Asat,To")] ClientList registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ClientList.aspx?From=" + registerreport.Asat.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Posting([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/PostingOrders.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To="+registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult Posting()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostingM([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/PostingOrdersM.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult PostingM()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsNSI([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsNS.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult MatchedDealsNSI()
        {
            return View();
        }
        public ActionResult Portfolio()
        {
            return View();
        }

        public ActionResult TrialBalance()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrialBalance([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TrialBalance.aspx?id="+registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
                return View();
        }

        public ActionResult IncomeStatement()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IncomeStatement([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/IncomeStatement.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecutedOrder([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ExecutedOrders.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ExecutedOrder()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderBook([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/OrderBookReport.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        public ActionResult OrderBook()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OutStanding([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/OutStandingOrders.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult OutStanding()
        {
            return View();
        }

        public ActionResult BalanceSheet()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BalanceSheet([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/BalanceSheetReport.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult DebtorsAndCreditors()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DebtorsAndCreditors([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/DebtorsAndCreditorsReport.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ShareReconciliation()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShareReconciliation([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ShareReconciliations.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }



        public ActionResult Commissions()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Commissions([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/BrokerCommissions.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountAuditR([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AccountAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AccountAuditR()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountAuditB([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/BrokerAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AccountAuditB()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsR([Bind(Include = "From,To,DT")] PostingD registerreport)
        {

            if (ModelState.IsValid)
            {
                if (registerreport.DT=="Date Posted")
                {
                    return Redirect("~/Reporting/MatchedDeals.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }else
                {
                    return Redirect("~/Reporting/MatchedDealss.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }
            }
            ViewBag.DT = regionD(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult MatchedDealsR()
        {
            ViewBag.DT = regionD();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsN([Bind(Include = "From,To")] Posting registerreport)
        {
            string m=System.Web.HttpContext.Current.Session["csd"].ToString();

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsN.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&csd="+m);
            }
            ViewBag.CSD = System.Web.HttpContext.Current.Session["csd"].ToString();
            return View();
        }

        public ActionResult MatchedDealsN()
        {
            ViewBag.CSD = System.Web.HttpContext.Current.Session["csd"].ToString();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashF([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/CashFlowReport.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult CashF()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancelled([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsC.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult Cancelled()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChargesScheduleR([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ChargesSchedule.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ChargesScheduleR()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransStatement([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FullStatement.aspx?Begin=" + registerreport.From.ToString("dd-MMM-yyyy") + "&End=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult TransStatement()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgeReport([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AgeReports.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AgeReport()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadedOrder([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/UploadedDeals.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult UploadedOrder()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsM([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsm.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult MatchedDealsM()
        {
            return View();
        }


        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Date Posted", Value = "Date Posted" });

            phy.Add(new SelectListItem { Text = "Settlement Date", Value = "Settlement Date" });

            return phy;
        }
        public List<SelectListItem> regionD(string m, string n)
        {
        

        List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });

            phy.Add(new SelectListItem { Text = "Date Posted", Value = "Date Posted" });

            phy.Add(new SelectListItem { Text = "Settlement Date", Value = "Settlement Date" });



            return phy;
        }

        public ActionResult MatchedDealsF()
        {
            ViewBag.Counter = regionE();

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsF([Bind(Include = "From,To,Fund")] PostingF registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsF.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&Fund=" + registerreport.Fund);
            }
            ViewBag.Counter = regionE(registerreport.Fund, registerreport.Fund);

            return View();
        }

        public ActionResult MatchedDealsNN()
        {
            ViewBag.Counter = regionE();

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsNN([Bind(Include = "From,To,Fund")] PostingF registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsNN.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&csd=" + registerreport.Fund);
            }
            ViewBag.Counter = regionE(registerreport.Fund, registerreport.Fund);

            return View();
        }
        public List<SelectListItem> regionE()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = (from v in db.Account_Creations.ToList()
                     where v.manAccount != null || v.manAccount != ""
                     select new { v.manAccount }).Distinct();
            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.manAccount, Value = z.manAccount });

            }

            return phy;
        }

        public List<SelectListItem> regionE(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });

            var p = (from v in db.Account_Creations.ToList()
                     where v.manAccount != null || v.manAccount != ""
                     select new { v.manAccount }).Distinct();
            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.manAccount, Value = z.manAccount });

            }

            return phy;
        }
        public ActionResult IncomeStatementM()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IncomeStatementM([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                DateTime date = Convert.ToDateTime(registerreport.DateHeld);
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));


                return Redirect("~/Reporting/IncomeStatementM.aspx?From=" + firstDayOfMonth.ToString("dd-MMM-yyyy") + "&To=" + lastDayOfMonth.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaxReturns([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TaxReturnss.aspx?Begin=" + registerreport.From.ToString("dd-MMM-yyyy") + "&End=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult TaxReturns()
        {
            return View();
        }

        // Chart of Accounts
        public ActionResult ChartAccounts()
        {
            return Redirect("~/Reporting/ChartAccounts.aspx");

        }

        //Journal Audit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult JournalAudit([Bind(Include = "From,To,DT")] PostingD registerreport)
        {

            if (ModelState.IsValid)
            {
                if (registerreport.DT == "All")
                {
                    return Redirect("~/Reporting/JournalAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }
                else
                {
                    return Redirect("~/Reporting/JournalAuditJ.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&Account=" + registerreport.DT.ToString());

                }
            }
            ViewBag.DT = regionF(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult JournalAudit()
        {
            ViewBag.DT = regionF();
            return View();
        }
        public List<SelectListItem> regionF()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "All", Value = "All" });

            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }

            return phy;
        }
        public List<SelectListItem> regionF(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });

            phy.Add(new SelectListItem { Text = "All", Value = "All" });
            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }


            return phy;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankRec([Bind(Include = "To,DT")] PostingW registerreport)
        {

            if (ModelState.IsValid)
            {

                return Redirect("~/Reporting/BankRecJ.aspx?To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&Account=" + registerreport.DT.ToString());

            }
            ViewBag.DT = regionQ(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult BankRec()
        {
            ViewBag.DT = regionQ();
            return View();
        }
        public List<SelectListItem> regionQ()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }

            return phy;
        }
        public List<SelectListItem> regionQ(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });


            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }


            return phy;
        }

        //
        public ActionResult Coporate()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Coporate([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/CorporateOrders.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        public ActionResult Unsettled()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Unsettled([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/UnsettledOrders.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        public ActionResult Settled()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settled([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/SettledOrders.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

    }
}