﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class AccountMaintenance
    {
        [Key]
        public int AccountMaintenanceID { get; set; }

        public string account_type { get; set; }

        public decimal amount { get; set; }

        public string frequency { get; set; }
    }
}