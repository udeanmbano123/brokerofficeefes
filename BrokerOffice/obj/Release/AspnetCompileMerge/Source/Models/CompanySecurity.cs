﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class CompanySecurity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [DisplayName("Security")]
        public string Security_Type { get; set; }
        public string Description { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public int CompanyID { get; set; }
    }
}