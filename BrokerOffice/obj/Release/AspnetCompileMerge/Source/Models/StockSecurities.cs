﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class StockSecurities
    {
        [Key]
        public int StockSecuritiesID { get; set; }
        [DisplayName("StockISIN")]
        [Required]
        public string StockISIN { get; set; }
        [DisplayName("Issuer")]
        [Required]
        public string Issuer { get; set; }
        [DisplayName("StockType")]
        [Required]
        public string StockType { get; set;}
        [DisplayName("StockDescription")]
        public string description { get; set; }
        [DisplayName("TotalIssuedQuantity")]
        public int stockQty { get; set; }
        [DisplayName("StockPrice")]
        public double StockPrice { get; set; }
        [DisplayName("StockStatus")]
        public string Stockstatus { get; set; }
        [DisplayName("IssuedPrice")]
        public decimal IssuedPrice { get; set; }
        [DisplayName("Current Price")]
        public decimal CurrentPrice { get; set; }
        public virtual ICollection<StockPrices> StockPrices { get; set; }
    }
}