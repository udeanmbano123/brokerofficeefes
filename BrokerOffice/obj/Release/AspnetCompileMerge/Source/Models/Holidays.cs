﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Holidays
    {
        [Key]
        public int HolidayID { get; set; }
        [DisplayName("Holiday Name")]
        [Required]
        public string HolidayName { get; set; }
        [DisplayName("Holiday Date")]
        [Required]
        public DateTime HolidayDate { get; set; }

    }
    public class TRP
    {
        public string DD { get; set; }
    }
    public class TRP2
    {
        public string Mo { get; set; }
    }
}