﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class ApplicationCapture
    {
        [Key]
        public int ApplicationCaptureID { get; set; }

        public string BatchRef{get;set;}

        public int BatchID { get; set; }

        public double BatchTotal { get; set; }

        
        public string ClientNumber { get; set; }

        public string ClientNames { get; set; }

        public string ClientID { get; set; }

        public double ClientUnits { get; set; }

        public double value { get; set; }
    public string tempstatus { get; set; }
    }
}