﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class StockTypes
    {
        public int StockTypesID { get; set; }
        public string stockname { get; set; }
    }
}