﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public  class Accounts_Audit
    {
        [Key]
        public int ID { get; set; }
        public string CDS_Number { get; set; }
        public string BrokerCode { get; set; }
        public string AccountType { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string Forenames { get; set; }
        public string Initials { get; set; }
        public string Title { get; set; }
        public string IDNoPP { get; set; }
        public string IDtype { get; set; }
        public string Nationality { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Gender { get; set; }
        public string Add_1 { get; set; }
        public string Add_2 { get; set; }
        public string Add_3 { get; set; }
        public string Add_4 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Category { get; set; }
        public string Custodian { get; set; }
        public string TradingStatus { get; set; }
        public string Industry { get; set; }
        public string Tax { get; set; }
        public string Div_Bank { get; set; }
        public string Div_Branch { get; set; }
        public string Div_AccountNo { get; set; }
        public string Cash_Bank { get; set; }
        public string Cash_Branch { get; set; }
        public string Cash_AccountNo { get; set; }
        public byte[] Client_Image { get; set; }
        public byte[] Documents { get; set; }
        public byte[] BioMatrix { get; set; }
        public string Attached_Documents { get; set; }
        public Nullable<System.DateTime> Date_Created { get; set; }
        public string Update_Type { get; set; }
        public string Created_By { get; set; }
        public string AuthorizationState { get; set; }
        public string Comments { get; set; }
        public Nullable<decimal> VerificationCode { get; set; }
        public string DivPayee { get; set; }
        public string SettlementPayee { get; set; }
        public string idnopp2 { get; set; }
        public string idtype2 { get; set; }
        public string client_image2 { get; set; }
        public string documents2 { get; set; }
        public string isin { get; set; }
        public string company_code { get; set; }
        public string mobile_money { get; set; }
        public string mobile_number { get; set; }
        public string currency { get; set; }
        public string Indegnous { get; set; }
        public string Race { get; set; }
        public string Disadvantaged { get; set; }
        public string NationalityBy { get; set; }
        public string Custody { get; set; }
        public string Trading { get; set; }

        
    }

    public class IPCompany
    {
        public string company { get; set; }
        public string company_name { get; set; }
    }

    public class IPAgents
    {
        public string desc { get; set; }
        public string code { get; set; }
    }

    public class IPPrice
    {
        public string price { get; set; }
    }
    public partial class Client_Companies
    {
        
         public string Company_name { get; set; }
        public string Company_Code { get; set; }
        public string Company_type { get; set; }
        public string AccountManager { get; set; }
        public string Account_Pass { get; set; }
        public string Adress_1 { get; set; }
        public string Adress_2 { get; set; }
        public string Adress_3 { get; set; }
        public string Adress_4 { get; set; }
        public string adress_5 { get; set; }
        public string contact_email { get; set; }
        public string contact_phone { get; set; }
        public string contact_person { get; set; }
        public string Job { get; set; }
        public Nullable<bool> status { get; set; }
        [Key]
        public int ID { get; set; }
        public string main_branch { get; set; }
        public string main_account { get; set; }
        public string trading_branch { get; set; }
        public string trading_account { get; set; }
        public string main_account_name { get; set; }
        public string trading_account_name { get; set; }
        public string trading_bank { get; set; }
        public string main_bank { get; set; }
        public Nullable<int> settlement_cycle { get; set; }
        public string main_branch_new { get; set; }
        public string trading_branch_new { get; set; }
        public string key_letter { get; set; }
        public string CDS_Number { get; set; }
        public string Broker_Custodian { get; set; }
    }
}