﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Batch
    {
        [Key]
        public int BatchID { get; set; }

        [Required]
        [DisplayName("Batch Ref")]
        public string Batchref { get; set; }
        [Required]
        [DisplayName("Batch Units")]
        public double BatchUnits { get; set; }
        [Required]
        [DisplayName("Batch Details")]
        public string BatchDetails { get; set; }

        public string tempstatus { get; set; }
        
        [DisplayName("Issuer IPO")]
        public string product { get; set; }

        public int IPOID { get; set; }

        public double value { get; set; }

    }

    public class BatchD
    {
        public string Security { get; set; }
          public string Deal { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string BuyerClient { get; set; }
        public string SellerClient { get; set; }
        public string DatePosted { get; set; }
        public string SettlementDate { get; set; }
        public string Status { get; set; }



    }

    public class UplodedTrans
    {
        [Key]
        public int UplodedTransID { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime ValueDate { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "money")]
        public decimal Debit { get; set; }
        [Column(TypeName = "money")]
        public decimal Credit { get; set; }
        [Column(TypeName = "money")]
        public decimal Balance { get; set; }

        public string clientnumber { get; set; }
        public string fileName { get; set; }
        public string UploadedBy { get; set; }

        public DateTime UploadedDate { get; set; }

        public string Batch { get; set; }


        public string transID { get; set; }
        public string reason { get; set; }
    }
}