﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class ClientsDue
    {
        [Key]
        public int id { get; set; }
        public string names { get; set; }
        public string accountnumber { get; set; }
        public decimal amounts { get; set; }
    }
}