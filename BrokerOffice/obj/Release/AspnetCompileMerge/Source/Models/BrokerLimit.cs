﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class BrokerLimit
    {
        [Key]
        public int BrokerLimitID { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal BuyLimit { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal SellLimit { get; set; }
        [Required]
        public string reaction { get; set; }

    }
    [TrackChanges]
    public class MinimumLimit
    {
        [Key]
        public int MinimumLimitID { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal BuyLimit { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal SellLimit { get; set; }
        [Required]
        public string reaction { get; set; }

    }
}