﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class BatchApplication
    {
        [Key]
        public int BatchApplicationID { get; set; }
        public string ClientName { get; set; }

        public decimal ClientAmount { get; set; }

        public decimal numberofshares { get; set; }

        public string Clientnumber { get; set; }
        public int BatchNo { get; set; }

    }
}