﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Tax
    {
     [Key]
     public int id { get; set; }
        [Required]
       [DisplayName("Name")]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [DisplayName("Description")]
        public string Description { get; set; }
        [Required]
        [DisplayName("Percentage")]
        public decimal Percentage { get; set; }

        public Boolean isset { get; set; }
    }
}