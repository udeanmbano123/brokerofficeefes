﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class BatchDeals
    {
        [Key]
        public int BatchDealsID { get; set; }
        public string Deal { get; set; }
        public DateTime myDD { get; set; }
    }
}