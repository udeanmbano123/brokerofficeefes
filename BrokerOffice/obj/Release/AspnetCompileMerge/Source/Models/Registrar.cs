﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Registrar
    {

        [Key]
        public int RegistrarID { get; set; }
        [Required]
        public string ParticipantName { get; set; }
        [Required]
        public string ParticipantCode { get; set; }
        public string ParticipantType { get; set; }

        [DataType(DataType.MultilineText)]
        [Required]
        public string Address { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string ParticipantEmail { get; set; }
        [DisplayName("Contact Person")]
        [Required]
        public string ContactPerson { get; set; }
        [DisplayName("Maximum Users")]
        public int MaxUsers { get; set; }


            //AUDIT
            //BROKER
            //CDS
            //CUSTODIAN
            //REFULATOR
            //TSec
    }

    public class ParticipantType
    {
        [Key]
        public int ID { get; set; }
        public string Type { get; set; }
    }
}