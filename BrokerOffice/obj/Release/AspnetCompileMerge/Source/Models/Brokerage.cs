﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Brokerage
    {
        [Key]
        public int BrokerageID { get; set; }
        [DisplayName("Company Name")]
        [Required]
        public string CompanyName { get; set; }
        [DisplayName("Operating Address")]
        [Required]
        public string OperatingAddress { get; set; }
        [DisplayName("Principal Agent")]
        [Required]
        public string PrincipalAgent { get; set; }
    public string brokername { get; set; }
    }
}