﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class ScriptRegister
    {
        [Key]
        public int ScriptRegisterID { get; set; }
        [Required]
        public string Exchange { get; set; }
        [Required]
        public string ClientNames { get; set; }
        [Required]
        public string ClientNumber { get; set; }
        [Required]
        public string Counter { get; set; }
        [Required]
        public string CertificateNumber { get; set; }
        [Required]
        public decimal NumberShares { get; set; }
        
        public string Status { get; set; }

        public string Registrar { get; set; }
    }

    [TrackChanges]
    [Table("Beneficiaries")]
    public class Beneficiaries
    {
    public int BeneficiariesID { get;set;}
    public string CertificateId { get; set; }
    public string CustomerNumber { get; set; }
    public string Fullnames { get; set; }

   public String RegistrarName { get; set; }
  public decimal Amount { get; set; }
   public int? ScriptRegisterID { get; set; }

   [ForeignKey("ScriptRegisterID")]
  public virtual ScriptRegister ScriptRegister { get; set; }


    }



}