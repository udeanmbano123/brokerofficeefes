﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Posting
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class PostingD
    {
        [Required]
        public DateTime From { get; set; }
        [Required]
        public DateTime To { get; set; }
        [Required]
        [DisplayName("Date Type")]
        public string DT { get; set; }
    }
    public class PostingF
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Fund { get; set; }
    }
    public class PostingW
    {
        [Required]
        public DateTime To { get; set; }
        [Required]
        [DisplayName("Date Type")]
        public string DT { get; set; }
    }
}