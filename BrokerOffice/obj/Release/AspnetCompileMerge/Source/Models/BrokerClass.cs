﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class BrokerClass
    {
        [Key]
        public int BrokerClassID { get; set; }
        public string BrokerName { get; set; }

        public string ATPCSD { get; set; }
    }
}