﻿using BrokerOffice.DAO;
using BrokerOffice.Reporting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.ClientServices
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();

        protected void Page_Load(object sender, EventArgs e)
        {

            var deal = Request.QueryString["Deal"];
            var buyer = Request.QueryString["Buyer"];
            var seller = Request.QueryString["Seller"];
            ExportDealNote(deal,Convert.ToBoolean(buyer),Convert.ToBoolean(seller));
        }

        //autogenerate deal notes
        public void ExportDealNote(string deal, bool buyer, bool seller)
        {
            var dealnote = db.DealerDGs.Where(a => a.Deal == deal).FirstOrDefault();

            if (buyer == true)
            {
                XtraDeal report = new XtraDeal();
                report.Parameters["Account"].Value = dealnote.Account1;
                report.Parameters["OrderNo"].Value = dealnote.Deal;
                report.Parameters["SID"].Value = dealnote.Deal;
                report.Parameters["QTY"].Value = dealnote.Quantity;
                report.Parameters["Dte"].Value = dealnote.SettDate;
                report.Parameters["Max"].Value = dealnote.Price;
                report.Parameters["TYPO"].Value = "BUY";
                report.Parameters["TYPO2"].Value = "PURCHASE";
                report.Parameters["Stamp"].Value = "Stamp duty";


                report.Parameters["Broker"].Value = dealnote.Security.ToUpper();
                report.Parameters["BrokerName"].Value = dealnote.Security.ToUpper();

                //address details
                var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == dealnote.Account1);
                foreach (var v in p)
                {
                    report.Parameters["Address"].Value = v.depname;
                }
                ExportReport(report, "PurchaseNote" + dealnote.Account1 + "" + dealnote.AccountName1 + ".pdf", "pdf", false);
            }
            else if (seller == true)
            {
                XtraDeal report = new XtraDeal();
                report.Parameters["Account"].Value = dealnote.Account2;
                report.Parameters["OrderNo"].Value = dealnote.Deal;
                report.Parameters["SID"].Value = dealnote.Deal;
                report.Parameters["QTY"].Value = dealnote.Quantity;
                report.Parameters["Dte"].Value = dealnote.SettDate;
                report.Parameters["Max"].Value = dealnote.Price;
                report.Parameters["TYPO"].Value = "SELL";
                report.Parameters["TYPO2"].Value = "SALE";
                report.Parameters["Stamp"].Value = "Zim";


                report.Parameters["Broker"].Value = dealnote.Security.ToUpper();
                report.Parameters["BrokerName"].Value = dealnote.Security.ToUpper();


                //address details
                var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == dealnote.Account2);
                foreach (var v in p)
                {
                    report.Parameters["Address"].Value = v.depname;
                }
                ExportReport(report, "SaleNote" + dealnote.Account2 + "" + dealnote.AccountName2 + ".pdf", "pdf", false);
            }
            else
            {

            }
        }

        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.Write("<script type=\"text/javascript\">window.close();</script>");
            //Response.End();
        }
    }
}