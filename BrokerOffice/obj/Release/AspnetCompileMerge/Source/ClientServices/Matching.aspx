﻿<%@ Page Language="C#" Async="true"   AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" CodeBehind="Matching.aspx.cs" Inherits="BrokerOffice.ClientServices.Matching" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
   .inlineBlock { display: inline-block;}
        .fl {
           
        }
</style>
    <script type = "text/javascript">
function DisableButtons() {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i in inputs) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
}
window.onbeforeunload = DisableButtons;
</script>
  <div style="background:white" class="content">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Internal Matching Orders</i>
       
    </div> 

<table>

<tr>
    <td>
 
        <div style="float:left">
            <dx:ASPxLabel ID="ASPxLabel2" CssClass="fl" runat="server" Text="Buy Orders" Theme="Glass"></dx:ASPxLabel>
<br />
        

                <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="OrderNo"  Width="100px" Font-Size="X-Small"   runat="server"  EnableCallBacks="False" Theme="Glass"  OnSelectionChanged="ASPxGridView1_SelectionChanged">
                        <SettingsPager AlwaysShowPager="True" PageSize="5">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    
                        <SettingsCommandButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
     
                                         
      

                      

                       

                    </dx:ASPxGridView>
</div> 
        </td>
    <td>
         <div style="padding-left:10%;padding-bottom:20%">
              <dx:ASPxLabel ID="ASPxLabel3" CssClass="fl" runat="server" Text="Sell Orders" Theme="Glass"></dx:ASPxLabel>
             <br />
          
          <dx:ASPxGridView ID="ASPxGridView2" Font-Size="X-Small"  KeyFieldName="OrderNo" Width="100px"    runat="server"  EnableCallBacks="False" Theme="Glass"  OnSelectionChanged="ASPxGridView2_SelectionChanged">
                        <SettingsPager AlwaysShowPager="True" PageSize="5">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    
                        <SettingsCommandButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
          
                      

                       

                    </dx:ASPxGridView>
            </div> 
 </td>
     
 
</tr>
 <tr>
        <td>
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Match your orders after selecting your orders from The Buy and Sell Grids"></dx:ASPxLabel>

        </td>
        <td>
          <asp:Button ID="Button2" CssClass="btn btn-primary" runat="server" Text="Mark as Matched"   OnClick="Button2_Click" />
       
        </td>
    </tr>
       <tr>
        <td>
            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Delete the selected order ?"></dx:ASPxLabel>

        </td>
        <td>
            <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Delete Order"   OnClick="Button1_Click"  OnClientClick="return confirm('Are you sure want to delete the order?');"/>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
        </td>
        <td>
         
            <asp:Label ID="Label2" runat="server" Visible="False"></asp:Label>
        </td>
    </tr>
      


      

   </table>
  
      </div>
</asp:content>