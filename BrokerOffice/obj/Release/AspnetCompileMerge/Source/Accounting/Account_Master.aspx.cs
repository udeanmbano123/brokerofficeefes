﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class Account_Master : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            id = Request.QueryString["id"];


            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
               
                if (id == " " || id == null)
                {
                     loadGL();
                }
                else
                {
                     getDetails(id);
                }
            }
        }
        protected void getDetails(string x)
        {
            drGroup.Items.Clear();
            drGroup.DataSource = null;
            DropDownList2.Items.Clear();
            DropDownList2.DataSource = null;
            DropDownList3.Items.Clear();
            DropDownList3.DataSource = null;

            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.Accounts_Masters.ToList().Where(a => a.Accounts_MasterID == Convert.ToInt32(x));
                 loadGL2();
                foreach (var my in acc)
                {
                    txtAccname.Text = my.AccountName;
                    accnum.Text = my.AccountNumber.ToString();
                    descr.Text = my.Description;

                    drGroup.Items.Insert(0, new ListItem(my.GroupName, my.GL_GroupID.ToString()));
                    DropDownList2.Items.Insert(0, new ListItem(my.FinancialStatement, my.FinancialStatement.ToString()));
                    DropDownList3.Items.Insert(0, new ListItem(my.GeneralLedgerAccount, my.GeneralLedgerAccount.ToString()));
                    DropDownList1.Items.Insert(0, new ListItem(my.DebitOrCredit, my.DebitOrCredit.ToString()));
                     }
            }
        
          
            return;
        }
        protected void drGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            //check gl group number
            var gl = db.GL_Groups.ToList().ToList().Where(a=>a.GL_GroupID==Convert.ToInt32(drGroup.SelectedValue.ToString()));
            int glstrt = 0;
                int groupid = 0;
            int acccheck = 0;
            var accmst = 0;
            foreach (var p in gl)
            {
                glstrt = p.StartingAccount;
                groupid = p.GL_GroupID;
            }

            //check if that account number exists if it does add 1 to it


            try
            {
              accmst = db.Accounts_Masters.ToList().Where(a => a.GL_GroupID == groupid).Max(a => a.AccountNumber);
            }
            catch (Exception)
            {

                accmst = 0;
            }
            acccheck = accmst;
            if (glstrt == accmst)
            {
                accmst = accmst + 1;
            }
            else if (accmst == null || accmst == 0)
            {
                accmst = glstrt;
            }
            else
            {
                accmst = accmst + 1;
            }

            accnum.Text = accmst.ToString();

        }
        protected void loadGL2()
        {
           
            var ds = from c in db.GL_Groups
                     select new

                     {

                         GroupID = c.GL_GroupID,

                         GroupName = c.GroupName,

                     };
            drGroup.DataSource = ds.ToList();
            drGroup.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            drGroup.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            drGroup.DataBind(); //binding dropdownlist
               // allocate.Items.Insert(0, "Select");

            var dss = from c in db.FinancialStatementss
                      select new

                      {

                          GroupID = c.Name,

                          GroupName = c.Name,

                      };
            DropDownList2.DataSource = dss.ToList();
            DropDownList2.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            DropDownList2.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
            DropDownList2.DataBind(); //binding dropdownlist
          
            var dsss = from c in db.GeneralLedgerAcca
                       select new

                       {

                           GroupID = c.Name,

                           GroupName = c.Name,

                       };
            DropDownList3.DataSource = dsss.ToList();
            DropDownList3.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            DropDownList3.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
            DropDownList3.DataBind(); //binding dropdownlist

            // allocate.Items.Insert(0, "Select");


        }
        protected void loadGL()
        {
            drGroup.Items.Clear();
            drGroup.DataSource = null;
            var ds = from c in db.GL_Groups
                     select new

                     {

                        GroupID = c.GL_GroupID,

                        GroupName = c.GroupName,

                     };
            drGroup.DataSource = ds.ToList();
            drGroup.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            drGroup.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            drGroup.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            drGroup.Items.Insert(0, new ListItem("Please Select a group", "0"));

            DropDownList2.Items.Clear();
            DropDownList2.DataSource = null;

            var dss = from c in db.FinancialStatementss
                     select new

                     {

                         GroupID = c.Name,

                         GroupName = c.Name,

                     };
            DropDownList2.DataSource = dss.ToList();
            DropDownList2.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            DropDownList2.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
            DropDownList2.DataBind(); //binding dropdownlist
                                      // allocate.Items.Insert(0, "Select");
            DropDownList2.Items.Insert(0, new ListItem("Please Select a Financial Statement", "0"));



            DropDownList3.Items.Clear();
            DropDownList3.DataSource = null;

            var dsss = from c in db.GeneralLedgerAcca
                      select new

                      {

                          GroupID = c.Name,

                          GroupName = c.Name,

                      };
            DropDownList3.DataSource = dsss.ToList();
            DropDownList3.DataTextField = "GroupName";
            // text field name of table dispalyed in dropdown
            DropDownList3.DataValueField = "GroupID"; // to retrive specific  textfield name 
                                                      //assigning datasource to the dropdownlist
            DropDownList3.DataBind(); //binding dropdownlist
                                      // allocate.Items.Insert(0, "Select");
            DropDownList3.Items.Insert(0, new ListItem("Please Select a General Ledger Account", "0"));
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            //check if account has been selected
            if(drGroup.SelectedItem.Text== "Please Select a group")
            {
                msgbox("A GL group must be selected");
                return;
            }else if(txtAccname.Text=="")
            {
                msgbox("Account name is required");
                txtAccname.Focus();
                return;
            }
            if (Submit.Text == "Submit")
            {
                BrokerOffice.Models.Accounts_Master my = new BrokerOffice.Models.Accounts_Master();
                my.AccountName = txtAccname.Text;
                my.AccountNumber = Convert.ToInt32(accnum.Text);
                my.Description = descr.Text;
                my.GL_GroupID = Convert.ToInt32(drGroup.SelectedValue.ToString());
                my.GroupName = drGroup.SelectedItem.Text;
                my.FinancialStatement = DropDownList2.SelectedValue.ToString();
                my.GeneralLedgerAccount = DropDownList3.SelectedValue.ToString();
                my.DebitOrCredit = DropDownList1.SelectedValue.ToString();

                var cz = db.GL_Groups.ToList().Where(a => a.GL_GroupID == my.GL_GroupID);
                int nums = 0;
                foreach(var d in cz)
                {
                    nums = d.StartingAccount;
                }
                my.Code = nums.ToString() + "/" + (my.AccountNumber - nums).ToString();
                db.Accounts_Masters.Add(my);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                //msgbox("Saved Successfully");
                //Update reduce the batch

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the account";
                Response.Redirect("~/Accounts_Master/Index");

            }
            else if (Submit.Text == "Update")
            {
                var my = db.Accounts_Masters.Find(Convert.ToInt32(txtFkey.Text));
                my.AccountName = txtAccname.Text;
                my.AccountNumber = Convert.ToInt32(accnum.Text);
                my.Description = descr.Text;
                my.GL_GroupID = Convert.ToInt32(drGroup.SelectedValue.ToString());
                my.GroupName = drGroup.SelectedItem.Text;
                my.FinancialStatement = DropDownList2.SelectedValue.ToString();
                my.GeneralLedgerAccount = DropDownList3.SelectedValue.ToString();
                my.DebitOrCredit = DropDownList1.SelectedValue.ToString();

                var cz = db.GL_Groups.ToList().Where(a => a.GL_GroupID == my.GL_GroupID);
                int nums = 0;
                foreach (var d in cz)
                {
                    nums = d.StartingAccount;
                }
if (my.AccountName == "Current Tax")
                    {
                        nums = 8000;
                    }
                if(my.AccountNumber - nums < 10)
                {
                    
                  my.Code = nums.ToString() + "/0" + (my.AccountNumber - nums).ToString();

                }else
                {
                    my.Code = nums.ToString() + "/" + (my.AccountNumber - nums).ToString();

                }
                db.Accounts_Masters.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the account";
                Response.Redirect("~/Accounts_Master/Index");
            }
        }
        protected void Clear()
        {
            Response.Redirect("~/Accounts_Master/Index");
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
    }
}