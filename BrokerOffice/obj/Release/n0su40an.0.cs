﻿#if _DYNAMIC_XMLSERIALIZER_COMPILATION
[assembly:System.Security.AllowPartiallyTrustedCallers()]
[assembly:System.Security.SecurityTransparent()]
[assembly:System.Security.SecurityRules(System.Security.SecurityRuleSet.Level1)]
#endif
[assembly:System.Reflection.AssemblyVersionAttribute("1.0.0.0")]
[assembly:System.Xml.Serialization.XmlSerializerVersionAttribute(ParentAssemblyId=@"3d60e95c-8e2a-4da8-acae-665468f178b3,", Version=@"4.0.0.0")]
namespace Microsoft.Xml.Serialization.GeneratedAssembly {

    public class XmlSerializationWriter1 : System.Xml.Serialization.XmlSerializationWriter {

        public void Write1_GetOrders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"GetOrders", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write2_GetOrdersInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write3_NewClientRegistration(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"NewClientRegistration", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"OtherName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"SurnameOrCompanyName", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"PostalCode", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Country", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementStringRaw(@"DateOfBirth", @"EscrowService", FromDateTime(((global::System.DateTime)p[8])));
            }
            if (pLength > 9) {
                WriteElementString(@"Gender", @"EscrowService", FromChar(((global::System.Char)p[9])));
            }
            if (pLength > 10) {
                WriteElementString(@"Nationality", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementStringRaw(@"Resident", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)p[11])));
            }
            if (pLength > 12) {
                WriteElementString(@"TelephoneNumber", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"MNOCustodian", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"CallbackUrl", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"BrokerCode", @"EscrowService", ((global::System.String)p[15]));
            }
            WriteEndElement();
        }

        public void Write4_NewClientRegistrationInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write5_IPOStatusCheck(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"IPOStatusCheck", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[0]));
            }
            WriteEndElement();
        }

        public void Write6_IPOStatusCheckInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write7_RegisterBid(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RegisterBid", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"Quantity", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Double)((global::System.Double)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"ReferenceNumber", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"ClientType", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Custodian", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"CSDAccountNumber", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"MPESATransactionID", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"TransNum", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementStringRaw(@"PledgeIndicator", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)p[11])));
            }
            if (pLength > 12) {
                WriteElementString(@"PledgeeBPID", @"EscrowService", ((global::System.String)p[12]));
            }
            WriteEndElement();
        }

        public void Write8_RegisterBidInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write9_E_Enquiry(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"E_Enquiry", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"CDSNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PerformFunction", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"Ref", @"EscrowService", ((global::System.String)p[5]));
            }
            WriteEndElement();
        }

        public void Write10_E_EnquiryInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write11_ReceiveUSSDBuyOrder(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDBuyOrder", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"TelephoneBorder", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NoofNotes", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"BasePrice", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write12_ReceiveUSSDBuyOrderInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write13_ReceiveUSSDSellOrder(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDSellOrder", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"NoofNotes", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"TelephoneSelorder", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"BasePrice", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write14_ReceiveUSSDSellOrderInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write15_CompanyIssues(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"CompanyIssues", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write16_CompanyIssuesInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write17_TransferNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"TransferNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"OldNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"NewNumber", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write18_TransferNumberInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write19_ISIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ISIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write20_ISINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write21_AcruedInterest(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"AcruedInterest", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write22_AcruedInterestInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write23_ChangePIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ChangePIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneC", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"OldPassword", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NewPassword", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"ConfirmNewPassword", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write24_ChangePINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write25_RESETPIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RESETPIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneC", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NewPIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write26_RESETPINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write27_PIN_Correct(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"PIN_Correct", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneIR", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write28_PIN_CorrectInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write29_InvestorRegistered(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"InvestorRegistered", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneIR", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write30_InvestorRegisteredInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write31_InvestorRegistered_IDNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"InvestorRegistered_IDNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write32_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write33_MyBroker(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MyBroker", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneB", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write34_MyBrokerInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write35_ChangeBROKER(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ChangeBROKER", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneB", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"NewBrokerCode", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[5]));
            }
            WriteEndElement();
        }

        public void Write36_ChangeBROKERInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write37_MNOReconciliations(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MNOReconciliations", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Record_Type", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"Client_Suffix", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"JointAcc", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Identification", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Title", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"Initials", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"Other_Names", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"Surname_Company_Name", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementString(@"Address1", @"EscrowService", ((global::System.String)p[11]));
            }
            if (pLength > 12) {
                WriteElementString(@"Address2", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"Address3", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"Town", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"Post_Code", @"EscrowService", ((global::System.String)p[15]));
            }
            if (pLength > 16) {
                WriteElementString(@"Country", @"EscrowService", ((global::System.String)p[16]));
            }
            if (pLength > 17) {
                WriteElementString(@"Fax_Number", @"EscrowService", ((global::System.String)p[17]));
            }
            if (pLength > 18) {
                WriteElementString(@"Email_address", @"EscrowService", ((global::System.String)p[18]));
            }
            if (pLength > 19) {
                WriteElementString(@"Date_of_Birth_Incorporation", @"EscrowService", ((global::System.String)p[19]));
            }
            if (pLength > 20) {
                WriteElementString(@"Gender", @"EscrowService", ((global::System.String)p[20]));
            }
            if (pLength > 21) {
                WriteElementString(@"Nationality", @"EscrowService", ((global::System.String)p[21]));
            }
            if (pLength > 22) {
                WriteElementString(@"Resident", @"EscrowService", ((global::System.String)p[22]));
            }
            if (pLength > 23) {
                WriteElementString(@"Tax_Bracket", @"EscrowService", ((global::System.String)p[23]));
            }
            if (pLength > 24) {
                WriteElementString(@"Telephone_Number", @"EscrowService", ((global::System.String)p[24]));
            }
            if (pLength > 25) {
                WriteElementString(@"Broker_Reference", @"EscrowService", ((global::System.String)p[25]));
            }
            if (pLength > 26) {
                WriteElementString(@"CallbackEndPointURL", @"EscrowService", ((global::System.String)p[26]));
            }
            if (pLength > 27) {
                WriteElementString(@"Client_CDSNum", @"EscrowService", ((global::System.String)p[27]));
            }
            WriteEndElement();
        }

        public void Write38_MNOReconciliationsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write39_MNOReconciliations_Bids(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MNOReconciliations_Bids", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"AmountPaid", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PaymentRefNo", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"TelephoneNumber", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"ReceiptNumber", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write40_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write41_PriceCalculator(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"PriceCalculator", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write42_PriceCalculatorInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write43_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DirectDebitForOBOPAY_12SEP_Test_Bulk", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"referenceid", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"customermsisdn", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"amount", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"batchref", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Decimal)((global::System.Decimal)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"narrative", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write44_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write45_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DirectDebitForOBOPAY_12SEP_Test", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"CustMobile", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"Amount", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Decimal)((global::System.Decimal)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"MerchantMSISDN", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write46_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write47_AIDA2016(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"AIDA2016", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"tel", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"amt", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"REF", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write48_AIDA2016InHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write49_Createnewaccount(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"Createnewaccount", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"acctype", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"broker", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"surname", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"middlename", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"first_name", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"initials", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"name_title", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"country", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"city", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"tel_no", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"mobile_no", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementString(@"currency", @"EscrowService", ((global::System.String)p[11]));
            }
            if (pLength > 12) {
                WriteElementString(@"id_type", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"id_no", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"nationality", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"date_of_birth", @"EscrowService", ((global::System.String)p[15]));
            }
            if (pLength > 16) {
                WriteElementString(@"gender", @"EscrowService", ((global::System.String)p[16]));
            }
            if (pLength > 17) {
                WriteElementString(@"address_1", @"EscrowService", ((global::System.String)p[17]));
            }
            if (pLength > 18) {
                WriteElementString(@"address_2", @"EscrowService", ((global::System.String)p[18]));
            }
            if (pLength > 19) {
                WriteElementString(@"address_3", @"EscrowService", ((global::System.String)p[19]));
            }
            if (pLength > 20) {
                WriteElementString(@"address_4", @"EscrowService", ((global::System.String)p[20]));
            }
            if (pLength > 21) {
                WriteElementString(@"email", @"EscrowService", ((global::System.String)p[21]));
            }
            if (pLength > 22) {
                WriteElementString(@"dvdnd_payee", @"EscrowService", ((global::System.String)p[22]));
            }
            if (pLength > 23) {
                WriteElementString(@"dvdnd_ac_class", @"EscrowService", ((global::System.String)p[23]));
            }
            if (pLength > 24) {
                WriteElementString(@"dvdnd_bank", @"EscrowService", ((global::System.String)p[24]));
            }
            if (pLength > 25) {
                WriteElementString(@"dvdnd_branch", @"EscrowService", ((global::System.String)p[25]));
            }
            if (pLength > 26) {
                WriteElementString(@"dvdnd_ac_no", @"EscrowService", ((global::System.String)p[26]));
            }
            if (pLength > 27) {
                WriteElementString(@"dvdnd_id_type", @"EscrowService", ((global::System.String)p[27]));
            }
            if (pLength > 28) {
                WriteElementString(@"dvdnd_id_no", @"EscrowService", ((global::System.String)p[28]));
            }
            if (pLength > 29) {
                WriteElementString(@"settlement_bank", @"EscrowService", ((global::System.String)p[29]));
            }
            if (pLength > 30) {
                WriteElementString(@"settlement_branch", @"EscrowService", ((global::System.String)p[30]));
            }
            if (pLength > 31) {
                WriteElementString(@"settlement_ac_no", @"EscrowService", ((global::System.String)p[31]));
            }
            if (pLength > 32) {
                WriteElementString(@"mobile_money", @"EscrowService", ((global::System.String)p[32]));
            }
            WriteEndElement();
        }

        public void Write50_CreatenewaccountInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write51_SubmitDocuments(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"SubmitDocuments", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"IDnumber", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Document", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"DocumentName", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Document_Type", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write52_SubmitDocumentsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write53_CheckIPOClosed_MNOs(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"CheckIPOClosed_MNOs", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"isin", @"EscrowService", ((global::System.String)p[0]));
            }
            WriteEndElement();
        }

        public void Write54_CheckIPOClosed_MNOsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write55_ReceiveUSSDReg(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDReg", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"NationalID", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            WriteEndElement();
        }

        public void Write56_ReceiveUSSDRegInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write57_IS_Global_Reached(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"IS_Global_Reached", @"EscrowService", null, false);
            WriteEndElement();
        }

        public void Write58_IS_Global_ReachedInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write59_ReceiveUSSDBID(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDBID", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_NumberBid", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"AmountBid", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"TelephoneBid", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            WriteEndElement();
        }

        public void Write60_ReceiveUSSDBIDInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write61_ReceiveUSSDEnquire(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDEnquire", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"TelephoneEnqure", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"emailaddress", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"ministt", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"company", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write62_ReceiveUSSDEnquireInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write63_BondIssues(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"BondIssues", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write64_BondIssuesInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write65_MyNumbers(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MyNumbers", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write66_MyNumbersInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write67_ListingDate(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ListingDate", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write68_ListingDateInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write69_RemoveNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RemoveNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write70_RemoveNumberInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write71_DeactivateAccount(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DeactivateAccount", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write72_DeactivateAccountInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write73_MNOReconciliations(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MNOReconciliations", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Record_Type", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"Client_Suffix", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"JointAcc", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Identification", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Title", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"Initials", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"Other_Names", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"Surname_Company_Name", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementString(@"Address1", @"EscrowService", ((global::System.String)p[11]));
            }
            if (pLength > 12) {
                WriteElementString(@"Address2", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"Address3", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"Town", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"Post_Code", @"EscrowService", ((global::System.String)p[15]));
            }
            if (pLength > 16) {
                WriteElementString(@"Country", @"EscrowService", ((global::System.String)p[16]));
            }
            if (pLength > 17) {
                WriteElementString(@"Fax_Number", @"EscrowService", ((global::System.String)p[17]));
            }
            if (pLength > 18) {
                WriteElementString(@"Email_address", @"EscrowService", ((global::System.String)p[18]));
            }
            if (pLength > 19) {
                WriteElementString(@"Date_of_Birth_Incorporation", @"EscrowService", ((global::System.String)p[19]));
            }
            if (pLength > 20) {
                WriteElementString(@"Gender", @"EscrowService", ((global::System.String)p[20]));
            }
            if (pLength > 21) {
                WriteElementString(@"Nationality", @"EscrowService", ((global::System.String)p[21]));
            }
            if (pLength > 22) {
                WriteElementString(@"Resident", @"EscrowService", ((global::System.String)p[22]));
            }
            if (pLength > 23) {
                WriteElementString(@"Tax_Bracket", @"EscrowService", ((global::System.String)p[23]));
            }
            if (pLength > 24) {
                WriteElementString(@"Telephone_Number", @"EscrowService", ((global::System.String)p[24]));
            }
            if (pLength > 25) {
                WriteElementString(@"Broker_Reference", @"EscrowService", ((global::System.String)p[25]));
            }
            if (pLength > 26) {
                WriteElementString(@"CallbackEndPointURL", @"EscrowService", ((global::System.String)p[26]));
            }
            if (pLength > 27) {
                WriteElementString(@"Client_CDSNum", @"EscrowService", ((global::System.String)p[27]));
            }
            WriteEndElement();
        }

        public void Write74_MNOReconciliationsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write75_MNOReconciliations_Bids(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MNOReconciliations_Bids", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"AmountPaid", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PaymentRefNo", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"TelephoneNumber", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"ReceiptNumber", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write76_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write77_PriceCalculator(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"PriceCalculator", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write78_PriceCalculatorInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write79_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DirectDebitForOBOPAY_12SEP_Test_Bulk", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"referenceid", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"customermsisdn", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"amount", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"batchref", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Decimal)((global::System.Decimal)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"narrative", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write80_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write81_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DirectDebitForOBOPAY_12SEP_Test", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"CustMobile", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"Amount", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Decimal)((global::System.Decimal)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"MerchantMSISDN", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write82_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write83_IS_Global_Reached(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"IS_Global_Reached", @"EscrowService", null, false);
            WriteEndElement();
        }

        public void Write84_IS_Global_ReachedInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write85_ReceiveUSSDBID(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDBID", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_NumberBid", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"AmountBid", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"TelephoneBid", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            WriteEndElement();
        }

        public void Write86_ReceiveUSSDBIDInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write87_ReceiveUSSDEnquire(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDEnquire", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"TelephoneEnqure", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"emailaddress", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"ministt", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"company", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write88_ReceiveUSSDEnquireInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write89_ReceiveUSSDBuyOrder(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDBuyOrder", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"TelephoneBorder", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NoofNotes", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"BasePrice", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write90_ReceiveUSSDBuyOrderInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write91_ReceiveUSSDSellOrder(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDSellOrder", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"CDS_Number", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"NoofNotes", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"TelephoneSelorder", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"BasePrice", @"EscrowService", ((global::System.String)p[8]));
            }
            WriteEndElement();
        }

        public void Write92_ReceiveUSSDSellOrderInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write93_CompanyIssues(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"CompanyIssues", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write94_CompanyIssuesInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write95_TransferNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"TransferNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"OldNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"NewNumber", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write96_TransferNumberInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write97_ISIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ISIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write98_ISINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write99_BondIssues(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"BondIssues", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write100_BondIssuesInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write101_MyNumbers(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MyNumbers", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write102_MyNumbersInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write103_ListingDate(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ListingDate", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write104_ListingDateInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write105_RemoveNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RemoveNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write106_RemoveNumberInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write107_DeactivateAccount(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"DeactivateAccount", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write108_DeactivateAccountInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write109_AcruedInterest(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"AcruedInterest", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"Company", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write110_AcruedInterestInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write111_ChangePIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ChangePIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneC", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"OldPassword", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NewPassword", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"ConfirmNewPassword", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write112_ChangePINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write113_RESETPIN(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RESETPIN", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneC", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"NewPIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[6]));
            }
            WriteEndElement();
        }

        public void Write114_RESETPINInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write115_PIN_Correct(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"PIN_Correct", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneIR", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[4]));
            }
            WriteEndElement();
        }

        public void Write116_PIN_CorrectInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write117_InvestorRegistered(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"InvestorRegistered", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneIR", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write118_InvestorRegisteredInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write119_InvestorRegistered_IDNumber(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"InvestorRegistered_IDNumber", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write120_Item(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write121_MyBroker(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"MyBroker", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneB", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write122_MyBrokerInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write123_ChangeBROKER(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ChangeBROKER", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"TelephoneB", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"NewBrokerCode", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[5]));
            }
            WriteEndElement();
        }

        public void Write124_ChangeBROKERInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write125_AIDA2016(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"AIDA2016", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"tel", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"amt", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"REF", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write126_AIDA2016InHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write127_Createnewaccount(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"Createnewaccount", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"broker", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"surname", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"middlename", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"first_name", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"initials", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"name_title", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"country", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"city", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"tel_no", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"mobile_no", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"currency", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementString(@"id_type", @"EscrowService", ((global::System.String)p[11]));
            }
            if (pLength > 12) {
                WriteElementString(@"id_no", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"nationality", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"date_of_birth", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"gender", @"EscrowService", ((global::System.String)p[15]));
            }
            if (pLength > 16) {
                WriteElementString(@"address_1", @"EscrowService", ((global::System.String)p[16]));
            }
            if (pLength > 17) {
                WriteElementString(@"address_2", @"EscrowService", ((global::System.String)p[17]));
            }
            if (pLength > 18) {
                WriteElementString(@"address_3", @"EscrowService", ((global::System.String)p[18]));
            }
            if (pLength > 19) {
                WriteElementString(@"address_4", @"EscrowService", ((global::System.String)p[19]));
            }
            if (pLength > 20) {
                WriteElementString(@"email", @"EscrowService", ((global::System.String)p[20]));
            }
            if (pLength > 21) {
                WriteElementString(@"dvdnd_payee", @"EscrowService", ((global::System.String)p[21]));
            }
            if (pLength > 22) {
                WriteElementString(@"dvdnd_ac_class", @"EscrowService", ((global::System.String)p[22]));
            }
            if (pLength > 23) {
                WriteElementString(@"dvdnd_bank", @"EscrowService", ((global::System.String)p[23]));
            }
            if (pLength > 24) {
                WriteElementString(@"dvdnd_branch", @"EscrowService", ((global::System.String)p[24]));
            }
            if (pLength > 25) {
                WriteElementString(@"dvdnd_ac_no", @"EscrowService", ((global::System.String)p[25]));
            }
            if (pLength > 26) {
                WriteElementString(@"dvdnd_id_type", @"EscrowService", ((global::System.String)p[26]));
            }
            if (pLength > 27) {
                WriteElementString(@"dvdnd_id_no", @"EscrowService", ((global::System.String)p[27]));
            }
            if (pLength > 28) {
                WriteElementString(@"settlement_bank", @"EscrowService", ((global::System.String)p[28]));
            }
            if (pLength > 29) {
                WriteElementString(@"settlement_branch", @"EscrowService", ((global::System.String)p[29]));
            }
            if (pLength > 30) {
                WriteElementString(@"settlement_ac_no", @"EscrowService", ((global::System.String)p[30]));
            }
            if (pLength > 31) {
                WriteElementString(@"mobile_money", @"EscrowService", ((global::System.String)p[31]));
            }
            WriteEndElement();
        }

        public void Write128_CreatenewaccountInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write129_SubmitDocuments(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"SubmitDocuments", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"IDnumber", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Document", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"DocumentName", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"Document_Type", @"EscrowService", ((global::System.String)p[3]));
            }
            WriteEndElement();
        }

        public void Write130_SubmitDocumentsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write131_CheckIPOClosed_MNOs(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"CheckIPOClosed_MNOs", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"isin", @"EscrowService", ((global::System.String)p[0]));
            }
            WriteEndElement();
        }

        public void Write132_CheckIPOClosed_MNOsInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write133_ReceiveUSSDReg(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"ReceiveUSSDReg", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Telephone", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"PIN", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"NationalID", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"UserName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"PassWord", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"MNO_", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Language", @"EscrowService", ((global::System.String)p[7]));
            }
            WriteEndElement();
        }

        public void Write134_ReceiveUSSDRegInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write135_NewClientRegistration(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"NewClientRegistration", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"IDNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"OtherName", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"SurnameOrCompanyName", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"PostalCode", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"Country", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementStringRaw(@"DateOfBirth", @"EscrowService", FromDateTime(((global::System.DateTime)p[8])));
            }
            if (pLength > 9) {
                WriteElementString(@"Gender", @"EscrowService", FromChar(((global::System.Char)p[9])));
            }
            if (pLength > 10) {
                WriteElementString(@"Nationality", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementStringRaw(@"Resident", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)p[11])));
            }
            if (pLength > 12) {
                WriteElementString(@"TelephoneNumber", @"EscrowService", ((global::System.String)p[12]));
            }
            if (pLength > 13) {
                WriteElementString(@"MNOCustodian", @"EscrowService", ((global::System.String)p[13]));
            }
            if (pLength > 14) {
                WriteElementString(@"CallbackUrl", @"EscrowService", ((global::System.String)p[14]));
            }
            if (pLength > 15) {
                WriteElementString(@"BrokerCode", @"EscrowService", ((global::System.String)p[15]));
            }
            WriteEndElement();
        }

        public void Write136_NewClientRegistrationInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write137_IPOStatusCheck(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"IPOStatusCheck", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[0]));
            }
            WriteEndElement();
        }

        public void Write138_IPOStatusCheckInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write139_RegisterBid(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"RegisterBid", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementStringRaw(@"Quantity", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Double)((global::System.Double)p[3])));
            }
            if (pLength > 4) {
                WriteElementString(@"ReferenceNumber", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"ClientType", @"EscrowService", ((global::System.String)p[5]));
            }
            if (pLength > 6) {
                WriteElementString(@"Custodian", @"EscrowService", ((global::System.String)p[6]));
            }
            if (pLength > 7) {
                WriteElementString(@"CSDAccountNumber", @"EscrowService", ((global::System.String)p[7]));
            }
            if (pLength > 8) {
                WriteElementString(@"MPESATransactionID", @"EscrowService", ((global::System.String)p[8]));
            }
            if (pLength > 9) {
                WriteElementString(@"ISIN", @"EscrowService", ((global::System.String)p[9]));
            }
            if (pLength > 10) {
                WriteElementString(@"TransNum", @"EscrowService", ((global::System.String)p[10]));
            }
            if (pLength > 11) {
                WriteElementStringRaw(@"PledgeIndicator", @"EscrowService", System.Xml.XmlConvert.ToString((global::System.Boolean)((global::System.Boolean)p[11])));
            }
            if (pLength > 12) {
                WriteElementString(@"PledgeeBPID", @"EscrowService", ((global::System.String)p[12]));
            }
            WriteEndElement();
        }

        public void Write140_RegisterBidInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write141_E_Enquiry(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"E_Enquiry", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"user_name", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"pass_word", @"EscrowService", ((global::System.String)p[2]));
            }
            if (pLength > 3) {
                WriteElementString(@"CDSNumber", @"EscrowService", ((global::System.String)p[3]));
            }
            if (pLength > 4) {
                WriteElementString(@"PerformFunction", @"EscrowService", ((global::System.String)p[4]));
            }
            if (pLength > 5) {
                WriteElementString(@"Ref", @"EscrowService", ((global::System.String)p[5]));
            }
            WriteEndElement();
        }

        public void Write142_E_EnquiryInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        public void Write143_GetOrders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
            WriteStartElement(@"GetOrders", @"EscrowService", null, false);
            if (pLength > 0) {
                WriteElementString(@"MNO", @"EscrowService", ((global::System.String)p[0]));
            }
            if (pLength > 1) {
                WriteElementString(@"Username", @"EscrowService", ((global::System.String)p[1]));
            }
            if (pLength > 2) {
                WriteElementString(@"Password", @"EscrowService", ((global::System.String)p[2]));
            }
            WriteEndElement();
        }

        public void Write144_GetOrdersInHeaders(object[] p) {
            WriteStartDocument();
            TopLevelElement();
            int pLength = p.Length;
        }

        protected override void InitCallbacks() {
        }
    }

    public class XmlSerializationReader1 : System.Xml.Serialization.XmlSerializationReader {

        public object[] Read1_GetOrdersResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations0 = 0;
            int readerCount0 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id1_GetOrdersResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations1 = 0;
                    int readerCount1 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_GetOrdersResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:GetOrdersResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:GetOrdersResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations1, ref readerCount1);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:GetOrdersResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations0, ref readerCount0);
            }
            return p;
        }

        public object[] Read2_GetOrdersResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations2 = 0;
            int readerCount2 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations2, ref readerCount2);
            }
            return p;
        }

        public object[] Read3_NewClientRegistrationResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations3 = 0;
            int readerCount3 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id4_NewClientRegistrationResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations4 = 0;
                    int readerCount4 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id5_NewClientRegistrationResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:NewClientRegistrationResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:NewClientRegistrationResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations4, ref readerCount4);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:NewClientRegistrationResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations3, ref readerCount3);
            }
            return p;
        }

        public object[] Read4_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations5 = 0;
            int readerCount5 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations5, ref readerCount5);
            }
            return p;
        }

        public object[] Read5_IPOStatusCheckResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations6 = 0;
            int readerCount6 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id6_IPOStatusCheckResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations7 = 0;
                    int readerCount7 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id7_IPOStatusCheckResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:IPOStatusCheckResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:IPOStatusCheckResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations7, ref readerCount7);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:IPOStatusCheckResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations6, ref readerCount6);
            }
            return p;
        }

        public object[] Read6_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations8 = 0;
            int readerCount8 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations8, ref readerCount8);
            }
            return p;
        }

        public object[] Read7_RegisterBidResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations9 = 0;
            int readerCount9 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id8_RegisterBidResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations10 = 0;
                    int readerCount10 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id9_RegisterBidResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RegisterBidResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RegisterBidResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations10, ref readerCount10);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RegisterBidResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations9, ref readerCount9);
            }
            return p;
        }

        public object[] Read8_RegisterBidResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations11 = 0;
            int readerCount11 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations11, ref readerCount11);
            }
            return p;
        }

        public object[] Read9_E_EnquiryResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations12 = 0;
            int readerCount12 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id10_E_EnquiryResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations13 = 0;
                    int readerCount13 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id11_E_EnquiryResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:E_EnquiryResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:E_EnquiryResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations13, ref readerCount13);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:E_EnquiryResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations12, ref readerCount12);
            }
            return p;
        }

        public object[] Read10_E_EnquiryResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations14 = 0;
            int readerCount14 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations14, ref readerCount14);
            }
            return p;
        }

        public object[] Read11_ReceiveUSSDBuyOrderResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations15 = 0;
            int readerCount15 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id12_ReceiveUSSDBuyOrderResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations16 = 0;
                    int readerCount16 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id13_ReceiveUSSDBuyOrderResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDBuyOrderResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDBuyOrderResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations16, ref readerCount16);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDBuyOrderResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations15, ref readerCount15);
            }
            return p;
        }

        public object[] Read12_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations17 = 0;
            int readerCount17 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations17, ref readerCount17);
            }
            return p;
        }

        public object[] Read13_ReceiveUSSDSellOrderResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations18 = 0;
            int readerCount18 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id14_ReceiveUSSDSellOrderResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations19 = 0;
                    int readerCount19 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id15_ReceiveUSSDSellOrderResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDSellOrderResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDSellOrderResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations19, ref readerCount19);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDSellOrderResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations18, ref readerCount18);
            }
            return p;
        }

        public object[] Read14_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations20 = 0;
            int readerCount20 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations20, ref readerCount20);
            }
            return p;
        }

        public object[] Read15_CompanyIssuesResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations21 = 0;
            int readerCount21 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id16_CompanyIssuesResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations22 = 0;
                    int readerCount22 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id17_CompanyIssuesResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CompanyIssuesResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CompanyIssuesResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations22, ref readerCount22);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CompanyIssuesResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations21, ref readerCount21);
            }
            return p;
        }

        public object[] Read16_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations23 = 0;
            int readerCount23 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations23, ref readerCount23);
            }
            return p;
        }

        public object[] Read17_TransferNumberResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations24 = 0;
            int readerCount24 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id18_TransferNumberResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations25 = 0;
                    int readerCount25 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id19_TransferNumberResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:TransferNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:TransferNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations25, ref readerCount25);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:TransferNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations24, ref readerCount24);
            }
            return p;
        }

        public object[] Read18_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations26 = 0;
            int readerCount26 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations26, ref readerCount26);
            }
            return p;
        }

        public object[] Read19_ISINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations27 = 0;
            int readerCount27 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id20_ISINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations28 = 0;
                    int readerCount28 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id21_ISINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ISINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ISINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations28, ref readerCount28);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ISINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations27, ref readerCount27);
            }
            return p;
        }

        public object[] Read20_ISINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations29 = 0;
            int readerCount29 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations29, ref readerCount29);
            }
            return p;
        }

        public object[] Read21_AcruedInterestResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations30 = 0;
            int readerCount30 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id22_AcruedInterestResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations31 = 0;
                    int readerCount31 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id23_AcruedInterestResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:AcruedInterestResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:AcruedInterestResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations31, ref readerCount31);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:AcruedInterestResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations30, ref readerCount30);
            }
            return p;
        }

        public object[] Read22_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations32 = 0;
            int readerCount32 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations32, ref readerCount32);
            }
            return p;
        }

        public object[] Read23_ChangePINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations33 = 0;
            int readerCount33 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id24_ChangePINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations34 = 0;
                    int readerCount34 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id25_ChangePINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ChangePINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ChangePINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations34, ref readerCount34);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ChangePINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations33, ref readerCount33);
            }
            return p;
        }

        public object[] Read24_ChangePINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations35 = 0;
            int readerCount35 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations35, ref readerCount35);
            }
            return p;
        }

        public object[] Read25_RESETPINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations36 = 0;
            int readerCount36 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id26_RESETPINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations37 = 0;
                    int readerCount37 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id27_RESETPINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RESETPINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RESETPINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations37, ref readerCount37);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RESETPINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations36, ref readerCount36);
            }
            return p;
        }

        public object[] Read26_RESETPINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations38 = 0;
            int readerCount38 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations38, ref readerCount38);
            }
            return p;
        }

        public object[] Read27_PIN_CorrectResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations39 = 0;
            int readerCount39 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id28_PIN_CorrectResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations40 = 0;
                    int readerCount40 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id29_PIN_CorrectResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:PIN_CorrectResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:PIN_CorrectResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations40, ref readerCount40);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:PIN_CorrectResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations39, ref readerCount39);
            }
            return p;
        }

        public object[] Read28_PIN_CorrectResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations41 = 0;
            int readerCount41 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations41, ref readerCount41);
            }
            return p;
        }

        public object[] Read29_InvestorRegisteredResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations42 = 0;
            int readerCount42 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id30_InvestorRegisteredResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations43 = 0;
                    int readerCount43 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id31_InvestorRegisteredResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:InvestorRegisteredResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:InvestorRegisteredResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations43, ref readerCount43);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:InvestorRegisteredResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations42, ref readerCount42);
            }
            return p;
        }

        public object[] Read30_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations44 = 0;
            int readerCount44 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations44, ref readerCount44);
            }
            return p;
        }

        public object[] Read31_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations45 = 0;
            int readerCount45 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id32_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations46 = 0;
                    int readerCount46 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id33_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:InvestorRegistered_IDNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:InvestorRegistered_IDNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations46, ref readerCount46);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:InvestorRegistered_IDNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations45, ref readerCount45);
            }
            return p;
        }

        public object[] Read32_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations47 = 0;
            int readerCount47 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations47, ref readerCount47);
            }
            return p;
        }

        public object[] Read33_MyBrokerResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations48 = 0;
            int readerCount48 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id34_MyBrokerResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations49 = 0;
                    int readerCount49 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id35_MyBrokerResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MyBrokerResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MyBrokerResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations49, ref readerCount49);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MyBrokerResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations48, ref readerCount48);
            }
            return p;
        }

        public object[] Read34_MyBrokerResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations50 = 0;
            int readerCount50 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations50, ref readerCount50);
            }
            return p;
        }

        public object[] Read35_ChangeBROKERResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations51 = 0;
            int readerCount51 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id36_ChangeBROKERResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations52 = 0;
                    int readerCount52 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id37_ChangeBROKERResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ChangeBROKERResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ChangeBROKERResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations52, ref readerCount52);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ChangeBROKERResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations51, ref readerCount51);
            }
            return p;
        }

        public object[] Read36_ChangeBROKERResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations53 = 0;
            int readerCount53 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations53, ref readerCount53);
            }
            return p;
        }

        public object[] Read37_MNOReconciliationsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations54 = 0;
            int readerCount54 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id38_MNOReconciliationsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations55 = 0;
                    int readerCount55 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id39_MNOReconciliationsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MNOReconciliationsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MNOReconciliationsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations55, ref readerCount55);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MNOReconciliationsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations54, ref readerCount54);
            }
            return p;
        }

        public object[] Read38_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations56 = 0;
            int readerCount56 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations56, ref readerCount56);
            }
            return p;
        }

        public object[] Read39_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations57 = 0;
            int readerCount57 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id40_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations58 = 0;
                    int readerCount58 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id41_MNOReconciliations_BidsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MNOReconciliations_BidsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MNOReconciliations_BidsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations58, ref readerCount58);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MNOReconciliations_BidsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations57, ref readerCount57);
            }
            return p;
        }

        public object[] Read40_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations59 = 0;
            int readerCount59 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations59, ref readerCount59);
            }
            return p;
        }

        public object[] Read41_PriceCalculatorResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations60 = 0;
            int readerCount60 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id42_PriceCalculatorResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations61 = 0;
                    int readerCount61 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id43_PriceCalculatorResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:PriceCalculatorResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:PriceCalculatorResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations61, ref readerCount61);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:PriceCalculatorResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations60, ref readerCount60);
            }
            return p;
        }

        public object[] Read42_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations62 = 0;
            int readerCount62 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations62, ref readerCount62);
            }
            return p;
        }

        public object[] Read43_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations63 = 0;
            int readerCount63 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id44_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations64 = 0;
                    int readerCount64 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id45_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations64, ref readerCount64);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations63, ref readerCount63);
            }
            return p;
        }

        public object[] Read44_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations65 = 0;
            int readerCount65 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations65, ref readerCount65);
            }
            return p;
        }

        public object[] Read45_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations66 = 0;
            int readerCount66 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id46_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations67 = 0;
                    int readerCount67 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id47_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations67, ref readerCount67);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations66, ref readerCount66);
            }
            return p;
        }

        public object[] Read46_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations68 = 0;
            int readerCount68 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations68, ref readerCount68);
            }
            return p;
        }

        public object[] Read47_AIDA2016Response() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations69 = 0;
            int readerCount69 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id48_AIDA2016Response, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations70 = 0;
                    int readerCount70 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id49_AIDA2016Result && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:AIDA2016Result");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:AIDA2016Result");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations70, ref readerCount70);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:AIDA2016Response");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations69, ref readerCount69);
            }
            return p;
        }

        public object[] Read48_AIDA2016ResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations71 = 0;
            int readerCount71 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations71, ref readerCount71);
            }
            return p;
        }

        public object[] Read49_CreatenewaccountResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations72 = 0;
            int readerCount72 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id50_CreatenewaccountResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations73 = 0;
                    int readerCount73 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id51_CreatenewaccountResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CreatenewaccountResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CreatenewaccountResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations73, ref readerCount73);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CreatenewaccountResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations72, ref readerCount72);
            }
            return p;
        }

        public object[] Read50_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations74 = 0;
            int readerCount74 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations74, ref readerCount74);
            }
            return p;
        }

        public object[] Read51_SubmitDocumentsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations75 = 0;
            int readerCount75 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id52_SubmitDocumentsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations76 = 0;
                    int readerCount76 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id53_SubmitDocumentsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:SubmitDocumentsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:SubmitDocumentsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations76, ref readerCount76);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:SubmitDocumentsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations75, ref readerCount75);
            }
            return p;
        }

        public object[] Read52_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations77 = 0;
            int readerCount77 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations77, ref readerCount77);
            }
            return p;
        }

        public object[] Read53_CheckIPOClosed_MNOsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations78 = 0;
            int readerCount78 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id54_CheckIPOClosed_MNOsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations79 = 0;
                    int readerCount79 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id55_CheckIPOClosed_MNOsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CheckIPOClosed_MNOsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CheckIPOClosed_MNOsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations79, ref readerCount79);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CheckIPOClosed_MNOsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations78, ref readerCount78);
            }
            return p;
        }

        public object[] Read54_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations80 = 0;
            int readerCount80 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations80, ref readerCount80);
            }
            return p;
        }

        public object[] Read55_ReceiveUSSDRegResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations81 = 0;
            int readerCount81 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id56_ReceiveUSSDRegResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations82 = 0;
                    int readerCount82 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id57_ReceiveUSSDRegResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDRegResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDRegResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations82, ref readerCount82);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDRegResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations81, ref readerCount81);
            }
            return p;
        }

        public object[] Read56_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations83 = 0;
            int readerCount83 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations83, ref readerCount83);
            }
            return p;
        }

        public object[] Read57_IS_Global_ReachedResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations84 = 0;
            int readerCount84 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id58_IS_Global_ReachedResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations85 = 0;
                    int readerCount85 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id59_IS_Global_ReachedResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:IS_Global_ReachedResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:IS_Global_ReachedResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations85, ref readerCount85);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:IS_Global_ReachedResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations84, ref readerCount84);
            }
            return p;
        }

        public object[] Read58_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations86 = 0;
            int readerCount86 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations86, ref readerCount86);
            }
            return p;
        }

        public object[] Read59_ReceiveUSSDBIDResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations87 = 0;
            int readerCount87 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id60_ReceiveUSSDBIDResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations88 = 0;
                    int readerCount88 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id61_ReceiveUSSDBIDResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDBIDResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDBIDResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations88, ref readerCount88);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDBIDResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations87, ref readerCount87);
            }
            return p;
        }

        public object[] Read60_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations89 = 0;
            int readerCount89 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations89, ref readerCount89);
            }
            return p;
        }

        public object[] Read61_ReceiveUSSDEnquireResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations90 = 0;
            int readerCount90 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id62_ReceiveUSSDEnquireResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations91 = 0;
                    int readerCount91 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id63_ReceiveUSSDEnquireResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDEnquireResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDEnquireResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations91, ref readerCount91);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDEnquireResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations90, ref readerCount90);
            }
            return p;
        }

        public object[] Read62_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations92 = 0;
            int readerCount92 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations92, ref readerCount92);
            }
            return p;
        }

        public object[] Read63_BondIssuesResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations93 = 0;
            int readerCount93 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id64_BondIssuesResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations94 = 0;
                    int readerCount94 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id65_BondIssuesResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:BondIssuesResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:BondIssuesResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations94, ref readerCount94);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:BondIssuesResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations93, ref readerCount93);
            }
            return p;
        }

        public object[] Read64_BondIssuesResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations95 = 0;
            int readerCount95 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations95, ref readerCount95);
            }
            return p;
        }

        public object[] Read65_MyNumbersResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations96 = 0;
            int readerCount96 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id66_MyNumbersResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations97 = 0;
                    int readerCount97 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id67_MyNumbersResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MyNumbersResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MyNumbersResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations97, ref readerCount97);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MyNumbersResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations96, ref readerCount96);
            }
            return p;
        }

        public object[] Read66_MyNumbersResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations98 = 0;
            int readerCount98 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations98, ref readerCount98);
            }
            return p;
        }

        public object[] Read67_ListingDateResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations99 = 0;
            int readerCount99 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id68_ListingDateResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations100 = 0;
                    int readerCount100 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id69_ListingDateResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ListingDateResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ListingDateResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations100, ref readerCount100);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ListingDateResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations99, ref readerCount99);
            }
            return p;
        }

        public object[] Read68_ListingDateResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations101 = 0;
            int readerCount101 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations101, ref readerCount101);
            }
            return p;
        }

        public object[] Read69_RemoveNumberResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations102 = 0;
            int readerCount102 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id70_RemoveNumberResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations103 = 0;
                    int readerCount103 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id71_RemoveNumberResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RemoveNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RemoveNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations103, ref readerCount103);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RemoveNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations102, ref readerCount102);
            }
            return p;
        }

        public object[] Read70_RemoveNumberResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations104 = 0;
            int readerCount104 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations104, ref readerCount104);
            }
            return p;
        }

        public object[] Read71_DeactivateAccountResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations105 = 0;
            int readerCount105 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id72_DeactivateAccountResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations106 = 0;
                    int readerCount106 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id73_DeactivateAccountResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DeactivateAccountResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DeactivateAccountResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations106, ref readerCount106);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DeactivateAccountResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations105, ref readerCount105);
            }
            return p;
        }

        public object[] Read72_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations107 = 0;
            int readerCount107 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations107, ref readerCount107);
            }
            return p;
        }

        public object[] Read73_MNOReconciliationsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations108 = 0;
            int readerCount108 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id38_MNOReconciliationsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations109 = 0;
                    int readerCount109 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id39_MNOReconciliationsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MNOReconciliationsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MNOReconciliationsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations109, ref readerCount109);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MNOReconciliationsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations108, ref readerCount108);
            }
            return p;
        }

        public object[] Read74_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations110 = 0;
            int readerCount110 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations110, ref readerCount110);
            }
            return p;
        }

        public object[] Read75_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations111 = 0;
            int readerCount111 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id40_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations112 = 0;
                    int readerCount112 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id41_MNOReconciliations_BidsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MNOReconciliations_BidsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MNOReconciliations_BidsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations112, ref readerCount112);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MNOReconciliations_BidsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations111, ref readerCount111);
            }
            return p;
        }

        public object[] Read76_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations113 = 0;
            int readerCount113 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations113, ref readerCount113);
            }
            return p;
        }

        public object[] Read77_PriceCalculatorResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations114 = 0;
            int readerCount114 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id42_PriceCalculatorResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations115 = 0;
                    int readerCount115 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id43_PriceCalculatorResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:PriceCalculatorResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:PriceCalculatorResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations115, ref readerCount115);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:PriceCalculatorResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations114, ref readerCount114);
            }
            return p;
        }

        public object[] Read78_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations116 = 0;
            int readerCount116 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations116, ref readerCount116);
            }
            return p;
        }

        public object[] Read79_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations117 = 0;
            int readerCount117 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id44_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations118 = 0;
                    int readerCount118 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id45_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations118, ref readerCount118);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DirectDebitForOBOPAY_12SEP_Test_BulkResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations117, ref readerCount117);
            }
            return p;
        }

        public object[] Read80_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations119 = 0;
            int readerCount119 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations119, ref readerCount119);
            }
            return p;
        }

        public object[] Read81_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations120 = 0;
            int readerCount120 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id46_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations121 = 0;
                    int readerCount121 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id47_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations121, ref readerCount121);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DirectDebitForOBOPAY_12SEP_TestResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations120, ref readerCount120);
            }
            return p;
        }

        public object[] Read82_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations122 = 0;
            int readerCount122 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations122, ref readerCount122);
            }
            return p;
        }

        public object[] Read83_IS_Global_ReachedResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations123 = 0;
            int readerCount123 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id58_IS_Global_ReachedResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations124 = 0;
                    int readerCount124 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id59_IS_Global_ReachedResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:IS_Global_ReachedResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:IS_Global_ReachedResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations124, ref readerCount124);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:IS_Global_ReachedResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations123, ref readerCount123);
            }
            return p;
        }

        public object[] Read84_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations125 = 0;
            int readerCount125 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations125, ref readerCount125);
            }
            return p;
        }

        public object[] Read85_ReceiveUSSDBIDResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations126 = 0;
            int readerCount126 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id60_ReceiveUSSDBIDResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations127 = 0;
                    int readerCount127 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id61_ReceiveUSSDBIDResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDBIDResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDBIDResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations127, ref readerCount127);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDBIDResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations126, ref readerCount126);
            }
            return p;
        }

        public object[] Read86_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations128 = 0;
            int readerCount128 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations128, ref readerCount128);
            }
            return p;
        }

        public object[] Read87_ReceiveUSSDEnquireResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations129 = 0;
            int readerCount129 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id62_ReceiveUSSDEnquireResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations130 = 0;
                    int readerCount130 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id63_ReceiveUSSDEnquireResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDEnquireResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDEnquireResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations130, ref readerCount130);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDEnquireResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations129, ref readerCount129);
            }
            return p;
        }

        public object[] Read88_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations131 = 0;
            int readerCount131 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations131, ref readerCount131);
            }
            return p;
        }

        public object[] Read89_ReceiveUSSDBuyOrderResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations132 = 0;
            int readerCount132 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id12_ReceiveUSSDBuyOrderResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations133 = 0;
                    int readerCount133 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id13_ReceiveUSSDBuyOrderResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDBuyOrderResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDBuyOrderResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations133, ref readerCount133);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDBuyOrderResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations132, ref readerCount132);
            }
            return p;
        }

        public object[] Read90_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations134 = 0;
            int readerCount134 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations134, ref readerCount134);
            }
            return p;
        }

        public object[] Read91_ReceiveUSSDSellOrderResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations135 = 0;
            int readerCount135 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id14_ReceiveUSSDSellOrderResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations136 = 0;
                    int readerCount136 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id15_ReceiveUSSDSellOrderResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDSellOrderResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDSellOrderResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations136, ref readerCount136);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDSellOrderResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations135, ref readerCount135);
            }
            return p;
        }

        public object[] Read92_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations137 = 0;
            int readerCount137 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations137, ref readerCount137);
            }
            return p;
        }

        public object[] Read93_CompanyIssuesResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations138 = 0;
            int readerCount138 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id16_CompanyIssuesResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations139 = 0;
                    int readerCount139 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id17_CompanyIssuesResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CompanyIssuesResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CompanyIssuesResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations139, ref readerCount139);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CompanyIssuesResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations138, ref readerCount138);
            }
            return p;
        }

        public object[] Read94_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations140 = 0;
            int readerCount140 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations140, ref readerCount140);
            }
            return p;
        }

        public object[] Read95_TransferNumberResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations141 = 0;
            int readerCount141 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id18_TransferNumberResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations142 = 0;
                    int readerCount142 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id19_TransferNumberResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:TransferNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:TransferNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations142, ref readerCount142);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:TransferNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations141, ref readerCount141);
            }
            return p;
        }

        public object[] Read96_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations143 = 0;
            int readerCount143 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations143, ref readerCount143);
            }
            return p;
        }

        public object[] Read97_ISINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations144 = 0;
            int readerCount144 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id20_ISINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations145 = 0;
                    int readerCount145 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id21_ISINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ISINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ISINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations145, ref readerCount145);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ISINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations144, ref readerCount144);
            }
            return p;
        }

        public object[] Read98_ISINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations146 = 0;
            int readerCount146 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations146, ref readerCount146);
            }
            return p;
        }

        public object[] Read99_BondIssuesResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations147 = 0;
            int readerCount147 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id64_BondIssuesResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations148 = 0;
                    int readerCount148 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id65_BondIssuesResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:BondIssuesResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:BondIssuesResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations148, ref readerCount148);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:BondIssuesResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations147, ref readerCount147);
            }
            return p;
        }

        public object[] Read100_BondIssuesResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations149 = 0;
            int readerCount149 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations149, ref readerCount149);
            }
            return p;
        }

        public object[] Read101_MyNumbersResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations150 = 0;
            int readerCount150 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id66_MyNumbersResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations151 = 0;
                    int readerCount151 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id67_MyNumbersResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MyNumbersResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MyNumbersResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations151, ref readerCount151);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MyNumbersResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations150, ref readerCount150);
            }
            return p;
        }

        public object[] Read102_MyNumbersResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations152 = 0;
            int readerCount152 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations152, ref readerCount152);
            }
            return p;
        }

        public object[] Read103_ListingDateResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations153 = 0;
            int readerCount153 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id68_ListingDateResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations154 = 0;
                    int readerCount154 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id69_ListingDateResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ListingDateResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ListingDateResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations154, ref readerCount154);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ListingDateResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations153, ref readerCount153);
            }
            return p;
        }

        public object[] Read104_ListingDateResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations155 = 0;
            int readerCount155 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations155, ref readerCount155);
            }
            return p;
        }

        public object[] Read105_RemoveNumberResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations156 = 0;
            int readerCount156 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id70_RemoveNumberResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations157 = 0;
                    int readerCount157 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id71_RemoveNumberResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RemoveNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RemoveNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations157, ref readerCount157);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RemoveNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations156, ref readerCount156);
            }
            return p;
        }

        public object[] Read106_RemoveNumberResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations158 = 0;
            int readerCount158 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations158, ref readerCount158);
            }
            return p;
        }

        public object[] Read107_DeactivateAccountResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations159 = 0;
            int readerCount159 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id72_DeactivateAccountResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations160 = 0;
                    int readerCount160 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id73_DeactivateAccountResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:DeactivateAccountResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:DeactivateAccountResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations160, ref readerCount160);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:DeactivateAccountResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations159, ref readerCount159);
            }
            return p;
        }

        public object[] Read108_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations161 = 0;
            int readerCount161 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations161, ref readerCount161);
            }
            return p;
        }

        public object[] Read109_AcruedInterestResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations162 = 0;
            int readerCount162 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id22_AcruedInterestResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations163 = 0;
                    int readerCount163 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id23_AcruedInterestResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:AcruedInterestResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:AcruedInterestResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations163, ref readerCount163);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:AcruedInterestResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations162, ref readerCount162);
            }
            return p;
        }

        public object[] Read110_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations164 = 0;
            int readerCount164 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations164, ref readerCount164);
            }
            return p;
        }

        public object[] Read111_ChangePINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations165 = 0;
            int readerCount165 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id24_ChangePINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations166 = 0;
                    int readerCount166 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id25_ChangePINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ChangePINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ChangePINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations166, ref readerCount166);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ChangePINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations165, ref readerCount165);
            }
            return p;
        }

        public object[] Read112_ChangePINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations167 = 0;
            int readerCount167 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations167, ref readerCount167);
            }
            return p;
        }

        public object[] Read113_RESETPINResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations168 = 0;
            int readerCount168 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id26_RESETPINResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations169 = 0;
                    int readerCount169 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id27_RESETPINResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RESETPINResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RESETPINResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations169, ref readerCount169);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RESETPINResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations168, ref readerCount168);
            }
            return p;
        }

        public object[] Read114_RESETPINResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations170 = 0;
            int readerCount170 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations170, ref readerCount170);
            }
            return p;
        }

        public object[] Read115_PIN_CorrectResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations171 = 0;
            int readerCount171 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id28_PIN_CorrectResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations172 = 0;
                    int readerCount172 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id29_PIN_CorrectResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:PIN_CorrectResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:PIN_CorrectResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations172, ref readerCount172);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:PIN_CorrectResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations171, ref readerCount171);
            }
            return p;
        }

        public object[] Read116_PIN_CorrectResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations173 = 0;
            int readerCount173 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations173, ref readerCount173);
            }
            return p;
        }

        public object[] Read117_InvestorRegisteredResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations174 = 0;
            int readerCount174 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id30_InvestorRegisteredResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations175 = 0;
                    int readerCount175 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id31_InvestorRegisteredResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:InvestorRegisteredResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:InvestorRegisteredResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations175, ref readerCount175);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:InvestorRegisteredResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations174, ref readerCount174);
            }
            return p;
        }

        public object[] Read118_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations176 = 0;
            int readerCount176 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations176, ref readerCount176);
            }
            return p;
        }

        public object[] Read119_Item() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations177 = 0;
            int readerCount177 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id32_Item, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations178 = 0;
                    int readerCount178 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id33_Item && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:InvestorRegistered_IDNumberResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:InvestorRegistered_IDNumberResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations178, ref readerCount178);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:InvestorRegistered_IDNumberResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations177, ref readerCount177);
            }
            return p;
        }

        public object[] Read120_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations179 = 0;
            int readerCount179 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations179, ref readerCount179);
            }
            return p;
        }

        public object[] Read121_MyBrokerResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations180 = 0;
            int readerCount180 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id34_MyBrokerResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations181 = 0;
                    int readerCount181 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id35_MyBrokerResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:MyBrokerResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:MyBrokerResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations181, ref readerCount181);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:MyBrokerResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations180, ref readerCount180);
            }
            return p;
        }

        public object[] Read122_MyBrokerResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations182 = 0;
            int readerCount182 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations182, ref readerCount182);
            }
            return p;
        }

        public object[] Read123_ChangeBROKERResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations183 = 0;
            int readerCount183 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id36_ChangeBROKERResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations184 = 0;
                    int readerCount184 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id37_ChangeBROKERResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ChangeBROKERResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ChangeBROKERResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations184, ref readerCount184);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ChangeBROKERResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations183, ref readerCount183);
            }
            return p;
        }

        public object[] Read124_ChangeBROKERResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations185 = 0;
            int readerCount185 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations185, ref readerCount185);
            }
            return p;
        }

        public object[] Read125_AIDA2016Response() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations186 = 0;
            int readerCount186 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id48_AIDA2016Response, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations187 = 0;
                    int readerCount187 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id49_AIDA2016Result && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:AIDA2016Result");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:AIDA2016Result");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations187, ref readerCount187);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:AIDA2016Response");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations186, ref readerCount186);
            }
            return p;
        }

        public object[] Read126_AIDA2016ResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations188 = 0;
            int readerCount188 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations188, ref readerCount188);
            }
            return p;
        }

        public object[] Read127_CreatenewaccountResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations189 = 0;
            int readerCount189 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id50_CreatenewaccountResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations190 = 0;
                    int readerCount190 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id51_CreatenewaccountResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CreatenewaccountResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CreatenewaccountResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations190, ref readerCount190);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CreatenewaccountResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations189, ref readerCount189);
            }
            return p;
        }

        public object[] Read128_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations191 = 0;
            int readerCount191 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations191, ref readerCount191);
            }
            return p;
        }

        public object[] Read129_SubmitDocumentsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations192 = 0;
            int readerCount192 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id52_SubmitDocumentsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations193 = 0;
                    int readerCount193 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id53_SubmitDocumentsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:SubmitDocumentsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:SubmitDocumentsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations193, ref readerCount193);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:SubmitDocumentsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations192, ref readerCount192);
            }
            return p;
        }

        public object[] Read130_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations194 = 0;
            int readerCount194 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations194, ref readerCount194);
            }
            return p;
        }

        public object[] Read131_CheckIPOClosed_MNOsResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations195 = 0;
            int readerCount195 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id54_CheckIPOClosed_MNOsResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations196 = 0;
                    int readerCount196 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id55_CheckIPOClosed_MNOsResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:CheckIPOClosed_MNOsResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:CheckIPOClosed_MNOsResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations196, ref readerCount196);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:CheckIPOClosed_MNOsResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations195, ref readerCount195);
            }
            return p;
        }

        public object[] Read132_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations197 = 0;
            int readerCount197 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations197, ref readerCount197);
            }
            return p;
        }

        public object[] Read133_ReceiveUSSDRegResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations198 = 0;
            int readerCount198 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id56_ReceiveUSSDRegResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations199 = 0;
                    int readerCount199 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id57_ReceiveUSSDRegResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:ReceiveUSSDRegResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:ReceiveUSSDRegResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations199, ref readerCount199);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:ReceiveUSSDRegResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations198, ref readerCount198);
            }
            return p;
        }

        public object[] Read134_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations200 = 0;
            int readerCount200 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations200, ref readerCount200);
            }
            return p;
        }

        public object[] Read135_NewClientRegistrationResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            p[0] = new global::System.Int32();
            Reader.MoveToContent();
            int whileIterations201 = 0;
            int readerCount201 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id4_NewClientRegistrationResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations202 = 0;
                    int readerCount202 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id5_NewClientRegistrationResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:NewClientRegistrationResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:NewClientRegistrationResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations202, ref readerCount202);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:NewClientRegistrationResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations201, ref readerCount201);
            }
            return p;
        }

        public object[] Read136_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations203 = 0;
            int readerCount203 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations203, ref readerCount203);
            }
            return p;
        }

        public object[] Read137_IPOStatusCheckResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations204 = 0;
            int readerCount204 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id6_IPOStatusCheckResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations205 = 0;
                    int readerCount205 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id7_IPOStatusCheckResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:IPOStatusCheckResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:IPOStatusCheckResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations205, ref readerCount205);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:IPOStatusCheckResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations204, ref readerCount204);
            }
            return p;
        }

        public object[] Read138_Item() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations206 = 0;
            int readerCount206 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations206, ref readerCount206);
            }
            return p;
        }

        public object[] Read139_RegisterBidResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations207 = 0;
            int readerCount207 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id8_RegisterBidResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations208 = 0;
                    int readerCount208 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id9_RegisterBidResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:RegisterBidResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:RegisterBidResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations208, ref readerCount208);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:RegisterBidResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations207, ref readerCount207);
            }
            return p;
        }

        public object[] Read140_RegisterBidResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations209 = 0;
            int readerCount209 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations209, ref readerCount209);
            }
            return p;
        }

        public object[] Read141_E_EnquiryResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations210 = 0;
            int readerCount210 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id10_E_EnquiryResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations211 = 0;
                    int readerCount211 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id11_E_EnquiryResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:E_EnquiryResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:E_EnquiryResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations211, ref readerCount211);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:E_EnquiryResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations210, ref readerCount210);
            }
            return p;
        }

        public object[] Read142_E_EnquiryResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations212 = 0;
            int readerCount212 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations212, ref readerCount212);
            }
            return p;
        }

        public object[] Read143_GetOrdersResponse() {
            Reader.MoveToContent();
            object[] p = new object[1];
            Reader.MoveToContent();
            int whileIterations213 = 0;
            int readerCount213 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.IsStartElement(id1_GetOrdersResponse, id2_EscrowService)) {
                    bool[] paramsRead = new bool[1];
                    if (Reader.IsEmptyElement) { Reader.Skip(); Reader.MoveToContent(); continue; }
                    Reader.ReadStartElement();
                    Reader.MoveToContent();
                    int whileIterations214 = 0;
                    int readerCount214 = ReaderCount;
                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_GetOrdersResult && (object) Reader.NamespaceURI == (object)id2_EscrowService)) {
                                {
                                    p[0] = Reader.ReadElementString();
                                }
                                paramsRead[0] = true;
                            }
                            else {
                                UnknownNode((object)p, @"EscrowService:GetOrdersResult");
                            }
                        }
                        else {
                            UnknownNode((object)p, @"EscrowService:GetOrdersResult");
                        }
                        Reader.MoveToContent();
                        CheckReaderCount(ref whileIterations214, ref readerCount214);
                    }
                    ReadEndElement();
                }
                else {
                    UnknownNode(null, @"EscrowService:GetOrdersResponse");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations213, ref readerCount213);
            }
            return p;
        }

        public object[] Read144_GetOrdersResponseOutHeaders() {
            Reader.MoveToContent();
            object[] p = new object[0];
            bool[] paramsRead = new bool[0];
            Reader.MoveToContent();
            int whileIterations215 = 0;
            int readerCount215 = ReaderCount;
            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                    UnknownNode((object)p, @"");
                }
                else {
                    UnknownNode((object)p, @"");
                }
                Reader.MoveToContent();
                CheckReaderCount(ref whileIterations215, ref readerCount215);
            }
            return p;
        }

        protected override void InitCallbacks() {
        }

        string id4_NewClientRegistrationResponse;
        string id51_CreatenewaccountResult;
        string id29_PIN_CorrectResult;
        string id16_CompanyIssuesResponse;
        string id52_SubmitDocumentsResponse;
        string id36_ChangeBROKERResponse;
        string id55_CheckIPOClosed_MNOsResult;
        string id64_BondIssuesResponse;
        string id2_EscrowService;
        string id12_ReceiveUSSDBuyOrderResponse;
        string id39_MNOReconciliationsResult;
        string id53_SubmitDocumentsResult;
        string id10_E_EnquiryResponse;
        string id58_IS_Global_ReachedResponse;
        string id72_DeactivateAccountResponse;
        string id30_InvestorRegisteredResponse;
        string id60_ReceiveUSSDBIDResponse;
        string id8_RegisterBidResponse;
        string id34_MyBrokerResponse;
        string id26_RESETPINResponse;
        string id49_AIDA2016Result;
        string id14_ReceiveUSSDSellOrderResponse;
        string id38_MNOReconciliationsResponse;
        string id59_IS_Global_ReachedResult;
        string id45_Item;
        string id66_MyNumbersResponse;
        string id31_InvestorRegisteredResult;
        string id54_CheckIPOClosed_MNOsResponse;
        string id46_Item;
        string id19_TransferNumberResult;
        string id15_ReceiveUSSDSellOrderResult;
        string id11_E_EnquiryResult;
        string id18_TransferNumberResponse;
        string id17_CompanyIssuesResult;
        string id1_GetOrdersResponse;
        string id6_IPOStatusCheckResponse;
        string id35_MyBrokerResult;
        string id22_AcruedInterestResponse;
        string id40_Item;
        string id63_ReceiveUSSDEnquireResult;
        string id68_ListingDateResponse;
        string id5_NewClientRegistrationResult;
        string id67_MyNumbersResult;
        string id43_PriceCalculatorResult;
        string id42_PriceCalculatorResponse;
        string id20_ISINResponse;
        string id33_Item;
        string id23_AcruedInterestResult;
        string id71_RemoveNumberResult;
        string id50_CreatenewaccountResponse;
        string id28_PIN_CorrectResponse;
        string id61_ReceiveUSSDBIDResult;
        string id21_ISINResult;
        string id41_MNOReconciliations_BidsResult;
        string id7_IPOStatusCheckResult;
        string id24_ChangePINResponse;
        string id13_ReceiveUSSDBuyOrderResult;
        string id32_Item;
        string id56_ReceiveUSSDRegResponse;
        string id44_Item;
        string id9_RegisterBidResult;
        string id27_RESETPINResult;
        string id37_ChangeBROKERResult;
        string id73_DeactivateAccountResult;
        string id25_ChangePINResult;
        string id65_BondIssuesResult;
        string id62_ReceiveUSSDEnquireResponse;
        string id70_RemoveNumberResponse;
        string id57_ReceiveUSSDRegResult;
        string id47_Item;
        string id3_GetOrdersResult;
        string id69_ListingDateResult;
        string id48_AIDA2016Response;

        protected override void InitIDs() {
            id4_NewClientRegistrationResponse = Reader.NameTable.Add(@"NewClientRegistrationResponse");
            id51_CreatenewaccountResult = Reader.NameTable.Add(@"CreatenewaccountResult");
            id29_PIN_CorrectResult = Reader.NameTable.Add(@"PIN_CorrectResult");
            id16_CompanyIssuesResponse = Reader.NameTable.Add(@"CompanyIssuesResponse");
            id52_SubmitDocumentsResponse = Reader.NameTable.Add(@"SubmitDocumentsResponse");
            id36_ChangeBROKERResponse = Reader.NameTable.Add(@"ChangeBROKERResponse");
            id55_CheckIPOClosed_MNOsResult = Reader.NameTable.Add(@"CheckIPOClosed_MNOsResult");
            id64_BondIssuesResponse = Reader.NameTable.Add(@"BondIssuesResponse");
            id2_EscrowService = Reader.NameTable.Add(@"EscrowService");
            id12_ReceiveUSSDBuyOrderResponse = Reader.NameTable.Add(@"ReceiveUSSDBuyOrderResponse");
            id39_MNOReconciliationsResult = Reader.NameTable.Add(@"MNOReconciliationsResult");
            id53_SubmitDocumentsResult = Reader.NameTable.Add(@"SubmitDocumentsResult");
            id10_E_EnquiryResponse = Reader.NameTable.Add(@"E_EnquiryResponse");
            id58_IS_Global_ReachedResponse = Reader.NameTable.Add(@"IS_Global_ReachedResponse");
            id72_DeactivateAccountResponse = Reader.NameTable.Add(@"DeactivateAccountResponse");
            id30_InvestorRegisteredResponse = Reader.NameTable.Add(@"InvestorRegisteredResponse");
            id60_ReceiveUSSDBIDResponse = Reader.NameTable.Add(@"ReceiveUSSDBIDResponse");
            id8_RegisterBidResponse = Reader.NameTable.Add(@"RegisterBidResponse");
            id34_MyBrokerResponse = Reader.NameTable.Add(@"MyBrokerResponse");
            id26_RESETPINResponse = Reader.NameTable.Add(@"RESETPINResponse");
            id49_AIDA2016Result = Reader.NameTable.Add(@"AIDA2016Result");
            id14_ReceiveUSSDSellOrderResponse = Reader.NameTable.Add(@"ReceiveUSSDSellOrderResponse");
            id38_MNOReconciliationsResponse = Reader.NameTable.Add(@"MNOReconciliationsResponse");
            id59_IS_Global_ReachedResult = Reader.NameTable.Add(@"IS_Global_ReachedResult");
            id45_Item = Reader.NameTable.Add(@"DirectDebitForOBOPAY_12SEP_Test_BulkResult");
            id66_MyNumbersResponse = Reader.NameTable.Add(@"MyNumbersResponse");
            id31_InvestorRegisteredResult = Reader.NameTable.Add(@"InvestorRegisteredResult");
            id54_CheckIPOClosed_MNOsResponse = Reader.NameTable.Add(@"CheckIPOClosed_MNOsResponse");
            id46_Item = Reader.NameTable.Add(@"DirectDebitForOBOPAY_12SEP_TestResponse");
            id19_TransferNumberResult = Reader.NameTable.Add(@"TransferNumberResult");
            id15_ReceiveUSSDSellOrderResult = Reader.NameTable.Add(@"ReceiveUSSDSellOrderResult");
            id11_E_EnquiryResult = Reader.NameTable.Add(@"E_EnquiryResult");
            id18_TransferNumberResponse = Reader.NameTable.Add(@"TransferNumberResponse");
            id17_CompanyIssuesResult = Reader.NameTable.Add(@"CompanyIssuesResult");
            id1_GetOrdersResponse = Reader.NameTable.Add(@"GetOrdersResponse");
            id6_IPOStatusCheckResponse = Reader.NameTable.Add(@"IPOStatusCheckResponse");
            id35_MyBrokerResult = Reader.NameTable.Add(@"MyBrokerResult");
            id22_AcruedInterestResponse = Reader.NameTable.Add(@"AcruedInterestResponse");
            id40_Item = Reader.NameTable.Add(@"MNOReconciliations_BidsResponse");
            id63_ReceiveUSSDEnquireResult = Reader.NameTable.Add(@"ReceiveUSSDEnquireResult");
            id68_ListingDateResponse = Reader.NameTable.Add(@"ListingDateResponse");
            id5_NewClientRegistrationResult = Reader.NameTable.Add(@"NewClientRegistrationResult");
            id67_MyNumbersResult = Reader.NameTable.Add(@"MyNumbersResult");
            id43_PriceCalculatorResult = Reader.NameTable.Add(@"PriceCalculatorResult");
            id42_PriceCalculatorResponse = Reader.NameTable.Add(@"PriceCalculatorResponse");
            id20_ISINResponse = Reader.NameTable.Add(@"ISINResponse");
            id33_Item = Reader.NameTable.Add(@"InvestorRegistered_IDNumberResult");
            id23_AcruedInterestResult = Reader.NameTable.Add(@"AcruedInterestResult");
            id71_RemoveNumberResult = Reader.NameTable.Add(@"RemoveNumberResult");
            id50_CreatenewaccountResponse = Reader.NameTable.Add(@"CreatenewaccountResponse");
            id28_PIN_CorrectResponse = Reader.NameTable.Add(@"PIN_CorrectResponse");
            id61_ReceiveUSSDBIDResult = Reader.NameTable.Add(@"ReceiveUSSDBIDResult");
            id21_ISINResult = Reader.NameTable.Add(@"ISINResult");
            id41_MNOReconciliations_BidsResult = Reader.NameTable.Add(@"MNOReconciliations_BidsResult");
            id7_IPOStatusCheckResult = Reader.NameTable.Add(@"IPOStatusCheckResult");
            id24_ChangePINResponse = Reader.NameTable.Add(@"ChangePINResponse");
            id13_ReceiveUSSDBuyOrderResult = Reader.NameTable.Add(@"ReceiveUSSDBuyOrderResult");
            id32_Item = Reader.NameTable.Add(@"InvestorRegistered_IDNumberResponse");
            id56_ReceiveUSSDRegResponse = Reader.NameTable.Add(@"ReceiveUSSDRegResponse");
            id44_Item = Reader.NameTable.Add(@"DirectDebitForOBOPAY_12SEP_Test_BulkResponse");
            id9_RegisterBidResult = Reader.NameTable.Add(@"RegisterBidResult");
            id27_RESETPINResult = Reader.NameTable.Add(@"RESETPINResult");
            id37_ChangeBROKERResult = Reader.NameTable.Add(@"ChangeBROKERResult");
            id73_DeactivateAccountResult = Reader.NameTable.Add(@"DeactivateAccountResult");
            id25_ChangePINResult = Reader.NameTable.Add(@"ChangePINResult");
            id65_BondIssuesResult = Reader.NameTable.Add(@"BondIssuesResult");
            id62_ReceiveUSSDEnquireResponse = Reader.NameTable.Add(@"ReceiveUSSDEnquireResponse");
            id70_RemoveNumberResponse = Reader.NameTable.Add(@"RemoveNumberResponse");
            id57_ReceiveUSSDRegResult = Reader.NameTable.Add(@"ReceiveUSSDRegResult");
            id47_Item = Reader.NameTable.Add(@"DirectDebitForOBOPAY_12SEP_TestResult");
            id3_GetOrdersResult = Reader.NameTable.Add(@"GetOrdersResult");
            id69_ListingDateResult = Reader.NameTable.Add(@"ListingDateResult");
            id48_AIDA2016Response = Reader.NameTable.Add(@"AIDA2016Response");
        }
    }

    public abstract class XmlSerializer1 : System.Xml.Serialization.XmlSerializer {
        protected override System.Xml.Serialization.XmlSerializationReader CreateReader() {
            return new XmlSerializationReader1();
        }
        protected override System.Xml.Serialization.XmlSerializationWriter CreateWriter() {
            return new XmlSerializationWriter1();
        }
    }

    public sealed class ArrayOfObjectSerializer : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write1_GetOrders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer1 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read1_GetOrdersResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer2 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write2_GetOrdersInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer3 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read2_GetOrdersResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer4 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistration", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write3_NewClientRegistration((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer5 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read3_NewClientRegistrationResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer6 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write4_NewClientRegistrationInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer7 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read4_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer8 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheck", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write5_IPOStatusCheck((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer9 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read5_IPOStatusCheckResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer10 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write6_IPOStatusCheckInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer11 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read6_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer12 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBid", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write7_RegisterBid((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer13 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read7_RegisterBidResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer14 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write8_RegisterBidInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer15 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read8_RegisterBidResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer16 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_Enquiry", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write9_E_Enquiry((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer17 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read9_E_EnquiryResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer18 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write10_E_EnquiryInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer19 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read10_E_EnquiryResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer20 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrder", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write11_ReceiveUSSDBuyOrder((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer21 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read11_ReceiveUSSDBuyOrderResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer22 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write12_ReceiveUSSDBuyOrderInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer23 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read12_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer24 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrder", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write13_ReceiveUSSDSellOrder((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer25 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read13_ReceiveUSSDSellOrderResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer26 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write14_ReceiveUSSDSellOrderInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer27 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read14_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer28 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssues", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write15_CompanyIssues((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer29 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read15_CompanyIssuesResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer30 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write16_CompanyIssuesInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer31 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read16_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer32 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write17_TransferNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer33 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read17_TransferNumberResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer34 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write18_TransferNumberInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer35 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read18_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer36 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write19_ISIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer37 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read19_ISINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer38 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write20_ISINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer39 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read20_ISINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer40 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterest", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write21_AcruedInterest((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer41 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read21_AcruedInterestResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer42 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write22_AcruedInterestInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer43 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read22_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer44 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write23_ChangePIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer45 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read23_ChangePINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer46 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write24_ChangePINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer47 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read24_ChangePINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer48 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write25_RESETPIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer49 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read25_RESETPINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer50 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write26_RESETPINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer51 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read26_RESETPINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer52 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_Correct", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write27_PIN_Correct((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer53 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read27_PIN_CorrectResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer54 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write28_PIN_CorrectInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer55 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read28_PIN_CorrectResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer56 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write29_InvestorRegistered((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer57 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read29_InvestorRegisteredResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer58 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write30_InvestorRegisteredInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer59 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read30_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer60 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write31_InvestorRegistered_IDNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer61 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read31_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer62 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write32_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer63 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read32_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer64 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBroker", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write33_MyBroker((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer65 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read33_MyBrokerResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer66 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write34_MyBrokerInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer67 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read34_MyBrokerResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer68 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKER", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write35_ChangeBROKER((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer69 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read35_ChangeBROKERResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer70 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write36_ChangeBROKERInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer71 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read36_ChangeBROKERResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer72 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write37_MNOReconciliations((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer73 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read37_MNOReconciliationsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer74 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write38_MNOReconciliationsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer75 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read38_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer76 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_Bids", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write39_MNOReconciliations_Bids((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer77 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read39_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer78 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write40_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer79 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read40_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer80 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculator", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write41_PriceCalculator((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer81 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read41_PriceCalculatorResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer82 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write42_PriceCalculatorInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer83 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read42_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer84 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_Bulk", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write43_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer85 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read43_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer86 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write44_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer87 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read44_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer88 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write45_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer89 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read45_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer90 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write46_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer91 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read46_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer92 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write47_AIDA2016((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer93 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016Response", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read47_AIDA2016Response();
        }
    }

    public sealed class ArrayOfObjectSerializer94 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016InHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write48_AIDA2016InHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer95 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016ResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read48_AIDA2016ResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer96 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"Createnewaccount", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write49_Createnewaccount((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer97 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read49_CreatenewaccountResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer98 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write50_CreatenewaccountInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer99 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read50_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer100 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocuments", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write51_SubmitDocuments((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer101 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read51_SubmitDocumentsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer102 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write52_SubmitDocumentsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer103 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read52_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer104 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOs", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write53_CheckIPOClosed_MNOs((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer105 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read53_CheckIPOClosed_MNOsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer106 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write54_CheckIPOClosed_MNOsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer107 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read54_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer108 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDReg", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write55_ReceiveUSSDReg((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer109 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read55_ReceiveUSSDRegResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer110 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write56_ReceiveUSSDRegInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer111 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read56_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer112 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_Reached", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write57_IS_Global_Reached((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer113 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read57_IS_Global_ReachedResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer114 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write58_IS_Global_ReachedInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer115 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read58_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer116 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBID", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write59_ReceiveUSSDBID((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer117 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read59_ReceiveUSSDBIDResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer118 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write60_ReceiveUSSDBIDInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer119 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read60_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer120 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquire", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write61_ReceiveUSSDEnquire((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer121 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read61_ReceiveUSSDEnquireResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer122 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write62_ReceiveUSSDEnquireInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer123 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read62_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer124 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssues", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write63_BondIssues((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer125 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read63_BondIssuesResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer126 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write64_BondIssuesInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer127 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read64_BondIssuesResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer128 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbers", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write65_MyNumbers((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer129 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read65_MyNumbersResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer130 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write66_MyNumbersInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer131 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read66_MyNumbersResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer132 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDate", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write67_ListingDate((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer133 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read67_ListingDateResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer134 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write68_ListingDateInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer135 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read68_ListingDateResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer136 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write69_RemoveNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer137 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read69_RemoveNumberResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer138 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write70_RemoveNumberInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer139 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read70_RemoveNumberResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer140 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccount", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write71_DeactivateAccount((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer141 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read71_DeactivateAccountResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer142 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write72_DeactivateAccountInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer143 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read72_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer144 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write73_MNOReconciliations((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer145 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read73_MNOReconciliationsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer146 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write74_MNOReconciliationsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer147 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliationsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read74_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer148 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_Bids", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write75_MNOReconciliations_Bids((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer149 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read75_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer150 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write76_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer151 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MNOReconciliations_BidsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read76_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer152 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculator", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write77_PriceCalculator((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer153 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read77_PriceCalculatorResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer154 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write78_PriceCalculatorInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer155 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PriceCalculatorResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read78_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer156 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_Bulk", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write79_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer157 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read79_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer158 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write80_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer159 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test_BulkResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read80_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer160 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_Test", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write81_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer161 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read81_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer162 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write82_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer163 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DirectDebitForOBOPAY_12SEP_TestResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read82_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer164 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_Reached", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write83_IS_Global_Reached((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer165 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read83_IS_Global_ReachedResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer166 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write84_IS_Global_ReachedInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer167 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IS_Global_ReachedResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read84_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer168 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBID", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write85_ReceiveUSSDBID((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer169 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read85_ReceiveUSSDBIDResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer170 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write86_ReceiveUSSDBIDInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer171 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBIDResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read86_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer172 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquire", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write87_ReceiveUSSDEnquire((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer173 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read87_ReceiveUSSDEnquireResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer174 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write88_ReceiveUSSDEnquireInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer175 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDEnquireResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read88_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer176 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrder", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write89_ReceiveUSSDBuyOrder((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer177 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read89_ReceiveUSSDBuyOrderResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer178 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write90_ReceiveUSSDBuyOrderInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer179 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDBuyOrderResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read90_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer180 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrder", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write91_ReceiveUSSDSellOrder((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer181 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read91_ReceiveUSSDSellOrderResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer182 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write92_ReceiveUSSDSellOrderInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer183 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDSellOrderResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read92_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer184 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssues", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write93_CompanyIssues((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer185 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read93_CompanyIssuesResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer186 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write94_CompanyIssuesInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer187 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CompanyIssuesResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read94_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer188 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write95_TransferNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer189 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read95_TransferNumberResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer190 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write96_TransferNumberInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer191 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"TransferNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read96_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer192 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write97_ISIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer193 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read97_ISINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer194 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write98_ISINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer195 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ISINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read98_ISINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer196 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssues", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write99_BondIssues((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer197 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read99_BondIssuesResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer198 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write100_BondIssuesInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer199 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"BondIssuesResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read100_BondIssuesResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer200 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbers", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write101_MyNumbers((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer201 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read101_MyNumbersResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer202 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write102_MyNumbersInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer203 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyNumbersResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read102_MyNumbersResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer204 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDate", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write103_ListingDate((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer205 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read103_ListingDateResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer206 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write104_ListingDateInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer207 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ListingDateResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read104_ListingDateResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer208 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write105_RemoveNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer209 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read105_RemoveNumberResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer210 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write106_RemoveNumberInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer211 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RemoveNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read106_RemoveNumberResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer212 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccount", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write107_DeactivateAccount((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer213 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read107_DeactivateAccountResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer214 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write108_DeactivateAccountInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer215 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"DeactivateAccountResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read108_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer216 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterest", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write109_AcruedInterest((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer217 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read109_AcruedInterestResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer218 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write110_AcruedInterestInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer219 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AcruedInterestResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read110_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer220 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write111_ChangePIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer221 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read111_ChangePINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer222 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write112_ChangePINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer223 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangePINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read112_ChangePINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer224 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPIN", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write113_RESETPIN((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer225 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read113_RESETPINResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer226 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write114_RESETPINInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer227 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RESETPINResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read114_RESETPINResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer228 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_Correct", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write115_PIN_Correct((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer229 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read115_PIN_CorrectResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer230 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write116_PIN_CorrectInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer231 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"PIN_CorrectResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read116_PIN_CorrectResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer232 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write117_InvestorRegistered((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer233 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read117_InvestorRegisteredResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer234 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write118_InvestorRegisteredInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer235 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegisteredResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read118_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer236 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumber", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write119_InvestorRegistered_IDNumber((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer237 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read119_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer238 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write120_Item((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer239 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"InvestorRegistered_IDNumberResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read120_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer240 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBroker", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write121_MyBroker((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer241 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read121_MyBrokerResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer242 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write122_MyBrokerInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer243 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"MyBrokerResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read122_MyBrokerResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer244 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKER", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write123_ChangeBROKER((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer245 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read123_ChangeBROKERResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer246 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write124_ChangeBROKERInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer247 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ChangeBROKERResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read124_ChangeBROKERResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer248 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write125_AIDA2016((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer249 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016Response", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read125_AIDA2016Response();
        }
    }

    public sealed class ArrayOfObjectSerializer250 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016InHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write126_AIDA2016InHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer251 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"AIDA2016ResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read126_AIDA2016ResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer252 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"Createnewaccount", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write127_Createnewaccount((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer253 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read127_CreatenewaccountResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer254 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write128_CreatenewaccountInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer255 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CreatenewaccountResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read128_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer256 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocuments", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write129_SubmitDocuments((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer257 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read129_SubmitDocumentsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer258 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write130_SubmitDocumentsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer259 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"SubmitDocumentsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read130_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer260 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOs", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write131_CheckIPOClosed_MNOs((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer261 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read131_CheckIPOClosed_MNOsResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer262 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write132_CheckIPOClosed_MNOsInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer263 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"CheckIPOClosed_MNOsResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read132_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer264 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDReg", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write133_ReceiveUSSDReg((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer265 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read133_ReceiveUSSDRegResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer266 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write134_ReceiveUSSDRegInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer267 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"ReceiveUSSDRegResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read134_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer268 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistration", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write135_NewClientRegistration((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer269 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read135_NewClientRegistrationResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer270 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write136_NewClientRegistrationInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer271 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"NewClientRegistrationResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read136_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer272 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheck", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write137_IPOStatusCheck((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer273 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read137_IPOStatusCheckResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer274 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write138_IPOStatusCheckInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer275 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"IPOStatusCheckResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read138_Item();
        }
    }

    public sealed class ArrayOfObjectSerializer276 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBid", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write139_RegisterBid((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer277 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read139_RegisterBidResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer278 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write140_RegisterBidInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer279 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"RegisterBidResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read140_RegisterBidResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer280 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_Enquiry", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write141_E_Enquiry((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer281 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read141_E_EnquiryResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer282 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write142_E_EnquiryInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer283 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"E_EnquiryResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read142_E_EnquiryResponseOutHeaders();
        }
    }

    public sealed class ArrayOfObjectSerializer284 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write143_GetOrders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer285 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersResponse", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read143_GetOrdersResponse();
        }
    }

    public sealed class ArrayOfObjectSerializer286 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersInHeaders", @"EscrowService");
        }

        protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
            ((XmlSerializationWriter1)writer).Write144_GetOrdersInHeaders((object[])objectToSerialize);
        }
    }

    public sealed class ArrayOfObjectSerializer287 : XmlSerializer1 {

        public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
            return xmlReader.IsStartElement(@"GetOrdersResponseOutHeaders", @"EscrowService");
        }

        protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
            return ((XmlSerializationReader1)reader).Read144_GetOrdersResponseOutHeaders();
        }
    }

    public class XmlSerializerContract : global::System.Xml.Serialization.XmlSerializerImplementation {
        public override global::System.Xml.Serialization.XmlSerializationReader Reader { get { return new XmlSerializationReader1(); } }
        public override global::System.Xml.Serialization.XmlSerializationWriter Writer { get { return new XmlSerializationWriter1(); } }
        System.Collections.Hashtable readMethods = null;
        public override System.Collections.Hashtable ReadMethods {
            get {
                if (readMethods == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):Response"] = @"Read1_GetOrdersResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):OutHeaders"] = @"Read2_GetOrdersResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):Response"] = @"Read3_NewClientRegistrationResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read4_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):Response"] = @"Read5_IPOStatusCheckResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):OutHeaders"] = @"Read6_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):Response"] = @"Read7_RegisterBidResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):OutHeaders"] = @"Read8_RegisterBidResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read9_E_EnquiryResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read10_E_EnquiryResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read11_ReceiveUSSDBuyOrderResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read12_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read13_ReceiveUSSDSellOrderResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read14_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):Response"] = @"Read15_CompanyIssuesResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):OutHeaders"] = @"Read16_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read17_TransferNumberResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read18_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):Response"] = @"Read19_ISINResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read20_ISINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read21_AcruedInterestResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read22_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read23_ChangePINResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read24_ChangePINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read25_RESETPINResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read26_RESETPINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read27_PIN_CorrectResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read28_PIN_CorrectResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):Response"] = @"Read29_InvestorRegisteredResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read30_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):Response"] = @"Read31_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read32_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):Response"] = @"Read33_MyBrokerResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read34_MyBrokerResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read35_ChangeBROKERResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read36_ChangeBROKERResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read37_MNOReconciliationsResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read38_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read39_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read40_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):Response"] = @"Read41_PriceCalculatorResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read42_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):Response"] = @"Read43_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders"] = @"Read44_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):Response"] = @"Read45_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders"] = @"Read46_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):Response"] = @"Read47_AIDA2016Response";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):OutHeaders"] = @"Read48_AIDA2016ResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read49_CreatenewaccountResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read50_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):Response"] = @"Read51_SubmitDocumentsResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read52_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):Response"] = @"Read53_CheckIPOClosed_MNOsResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):OutHeaders"] = @"Read54_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read55_ReceiveUSSDRegResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read56_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():Response"] = @"Read57_IS_Global_ReachedResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():OutHeaders"] = @"Read58_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read59_ReceiveUSSDBIDResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read60_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read61_ReceiveUSSDEnquireResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read62_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):Response"] = @"Read63_BondIssuesResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read64_BondIssuesResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):Response"] = @"Read65_MyNumbersResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read66_MyNumbersResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):Response"] = @"Read67_ListingDateResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read68_ListingDateResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):Response"] = @"Read69_RemoveNumberResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read70_RemoveNumberResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):Response"] = @"Read71_DeactivateAccountResponse";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read72_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read73_MNOReconciliationsResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read74_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read75_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read76_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):Response"] = @"Read77_PriceCalculatorResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read78_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):Response"] = @"Read79_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders"] = @"Read80_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):Response"] = @"Read81_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders"] = @"Read82_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():Response"] = @"Read83_IS_Global_ReachedResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():OutHeaders"] = @"Read84_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read85_ReceiveUSSDBIDResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read86_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read87_ReceiveUSSDEnquireResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read88_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read89_ReceiveUSSDBuyOrderResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read90_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read91_ReceiveUSSDSellOrderResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read92_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):Response"] = @"Read93_CompanyIssuesResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):OutHeaders"] = @"Read94_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read95_TransferNumberResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read96_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):Response"] = @"Read97_ISINResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read98_ISINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):Response"] = @"Read99_BondIssuesResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read100_BondIssuesResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):Response"] = @"Read101_MyNumbersResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read102_MyNumbersResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):Response"] = @"Read103_ListingDateResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read104_ListingDateResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):Response"] = @"Read105_RemoveNumberResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read106_RemoveNumberResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):Response"] = @"Read107_DeactivateAccountResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read108_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read109_AcruedInterestResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read110_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read111_ChangePINResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read112_ChangePINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read113_RESETPINResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read114_RESETPINResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):Response"] = @"Read115_PIN_CorrectResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read116_PIN_CorrectResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):Response"] = @"Read117_InvestorRegisteredResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read118_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):Response"] = @"Read119_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read120_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):Response"] = @"Read121_MyBrokerResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read122_MyBrokerResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read123_ChangeBROKERResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read124_ChangeBROKERResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):Response"] = @"Read125_AIDA2016Response";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):OutHeaders"] = @"Read126_AIDA2016ResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read127_CreatenewaccountResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read128_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):Response"] = @"Read129_SubmitDocumentsResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):OutHeaders"] = @"Read130_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):Response"] = @"Read131_CheckIPOClosed_MNOsResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):OutHeaders"] = @"Read132_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read133_ReceiveUSSDRegResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read134_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):Response"] = @"Read135_NewClientRegistrationResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read136_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):Response"] = @"Read137_IPOStatusCheckResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):OutHeaders"] = @"Read138_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):Response"] = @"Read139_RegisterBidResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):OutHeaders"] = @"Read140_RegisterBidResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):Response"] = @"Read141_E_EnquiryResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders"] = @"Read142_E_EnquiryResponseOutHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):Response"] = @"Read143_GetOrdersResponse";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):OutHeaders"] = @"Read144_GetOrdersResponseOutHeaders";
                    if (readMethods == null) readMethods = _tmp;
                }
                return readMethods;
            }
        }
        System.Collections.Hashtable writeMethods = null;
        public override System.Collections.Hashtable WriteMethods {
            get {
                if (writeMethods == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String)"] = @"Write1_GetOrders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):InHeaders"] = @"Write2_GetOrdersInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String)"] = @"Write3_NewClientRegistration";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):InHeaders"] = @"Write4_NewClientRegistrationInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String)"] = @"Write5_IPOStatusCheck";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):InHeaders"] = @"Write6_IPOStatusCheckInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String)"] = @"Write7_RegisterBid";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):InHeaders"] = @"Write8_RegisterBidInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write9_E_Enquiry";
                    _tmp[@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write10_E_EnquiryInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write11_ReceiveUSSDBuyOrder";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write12_ReceiveUSSDBuyOrderInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write13_ReceiveUSSDSellOrder";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write14_ReceiveUSSDSellOrderInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String)"] = @"Write15_CompanyIssues";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):InHeaders"] = @"Write16_CompanyIssuesInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String)"] = @"Write17_TransferNumber";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write18_TransferNumberInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String)"] = @"Write19_ISIN";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):InHeaders"] = @"Write20_ISINInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String)"] = @"Write21_AcruedInterest";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write22_AcruedInterestInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write23_ChangePIN";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write24_ChangePINInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write25_RESETPIN";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write26_RESETPINInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String)"] = @"Write27_PIN_Correct";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write28_PIN_CorrectInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String)"] = @"Write29_InvestorRegistered";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):InHeaders"] = @"Write30_InvestorRegisteredInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String)"] = @"Write31_InvestorRegistered_IDNumber";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):InHeaders"] = @"Write32_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String)"] = @"Write33_MyBroker";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):InHeaders"] = @"Write34_MyBrokerInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write35_ChangeBROKER";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write36_ChangeBROKERInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write37_MNOReconciliations";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write38_MNOReconciliationsInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write39_MNOReconciliations_Bids";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write40_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String)"] = @"Write41_PriceCalculator";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):InHeaders"] = @"Write42_PriceCalculatorInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String)"] = @"Write43_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):InHeaders"] = @"Write44_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String)"] = @"Write45_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):InHeaders"] = @"Write46_Item";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String)"] = @"Write47_AIDA2016";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):InHeaders"] = @"Write48_AIDA2016InHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write49_Createnewaccount";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write50_CreatenewaccountInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String)"] = @"Write51_SubmitDocuments";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):InHeaders"] = @"Write52_SubmitDocumentsInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String)"] = @"Write53_CheckIPOClosed_MNOs";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):InHeaders"] = @"Write54_CheckIPOClosed_MNOsInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write55_ReceiveUSSDReg";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write56_ReceiveUSSDRegInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached()"] = @"Write57_IS_Global_Reached";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():InHeaders"] = @"Write58_IS_Global_ReachedInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write59_ReceiveUSSDBID";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write60_ReceiveUSSDBIDInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write61_ReceiveUSSDEnquire";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write62_ReceiveUSSDEnquireInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String)"] = @"Write63_BondIssues";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):InHeaders"] = @"Write64_BondIssuesInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String)"] = @"Write65_MyNumbers";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):InHeaders"] = @"Write66_MyNumbersInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String)"] = @"Write67_ListingDate";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):InHeaders"] = @"Write68_ListingDateInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String)"] = @"Write69_RemoveNumber";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):InHeaders"] = @"Write70_RemoveNumberInHeaders";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String)"] = @"Write71_DeactivateAccount";
                    _tmp[@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):InHeaders"] = @"Write72_DeactivateAccountInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write73_MNOReconciliations";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write74_MNOReconciliationsInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write75_MNOReconciliations_Bids";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write76_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String)"] = @"Write77_PriceCalculator";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):InHeaders"] = @"Write78_PriceCalculatorInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String)"] = @"Write79_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):InHeaders"] = @"Write80_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String)"] = @"Write81_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):InHeaders"] = @"Write82_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached()"] = @"Write83_IS_Global_Reached";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():InHeaders"] = @"Write84_IS_Global_ReachedInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write85_ReceiveUSSDBID";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write86_ReceiveUSSDBIDInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write87_ReceiveUSSDEnquire";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write88_ReceiveUSSDEnquireInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write89_ReceiveUSSDBuyOrder";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write90_ReceiveUSSDBuyOrderInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write91_ReceiveUSSDSellOrder";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write92_ReceiveUSSDSellOrderInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String)"] = @"Write93_CompanyIssues";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):InHeaders"] = @"Write94_CompanyIssuesInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String)"] = @"Write95_TransferNumber";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write96_TransferNumberInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String)"] = @"Write97_ISIN";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):InHeaders"] = @"Write98_ISINInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String)"] = @"Write99_BondIssues";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):InHeaders"] = @"Write100_BondIssuesInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String)"] = @"Write101_MyNumbers";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):InHeaders"] = @"Write102_MyNumbersInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String)"] = @"Write103_ListingDate";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):InHeaders"] = @"Write104_ListingDateInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String)"] = @"Write105_RemoveNumber";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):InHeaders"] = @"Write106_RemoveNumberInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String)"] = @"Write107_DeactivateAccount";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):InHeaders"] = @"Write108_DeactivateAccountInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String)"] = @"Write109_AcruedInterest";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write110_AcruedInterestInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write111_ChangePIN";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write112_ChangePINInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write113_RESETPIN";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write114_RESETPINInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String)"] = @"Write115_PIN_Correct";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write116_PIN_CorrectInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String)"] = @"Write117_InvestorRegistered";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):InHeaders"] = @"Write118_InvestorRegisteredInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String)"] = @"Write119_InvestorRegistered_IDNumber";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):InHeaders"] = @"Write120_Item";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String)"] = @"Write121_MyBroker";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):InHeaders"] = @"Write122_MyBrokerInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write123_ChangeBROKER";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write124_ChangeBROKERInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String)"] = @"Write125_AIDA2016";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):InHeaders"] = @"Write126_AIDA2016InHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write127_Createnewaccount";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write128_CreatenewaccountInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String)"] = @"Write129_SubmitDocuments";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):InHeaders"] = @"Write130_SubmitDocumentsInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String)"] = @"Write131_CheckIPOClosed_MNOs";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):InHeaders"] = @"Write132_CheckIPOClosed_MNOsInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write133_ReceiveUSSDReg";
                    _tmp[@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write134_ReceiveUSSDRegInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String)"] = @"Write135_NewClientRegistration";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):InHeaders"] = @"Write136_NewClientRegistrationInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String)"] = @"Write137_IPOStatusCheck";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):InHeaders"] = @"Write138_IPOStatusCheckInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String)"] = @"Write139_RegisterBid";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):InHeaders"] = @"Write140_RegisterBidInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String)"] = @"Write141_E_Enquiry";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders"] = @"Write142_E_EnquiryInHeaders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String)"] = @"Write143_GetOrders";
                    _tmp[@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):InHeaders"] = @"Write144_GetOrdersInHeaders";
                    if (writeMethods == null) writeMethods = _tmp;
                }
                return writeMethods;
            }
        }
        System.Collections.Hashtable typedSerializers = null;
        public override System.Collections.Hashtable TypedSerializers {
            get {
                if (typedSerializers == null) {
                    System.Collections.Hashtable _tmp = new System.Collections.Hashtable();
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String)", new ArrayOfObjectSerializer92());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer16());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer194());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer265());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer171());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer212());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer73());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String)", new ArrayOfObjectSerializer84());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer270());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer117());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():InHeaders", new ArrayOfObjectSerializer166());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer37());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer257());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String)", new ArrayOfObjectSerializer12());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer229());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer5());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer44());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer180());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer216());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer74());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer72());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer213());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer224());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):Response", new ArrayOfObjectSerializer9());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer65());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer174());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer237());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer7());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer79());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer250());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer143());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer102());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer141());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):OutHeaders", new ArrayOfObjectSerializer275());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer215());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer221());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer128());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer188());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer23());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer259());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer142());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer204());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):OutHeaders", new ArrayOfObjectSerializer15());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer193());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):InHeaders", new ArrayOfObjectSerializer162());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String)", new ArrayOfObjectSerializer104());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer60());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):Response", new ArrayOfObjectSerializer89());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer249());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer51());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer82());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):Response", new ArrayOfObjectSerializer161());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer238());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String)", new ArrayOfObjectSerializer184());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer118());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer77());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer49());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer210());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):InHeaders", new ArrayOfObjectSerializer158());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer100());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer3());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer146());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer39());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String)", new ArrayOfObjectSerializer272());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer252());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders", new ArrayOfObjectSerializer163());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer233());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer170());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer98());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer125());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer155());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer4());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String)", new ArrayOfObjectSerializer());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer254());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer223());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer282());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer200());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer209());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer150());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer127());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer240());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer52());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer168());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer95());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer69());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer140());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer271());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer236());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer198());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer267());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer123());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached()", new ArrayOfObjectSerializer112());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):InHeaders", new ArrayOfObjectSerializer14());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String)", new ArrayOfObjectSerializer260());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():Response", new ArrayOfObjectSerializer113());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer266());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer97());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer138());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer38());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer153());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer203());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer93());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer24());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer116());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String)", new ArrayOfObjectSerializer156());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer56());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer147());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer137());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer283());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):OutHeaders", new ArrayOfObjectSerializer279());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer197());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer220());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached()", new ArrayOfObjectSerializer164());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer31());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer199());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer34());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String)", new ArrayOfObjectSerializer248());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer45());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():InHeaders", new ArrayOfObjectSerializer114());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer120());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer75());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer187());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer154());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer59());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer6());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer243());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer195());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer109());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):Response", new ArrayOfObjectSerializer13());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):OutHeaders", new ArrayOfObjectSerializer107());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer57());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer71());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer235());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer225());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer183());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer133());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer130());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer222());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer144());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer46());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer42());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer43());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):InHeaders", new ArrayOfObjectSerializer262());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders", new ArrayOfObjectSerializer87());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer131());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer202());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer176());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer255());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer145());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer110());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer185());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer242());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer151());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer99());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders", new ArrayOfObjectSerializer159());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer190());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer40());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer244());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer63());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):InHeaders", new ArrayOfObjectSerializer278());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DeactivateAccount(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer214());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):Response", new ArrayOfObjectSerializer157());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer191());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer169());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer66());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer182());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer96());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer268());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer139());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String)", new ArrayOfObjectSerializer88());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer64());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer25());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer287());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer189());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer103());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer61());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer22());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer228());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer280());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String Createnewaccount(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer253());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer17());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer18());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):InHeaders", new ArrayOfObjectSerializer90());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():Response", new ArrayOfObjectSerializer165());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer33());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangePIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer47());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):Response", new ArrayOfObjectSerializer105());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer205());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer172());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer217());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer55());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String):Response", new ArrayOfObjectSerializer277());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:Int32 NewClientRegistration(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.DateTime, Char, System.String, Boolean, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer269());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String)", new ArrayOfObjectSerializer284());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String)", new ArrayOfObjectSerializer160());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):InHeaders", new ArrayOfObjectSerializer274());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer50());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer70());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer136());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer206());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer178());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer32());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer67());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer81());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer207());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer68());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String IS_Global_Reached():OutHeaders", new ArrayOfObjectSerializer115());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer124());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer78());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer152());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String)", new ArrayOfObjectSerializer8());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer129());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer286());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer258());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer149());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer54());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):InHeaders", new ArrayOfObjectSerializer106());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer132());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer246());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):InHeaders", new ArrayOfObjectSerializer86());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer232());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBID(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer119());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer186());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer26());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer53());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer80());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer231());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer264());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer196());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer256());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer36());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyBroker(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer241());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer211());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer29());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer245());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String TransferNumber(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer35());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer19());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer122());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer175());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MyNumbers(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer201());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer21());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer2());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer30());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer108());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer27());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer20());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer219());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer234());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String SubmitDocuments(System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer101());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer48());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer239());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String InvestorRegistered_IDNumber(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer62());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer41());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer76());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer227());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String GetOrders(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer285());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDSellOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer181());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RemoveNumber(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer208());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String E_Enquiry(System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer281());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String MNOReconciliations_Bids(System.String, System.String, System.String, System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer148());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:Int32 InvestorRegistered(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer58());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String RESETPIN(System.String, System.String, System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer226());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):OutHeaders", new ArrayOfObjectSerializer263());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer177());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test(System.String, System.String, System.String, System.Decimal, System.String):OutHeaders", new ArrayOfObjectSerializer91());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):OutHeaders", new ArrayOfObjectSerializer11());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String CompanyIssues(System.String, System.String, System.String)", new ArrayOfObjectSerializer28());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String IPOStatusCheck(System.String):InHeaders", new ArrayOfObjectSerializer10());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer134());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:Int32 PIN_Correct(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer230());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String BondIssues(System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer126());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String PriceCalculator(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer83());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ISIN(System.String, System.String, System.String, System.String)", new ArrayOfObjectSerializer192());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String IPOStatusCheck(System.String):Response", new ArrayOfObjectSerializer273());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer94());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ListingDate(System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer135());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String DirectDebitForOBOPAY_12SEP_Test_Bulk(System.String, System.String, System.String, System.Decimal, System.String):Response", new ArrayOfObjectSerializer85());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AcruedInterest(System.String, System.String, System.String, System.String, System.String):InHeaders", new ArrayOfObjectSerializer218());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer173());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowService:System.String RegisterBid(System.String, System.String, System.String, Double, System.String, System.String, System.String, System.String, System.String, System.String, System.String, Boolean, System.String)", new ArrayOfObjectSerializer276());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String IS_Global_Reached():OutHeaders", new ArrayOfObjectSerializer167());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String CheckIPOClosed_MNOs(System.String):Response", new ArrayOfObjectSerializer261());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDReg(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer111());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ReceiveUSSDBuyOrder(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer179());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String ChangeBROKER(System.String, System.String, System.String, System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer247());
                    _tmp.Add(@"BrokerOffice.WebReference2.EscrowServiceSoap:System.String AIDA2016(System.String, System.String, System.String):OutHeaders", new ArrayOfObjectSerializer251());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowServiceSoap:System.String ReceiveUSSDEnquire(System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String, System.String):Response", new ArrayOfObjectSerializer121());
                    _tmp.Add(@"BrokerOffice.WebReference.EscrowService:System.String GetOrders(System.String, System.String, System.String):Response", new ArrayOfObjectSerializer1());
                    if (typedSerializers == null) typedSerializers = _tmp;
                }
                return typedSerializers;
            }
        }
        public override System.Boolean CanSerialize(System.Type type) {
            if (type == typeof(global::BrokerOffice.WebReference.EscrowService)) return true;
            if (type == typeof(global::BrokerOffice.WebReference.EscrowServiceSoap)) return true;
            if (type == typeof(global::BrokerOffice.WebReference2.EscrowServiceSoap)) return true;
            if (type == typeof(global::BrokerOffice.WebReference2.EscrowService)) return true;
            return false;
        }
        public override System.Xml.Serialization.XmlSerializer GetSerializer(System.Type type) {
            return null;
        }
    }
}
