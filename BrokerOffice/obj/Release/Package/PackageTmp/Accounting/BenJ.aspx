﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="BenJ.aspx.cs" Inherits="BrokerOffice.Accounting.BenJ" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Beneficiaries/Donations</i>
    </div>

      <br />
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                   Beneficiaries/Donations</a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                     </a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                      <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
<asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                              <tr>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Script ID"></dx:ASPxLabel>
                                  </td>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Certificate Number"></dx:ASPxLabel>
                                  </td>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text=" Total  Shares"></dx:ASPxLabel>
                                  </td>
                              </tr>
                                    <tr>
                                  <td>
                                      <dx:ASPxTextBox ID="bnumber" runat="server" CssClass="form-control" ReadOnly="True" OnTextChanged="bnumber_TextChanged"></dx:ASPxTextBox>
                                  </td>
                                  <td>
                                      <dx:ASPxTextBox ID="btran" runat="server" CssClass="form-control" ReadOnly="True"></dx:ASPxTextBox>
                                  </td> <td>
                                      <dx:ASPxTextBox ID="btotal" runat="server" CssClass="form-control"  ReadOnly="True"></dx:ASPxTextBox>
                                  </td>
                              </tr>
                                  
                                   <tr>
                                       
                                       <td>
                                           <asp:Label ID="Label4" runat="server" Text="Select Customer"></asp:Label>
                                             <dx:ASPxComboBox ID="drpCredit" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpCredit_SelectedIndexChanged" AutoPostBack="true"  TextField="FullNames" Theme="Glass" ValueField="id" CssClass="form-control" ></dx:ASPxComboBox>
                                                                                </td>
                                  
                                        <td>
                                           <asp:Label ID="Label5" runat="server" Text="Enter Number Of Shares"></asp:Label>
                                           <br /><asp:TextBox ID="txtamt" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </td>
                                        </tr>
                                   
                      <tr>
                          <td>
                                  
 <asp:Button runat="server" CssClass="btn btn-primary" ID="Submit" Text="Submit" OnClick="Submit_Click" style="height:35px;width:auto"  ></asp:Button>
                           
                       
                                     </td>
                          <td>
 <asp:Button runat="server" CssClass="btn btn-primary" ID="Button1" Text="Back" style="height:35px;width:auto" OnClick="Button1_Click" ></asp:Button>
                       
                        
                       </td>
                          <td>
                              &nbsp;</td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                    <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                   <dx:ASPxLabel ID="CrId" runat="server" Visible="false" Text="ASPxLabel"></dx:ASPxLabel>
                                   <dx:ASPxLabel ID="DrId" runat="server" Visible="false" Text="ASPxLabel"></dx:ASPxLabel>
                               </td>
                          
                      </tr>
                             </table>
                          <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click" Visible="False"  ></dx:ASPxButton></dx>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">

                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" Visible="False" ></dx:ASPxButton>
                       
                  
                          </dx>
              
                </div>
      
            </div>
        </div>
    </div>
        
<asp:Button runat="server" CssClass="btn btn-primary" ID="Button2" Text="View Cover" style="height:35px;width:auto"  OnClick="Button2_Click"></asp:Button>
                       
<asp:Button runat="server" CssClass="btn btn-primary" ID="Button3" Text="Download Double Cover" style="height:35px;width:auto" OnClick="Button3_Click"  ></asp:Button>
   
        <table>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Select Security" Theme="Glass"></dx:ASPxLabel>
                </td>
                <td><dx:ASPxComboBox ID="drpname" runat="server" ValueType="System.String"  AutoPostBack="true" Height="17px" TextField="FullNames" Theme="BlackGlass" ValueField="id" Width="250px"></dx:ASPxComboBox></td>
                <td><asp:Button runat="server" CssClass="btn btn-primary" ID="Button4" Text="View Company Cover" style="height:35px;width:auto" OnClick="Button4_Click"></asp:Button>
   </td>
            </tr>
        </table>
                            
    <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="BeneficiariesID"  AutoGenerateColumns="false"  runat="server" EnableCallBacks="False"  Theme="BlackGlass" OnRowDeleting="ASPxGridView1_RowDeleting"  OnSelectionChanged="ASPxGridView1_SelectionChanged" Visible="true" EnablePagingGestures="True" Width="960px" style="margin-right: 0px" Font-Size="X-Small"  >
                        <SettingsPager AlwaysShowPager="True" PageSize="30" EnableAdaptivity="True">
                            <PageSizeItemSettings Visible="True">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsBehavior  AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />
                          <SettingsCommandButton>
                              <NewButton ButtonType="Button" Text="Audit">
                              </NewButton>
                              <CancelButton ButtonType="Button" Text="Audit">
                              </CancelButton>
                              <EditButton ButtonType="Button"  Text="Audit">
                              </EditButton>
                            <DeleteButton   ButtonType="Button" Text="Delete">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                        <SettingsText CommandDelete="Are you sure you want to delete the batch transaction ?" ConfirmDelete="Are you sure you want to delete the batch transaction ?" />
        <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                     
             <dx:GridViewDataTextColumn Width="150px" Caption="BeneficiariesID" FieldName="BeneficiariesID" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Width="150px" Caption="Certificate Number" FieldName="CertificateId" ReadOnly="True" VisibleIndex="2">
            
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn Width="150px" FieldName="CustomerNumber" ReadOnly="True" VisibleIndex="3">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Width="150px"  Caption="Full Names" FieldName="Fullnames" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Width="150px" Caption="Registrar" FieldName="RegistrarName" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="150px" Caption="Scrip" FieldName="ScriptRegisterID" VisibleIndex="6">
                </dx:GridViewDataTextColumn>                                                                           
                              <dx:GridViewDataTextColumn Width="150px" FieldName="Amount" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                                   
                     
            </Columns>
                       

                    </dx:ASPxGridView>
         </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>