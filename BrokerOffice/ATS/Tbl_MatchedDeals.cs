﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.ATS
{
    [TrackChanges]
    public partial class Tbl_MatchedDeals
    {
        [Key]
        public long ID { get; set; }
        public String Deal { get; set; }
        public string BuyCompany { get; set; }
        public string SellCompany { get; set; }
        public string Buyercdsno { get; set; }
        public string Sellercdsno { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<System.DateTime> Trade { get; set; }
        public Nullable<decimal> DealPrice { get; set; }
        public string DealFlag { get; set; }
        public string Instrument { get; set; }
        public Nullable<bool> Affirmation { get; set; }
        public string buybroker { get; set; }
        public string sellbroker { get; set; }

        public int RefID { get; set; }
        public string BrokerCode { get; set; }

        public string BuyerName { get; set;}
        public string SellerName { get; set; }
        public string exchange { get; set; }
    }
}
