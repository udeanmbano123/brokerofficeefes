﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.Reporting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class BenJ : System.Web.UI.Page
    {
        public string id = "";
        public string ff = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            //ff = Request.QueryString["ff"].ToString();
            if (IsPostBack == false)
            {
               string batchno = Request.QueryString["id"].ToString();
                var pc = db.ScriptRegisters.ToList().Where(a => a.CertificateNumber == batchno);
                foreach (var z in pc)
                {
                    bnumber.Text = z.ScriptRegisterID.ToString();
                    btran.Text = z.CertificateNumber;;
                    btotal.Text = z.NumberShares.ToString();
                   ViewState["trp"] = z.CertificateNumber;
                    ViewState["Cp"] = z.ClientNumber;
                }

               drpCredit.DataSource = GetDataSource();
                drpCredit.DataBind();

                drpname.DataSource = GetDataSource2();
                drpname.DataBind();
                drpname.SelectedIndex = 0;
            }

            try
            {
                BindGrid();
            }
            catch (Exception)
            {

                
            }
        }
        private DataTable GetDataSource2()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";
    var d = from c in db.para_issuers
                        
                        select c;
            var z = 0;
                foreach (var e in d)
                {
                dtSource.Rows.Add(z,e.ISIN);
                z += 1;
                }


            return dtSource;
        }
        public void BindGrid()
        {
             string Batch = ViewState["trp"].ToString();
            // check batch status
            var pp = db.Beneficiaruess.ToList().Where(a => a.CertificateId==Batch);
            ASPxGridView1.DataSource = pp;
            ASPxGridView1.DataBind();
        }


        protected void drpCredit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtCrAcc.Text=drpCredit.SelectedItem.Value.ToString();
        }

        protected void Debit_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private DataTable GetDataSource()
        {
            string name = "";
           //Request.QueryString["drid"]
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            
                dtSource.Rows.Add("1", "Please Select a client");
                var dds = from c in db.Account_Creations
                         where c.StatuSActive == true 
                         select c;
                foreach (var p in dds)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            //Fill rows



            return dtSource;
        }

        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        
        protected void Submit_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(drpCredit.SelectedItem.Text))
            {
                msgbox("An account must be selected");
                return;
            }

            if (drpCredit.SelectedItem.Text == "Please Select a group" || drpCredit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected");
                return;
            }
            else if (txtamt.Text == "")
            {
                msgbox("Amount is required");
                return;
           }
            
            //check existing sum
          decimal? cr = 0;
            decimal  dbr = 0;

                        
           
            var nw = Convert.ToDecimal(txtamt.Text);
            

            decimal n;
            bool isNumericT = decimal.TryParse(txtamt.Text, out n);

            if (IsNumeric(txtamt.Text) == false)
            {
                msgbox("The amount has to be a figure");
                txtamt.Focus();
                return;
            }
           
            if (Submit.Text=="Submit")
            {
                //submit
                BrokerOffice.Models.Beneficiaries my = new BrokerOffice.Models.Beneficiaries();
                my.CertificateId = ViewState["trp"].ToString();
                my.CustomerNumber=drpCredit.SelectedItem.Value.ToString();
                my.Fullnames= drpCredit.SelectedItem.Text;
                my.RegistrarName = "N/A";
                int idd = Convert.ToInt32(bnumber.Text);
                my.ScriptRegisterID=idd;
                my.Amount = Convert.ToDecimal(txtamt.Text);
                db.Beneficiaruess.AddOrUpdate(my);
                db.SaveChanges();
                msgbox("You have successfully added the share allocation");

            }
            else if (Submit.Text=="Update")
            {
                Submit.Text = "Submit";
                //
                int x = Convert.ToInt32(Label44.Text);
                var my = db.Beneficiaruess.Find(x);
                my.CertificateId = ViewState["trp"].ToString();
                my.CustomerNumber = drpCredit.SelectedItem.Value.ToString();
                my.Fullnames = drpCredit.SelectedItem.Text;
                my.RegistrarName = "N/A";
                int idd = Convert.ToInt32(bnumber.Text);
                my.ScriptRegisterID = idd;
                my.Amount = Convert.ToDecimal(txtamt.Text);
                db.Beneficiaruess.AddOrUpdate(my);
                db.SaveChanges();
                try
                {
                    ASPxGridView1.Selection.UnselectAll();

                }
                catch (Exception)
                {

                  
                }
                try
                {
                    ASPxGridView1.Selection.CancelSelection();

                }
                catch (Exception)
                {

                    
                }
                msgbox("You have updated the share allocation");
                Submit.Text = "Submit";
            }
            BindGrid();
            
            //Clear();
        }
        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }
        protected void Clear()
        {
            if(Submit.Text=="Submit")
            {
     System.Web.HttpContext.Current.Session["NOT"] = "";

            }else
            {
                System.Web.HttpContext.Current.Session["NOT"] = "";

            }

            Response.Redirect("~/ScriptRegisters/Index");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

        protected void drpDebit_SelectedIndexChanged(object sender, EventArgs e)
        {
        //    txtamt.Text = drpDebit.SelectedItem.Value.ToString();
        }

        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {
            drpCredit.SelectedIndex = 0;
            object key = null;
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView1.GetRowValues(i, "BeneficiariesID");

                }

            }

            int my = Convert.ToInt32(key);
            Label44.Text = my.ToString();
            var z = db.Beneficiaruess.ToList().Where(a=>a.BeneficiariesID==my);

            foreach (var q in z)
            {
                drpCredit.SelectedItem.Text = q.Fullnames;
                drpCredit.SelectedItem.Value =q.CustomerNumber;
                drpCredit.SelectedIndex = 0;
                txtamt.Text = q.Amount.ToString();
            }
            Submit.Text = "Update";

            }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
           int Batch = Convert.ToInt32(bnumber.Text);

            Response.Redirect("~/Reporting/CoverNote.aspx?id="+Batch+"");
        }

        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(e.Keys[ASPxGridView1.KeyFieldName].ToString());
                string c = i.ToString();

                Beneficiaries user = db.Beneficiaruess.Find(i);
                db.Beneficiaruess.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                BindGrid();
                msgbox("You have removed the share allocation");
            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }
        }

        protected void bnumber_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            int idd = Convert.ToInt32(bnumber.Text);

            XtraCover report = new XtraCover();
            report.Parameters["ID"].Value = idd;

            report.CreateDocument();
            XtraCover2 report2 = new XtraCover2();
            report2.Parameters["ID"].Value = idd;
            report2.CreateDocument();
            //report.Parameters["yourParameter2"].Value = secondValue;
            int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
            for (int g = 0; g < minPageCount; g++)
            {
                report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
            }
            if (report2.Pages.Count != minPageCount)
            {
                for (int g = minPageCount; g < report2.Pages.Count; g++)
                {
                    report.Pages.Add(report2.Pages[g]);
                }
            }
            string nme = btran.Text+DateTime.Now.ToString("dd-MMM-yyy");

            // Reset all page numbers in the resulting document. 
            report.PrintingSystem.ContinuousPageNumbering = true;
            ExportReport(report, "CoverNote" + nme + ".pdf", "pdf", false);

        }

        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (drpname.SelectedItem.Value.ToString()=="")
            {
                msgbox("Select company to view Company Cover Report");
                return;
            }else
            {
                int Batch = Convert.ToInt32(bnumber.Text);
                string Cust = ViewState["Cp"].ToString();
                string neme = drpname.SelectedItem.Text;
                Response.Redirect("~/Reporting/FullNote.aspx?id=" + Batch + "&cust=" + Cust + "&sec=" + neme + "");

            }
        }
    }
}