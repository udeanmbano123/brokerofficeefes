﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class ConsolidatedBalances : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        public string date = "";
        public string broker = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["date"];
          

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName.ToString());
                foreach (var d in c)
                {
                    broker = d.BrokerCode;
                }

                //

                XtraConsolidateBalances report = new XtraConsolidateBalances();
                report.Parameters["Broker"].Value = broker;
                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}