﻿using BrokerOffice.DAO;
using BrokerOffice.Reporting.Consolidated;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class PagedDealReport1: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
 PagedDeal22 report = new PagedDeal22();
                    report.Parameters["AccName"].Value = HttpContext.Current.Session["AccName"];
                    report.Parameters["AccNum"].Value = HttpContext.Current.Session["AccNum"];
                    report.Parameters["AccQ"].Value = HttpContext.Current.Session["AccQ"];
                    report.Parameters["AccBR"].Value = HttpContext.Current.Session["AccBR"];
                    report.Parameters["DTePost"].Value = HttpContext.Current.Session["DTePost"];
                    report.Parameters["Price"].Value = HttpContext.Current.Session["Price"];
                   
                    report.Parameters["STD"].Value= HttpContext.Current.Session["sett"];
                    
                    report.Parameters["sec"].Value= HttpContext.Current.Session["sec"];
                    ASPxDocumentViewer1.Report = report;
              
            }
        }
        public DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }

        public void sendM(Attachment att, string acc, string accountname)
        {
            // Create a new message and attach the PDF report to it.
            string nn = investormail(acc);
            if (nn != "")
            {
                // Send the e-mail message via the specified SMTP server.
                //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
                SmtpClient clients = new SmtpClient();
                clients.Port = 587;
                clients.Host = "smtp.office365.com";
                clients.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
           X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                clients.Timeout =0;
                clients.DeliveryMethod = SmtpDeliveryMethod.Network;
                clients.UseDefaultCredentials = false;
                clients.Credentials = new System.Net.NetworkCredential("statements.efesecurities@efesecurities.co.zw", "Temp4now");

                MailMessage mail = new MailMessage();
                mail.Attachments.Add(att);

                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress("statements.efesecurities@efesecurities.co.zw", "Broker Office");
                mail.To.Add(new MailAddress(investormail(acc), accountname));
                mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                // Specify other e-mail options.
                mail.Subject = "Broker Note";
                mail.Body = "This is a test e-mail message sent by an application.";

                clients.Send(mail);

            }

        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public string custodians(string acc)
        {
            string validate = "";
            var client = new RestClient("http://192.168.3.245/BrokerService");
            var request = new RestRequest("Custodian/{s}", Method.POST);
            request.AddUrlSegment("s", acc);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();


            return validate;


        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
            return mm;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            try
            {
                PagedDeal22 report = new PagedDeal22();
                report.Parameters["AccName"].Value = HttpContext.Current.Session["AccName"];
                report.Parameters["AccNum"].Value = HttpContext.Current.Session["AccNum"];
                report.Parameters["AccQ"].Value = HttpContext.Current.Session["AccQ"];
                report.Parameters["AccBR"].Value = HttpContext.Current.Session["AccBR"];
                report.Parameters["DTePost"].Value = HttpContext.Current.Session["DTePost"];
                report.Parameters["Price"].Value = HttpContext.Current.Session["Price"];

                //ASPxDocumentViewer1.Report = report;
                // Create a new memory stream and export the report into it as PDF.
                MemoryStream mem = new MemoryStream();
                report.ExportToPdf(mem);

                // Create a new attachment and put the PDF report into it.
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                DateTime dte = Convert.ToDateTime(HttpContext.Current.Session["DTePost"].ToString());
                string accnum = HttpContext.Current.Session["AccNum"].ToString();
                string accname = HttpContext.Current.Session["AccName"].ToString();

                sendM(att, accnum, accname);




                // Close the memory stream.
                mem.Close();
                msgbox("Report has been sent");
            }
            catch (Exception)
            {

                msgbox("Emailing report has failed");
            }

        }
    }
}