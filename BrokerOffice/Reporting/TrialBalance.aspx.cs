﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace BrokerOffice.Reporting
{
    public partial class TrialBalance : System.Web.UI.Page
    {
        public string id = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["id"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraBal report = new XtraBal();
                report.Parameters["tDate"].Value =id;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }

        
    }
}