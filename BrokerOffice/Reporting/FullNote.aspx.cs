﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class FullNote : System.Web.UI.Page
    {
        public string id = "";
        public string Cust = "";
        public string sec = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["id"];
            Cust = Request.QueryString["cust"];
            sec = Request.QueryString["sec"];



            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraCertificates report = new XtraCertificates();
                report.Parameters["ID"].Value = id;
                report.Parameters["Counter"].Value = sec;
                report.Parameters["Cust"].Value = Cust;            
                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}