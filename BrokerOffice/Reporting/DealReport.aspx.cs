﻿using BrokerOffice.DAO;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class DealReport: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                  XtraDeal report = new XtraDeal();
                    report.Parameters["Account"].Value = HttpContext.Current.Session["Acc"].ToString();
                    report.Parameters["OrderNo"].Value = HttpContext.Current.Session["Ord"].ToString();
                    report.Parameters["SID"].Value = HttpContext.Current.Session["SID"].ToString();
                    report.Parameters["QTY"].Value = HttpContext.Current.Session["qty"].ToString();
                    report.Parameters["Dte"].Value = HttpContext.Current.Session["Dte"].ToString();
                    report.Parameters["Max"].Value = HttpContext.Current.Session["max"].ToString();

              

                        report.Parameters["Broker"].Value = HttpContext.Current.Session["Company"].ToString().ToUpper();
                        report.Parameters["BrokerName"].Value = HttpContext.Current.Session["Company"].ToString().ToUpper();

                    

                    
                    string z = HttpContext.Current.Session["Acc"].ToString();
                    var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                    foreach (var v in p)
                    {
                        report.Parameters["Address"].Value = v.depname;
                    }
                    report.Parameters["TYPO"].Value = HttpContext.Current.Session["Type"].ToString();

                    if (HttpContext.Current.Session["Type"].ToString()!="BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }
                    else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                    report.Parameters["TYPO2"].Value = HttpContext.Current.Session["Type2"].ToString();

                    ASPxDocumentViewer1.Report = report;
              
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            try
            {
                XtraDeal report = new XtraDeal();
                report.Parameters["Account"].Value = HttpContext.Current.Session["Acc"].ToString();
                
                report.Parameters["OrderNo"].Value = HttpContext.Current.Session["Ord"].ToString();
                report.Parameters["SID"].Value = HttpContext.Current.Session["SID"].ToString();
                report.Parameters["QTY"].Value = HttpContext.Current.Session["qty"].ToString();
                report.Parameters["Dte"].Value = HttpContext.Current.Session["Dte"].ToString();
                report.Parameters["Max"].Value = HttpContext.Current.Session["max"].ToString();

            
              
                    report.Parameters["Broker"].Value = HttpContext.Current.Session["Company"].ToString().ToUpper();
                    report.Parameters["BrokerName"].Value = HttpContext.Current.Session["Company"].ToString().ToUpper();

                string z = HttpContext.Current.Session["Acc"].ToString();
                string cf = "";
                var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                foreach (var v in p)
                {
                    cf = v.Surname_CompanyName + " " + v.OtherNames;
                    report.Parameters["Address"].Value = v.depname;
                }
                report.Parameters["TYPO"].Value = HttpContext.Current.Session["Type"].ToString();

                if (HttpContext.Current.Session["Type"].ToString() != "BUY")
                {
                    report.Parameters["Stamp"].Value = "Stamp duty";
                }
                else
                {
                    report.Parameters["Stamp"].Value = "Zim";
                }
                report.Parameters["TYPO2"].Value = HttpContext.Current.Session["Type2"].ToString();

                //ASPxDocumentViewer1.Report = report;
                // Create a new memory stream and export the report into it as PDF.
                MemoryStream mem = new MemoryStream();
                report.ExportToPdf(mem);

                // Create a new attachment and put the PDF report into it.
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                Attachment att = new Attachment(mem, "BrokerNote"+DateTime.Now.ToString("dd-MMM-yyyy")+".pdf", "application/pdf");
                sendM(att,z,cf);
                // Close the memory stream.
                mem.Close();
                msgbox("Report has been sent");
            }
            catch (Exception f)
            {
                msgbox(f.ToString());

            }
            
        }
        public void sendM(Attachment att, string acc, string accountname)
        {
            // Create a new message and attach the PDF report to it.
 
            // Send the e-mail message via the specified SMTP server.
            //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
            SmtpClient clients = new SmtpClient();
            clients.Port = 587;
            clients.Host = "smtp.office365.com";
            clients.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            clients.Timeout = 10000;
            clients.DeliveryMethod = SmtpDeliveryMethod.Network;
            clients.UseDefaultCredentials = false;
            clients.Credentials = new System.Net.NetworkCredential("statements.efesecurities@efesecurities.co.zw", "Temp4now");

            MailMessage mail = new MailMessage();
            mail.Attachments.Add(att);

            // Specify sender and recipient options for the e-mail message.
            mail.From = new MailAddress("statements.efesecurities@efesecurities.co.zw", "BrokerOffice");
            mail.To.Add(new MailAddress(investormail(acc),accountname));
            mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
            mail.BodyEncoding = UTF8Encoding.UTF8;
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            // Specify other e-mail options.
            mail.Subject = "Broker Note";
            mail.Body = "Please find attached your brokers note.";

            clients.Send(mail);



        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public string custodians(string acc)
        {
          

            return "";


        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
            return mm;
        }

    }
}