﻿using BrokerOffice.DAO;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class PagedDealReport: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                    PagedDeal report = new PagedDeal();
               
                    report.Parameters["Type"].Value = HttpContext.Current.Session["Type"].ToString();
                    report.Parameters["Trade"].Value = HttpContext.Current.Session["Trade"].ToString();
                    report.Parameters["Trade2"].Value = HttpContext.Current.Session["Trade2"].ToString();
                    if (HttpContext.Current.Session["Type"].ToString() != "BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }

                    ASPxDocumentViewer1.Report = report;
               
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            

            try
            {

                string mac = HttpContext.Current.Session["Type"].ToString();
                DateTime dte = Convert.ToDateTime(HttpContext.Current.Session["Trade"].ToString());
                DateTime dte2 = Convert.ToDateTime(HttpContext.Current.Session["Trade2"].ToString());
                var c = db.DealerDGs.ToList().Where(a => Convert.ToDateTime(a.DatePosted) >=dte && Convert.ToDateTime(a.DatePosted) < dte2.AddDays(1));
                var d = db.DealerDGs.ToList().Where(a => Convert.ToDateTime(a.DatePosted) > dte && Convert.ToDateTime(a.DatePosted) < dte2.AddDays(1)).Count();
                if (mac == "BUY")
                {
                    foreach (var f in c)
                    {
                        XtraDeal report = new XtraDeal();
                        report.Parameters["Account"].Value = f.Account1;

                        report.Parameters["OrderNo"].Value =f.Deal;
                        report.Parameters["SID"].Value =f.Deal;
                        report.Parameters["QTY"].Value = f.Quantity;
                        report.Parameters["Dte"].Value = f.DatePosted;
                        report.Parameters["Max"].Value = f.Price;

                     
                     
                            report.Parameters["Broker"].Value = f.Security;
                            report.Parameters["BrokerName"].Value =f.Security;
                        
                        string z =f.Account1;
                        string cf = "";
                        var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report.Parameters["Address"].Value = v.depname;
                        }
                        report.Parameters["TYPO"].Value = "BUY";

                        
                            //report.Parameters["Stamp"].Value = "Stamp duty";
                      
                            report.Parameters["Stamp"].Value = "Zim";
                        
                        report.Parameters["TYPO2"].Value = "PURCHASE";

                        //ASPxDocumentViewer1.Report = report;
                        // Create a new memory stream and export the report into it as PDF.
                        MemoryStream mem = new MemoryStream();
                        report.ExportToPdf(mem);

                        // Create a new attachment and put the PDF report into it.
                        mem.Seek(0, System.IO.SeekOrigin.Begin);
                        Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                        sendM(att, z, cf);
                        // Close the memory stream.
                        mem.Close();
                       
                    }

                }
                else
                {
                    foreach (var f in c)
                    {

                        XtraDeal report = new XtraDeal();
                        report.Parameters["Account"].Value = f.Account1;

                        report.Parameters["OrderNo"].Value = f.Deal;
                        report.Parameters["SID"].Value = f.Deal;
                        report.Parameters["QTY"].Value = f.Quantity;
                        report.Parameters["Dte"].Value = f.DatePosted;
                        report.Parameters["Max"].Value = f.Price;

                     
                       
                            report.Parameters["Broker"].Value = f.Security;
                            report.Parameters["BrokerName"].Value =f.Security;
                        
                        string z = f.Account1;
                        string cf = "";
                        var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report.Parameters["Address"].Value = v.depname;
                        }
                        report.Parameters["TYPO"].Value = "SELL";


                        report.Parameters["Stamp"].Value = "Stamp duty";

                        //report.Parameters["Stamp"].Value = "Zim";

                        report.Parameters["TYPO2"].Value = "SALE";

                        //ASPxDocumentViewer1.Report = report;
                        // Create a new memory stream and export the report into it as PDF.
                        MemoryStream mem = new MemoryStream();
                        report.ExportToPdf(mem);

                        // Create a new attachment and put the PDF report into it.
                        mem.Seek(0, System.IO.SeekOrigin.Begin);
                        Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                        sendM(att, z, cf);
                        // Close the memory stream.
                        mem.Close();
                    } 
                }

                msgbox("The report has been sent");
            }
            catch (Exception)
            {

                msgbox("Emailing report has failed");
            }
            return;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public void sendM(Attachment att,string  acc,string accountname)
        {
            // Create a new message and attach the PDF report to it.

            // Send the e-mail message via the specified SMTP server.
            //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
            string nn = investormail(acc);
            if (nn != "")
            {


                SmtpClient clients = new SmtpClient();
                clients.Port = 587;
                clients.Host = "smtp.office365.com";
                clients.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
           X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                clients.Timeout=0;
                clients.DeliveryMethod = SmtpDeliveryMethod.Network;
                clients.UseDefaultCredentials = false;
                clients.Credentials = new System.Net.NetworkCredential("statements.efesecurities@efesecurities.co.zw", "Temp4now");

                MailMessage mail = new MailMessage();
                mail.Attachments.Add(att);

                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress("statements.efesecurities@efesecurities.co.zw", "BrokerOffice");
                mail.To.Add(new MailAddress(investormail(acc), accountname));
                mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                // Specify other e-mail options.
                mail.Subject = "Broker Note";
                mail.Body = "This is a Broker Note sent by Broker Office.";

                clients.Send(mail);

            }

        }

        public string custodians(string acc)
        {
            string validate = "";
            var client = new RestClient("http://192.168.3.245/BrokerService");
            var request = new RestRequest("Custodian/{s}", Method.POST);
            request.AddUrlSegment("s", acc);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();


            return validate;

         
        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
           
            return mm;
        }
    }
}