﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class CompanyReportConsolidated : System.Web.UI.Page
    {
        public string date = "";
        public string type = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["date"];
            type= Request.QueryString["Type"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

              XtraCompanyConsolidated report = new XtraCompanyConsolidated();
                report.Parameters["SelDate"].Value =date;
                report.Parameters["Type"].Value = type;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}