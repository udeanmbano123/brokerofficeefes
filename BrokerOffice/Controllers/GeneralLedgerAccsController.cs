﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{
    // GET: Admin
    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class GeneralLedgerAccsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: GeneralLedgerAccs
        public async Task<ActionResult> Index()
        {
            return View(await db.GeneralLedgerAcca.ToListAsync());
        }

        // GET: GeneralLedgerAccs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralLedgerAcc generalLedgerAcc = await db.GeneralLedgerAcca.FindAsync(id);
            if (generalLedgerAcc == null)
            {
                return HttpNotFound();
            }
            return View(generalLedgerAcc);
        }

        // GET: GeneralLedgerAccs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GeneralLedgerAccs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GeneralLedgerAccID,Name")] GeneralLedgerAcc generalLedgerAcc)
        {
            if (ModelState.IsValid)
            {
                db.GeneralLedgerAcca.Add(generalLedgerAcc);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the General Ledger Account";
                return RedirectToAction("Index");
            }

            return View(generalLedgerAcc);
        }

        // GET: GeneralLedgerAccs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralLedgerAcc generalLedgerAcc = await db.GeneralLedgerAcca.FindAsync(id);
            if (generalLedgerAcc == null)
            {
                return HttpNotFound();
            }
            return View(generalLedgerAcc);
        }

        // POST: GeneralLedgerAccs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GeneralLedgerAccID,Name")] GeneralLedgerAcc generalLedgerAcc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(generalLedgerAcc).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the General Ledger Account";
                return RedirectToAction("Index");
            }
            return View(generalLedgerAcc);
        }

        // GET: GeneralLedgerAccs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneralLedgerAcc generalLedgerAcc = await db.GeneralLedgerAcca.FindAsync(id);
            if (generalLedgerAcc == null)
            {
                return HttpNotFound();
            }
            return View(generalLedgerAcc);
        }

        // POST: GeneralLedgerAccs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            GeneralLedgerAcc generalLedgerAcc = await db.GeneralLedgerAcca.FindAsync(id);
            db.GeneralLedgerAcca.Remove(generalLedgerAcc);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully removed the General Ledger Account";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
