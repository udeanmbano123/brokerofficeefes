﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,General,BrokerAdmin2,BrokerAdmin,Approver,ApproverUser", NotifyUrl = "/UnauthorizedPage")]

    public class HolidaysController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Holidays
        public async Task<ActionResult> Index()
        {
            return View(await db.Holidayss.ToListAsync());
        }

        // GET: Holidays/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Holidays holidays = await db.Holidayss.FindAsync(id);
            if (holidays == null)
            {
                return HttpNotFound();
            }
            return View(holidays);
        }

        // GET: Holidays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Holidays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "HolidayID,HolidayName,HolidayDate")] Holidays holidays)
        {
            if (ModelState.IsValid)
            {
                db.Holidayss.Add(holidays);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "Public holiday added successfully";

                return RedirectToAction("Index");
            }

            return View(holidays);
        }

        // GET: Holidays/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Holidays holidays = await db.Holidayss.FindAsync(id);
            if (holidays == null)
            {
                return HttpNotFound();
            }
            return View(holidays);
        }

        // POST: Holidays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HolidayID,HolidayName,HolidayDate")] Holidays holidays)
        {
            if (ModelState.IsValid)
            {
                db.Entry(holidays).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "Public holiday updated successfully";

                return RedirectToAction("Index");
            }
            return View(holidays);
        }

        // GET: Holidays/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Holidays holidays = await db.Holidayss.FindAsync(id);
            if (holidays == null)
            {
                return HttpNotFound();
            }
            return View(holidays);
        }

        // POST: Holidays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Holidays holidays = await db.Holidayss.FindAsync(id);
            db.Holidayss.Remove(holidays);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Public holiday removed successfully";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
