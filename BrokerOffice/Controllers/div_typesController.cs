﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{
    // GET: Admin
    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class div_typesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: div_types
        public async Task<ActionResult> Index()
        {
            return View(await db.DivTypes.ToListAsync());
        }

        // GET: div_types/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            div_types div_types = await db.DivTypes.FindAsync(id);
            if (div_types == null)
            {
                return HttpNotFound();
            }
            return View(div_types);
        }

        // GET: div_types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: div_types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "div_typesID,DivType,DiviTypeName")] div_types div_types)
        {
            if (ModelState.IsValid)
            {
                db.DivTypes.Add(div_types);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the dividend type";

                return RedirectToAction("Index");
            }

            return View(div_types);
        }

        // GET: div_types/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            div_types div_types = await db.DivTypes.FindAsync(id);
            if (div_types == null)
            {
                return HttpNotFound();
            }
            return View(div_types);
        }

        // POST: div_types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "div_typesID,DivType,DiviTypeName")] div_types div_types)
        {
            if (ModelState.IsValid)
            {
                db.Entry(div_types).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the dividend type";

                return RedirectToAction("Index");
            }
            return View(div_types);
        }

        // GET: div_types/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            div_types div_types = await db.DivTypes.FindAsync(id);
            if (div_types == null)
            {
                return HttpNotFound();
            }
            return View(div_types);
        }

        // POST: div_types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            div_types div_types = await db.DivTypes.FindAsync(id);
            db.DivTypes.Remove(div_types);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the dividend type";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
