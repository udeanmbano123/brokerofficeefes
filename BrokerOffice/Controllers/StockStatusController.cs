﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class StockStatusController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: StockStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.StockStatus.ToListAsync());
        }

        // GET: StockStatus/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockStatus stockStatus = await db.StockStatus.FindAsync(id);
            if (stockStatus == null)
            {
                return HttpNotFound();
            }
            return View(stockStatus);
        }

        // GET: StockStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StockStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StockStatusID,stockstatius")] StockStatus stockStatus)
        {
            if (ModelState.IsValid)
            {
                db.StockStatus.Add(stockStatus);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Stock Status";

                return RedirectToAction("Index");
            }

            return View(stockStatus);
        }

        // GET: StockStatus/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockStatus stockStatus = await db.StockStatus.FindAsync(id);
            if (stockStatus == null)
            {
                return HttpNotFound();
            }
            return View(stockStatus);
        }

        // POST: StockStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StockStatusID,stockstatius")] StockStatus stockStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockStatus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Stock Status";

                return RedirectToAction("Index");
            }
            return View(stockStatus);
        }

        // GET: StockStatus/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockStatus stockStatus = await db.StockStatus.FindAsync(id);
            if (stockStatus == null)
            {
                return HttpNotFound();
            }
            return View(stockStatus);
        }

        // POST: StockStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            StockStatus stockStatus = await db.StockStatus.FindAsync(id);
            db.StockStatus.Remove(stockStatus);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Stock Status";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
