﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using RestSharp;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "Admin,User,General,BrokerAdmin2,BrokerAdmin,Approver,ApproverUser", NotifyUrl = "/UnauthorizedPage")]

    public class MinimumLimitsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: MinimumLimits
        public async Task<ActionResult> Index()
        {
            return View(await db.MinimumLimits.ToListAsync());
        }

        // GET: MinimumLimits/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinimumLimit MinimumLimit = await db.MinimumLimits.FindAsync(id);
            if (MinimumLimit == null)
            {
                return HttpNotFound();
            }
            return View(MinimumLimit);
        }

        // GET: MinimumLimits/Create
        public ActionResult Create()
        {
            var zs = 0;
            try
            {
                zs = db.MinimumLimits.ToList().Count();

            }
            catch (Exception)
            {

                zs = 0;
            }
            if (zs>0)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "You may set only one limit";
              return  Redirect("~/MinimumLimits/Index");
            }
            ViewBag.Stat = regionS();
            return View();
        }

        // POST: MinimumLimits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MinimumLimitID,BuyLimit,SellLimit,reaction")] MinimumLimit MinimumLimit)
        {
            if (ModelState.IsValid)
            {
                db.MinimumLimits.Add(MinimumLimit);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully set Minimum Limit";
                //add limit

                try
                {
                    var client = new RestClient("http://192.168.3.245/BrokerService");
                var request = new RestRequest("BuySellB/{a}/{b}/{c}/{d}", Method.POST);
                request.AddUrlSegment("a",MinimumLimit.BuyLimit.ToString());
                request.AddUrlSegment("b", MinimumLimit.SellLimit.ToString());
                request.AddUrlSegment("c", MinimumLimit.reaction.Replace(" ","-"));
                request.AddUrlSegment("d", "EFES");
                IRestResponse response = client.Execute(request);
                
                }
                catch (Exception)
                {

                    throw;
                }
//validate = response.Content;
                return RedirectToAction("Index");
            }
            ViewBag.Stat = regionS(MinimumLimit.reaction,MinimumLimit.reaction);
            return View(MinimumLimit);
        }

        // GET: MinimumLimits/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinimumLimit MinimumLimit = await db.MinimumLimits.FindAsync(id);
            if (MinimumLimit == null)
            {
                return HttpNotFound();
            }
            ViewBag.Stat = regionS(MinimumLimit.reaction, MinimumLimit.reaction);

            return View(MinimumLimit);
        }

        // POST: MinimumLimits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MinimumLimitID,BuyLimit,SellLimit,reaction")] MinimumLimit MinimumLimit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(MinimumLimit).State = EntityState.Modified;
                await db.SaveChangesAsync();
                var client = new RestClient("http://192.168.3.245/BrokerService");
                var request = new RestRequest("BuySellUB/{a}/{b}/{c}/{d}", Method.POST);
                request.AddUrlSegment("a", MinimumLimit.BuyLimit.ToString());
                request.AddUrlSegment("b", MinimumLimit.SellLimit.ToString());
                request.AddUrlSegment("c", MinimumLimit.reaction.Replace(" ", "-"));
                request.AddUrlSegment("d", "EFES");
                IRestResponse response = client.Execute(request);
                //validate = response.Content;
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated Minimum Limit";
                return RedirectToAction("Index");
            }
            ViewBag.Stat = regionS(MinimumLimit.reaction, MinimumLimit.reaction);

            return View(MinimumLimit);
        }

        // GET: MinimumLimits/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinimumLimit MinimumLimit = await db.MinimumLimits.FindAsync(id);
            if (MinimumLimit == null)
            {
                return HttpNotFound();
            }
            return View(MinimumLimit);
        }

        // POST: MinimumLimits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            MinimumLimit MinimumLimit = await db.MinimumLimits.FindAsync(id);
            db.MinimumLimits.Remove(MinimumLimit);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deketed Minimum Limit";
           return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public List<SelectListItem> regionS()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
           phy.Add(new SelectListItem { Text = "Drop Transaction", Value = "Drop Transaction" });
            phy.Add(new SelectListItem { Text = "Authorise Transaction", Value = "Authorise Transaction" });
            //phy.Add(new SelectListItem { Text = "Half Settlement", Value = "Half Settlement" });
            //phy.Add(new SelectListItem { Text = "Freeze Account", Value = "Freeze Account" });
            //phy.Add(new SelectListItem { Text = "Send Email But Proceed", Value = "Send Email But Proceed" });

            return phy;
        }
        public List<SelectListItem> regionS(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });
            phy.Add(new SelectListItem { Text = "Drop Transaction", Value = "Drop Transaction" });
            phy.Add(new SelectListItem { Text = "Authorise Transaction", Value = "Authorise Transaction" });
            //phy.Add(new SelectListItem { Text = "Half Settlement", Value = "Half Settlement" });
            //phy.Add(new SelectListItem { Text = "Freeze Account", Value = "Freeze Account" });
            //phy.Add(new SelectListItem { Text = "Send Email But Proceed", Value = "Send Email But Proceed" });

            return phy;
        }
    }
}
