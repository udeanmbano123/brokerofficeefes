﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class ParticipantTypesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: ParticipantTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.ParticipantTypes.ToListAsync());
        }

        // GET: ParticipantTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParticipantType participantType = await db.ParticipantTypes.FindAsync(id);
            if (participantType == null)
            {
                return HttpNotFound();
            }
            return View(participantType);
        }

        // GET: ParticipantTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ParticipantTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Type")] ParticipantType participantType)
        {
            if (ModelState.IsValid)
            {
                db.ParticipantTypes.Add(participantType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(participantType);
        }

        // GET: ParticipantTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParticipantType participantType = await db.ParticipantTypes.FindAsync(id);
            if (participantType == null)
            {
                return HttpNotFound();
            }
            return View(participantType);
        }

        // POST: ParticipantTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Type")] ParticipantType participantType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(participantType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(participantType);
        }

        // GET: ParticipantTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParticipantType participantType = await db.ParticipantTypes.FindAsync(id);
            if (participantType == null)
            {
                return HttpNotFound();
            }
            return View(participantType);
        }

        // POST: ParticipantTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ParticipantType participantType = await db.ParticipantTypes.FindAsync(id);
            db.ParticipantTypes.Remove(participantType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
