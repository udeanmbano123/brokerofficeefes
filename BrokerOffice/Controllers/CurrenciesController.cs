﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class CurrenciesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Currencies
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.para_Currencies
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.CurrencyName);
                    break;

                case "module":
                    per = per.OrderBy(s => s.CurrencyCode);
                    break;

                default:
                    per = per.OrderBy(s => s.id);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
            //return View(await db.para_Currencies.ToListAsync());
        }

        // GET: Currencies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_Currencies para_Currencies = await db.para_Currencies.FindAsync(id);
            if (para_Currencies == null)
            {
                return HttpNotFound();
            }
            return View(para_Currencies);
        }

        // GET: Currencies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Currencies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,CurrencyCode,CurrencyName,CurrencySymbol,InternationalSTD,CurrencyStatus,Country,Pref")] para_Currencies para_Currencies)
        {
            if (ModelState.IsValid)
            {
                db.para_Currencies.Add(para_Currencies);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the currency";

                return RedirectToAction("Index");
            }

            return View(para_Currencies);
        }

        // GET: Currencies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_Currencies para_Currencies = await db.para_Currencies.FindAsync(id);
            if (para_Currencies == null)
            {
                return HttpNotFound();
            }
            return View(para_Currencies);
        }

        // POST: Currencies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,CurrencyCode,CurrencyName,CurrencySymbol,InternationalSTD,CurrencyStatus,Country,Pref")] para_Currencies para_Currencies)
        {
            if (ModelState.IsValid)
            {
                db.Entry(para_Currencies).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Currency";

                return RedirectToAction("Index");
            }
            return View(para_Currencies);
        }

        // GET: Currencies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            para_Currencies para_Currencies = await db.para_Currencies.FindAsync(id);
            if (para_Currencies == null)
            {
                return HttpNotFound();
            }
            return View(para_Currencies);
        }

        // POST: Currencies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            para_Currencies para_Currencies = await db.para_Currencies.FindAsync(id);
            db.para_Currencies.Remove(para_Currencies);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Country";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
