﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class FinancialStatementsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: FinancialStatements
        public async Task<ActionResult> Index()
        {
            return View(await db.FinancialStatementss.ToListAsync());
        }

        // GET: FinancialStatements/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FinancialStatements financialStatements = await db.FinancialStatementss.FindAsync(id);
            if (financialStatements == null)
            {
                return HttpNotFound();
            }
            return View(financialStatements);
        }

        // GET: FinancialStatements/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FinancialStatements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FinancialStatementsID,Name")] FinancialStatements financialStatements)
        {
            if (ModelState.IsValid)
            {
                db.FinancialStatementss.Add(financialStatements);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Financial Statement";
                return RedirectToAction("Index");
            }

            return View(financialStatements);
        }

        // GET: FinancialStatements/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FinancialStatements financialStatements = await db.FinancialStatementss.FindAsync(id);
            if (financialStatements == null)
            {
                return HttpNotFound();
            }
            return View(financialStatements);
        }

        // POST: FinancialStatements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FinancialStatementsID,Name")] FinancialStatements financialStatements)
        {
            if (ModelState.IsValid)
            {
                db.Entry(financialStatements).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Financial Statement";
                return RedirectToAction("Index");
            }
            return View(financialStatements);
        }

        // GET: FinancialStatements/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FinancialStatements financialStatements = await db.FinancialStatementss.FindAsync(id);
            if (financialStatements == null)
            {
                return HttpNotFound();
            }
            return View(financialStatements);
        }

        // POST: FinancialStatements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FinancialStatements financialStatements = await db.FinancialStatementss.FindAsync(id);
            db.FinancialStatementss.Remove(financialStatements);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully removed the Financial Statement";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
