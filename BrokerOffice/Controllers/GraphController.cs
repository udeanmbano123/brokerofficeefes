﻿using BrokerOffice.API;
using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace BrokerOffice.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs

    public class GraphController : ApiController
    {
        SBoardContext db = new SBoardContext();
        private Random rand = new Random();

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Graph1/{s}")]
        [System.Web.Http.AllowAnonymous]
        public dynamic Deals(string s)
        {
            int matched = 0;
            int posted = 0;

            try
            {
                posted = db.Order_Lives.ToList().Where(a=>a.Broker_Code == s).Count();
            }
            catch (Exception)
            {

                posted = 0;
            }
            try
            {
                matched = db.TblDealss.ToList().Where(a =>a.BrokerCode==s).Count();
            }
            catch (Exception)
            {

                matched = 0;
            }
            List<Orders> dept = new List<Orders>();

            dept.Add(new Orders{ Type = "POSTED ORDERS", total = posted, color = GetRandomColorName() });
            dept.Add(new Orders { Type = "MATCHED ORDERS", total = matched, color = GetRandomColorName() });

            return dept;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Graph2/{s}")]
        [System.Web.Http.AllowAnonymous]
        public dynamic Dealss(string s)
        {
            int individual = 0;
            int corporate = 0;
            int joint = 0;

            try
            {
                individual= db.Account_Creations.ToList().Where(a=>a.accountcategory=="LI" && a.Broker == s).Count();
            }
            catch (Exception)
            {

                individual = 0;
            }
            try
            {
                corporate = db.Account_Creations.ToList().Where(a => a.accountcategory == "LE" && a.Broker == s).Count();
            }
            catch (Exception)
            {

                corporate = 0;
            }
            try
            {
                joint = db.Account_Creations.ToList().Where(a => a.accountcategory == "LJ" && a.Broker == s).Count();
            }
            catch (Exception)
            {

                joint = 0;
            }


            List<ClientDistribution> dept = new List<ClientDistribution>();

            dept.Add(new ClientDistribution { ClientType = "CORPORATE", total = corporate,color = GetRandomColorName() });
            dept.Add(new ClientDistribution { ClientType = "INDIVIDUAL", total = individual, color = GetRandomColorName() });
            dept.Add(new ClientDistribution { ClientType = "JOINT", total= joint, color = GetRandomColorName() });

            return dept;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Graph3/{s}")]
        [System.Web.Http.AllowAnonymous]
        public dynamic Dealsss(string s)
        {
            int buy = 0;
            int sell = 0;

            try
            {
               buy = db.Order_Lives.ToList().Where(a=>a.OrderType=="Buy" && a.Broker_Code == s).Count();
            }
            catch (Exception)
            {

               buy= 0;
            }
            try
            {
                sell = db.Order_Lives.ToList().Where(a => a.OrderType == "Sell"&& a.Broker_Code == s).Count();
            }
            catch (Exception)
            {

                sell = 0;
            }
            List<SelectListItem> dept = new List<SelectListItem>();

            dept.Add(new SelectListItem { Text = "BUY ORDERS", Value = buy.ToString() });
            dept.Add(new SelectListItem { Text = "SELL ORDERS", Value = sell.ToString() });

            return dept;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Graph4/{s}")]
        [System.Web.Http.AllowAnonymous]
        public dynamic Dealssss(string s)
        {
            int buy = 0;
            int sell = 0;

            try
            {
                buy = db.Account_Creations.ToList().Where(a => a.MNO_== "LOCAL" && a.Broker==s).Count();
            }
            catch (Exception)
            {

                buy = 0;
            }
            try
            {
                sell = db.Account_Creations.ToList().Where(a => a.MNO_ == "GLOBAL" && a.Broker == s).Count();
            }
            catch (Exception)
            {

                sell = 0;
            }
            List<SelectListItem> dept = new List<SelectListItem>();

            dept.Add(new SelectListItem { Text = "EXSISTING", Value = buy.ToString() });
            dept.Add(new SelectListItem { Text = "NEW ", Value = sell.ToString() });

            return dept;
        }

        static readonly Color[] Colors =
      typeof(Color).GetProperties(BindingFlags.Public | BindingFlags.Static)
     .Select(propInfo => propInfo.GetValue(null, null))
     .Cast<Color>()
     .ToArray();

        static readonly string[] ColorNames =
            typeof(Color).GetProperties(BindingFlags.Public | BindingFlags.Static)
            .Select(propInfo => propInfo.Name)
            .ToArray();

     

        public Color GetRandomColor()
        {
            return Colors[rand.Next(0, Colors.Length)];
        }

        public string GetRandomColorName()
        {
            return ColorNames[rand.Next(0, Colors.Length)];
        }

    }
}
